# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Person, type: :model do
  context 'Basic architecture' do
    it { should validate_presence_of(:last_name) }
    it {
      should validate_length_of(:biography).is_at_least(10)
                                           .with_message("Merci d'ajouter un peu de matière ;)")
    }

    it {
      should validate_length_of(:biography).is_at_most(1000)
                                           .with_message('Faites plus court, merci')
    }
  end
end
