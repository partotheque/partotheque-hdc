# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Score, type: :model do
  context 'Basic architecture' do
    it { should have_and_belong_to_many(:concerts) }
    it { should validate_numericality_of(:duration) }

    %i[title archive_ref number].each do |table|
      it { should validate_presence_of(table) }
    end
  end

  context 'Relations' do
    it 'should be related to a composer at least' do
      expect(create(:score).composed_by).to be_a Integer
    end

    it 'ideally should be related to an arranger too' do
      expect(create(:score).arranged_by).to be_a Integer
    end
  end
end
