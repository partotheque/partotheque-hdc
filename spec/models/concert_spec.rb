# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Concert, type: :model do
  context 'Basic architecture' do
    it { should have_and_belong_to_many(:scores) }
    it { should validate_presence_of(:name) }
  end

  context 'Relations' do
    it 'should at least be related to a conductor' do
      expect(create(:concert).conductors.count).to be > 0
    end

    it 'should (ideally) at least be related to a score' do
      expect(create(:concert).scores.count).to be > 0
    end
  end
end
