# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it 'fails because of no password' do
    user = build(:user, password: '')
    expect(user.save).to be_falsey
  end

  it 'fails because password is too short' do
    user = build(:user, password: 'eee')
    expect(user.save).to be_falsey
  end

  it 'succeeds because password is long enough' do
    user = build(:user, password: 'eeeeee')
    expect(user.save).to be_truthy
  end
end
