# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'POST /users/sign_in', type: :request do
  let(:user) { create(:user) }
  let(:url) { '/users/sign_in' }
  let(:params) do
    {
      user: {
        email: user.email,
        password: user.password
      }
    }
  end

  context 'when params are correct' do
    before do
      post url, params: params
    end

    it 'returns 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns XSRF token in authorization header' do
      expect(response.headers['X-XSRF-TOKEN']).to be_present
    end

    it 'drops cookies' do
      expect(cookies['_session_id']).to be_present
      expect(cookies['XSRF-TOKEN']).to be_present
    end

    it 'should be the same XSRF token both in cookie and header' do
      expect(@request.cookie_jar.signed['XSRF-TOKEN']).to eq(response.headers['X-XSRF-TOKEN'])
    end
  end

  context 'when login params are incorrect' do
    before { post url }

    it 'returns unauthorized status' do
      expect(response.status).to eq 401
    end

    it 'does not drop any cookies' do
      expect(cookies.to_json).to eq('{}')
    end

    it 'does not return XSRF token in authorization header' do
      expect(response.headers['XSRF-TOKEN']).to_not be_present
    end
  end
end

RSpec.describe 'DELETE /logout', type: :request do
  let(:url) { '/users/sign_out' }

  it 'returns 204, no content' do
    delete url
    expect(response).to have_http_status(204)
  end
end
