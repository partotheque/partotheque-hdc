# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { 'john@d.oe' }
    password { 'motdepasse' }
  end
end
