# frozen_string_literal: true

FactoryBot.define do
  factory :person do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    birth_country { Faker::Games::Witcher.location }
    birth_date { Faker::Date.birthday(min_age: 40, max_age: 85).year }
    death_date { Faker::Date.birthday(min_age: 1, max_age: 17).year }
    biography { Faker::TvShows::DrWho.quote }
    nicknames { Faker::Name.initials }

    trait :minimal do
      first_name {}
      last_name {}
      birth_country { Faker::Games::Witcher.location }
      birth_date { Faker::Date.birthday(min_age: 40, max_age: 85).year }
      death_date { Faker::Date.birthday(min_age: 1, max_age: 17).year }
      biography { '' }
      nicknames {}
    end
  end
end
