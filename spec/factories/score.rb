# frozen_string_literal: true

CLASSIFICATIONS = ['Ballet', 'Classique', 'Fantaisie', 'Grandes Marches',
                   'Jazz', 'Grenier', 'Marche avec clique',
                   'Fantaisie 30 et Marche avec clique',
                   ' Grenier 26 et Marche avec clique',
                   'Marche Sans clique', 'Marche sans clique',
                   'Opera', 'Ouverture', 'Religieux et Funèbre'].freeze

DIFFICULTIES = ['Normal', 'Difficile', 'Facile',
                'Très difficile', 'Très facile'].freeze

SCORE_TYPES = ['Carton', 'Vieux papier', 'Papier neuf'].freeze

CONDUCTOR_TYPES = ['Réduit en Sib', 'Absent', 'Complet transposé',
                   'Réduit en Ut', 'Complet en Ut'].freeze

FactoryBot.define do
  factory :score do
    comment { Faker::TvShows::HowIMetYourMother.quote }
    acquisition_date { (1800..2019).to_a.sample }
    classification { CLASSIFICATIONS.sample }
    number { (1..12).to_a.sample }
    conductor_ref { Faker::Code.isbn }
    archive_ref { Faker::Code.nric }
    audio_support { [true, false].sample }
    editor { Faker::Book.publisher }
    title { Faker::Book.title }
    duration { (30..1000).to_a.sample }
    difficulty { DIFFICULTIES.sample }
    writing_date { (1800..2019).to_a.sample }
    editing_date { (1950..2019).to_a.sample }
    score_type { SCORE_TYPES.sample }
    conductor_type { CONDUCTOR_TYPES.sample }
    style { Faker::Book.genre }
    composed_by { create(:person).id }
    arranged_by { create(:person).id }

    trait :minimal do
      comment {}
      editor {}
      title {}
      style {}
      composed_by {}
      arranged_by {}
    end
  end
end
