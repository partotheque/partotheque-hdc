# frozen_string_literal: true

FactoryBot.define do
  factory :concert do
    organizer { Faker::TvShows::Simpsons.character }
    place { Faker::TvShows::Simpsons.location }
    participant { Faker::TvShows::Simpsons.character }
    date { Faker::Date.between(from: 150.years.ago, to: Date.today) }
    name { Faker::TvShows::Buffy.episode }
    scores { [create(:score, :minimal, title: 'Kitty Cat')] }
    conductors { [create(:person)] }

    trait :minimal do
      organizer {}
      place {}
      participant {}
      date { Faker::Date.between(from: 150.years.ago, to: Date.today) }
      name {}
      conductors { [] }
    end
  end
end
