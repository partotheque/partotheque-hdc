# frozen_string_literal: true

# https://stackoverflow.com/a/26897302
require 'rails_helper'

RSpec.describe PasswordsController, type: :controller do
  before(:each) { @request.env['devise.mapping'] = Devise.mappings[:user] }

  let(:user) { create(:user) }

  context 'Reset password with an existing email address' do
    it 'should respond with 204 and send an email with the good token' do
      old_password = user.encrypted_password

      post :create, params: { user: {
        email: user.email
      } }

      expect(response).to have_http_status(204)

      user.reload

      expect(user.reset_password_token).to_not be_nil
      expect(ActionMailer::Base.deliveries.count).to eq(1)

      # Get the email, and get the reset password token from it
      message = ActionMailer::Base.deliveries[0].body.to_s
      rpt_index = message.index('reset_password_token') + 'reset_password_token'.length + 1
      reset_password_token = message[rpt_index...message.index('"', rpt_index)]

      patch(:update, params: { user: {
              reset_password_token: 'bad reset token',
              password: 'new-password',
              password_confirmation: 'new-password'
            } })

      expect(response).to have_http_status(200)
      expect(user.encrypted_password).to eq(old_password)

      patch(:update, params: { user: {
              reset_password_token: reset_password_token,
              password: 'new-password',
              password_confirmation: 'new-password'
            } })

      user.reload

      expect(response).to have_http_status(200)
      expect(user.encrypted_password).to_not eq(old_password)
    end
  end

  context 'Trying to reset password with a nonexistent email address' do
    before do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      post(:create, params: { user: {
             email: 'tata@tu.tu'
           } })
    end

    it 'should answer with a 204 as well but not send an email' do
      expect(response).to have_http_status(204)
      expect(ActionMailer::Base.deliveries.count).to eq(0)
    end
  end

  context 'Trying to reset password with a non real email address' do
    before do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      post(:create, params: { user: {
             email: 'tata'
           } })
    end

    it 'should answer with a 400 and not send an email' do
      expect(response).to have_http_status(400)
      expect(ActionMailer::Base.deliveries.count).to eq(0)
    end
  end
end
