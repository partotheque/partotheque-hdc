# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PingController, type: :controller do
  context 'Basic ping' do
    it 'should respond with 204' do
      get 'ping'
      expect(response.status).to eq(204)
    end
  end
end
