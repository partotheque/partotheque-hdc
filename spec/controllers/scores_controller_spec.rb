# frozen_string_literal: true

require 'rails_helper'
require_relative 'basic_controller_spec'

RSpec.describe ScoresController, type: :request do
  it_should_behave_like 'a basic controller' do
    let(:record_name) { 'score' }
    let(:basic_attribute) { record.title }

    let(:records_path) { scores_path }
    let(:record_path) { score_path(record.id) }

    let(:valid_params) do
      { score: {
        title: 'Citizen Kane',
        archive_ref: 'xxx-xxx',
        number: 1,
        composed_by: { id: Person.last.id },
        arranged_by: { last_name: 'Carioca' }
      } }
    end

    let(:invalid_params) do
      { score: { title: '' } }
    end

    let(:update_and_reload_record) do
      put record_path, params: { score: { title: new_value } }, headers: @headers
      record.reload
      @update_attribute = record.title
    end
  end

  let(:user) { create :user }
  let(:headers) do
    post user_session_path, params: {
      user: {
        email: user.email,
        password: user.password
      }
    }
    { 'X-XSRF-TOKEN' => response.headers['X-XSRF-TOKEN'] }
  end

  let(:concert1) { create(:concert) }
  let(:concert2) { create(:concert) }

  let(:composer) { create(:person, first_name: 'Jane', last_name: 'Smith') }
  let(:arranger) { create(:person, first_name: 'John', last_name: 'Doe') }

  let!(:score_without_concerts) { create(:score, composed_by: composer.id, arranged_by: arranger.id) }
  let!(:score_without_concerts_or_arranger) { create(:score, composed_by: composer.id, arranged_by: nil) }
  let!(:score_without_concerts_or_composer) { create(:score, composed_by: nil, arranged_by: arranger.id) }
  let!(:score_without_concerts_or_composer_or_arranger) { create(:score, composed_by: nil, arranged_by: nil) }

  let!(:score_without_arranger) do
    create(:score, composed_by: composer.id, arranged_by: nil, concerts: [concert1, concert2])
  end
  let!(:score_without_composer) do
    create(:score, composed_by: nil, arranged_by: arranger.id, concerts: [concert1, concert2])
  end
  let!(:score_without_composer_or_arranger) do
    create(:score, composed_by: nil, arranged_by: nil, concerts: [concert1, concert2])
  end
  let!(:score_with_composer_arranger_and_concerts) do
    create(:score, composed_by: composer.id, arranged_by: arranger.id, concerts: [concert1, concert2])
  end

  let(:arranger_attributes) do
    { 'id' => arranger.id, 'first_name' => arranger.first_name,
      'last_name' => arranger.last_name }
  end
  let(:composer_attributes) do
    { 'id' => composer.id, 'first_name' => composer.first_name,
      'last_name' => composer.last_name }
  end

  describe '#index' do
    before do
      get scores_path, headers: headers

      @scores = JSON.parse(response.body)
    end

    def find_score(score_to_find)
      @scores.find { |score| score['id'] == score_to_find.id }
    end

    context 'with composer' do
      it 'should return the `OK` HTTP code' do
        expect(response).to have_http_status(:ok)
      end

      it 'should include the attributes of both composer and arranger' do
        score_with_both_response = find_score(score_without_concerts)

        expect(score_with_both_response['composer']).to include(composer_attributes)
        expect(score_with_both_response['arranger']).to include(arranger_attributes)
      end
    end

    context 'without composer' do
      it 'should return the `OK` HTTP code' do
        expect(response).to have_http_status(:ok)
      end

      it 'should include the attributes of the arranger' do
        score_without_composer_response = find_score(score_without_composer)

        expect(score_without_composer_response['composer']).to be_nil
        expect(score_without_composer_response['arranger']).to include(arranger_attributes)
      end
    end

    context 'without arranger' do
      it 'should return the `OK` HTTP code' do
        expect(response).to have_http_status(:ok)
      end

      it 'should include the attributes of the composer' do
        score_without_arranger_response = find_score(score_without_arranger)

        expect(score_without_arranger_response['composer']).to include(composer_attributes)
        expect(score_without_arranger_response['arranger']).to be_nil
      end
    end

    context 'without either composer or arranger' do
      it 'should return the `OK` HTTP code' do
        expect(response).to have_http_status(:ok)
      end

      it 'should include the score with composers and arrangers' do
        score_with_neither_response = find_score(score_without_composer_or_arranger)

        expect(score_with_neither_response['composer']).to be_nil
        expect(score_with_neither_response['arranger']).to be_nil
      end
    end
  end

  describe '#show' do
    before do
      get score_path(a_score.id), headers: headers

      @score = JSON.parse(response.body)
    end

    let(:concert1_attributes) do
      { 'id' => concert1.id, 'organizer' => concert1.organizer, 'place' => concert1.place,
        'participant' => concert1.participant, 'date' => concert1.date, 'name' => concert1.name }
    end
    let(:concert2_attributes) do
      { 'id' => concert2.id, 'organizer' => concert2.organizer, 'place' => concert2.place,
        'participant' => concert2.participant, 'date' => concert2.date, 'name' => concert2.name }
    end

    context 'with concert' do
      context 'with composer' do
        context 'with arranger' do
          let(:a_score) { score_with_composer_arranger_and_concerts }

          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the score with composer, arranger and concerts' do
            expect(@score['concerts']).to match_array([hash_including(concert1_attributes),
                                                       hash_including(concert2_attributes)])

            expect(@score['composer']).to include(composer_attributes)
            expect(@score['arranger']).to include(arranger_attributes)
          end
        end

        context 'without arranger' do
          let(:a_score) { score_without_arranger }

          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the score with composer and concerts' do
            expect(@score['concerts']).to match_array([hash_including(concert1_attributes),
                                                       hash_including(concert2_attributes)])

            expect(@score['composer']).to include(composer_attributes)
            expect(@score['arranger']).to be_nil
          end
        end
      end

      context 'without composer' do
        context 'with arranger' do
          let(:a_score) { score_without_composer }

          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the score with arranger and concerts' do
            expect(@score['concerts']).to match_array([hash_including(concert1_attributes),
                                                       hash_including(concert2_attributes)])

            expect(@score['composer']).to be_nil
            expect(@score['arranger']).to include(arranger_attributes)
          end
        end

        context 'without arranger' do
          let(:a_score) { score_without_composer_or_arranger }

          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the score with concerts' do
            expect(@score['concerts']).to match_array([hash_including(concert1_attributes),
                                                       hash_including(concert2_attributes)])

            expect(@score['composer']).to be_nil
            expect(@score['arranger']).to be_nil
          end
        end
      end
    end

    context 'without concert' do
      context 'with composer' do
        context 'with arranger' do
          let(:a_score) { score_without_concerts }

          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the score with composer and arranger' do
            expect(@score['concerts']).to match([])

            expect(@score['composer']).to include(composer_attributes)
            expect(@score['arranger']).to include(arranger_attributes)
          end
        end

        context 'without arranger' do
          let(:a_score) { score_without_concerts_or_arranger }

          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the score with composer, arranger and concerts' do
            expect(@score['concerts']).to match([])

            expect(@score['composer']).to include(composer_attributes)
            expect(@score['arranger']).to be_nil
          end
        end
      end

      context 'without composer' do
        context 'with arranger' do
          let(:a_score) { score_without_concerts_or_composer }

          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the score with composer, arranger and concerts' do
            expect(@score['concerts']).to match([])

            expect(@score['composer']).to be_nil
            expect(@score['arranger']).to include(arranger_attributes)
          end
        end

        context 'without arranger' do
          let(:a_score) { score_without_concerts_or_composer_or_arranger }

          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the score with composer, arranger and concerts' do
            expect(@score['concerts']).to match([])

            expect(@score['composer']).to be_nil
            expect(@score['arranger']).to be_nil
          end
        end
      end
    end
  end

  describe '#create' do
    let(:new_score) { build(:score) }

    before do
      post scores_path, headers: headers, params: params

      @score = JSON.parse(response.body)
    end

    context 'invalid parameters' do
      context 'empty mandatory parameters' do
        let(:params) do
          { score: new_score.attributes.merge('title' => '', 'archive_ref' => '', 'number' => '') }
        end

        it 'should respond with the `unprocessable entity` HTTP code' do
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'should return an error message about the title' do
          expect(@score['title']).to match_array('doit être rempli(e)')
        end

        it 'should return an error message about the archive reference' do
          expect(@score['archive_ref']).to match_array('doit être rempli(e)')
        end

        it 'should return an error message about the number' do
          expect(@score['number']).to match_array('doit être rempli(e)')
        end
      end
    end

    context 'valid parameters' do
      context 'with only composed_by parameter' do
        context 'with an existent person' do
          let(:params) do
            { score: new_score.attributes.merge('composed_by' => { id: composer.id }) }
          end

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:created)
          end

          it 'should return the new score with the attributes correctly set' do
            expect(@score['title']).to eq(params[:score]['title'])
            expect(@score['composer']['id']).to eq(composer.id)
          end

          it 'should not have create a new composer' do
            expect(Person.where(last_name: composer.last_name).count).to eq(1)
          end
        end

        context 'with a non-existent person' do
          let(:new_composer_attributes) { { last_name: 'Chaminade', first_name: 'Cécile' } }
          let(:params) do
            { score: new_score.attributes.merge('composed_by' => new_composer_attributes) }
          end
          let(:new_composer) { Person.find_by(new_composer_attributes) }

          it 'should return the new score with the attributes correctly set' do
            expect(@score['title']).to eq(params[:score]['title'])
            expect(@score['composer']['id']).to eq(new_composer.id)
          end

          it 'should have create a new composer' do
            expect(new_composer).to be_present
          end
        end
      end

      context 'with only arranged_by parameter' do
        context 'with an existent person' do
          let(:params) do
            { score: new_score.attributes.merge('arranged_by' => { id: arranger.id }) }
          end

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:created)
          end

          it 'should return the new score with the attributes correctly set' do
            expect(@score['title']).to eq(params[:score]['title'])
            expect(@score['arranger']['id']).to eq(arranger.id)
          end

          it 'should not have create a new arranger' do
            expect(Person.where(last_name: arranger.last_name)).to exist
          end
        end

        context 'with a non-existent person' do
          let(:new_arranger_attributes) { { last_name: 'Ducol', first_name: 'Clément' } }
          let(:params) do
            { score: new_score.attributes.merge('arranged_by' => new_arranger_attributes) }
          end
          let(:new_arranger) { Person.find_by(new_arranger_attributes) }

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:created)
          end

          it 'should return the new score with the attributes correctly set' do
            expect(@score['title']).to eq(params[:score]['title'])
            expect(@score['arranger']['id']).to eq(new_arranger.id)
          end

          it 'should have create a new arranger' do
            expect(Person.where(last_name: new_arranger_attributes[:last_name])).to exist
          end
        end
      end

      context 'with both composed_by and arranged_by parameters' do
        context 'with existent people' do
          let(:params) do
            { score: new_score.attributes.merge('composed_by' => { id: composer.id },
                                                'arranged_by' => { id: arranger.id }) }
          end

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:created)
          end

          it 'should return the new score with the attributes correctly set' do
            expect(@score['title']).to eq(params[:score]['title'])
            expect(@score['composer']['id']).to eq(composer.id)
            expect(@score['arranger']['id']).to eq(arranger.id)
          end

          it 'should not have create a new composer' do
            expect(Person.where(last_name: composer.last_name)).to exist
          end

          it 'should not have create a new arranger' do
            expect(Person.where(last_name: arranger.last_name)).to exist
          end
        end

        context 'with non-existent people' do
          let(:new_composer_attributes) { { last_name: 'Chaminade', first_name: 'Cécile' } }
          let(:new_arranger_attributes) { { last_name: 'Ducol', first_name: 'Clément' } }
          let(:params) do
            { score: new_score.attributes.merge('composed_by' => new_composer_attributes,
                                                'arranged_by' => new_arranger_attributes) }
          end
          let(:new_composer) { Person.find_by(new_composer_attributes) }
          let(:new_arranger) { Person.find_by(new_arranger_attributes) }

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:created)
          end

          it 'should return the new score with the attributes correctly set' do
            expect(@score['title']).to eq(params[:score]['title'])
            expect(@score['composer']['id']).to eq(new_composer.id)
            expect(@score['arranger']['id']).to eq(new_arranger.id)
          end

          it 'should have create a new composer' do
            expect(Person.where(last_name: new_composer_attributes[:last_name])).to exist
          end

          it 'should have create a new arranger' do
            expect(Person.where(last_name: new_arranger_attributes[:last_name])).to exist
          end
        end
      end

      context 'without composed_by or arranged_by parameter' do
        let(:params) do
          { score: new_score.attributes.merge('composed_by' => {}, 'arranged_by' => {}) }
        end

        it 'should respond with the `created` HTTP code' do
          expect(response).to have_http_status(:created)
        end

        it 'should return the new score with the attributes correctly set' do
          expect(@score['title']).to eq(params[:score]['title'])
        end
      end
    end
  end

  describe '#update' do
    before do
      patch score_path(score_with_composer_arranger_and_concerts.id), headers: headers, params: params

      @score = JSON.parse(response.body)
    end

    context 'invalid parameters' do
      let(:params) { { score: { 'title' => '' } } }

      it 'should respond with the `unprocessable entity` HTTP code' do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should return the error message' do
        expect(@score['title']).to match_array('doit être rempli(e)')
      end
    end

    context 'valid parameters' do
      let(:new_score) { build(:score) }

      context 'with only composed_by parameter' do
        context 'with an existent person' do
          let(:params) do
            { score: new_score.attributes.merge('composed_by' => { id: composer.id }) }
          end

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the new score with the attributes correctly set' do
            expect(@score['title']).to eq(params[:score]['title'])
            expect(@score['composer']['id']).to eq(composer.id)
          end

          it 'should not have create a new composer' do
            expect(Person.where(last_name: composer.last_name)).to exist
          end
        end

        context 'with a non-existent person' do
          let(:new_composer_attributes) { { last_name: 'Chaminade', first_name: 'Cécile' } }
          let(:params) do
            { score: new_score.attributes.merge('composed_by' => new_composer_attributes) }
          end
          let(:new_composer) { Person.find_by(new_composer_attributes) }

          it 'should return the new score with the attributes correctly set' do
            expect(@score['title']).to eq(params[:score]['title'])
            expect(@score['composer']['id']).to eq(new_composer.id)
          end

          it 'should have create a new composer' do
            expect(new_composer).to be_present
          end
        end
      end

      context 'with only arranged_by parameter' do
        context 'with an existent person' do
          let(:params) do
            { score: new_score.attributes.merge('arranged_by' => { id: arranger.id }) }
          end

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the new score with the attributes correctly set' do
            expect(@score['title']).to eq(params[:score]['title'])
            expect(@score['arranger']['id']).to eq(arranger.id)
          end

          it 'should not have create a new arranger' do
            expect(Person.where(last_name: arranger.last_name)).to exist
          end
        end

        context 'with a non-existent person' do
          let(:new_arranger_attributes) { { last_name: 'Ducol', first_name: 'Clément' } }
          let(:params) do
            { score: new_score.attributes.merge('arranged_by' => new_arranger_attributes) }
          end
          let(:new_arranger) { Person.find_by(new_arranger_attributes) }

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the new score with the attributes correctly set' do
            expect(@score['title']).to eq(params[:score]['title'])
            expect(@score['arranger']['id']).to eq(new_arranger.id)
          end

          it 'should have create a new arranger' do
            expect(new_arranger).to be_present
          end
        end
      end

      context 'with both composed_by and arranged_by parameters' do
        context 'with existent people' do
          let(:params) do
            { score: new_score.attributes.merge('composed_by' => { id: composer.id },
                                                'arranged_by' => { id: arranger.id }) }
          end

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the new score with the attributes correctly set' do
            expect(@score['title']).to eq(params[:score]['title'])
            expect(@score['composer']['id']).to eq(composer.id)
            expect(@score['arranger']['id']).to eq(arranger.id)
          end

          it 'should not have create a new composer' do
            expect(Person.where(last_name: composer.last_name)).to exist
          end

          it 'should not have create a new arranger' do
            expect(Person.where(last_name: arranger.last_name)).to exist
          end
        end

        context 'with non-existent people' do
          let(:new_composer_attributes) { { last_name: 'Chaminade', first_name: 'Cécile' } }
          let(:new_arranger_attributes) { { last_name: 'Ducol', first_name: 'Clément' } }
          let(:params) do
            { score: new_score.attributes.merge('composed_by' => new_composer_attributes,
                                                'arranged_by' => new_arranger_attributes) }
          end
          let(:new_composer) { Person.find_by(new_composer_attributes) }
          let(:new_arranger) { Person.find_by(new_arranger_attributes) }

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the new score with the attributes correctly set' do
            expect(@score['title']).to eq(params[:score]['title'])
            expect(@score['composer']['id']).to eq(new_composer.id)
            expect(@score['arranger']['id']).to eq(new_arranger.id)
          end

          it 'should have create a new composer' do
            expect(Person.where(last_name: new_composer_attributes[:last_name])).to exist
          end

          it 'should have create a new arranger' do
            expect(Person.where(last_name: new_arranger_attributes[:last_name])).to exist
          end
        end
      end

      context 'without composed_by or arranged_by parameter' do
        let(:params) do
          { score: new_score.attributes.merge('composed_by' => {}, 'arranged_by' => {}) }
        end

        it 'should respond with the `created` HTTP code' do
          expect(response).to have_http_status(:ok)
        end

        it 'should return the new score with the attributes correctly set' do
          expect(@score['title']).to eq(params[:score]['title'])
        end
      end
    end
  end

  describe '#search' do
    let!(:good_score) { create(:score, :minimal, title: 'Good Title') }
    let!(:bad_score) { create(:score, :minimal, title: 'Bad Title') }
    let!(:ugly_score) { create(:score, :minimal, title: 'Ugly Name') }

    before do
      post scores_search_path, headers: headers, params: params

      @result = JSON.parse(response.body)
    end

    context 'same name' do
      let(:params) { { 'search' => { title: 'Title' } } }

      it 'should return only the accurate scores' do
        expect(@result.count).to eq(2)
        expect(@result.first['title']).to eq(good_score.title)
        expect(@result.last['title']).to eq(bad_score.title)
      end
    end

    context 'random name' do
      let(:params) { { 'search' => { title: 'Random' } } }

      it 'should return absolutely nothing' do
        expect(@result.count).to be_zero
      end
    end
  end
end
