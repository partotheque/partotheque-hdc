# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SearchController, type: :request do
  let(:user) { create(:user) }
  let(:headers) do
    post user_session_path, params: {
      user: {
        email: user.email,
        password: user.password
      }
    }
    { 'X-XSRF-TOKEN' => response.headers['X-XSRF-TOKEN'] }
  end

  let!(:good_score) { create(:score, :minimal, title: 'Good Title') }
  let!(:bad_score) { create(:score, :minimal, title: 'Bad Title') }
  let!(:ugly_score) { create(:score, :minimal, title: 'Ugly Name') }

  let!(:good_concert) { create(:concert, :minimal, name: 'Good Name') }
  let!(:bad_concert) { create(:concert, :minimal, name: 'Bad Name') }
  let!(:ugly_concert) { create(:concert, :minimal, name: 'Ugly Name') }

  let!(:good_person) { create(:person, :minimal, last_name: 'Good Name') }
  let!(:bad_person) { create(:person, :minimal, last_name: 'Bad Name') }
  let!(:ugly_person) { create(:person, :minimal, last_name: 'Ugly Name') }

  describe '#multi_search' do
    before do
      post multi_search_path, headers: headers, params: params

      @result = JSON.parse(response.body)
    end

    context 'same name' do
      let(:params) { { 'search' => 'Good' } }

      it 'should return only the accurate records' do
        expect(@result.values.flatten.count).to eq(3)
        expect(@result['scores'].first['title']).to eq(good_score.title)
        expect(@result['concerts'].first['name']).to eq(good_concert.name)
        expect(@result['people'].first['last_name']).to eq(good_person.last_name)
      end
    end

    context 'nearly same name' do
      let(:params) { { 'search' => 'Titl' } }

      it 'should return only the accurate records' do
        expect(@result.values.flatten.count).to eq(2)
        expect(@result['scores'].first['title']).to eq(good_score.title)
        expect(@result['scores'].last['title']).to eq(bad_score.title)
      end
    end

    context 'random name' do
      let(:params) { { 'search' => { title: 'Random' } } }

      it 'should return absolutely nothing' do
        expect(@result.count).to be_zero
      end
    end
  end
end
