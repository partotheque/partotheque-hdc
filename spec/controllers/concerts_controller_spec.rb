# frozen_string_literal: true

require 'rails_helper'
require_relative 'basic_controller_spec'

RSpec.describe ConcertsController, type: :request do
  it_should_behave_like 'a basic controller' do
    let(:record_name) { 'concert' }
    let(:basic_attribute) { record.organizer }

    let(:records_path) { concerts_path }
    let(:record_path) { concert_path(record.id) }

    let(:valid_params) do
      { concert: {
        name: 'Agnes Moorehead',
        conductors: [
          { id: Person.first.id },
          { id: nil, last_name: 'Person' }
        ]
      } }
    end

    let(:invalid_params) do
      { concert: {
        name: ''
      } }
    end

    let(:update_and_reload_record) do
      put record_path, params: { concert: { organizer: new_value } }, headers: @headers
      record.reload
      @update_attribute = record.organizer
    end
  end

  let(:user) { create :user }
  let(:headers) do
    post user_session_path, params: {
      user: {
        email: user.email,
        password: user.password
      }
    }
    { 'X-XSRF-TOKEN' => response.headers['X-XSRF-TOKEN'] }
  end

  describe '#index' do
    let(:first_conductor) { create(:person) }
    let(:second_conductor) { create(:person) }

    let!(:concert_without_conductors) { create(:concert, conductors: []) }
    let!(:concert_with_a_conductor) { create(:concert, conductors: [first_conductor]) }
    let!(:concert_with_two_conductors) { create(:concert, conductors: [first_conductor, second_conductor]) }

    before do
      get concerts_path, headers: headers

      @concerts = JSON.parse(response.body)
    end

    def find_concert(concert_to_find)
      @concerts.find { |concert| concert['id'] == concert_to_find.id }
    end

    context 'with one conductor' do
      it 'should return the `OK` HTTP code' do
        expect(response).to have_http_status(:ok)
      end

      it 'should include the conductor' do
        expect(find_concert(concert_with_a_conductor)['conductors']).to match_array(
          hash_including(
            {
              'id' => first_conductor.id,
              'last_name' => first_conductor.last_name,
              'first_name' => first_conductor.first_name
            }
          )
        )
      end
    end

    context 'with two conductors' do
      it 'should return the `OK` HTTP code' do
        expect(response).to have_http_status(:ok)
      end

      it 'should include the conductors' do
        expect(find_concert(concert_with_two_conductors)['conductors']).to match_array(
          [
            hash_including(
              {
                'id' => first_conductor.id,
                'last_name' => first_conductor.last_name,
                'first_name' => first_conductor.first_name
              }
            ),
            hash_including(
              {
                'id' => second_conductor.id,
                'last_name' => second_conductor.last_name,
                'first_name' => second_conductor.first_name
              }
            )
          ]
        )
      end
    end

    context 'without any conductors' do
      it 'should return the `OK` HTTP code' do
        expect(response).to have_http_status(:ok)
      end

      it 'should not include any conductors' do
        expect(find_concert(concert_without_conductors)['conductors']).to eq([])
      end
    end
  end

  describe '#show' do
    let(:a_score) { create(:score) }
    let(:first_conductor) { create(:person) }
    let(:second_conductor) { create(:person) }

    let!(:concert_without_conductors) { create(:concert, conductors: [], scores: []) }
    let!(:concert_without_conductors_but_a_score) { create(:concert, conductors: [], scores: [a_score]) }

    let!(:concert_with_a_conductor) { create(:concert, conductors: [first_conductor], scores: []) }
    let!(:concert_with_a_conductor_and_a_score) do
      create(:concert, conductors: [first_conductor],
                       scores: [a_score])
    end

    let!(:concert_with_two_conductors) do
      create(:concert, conductors: [first_conductor, second_conductor], scores: [])
    end
    let!(:concert_with_two_conductors_and_a_score) do
      create(:concert, conductors: [first_conductor, second_conductor],
                       scores: [a_score])
    end

    before do
      get concert_path(concert.id), headers: headers

      @concert = JSON.parse(response.body)
    end

    context 'with one conductor' do
      context 'without a score' do
        let(:concert) { concert_with_a_conductor }

        it 'should return the `OK` HTTP code' do
          expect(response).to have_http_status(:ok)
        end

        it 'should include the conductor' do
          expect(@concert['conductors']).to match_array(
            hash_including(
              {
                'id' => first_conductor.id,
                'last_name' => first_conductor.last_name,
                'first_name' => first_conductor.first_name
              }
            )
          )
        end

        it 'should not include any score' do
          expect(@concert['scores']).to be_empty
        end
      end

      context 'with one score' do
        let(:concert) { concert_with_a_conductor_and_a_score }

        it 'should return the `OK` HTTP code' do
          expect(response).to have_http_status(:ok)
        end

        it 'should include the conductor' do
          expect(@concert['conductors']).to match_array(
            hash_including(
              {
                'id' => first_conductor.id,
                'last_name' => first_conductor.last_name,
                'first_name' => first_conductor.first_name
              }
            )
          )
        end

        it 'should include the score' do
          expect(@concert['scores']).to match_array(
            hash_including(
              'id' => a_score.id,
              'title' => a_score.title,
              'archive_ref' => a_score.archive_ref,
              'conductor_ref' => a_score.conductor_ref,
              'editor' => a_score.editor
            )
          )
        end
      end
    end

    context 'with two conductors' do
      context 'without a score' do
        let(:concert) { concert_with_two_conductors }

        it 'should return the `OK` HTTP code' do
          expect(response).to have_http_status(:ok)
        end

        it 'should include the conductors' do
          expect(@concert['conductors']).to match_array(
            [
              hash_including(
                {
                  'id' => first_conductor.id,
                  'last_name' => first_conductor.last_name,
                  'first_name' => first_conductor.first_name
                }
              ),
              hash_including(
                {
                  'id' => second_conductor.id,
                  'last_name' => second_conductor.last_name,
                  'first_name' => second_conductor.first_name
                }
              )
            ]
          )
        end

        it 'should not include any score' do
          expect(@concert['scores']).to be_empty
        end
      end

      context 'with one score' do
        let(:concert) { concert_with_two_conductors_and_a_score }

        it 'should return the `OK` HTTP code' do
          expect(response).to have_http_status(:ok)
        end

        it 'should include the conductors' do
          expect(@concert['conductors']).to match_array(
            [
              hash_including(
                {
                  'id' => first_conductor.id,
                  'last_name' => first_conductor.last_name,
                  'first_name' => first_conductor.first_name
                }
              ),
              hash_including(
                {
                  'id' => second_conductor.id,
                  'last_name' => second_conductor.last_name,
                  'first_name' => second_conductor.first_name
                }
              )
            ]
          )
        end

        it 'should include the score' do
          expect(@concert['scores']).to match_array(
            hash_including(
              'id' => a_score.id,
              'title' => a_score.title,
              'archive_ref' => a_score.archive_ref,
              'conductor_ref' => a_score.conductor_ref,
              'editor' => a_score.editor
            )
          )
        end
      end
    end

    context 'without a conductor' do
      context 'without a score' do
        let(:concert) { concert_without_conductors }

        it 'should return the `OK` HTTP code' do
          expect(response).to have_http_status(:ok)
        end

        it 'should not include any conductor' do
          expect(@concert['conductors']).to eq([])
        end

        it 'should not include any score' do
          expect(@concert['scores']).to be_empty
        end
      end

      context 'with one score' do
        let(:concert) { concert_without_conductors_but_a_score }

        it 'should return the `OK` HTTP code' do
          expect(response).to have_http_status(:ok)
        end

        it 'should not include any conductor' do
          expect(@concert['conductors']).to eq([])
        end

        it 'should include the score' do
          expect(@concert['scores']).to match_array(
            hash_including(
              'id' => a_score.id,
              'title' => a_score.title,
              'archive_ref' => a_score.archive_ref,
              'conductor_ref' => a_score.conductor_ref,
              'editor' => a_score.editor
            )
          )
        end
      end
    end
  end

  describe '#create' do
    before do
      post concerts_path, headers: headers, params: params

      @concert = JSON.parse(response.body)
    end

    context 'invalid parameters' do
      let(:params) { { concert: { 'key' => 'value' } } }

      it 'should respond with the `unprocessable entity` HTTP code' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'valid parameters' do
      context 'without conductors' do
        let(:params) { { concert: { 'name' => 'A Name' } } }

        it 'should respond with the `created` HTTP code' do
          expect(response).to have_http_status(:created)
        end

        it 'should return the new concert with the attributes correctly set' do
          expect(@concert['name']).to eq(params[:concert]['name'])
        end
      end

      context 'with one conductor' do
        context 'who exists' do
          let(:first_conductor) { create(:person) }
          let(:params) { { concert: { 'name' => 'New Name', 'conductors' => [{ 'id' => first_conductor.id }] } } }

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:created)
          end

          it 'should return the new concert with the attributes correctly set' do
            expect(@concert['name']).to eq(params[:concert]['name'])
          end

          it 'should return the conductor' do
            expect(@concert['conductors'].first['id']).to eq(first_conductor.id)
          end

          it 'should not have create a new person' do
            expect(Person.where(last_name: first_conductor.last_name).count).to eq(1)
          end
        end

        context 'who does not exist' do
          let(:params) { { concert: { 'name' => 'New Name', 'conductors' => [{ last_name: 'Doe' }] } } }

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:created)
          end

          it 'should return the new concert with the attributes correctly set' do
            expect(@concert['name']).to eq(params[:concert]['name'])
          end

          it 'should return the conductor' do
            expect(@concert['conductors'].first['id']).to eq(Person.last.id)
          end

          it 'should have create a new person' do
            expect(Person.where(last_name: params[:concert]['conductors'].first[:last_name]).count).to eq(1)
          end
        end
      end

      context 'with two conductors' do
        context 'who exists' do
          let(:first_conductor) { create(:person) }
          let(:second_conductor) { create(:person) }
          let(:params) do
            { concert: { 'name' => 'New Name',
                         'conductors' => [{ id: first_conductor.id }, { id: second_conductor.id }] } }
          end

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:created)
          end

          it 'should return the new concert with the attributes correctly set' do
            expect(@concert['name']).to eq(params[:concert]['name'])
          end

          it 'should return the two conductors' do
            expect(@concert['conductors'].map { |c| c['id'] }).to match_array(Person.last(2).map(&:id))
          end

          it 'should not have create new people' do
            expect(Person.where(last_name: first_conductor.last_name).count).to eq(1)
            expect(Person.where(last_name: second_conductor.last_name).count).to eq(1)
          end
        end

        context 'who does not exist' do
          let(:params) do
            { concert: { 'name' => 'New Name', 'conductors' => [{ last_name: 'Doe' }, { last_name: 'Smith' }] } }
          end

          it 'should respond with the `created` HTTP code' do
            expect(response).to have_http_status(:created)
          end

          it 'should return the new concert with the attributes correctly set' do
            expect(@concert['name']).to eq(params[:concert]['name'])
          end

          it 'should return the conductors' do
            expect(@concert['conductors'].map { |c| c['id'] }).to match_array(Person.last(2).map(&:id))
          end

          it 'should have create two new people' do
            expect(Person.where(last_name: params[:concert]['conductors'].first[:last_name]).count).to eq(1)
            expect(Person.where(last_name: params[:concert]['conductors'].last[:last_name]).count).to eq(1)
          end
        end
      end
    end
  end

  describe '#update' do
    let!(:concert) { create(:concert) }

    before do
      post concerts_path(concert.id), headers: headers, params: params

      @concert = JSON.parse(response.body)
    end

    context 'invalid parameters' do
      let(:params) { { concert: { 'name' => '' } } }

      it 'should respond with the `unprocessable entity` HTTP code' do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should return the error message' do
        expect(@concert['name']).to match_array('doit être rempli(e)')
      end
    end

    context 'valid parameters' do
      let(:new_concert) { build(:concert) }

      context 'without conductors' do
        let(:params) { { concert: { 'name' => 'A Name' } } }

        it 'should respond with the `ok` HTTP code' do
          expect(response).to have_http_status(:created)
        end

        it 'should return the new concert with the attributes correctly set' do
          expect(@concert['name']).to eq(params[:concert]['name'])
        end
      end

      context 'with one conductor' do
        context 'who exists' do
          let(:first_conductor) { create(:person) }
          let(:params) { { concert: { 'name' => 'New Name', 'conductors' => [{ 'id' => first_conductor.id }] } } }

          it 'should respond with the `ok` HTTP code' do
            expect(response).to have_http_status(:created)
          end

          it 'should return the new concert with the attributes correctly set' do
            expect(@concert['name']).to eq(params[:concert]['name'])
          end

          it 'should return the conductor' do
            expect(@concert['conductors'].first['id']).to eq(first_conductor.id)
          end

          it 'should not have create a new person' do
            expect(Person.where(last_name: first_conductor.last_name).count).to eq(1)
          end
        end

        context 'who does not exist' do
          let(:params) { { concert: { 'name' => 'New Name', 'conductors' => [{ last_name: 'Doe' }] } } }

          it 'should respond with the `ok` HTTP code' do
            expect(response).to have_http_status(:created)
          end

          it 'should return the new concert with the attributes correctly set' do
            expect(@concert['name']).to eq(params[:concert]['name'])
          end

          it 'should return the conductor' do
            expect(@concert['conductors'].first['id']).to eq(Person.last.id)
          end

          it 'should have create a new person' do
            expect(Person.where(last_name: params[:concert]['conductors'].first[:last_name]).count).to eq(1)
          end
        end
      end

      context 'with two conductors' do
        context 'who exists' do
          let(:first_conductor) { create(:person, last_name: 'Smith') }
          let(:second_conductor) { create(:person, last_name: 'Doe') }
          let(:params) do
            { concert: { 'name' => 'New Name',
                         'conductors' => [{ id: first_conductor.id }, { id: second_conductor.id }] } }
          end

          it 'should respond with the `ok` HTTP code' do
            expect(response).to have_http_status(:created)
          end

          it 'should return the new concert with the attributes correctly set' do
            expect(@concert['name']).to eq(params[:concert]['name'])
          end

          it 'should return the two conductors' do
            expect(@concert['conductors'].map { |c| c['id'] }).to match_array(Person.last(2).map(&:id))
          end

          it 'should not have create new people' do
            expect(Person.where(last_name: first_conductor.last_name).count).to eq(1)
            expect(Person.where(last_name: second_conductor.last_name).count).to eq(1)
          end
        end

        context 'who does not exist' do
          let(:params) do
            { concert: { 'name' => 'New Name', 'conductors' => [{ last_name: 'Doe' }, { last_name: 'Smith' }] } }
          end

          it 'should respond with the `ok` HTTP code' do
            expect(response).to have_http_status(:created)
          end

          it 'should return the new concert with the attributes correctly set' do
            expect(@concert['name']).to eq(params[:concert]['name'])
          end

          it 'should return the conductors' do
            expect(@concert['conductors'].map { |c| c['id'] }).to match_array(Person.last(2).map(&:id))
          end

          it 'should have create two new people' do
            expect(Person.where(last_name: params[:concert]['conductors'].first[:last_name]).count).to eq(1)
            expect(Person.where(last_name: params[:concert]['conductors'].last[:last_name]).count).to eq(1)
          end
        end
      end
    end
  end

  describe '#search' do
    let!(:good_concert) { create(:concert, :minimal, name: 'Good Name') }
    let!(:bad_concert) { create(:concert, :minimal, name: 'Bad Name') }
    let!(:ugly_concert) { create(:concert, :minimal, name: 'Ugly Title') }

    before do
      post concerts_search_path, headers: headers, params: params

      @result = JSON.parse(response.body)
    end

    context 'same name' do
      let(:params) { { 'search' => { name: 'Name' } } }

      it 'should return only the accurate concerts' do
        expect(@result.count).to eq(2)
        expect(@result.first['name']).to eq(good_concert.name)
        expect(@result.last['name']).to eq(bad_concert.name)
      end
    end

    context 'random name' do
      let(:params) { { 'search' => { name: 'Random' } } }

      it 'should return absolutely nothing' do
        expect(@result.count).to be_zero
      end
    end
  end
end
