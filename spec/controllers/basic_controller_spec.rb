# frozen_string_literal: true

shared_examples_for 'a basic controller' do
  let!(:user) { create(:user) }
  let!(:new_value) { 'Xanadu' }
  let!(:record) { create(record_name.to_sym) }

  context 'When the user is not connected' do
    it 'is impossible to do any CRUD action' do
      crud_actions(false)
    end
  end

  context 'When the user is not connected, with headers' do
    it 'is impossible to do any CRUD action' do
      get records_path
      set_headers

      crud_actions(false)
    end
  end

  context 'When the user sign out' do
    it 'should fail for any CRUD action' do
      sign_in user
      set_headers
      sign_out user

      expect(cookies['XSRF-TOKEN']).to be_blank
      crud_actions(false)
    end
  end

  context 'When the user is logged in' do
    it 'should success only on the basis of validity of params' do
      sign_in user
      set_headers

      crud_actions(true)
    end
  end

  context 'When the user is logged in, without headers' do
    it 'should fail for any CRUD action' do
      sign_in user
      @headers = nil

      crud_actions(false)
    end
  end

  context 'When the user is logged in, with headers but no cookies' do
    it 'should fail for any CRUD action' do
      sign_in user

      crud_actions(false)
    end
  end

  context "When the user is logged in, with headers but without '_session_id' cookie" do
    it 'should fail for any CRUD action' do
      sign_in user
      cookies['_session_id'] = nil

      crud_actions(false)
    end
  end

  context "When the user is logged in, with headers but without 'XSRF-TOKEN' cookie" do
    it 'should fail for any CRUD action' do
      sign_in user
      cookies['XSRF-TOKEN'] = nil

      crud_actions(false)
    end
  end
end

private

def list_records(authorized)
  get records_path, headers: @headers

  if authorized
    expect_ok
    expect(response.body).to_not eq('[]')
  else
    expect_unauthorized
  end
end

def describe_record(authorized)
  get record_path, headers: @headers

  if authorized
    expect_ok
    expect(response.body).to match(basic_attribute)
  else
    expect_unauthorized
    expect(response.body).to_not match(basic_attribute)
  end
end

def create_record(authorized)
  post records_path, params: valid_params, headers: @headers

  authorized ? expect_status(201) : expect_unauthorized
end

def invalid_create(authorized)
  post records_path, params: invalid_params, headers: @headers

  authorized ? expect_status(422) : expect_unauthorized
end

def update_record(authorized)
  update_and_reload_record

  if authorized
    expect_ok
    expect(response.body).to match(new_value)
    expect(@update_attribute).to eq(new_value)
  else
    expect_unauthorized
    expect(response.body).to_not match(new_value)
    expect(@update_attribute).to_not eq(new_value)
  end
end

def invalid_update(authorized)
  put record_path, params: invalid_params, headers: @headers

  authorized ? expect_status(422) : expect_unauthorized
end

def delete_record(authorized)
  delete record_path, headers: @headers

  model_name = record_name.camelize.constantize
  if authorized
    expect_status(204)
    expect { model_name.find(record.id) }.to raise_error(ActiveRecord::RecordNotFound)
  else
    expect_unauthorized
    expect(model_name.find(record.id)).to eq(record)
  end
end

def crud_actions(authorized)
  list_records(authorized)
  describe_record(authorized)
  create_record(authorized)
  invalid_create(authorized)
  update_record(authorized)
  invalid_update(authorized)
  delete_record(authorized)
end

def expect_ok
  expect_status(200)
end

def expect_unauthorized
  expect_status(401)
end

def expect_status(status)
  expect(response).to have_http_status(status)
end

def sign_in(user)
  post user_session_path, params: {
    user: {
      email: user.email,
      password: user.password
    }
  }
end

def sign_out(user)
  delete destroy_user_session_path, params: {
    user: {
      email: user.email
    }
  }
end

def set_headers
  @headers = { 'X-XSRF-TOKEN' => response.headers['X-XSRF-TOKEN'] }
end
