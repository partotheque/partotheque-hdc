# frozen_string_literal: true

require 'rails_helper'
require_relative 'basic_controller_spec'

RSpec.describe PeopleController, type: :request do
  it_should_behave_like 'a basic controller' do
    let(:record_name) { 'person' }
    let(:basic_attribute) { record.first_name }

    let(:records_path) { people_path }
    let(:record_path) { person_path(record.id) }

    let(:valid_params) do
      { person: {
        last_name: 'Kane'
      } }
    end

    let(:invalid_params) do
      { person: {
        last_name: ''
      } }
    end

    let(:update_and_reload_record) do
      put record_path, params: { person: { first_name: new_value } }, headers: @headers
      record.reload
      @update_attribute = record.first_name
    end
  end

  let(:user) { create :user }
  let(:headers) do
    post user_session_path, params: {
      user: {
        email: user.email,
        password: user.password
      }
    }
    { 'X-XSRF-TOKEN' => response.headers['X-XSRF-TOKEN'] }
  end

  describe '#index' do
    let!(:people) { create_list(:person, 2) }

    before do
      get people_path, headers: headers

      @people = JSON.parse(response.body)
    end

    it 'should return the `OK` HTTP code' do
      expect(response).to have_http_status(:ok)
    end

    it 'should include all the people' do
      expect(@people.count).to eq(people.count)
      expect(@people.map { |p| p['id'] }).to match_array(people.map(&:id))
    end
  end

  describe '#show' do
    let(:person) { create(:person) }

    let(:a_composer) {}
    let(:an_arranger) {}
    let!(:a_score_with_composer) { create(:score, composed_by: a_composer&.id) }
    let!(:a_score_with_arranger) { create(:score, arranged_by: an_arranger&.id) }

    let(:a_conductor) { create(:person) }
    let!(:a_concert) { create(:concert, conductors: [a_conductor]) }

    before do
      get person_path(person.id), headers: headers

      @person = JSON.parse(response.body)
    end

    context 'without a conducted concert' do
      context 'without a composed score' do
        context 'without an arranged score' do
          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the person without any associated association' do
            expect(@person['concerts']).to match_array([])

            expect(@person['composed_scores']).to match_array([])
            expect(@person['arranged_scores']).to match_array([])
          end
        end

        context 'with one arranged score' do
          let(:an_arranger) { person }

          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the person with its conducted concert, composed score and arranged score' do
            expect(@person['concerts']).to match_array([])

            expect(@person['composed_scores']).to match_array([])
            expect(@person['arranged_scores']).to match_array(
              hash_including(
                'id' => a_score_with_arranger.id,
                'title' => a_score_with_arranger.title,
                'archive_ref' => a_score_with_arranger.archive_ref,
                'conductor_ref' => a_score_with_arranger.conductor_ref,
                'editor' => a_score_with_arranger.editor
              )
            )
          end
        end
      end

      context 'with one composed score' do
        let(:a_composer) { person }

        context 'without an arranged score' do
          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the person with its composed score only' do
            expect(@person['concerts']).to match_array([])

            expect(@person['composed_scores']).to match_array(
              hash_including(
                'id' => a_score_with_composer.id,
                'title' => a_score_with_composer.title,
                'archive_ref' => a_score_with_composer.archive_ref,
                'conductor_ref' => a_score_with_composer.conductor_ref,
                'editor' => a_score_with_composer.editor
              )
            )
            expect(@person['arranged_scores']).to match_array([])
          end
        end

        context 'with one arranged score' do
          let(:an_arranger) { person }

          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the person with its composed score and arranged score' do
            expect(@person['concerts']).to match_array([])

            expect(@person['composed_scores']).to match_array(
              hash_including(
                'id' => a_score_with_composer.id,
                'title' => a_score_with_composer.title,
                'archive_ref' => a_score_with_composer.archive_ref,
                'conductor_ref' => a_score_with_composer.conductor_ref,
                'editor' => a_score_with_composer.editor
              )
            )
            expect(@person['arranged_scores']).to match_array(
              hash_including(
                'id' => a_score_with_arranger.id,
                'title' => a_score_with_arranger.title,
                'archive_ref' => a_score_with_arranger.archive_ref,
                'conductor_ref' => a_score_with_arranger.conductor_ref,
                'editor' => a_score_with_arranger.editor
              )
            )
          end
        end
      end
    end

    context 'with one conducted concert' do
      let(:a_conductor) { person }

      context 'with one composed score' do
        let(:a_composer) { person }

        context 'with one arranged score' do
          let(:an_arranger) { person }

          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the person with its conducted concert, composed score and arranged score' do
            expect(@person['concerts']).to match_array(
              hash_including(
                'id' => a_concert.id,
                'name' => a_concert.name,
                'date' => a_concert.date,
                'place' => a_concert.place
              )
            )

            expect(@person['composed_scores']).to match_array(
              hash_including(
                'id' => a_score_with_composer.id,
                'title' => a_score_with_composer.title,
                'archive_ref' => a_score_with_composer.archive_ref,
                'conductor_ref' => a_score_with_composer.conductor_ref,
                'editor' => a_score_with_composer.editor
              )
            )
            expect(@person['arranged_scores']).to match_array(
              hash_including(
                'id' => a_score_with_arranger.id,
                'title' => a_score_with_arranger.title,
                'archive_ref' => a_score_with_arranger.archive_ref,
                'conductor_ref' => a_score_with_arranger.conductor_ref,
                'editor' => a_score_with_arranger.editor
              )
            )
          end
        end

        context 'without an arranged score' do
          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the person with its conducted concert and composed score' do
            expect(@person['concerts']).to match_array(
              hash_including(
                'id' => a_concert.id,
                'name' => a_concert.name,
                'date' => a_concert.date,
                'place' => a_concert.place
              )
            )

            expect(@person['composed_scores']).to match_array(
              hash_including(
                'id' => a_score_with_composer.id,
                'title' => a_score_with_composer.title,
                'archive_ref' => a_score_with_composer.archive_ref,
                'conductor_ref' => a_score_with_composer.conductor_ref,
                'editor' => a_score_with_composer.editor
              )
            )
            expect(@person['arranged_scores']).to match_array([])
          end
        end
      end

      context 'without a composed score' do
        context 'with one arranged score' do
          let(:an_arranger) { person }

          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the person with its conducted concert and arranged score' do
            expect(@person['concerts']).to match_array(
              hash_including(
                'id' => a_concert.id,
                'name' => a_concert.name,
                'date' => a_concert.date,
                'place' => a_concert.place
              )
            )

            expect(@person['composed_scores']).to match_array([])
            expect(@person['arranged_scores']).to match_array(
              hash_including(
                'id' => a_score_with_arranger.id,
                'title' => a_score_with_arranger.title,
                'archive_ref' => a_score_with_arranger.archive_ref,
                'conductor_ref' => a_score_with_arranger.conductor_ref,
                'editor' => a_score_with_arranger.editor
              )
            )
          end
        end

        context 'without one arranged score' do
          it 'should return the `OK` HTTP code' do
            expect(response).to have_http_status(:ok)
          end

          it 'should return the person with its conducted concert only' do
            expect(@person['concerts']).to match_array(
              hash_including(
                'id' => a_concert.id,
                'name' => a_concert.name,
                'date' => a_concert.date,
                'place' => a_concert.place
              )
            )

            expect(@person['composed_scores']).to match_array([])
            expect(@person['arranged_scores']).to match_array([])
          end
        end
      end
    end
  end

  describe '#create' do
    before do
      post people_path, headers: headers, params: params

      @person = JSON.parse(response.body)
    end

    context 'invalid parameters' do
      let(:params) { { person: { 'key' => 'value' } } }

      it 'should respond with the `unprocessable entity` HTTP code' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'valid parameters' do
      context 'without conductors' do
        let(:params) { { person: { 'last_name' => 'A Name' } } }

        it 'should respond with the `created` HTTP code' do
          expect(response).to have_http_status(:created)
        end

        it 'should return the new person with the attributes correctly set' do
          expect(@person['last_name']).to eq(params[:person]['last_name'])
        end
      end
    end
  end

  describe '#update' do
    let!(:person) { create(:person) }

    before do
      post people_path(person.id), headers: headers, params: params

      @person = JSON.parse(response.body)
    end

    context 'invalid parameters' do
      let(:params) { { person: { 'last_name' => '' } } }

      it 'should respond with the `unprocessable entity` HTTP code' do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should return the error message' do
        expect(@person['last_name']).to match_array('doit être rempli(e)')
      end
    end

    context 'valid parameters' do
      let(:new_person) { build(:person) }

      context 'without conductors' do
        let(:params) { { person: { 'last_name' => 'A Name' } } }

        it 'should respond with the `ok` HTTP code' do
          expect(response).to have_http_status(:created)
        end

        it 'should return the new person with the attributes correctly set' do
          expect(@person['last_name']).to eq(params[:person]['last_name'])
        end
      end
    end
  end

  describe '#search' do
    let!(:good_person) { create(:person, :minimal, last_name: 'Good Name') }
    let!(:bad_person) { create(:person, :minimal, last_name: 'Bad Name') }
    let!(:ugly_person) { create(:person, :minimal, last_name: 'Ugly Last') }

    before do
      post people_search_path, headers: headers, params: params

      @result = JSON.parse(response.body)
    end

    context 'same last_name' do
      let(:params) { { 'search' => { last_name: 'Name' } } }

      it 'should return the `OK` HTTP code' do
        expect(response).to have_http_status(:ok)
      end

      it 'should return only the accurate people' do
        expect(@result.count).to eq(2)
        expect(@result.first['last_name']).to eq(good_person.last_name)
        expect(@result.last['last_name']).to eq(bad_person.last_name)
      end
    end

    context 'random last_name' do
      let(:params) { { 'search' => { last_name: 'Random' } } }

      it 'should return absolutely nothing' do
        expect(@result.count).to be_zero
      end
    end
  end
end
