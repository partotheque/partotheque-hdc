# frozen_string_literal: true

require 'roo'

PERSON_ATTRIBUTES = {
  first_name: 'Prénom',
  last_name: 'Nom',
  birth_country: 'Pays de naissance',
  birth_date: 'Naissance',
  death_date: 'Mort',
  biography: /Commentaire/
}.freeze

SCORE_ATTRIBUTES = {
  comment: 'Commentaire',
  acquisition_date: /HdC/,
  classification: 'Classification',
  number: 'n°',
  conductor_ref: 'réf conducteur',
  archive_ref: 'Ref Archive',
  audio_support: 'Support audio',
  editor: 'Éditeur',
  title: 'Titre',
  duration: 'Durée',
  difficulty: 'Difficulté',
  writing_date: 'Date d’écriture',
  editing_date: 'date',
  score_type: 'Type Partition',
  conductor_type: 'Type Conducteur',
  author_first_name: 'Prénom',
  author_last_name: 'Nom',
  arr_first_name: 'prénom',
  arr_last_name: 'nom',
  style: 'Style',
  substyle: 'Sous Style'
}.freeze

def create_persons
  puts 'Création des personnes'
  pseudos = []
  import_persons.each do |person|
    pseudos << "#{person[:first_name]} #{person[:last_name]}" if /pseudo/i.match?(person[:biography])
    Person.create!(person)
  end
  puts "Se méfier de ces noms : #{pseudos}"
end

def create_scores
  puts 'Création des partitions'
  import_scores.each do |score|
    if score[:duration]
      duration = score[:duration].to_s.sub("'", '’').split('’').map(&:to_i)
      score[:duration] = duration.first * 60 + duration.last
    end

    score[:composed_by] = create_person_if_nonexistent(score[:author_first_name], score[:author_last_name])&.id
    score[:arranged_by] = create_person_if_nonexistent(score[:arr_first_name], score[:arr_last_name])&.id

    Score.create!(
      # suppression des champs inutiles pour créer la partition
      score.except(
        :author_first_name,
        :author_last_name,
        :arr_first_name,
        :arr_last_name
      )
    )
  end
end

def create_concerts
  puts 'Création des concerts'
  import_concerts.each do |concert|
    next unless concert[:name]

    concert[:conductors].map! do |conductor|
      name = conductor.split(' ')
      first_name = name.length > 1 ? name.first : nil
      last_name = name.last
      create_person_if_nonexistent(first_name, last_name)
    end

    concert_db = Concert.create!(
      organizer: concert[:organizer],
      place: concert[:place],
      participant: concert[:participant],
      date: concert[:date],
      name: concert[:name]
    )

    concert_db.conductors << concert[:conductors]

    concert[:scores].each do |score|
      Score.find_by(
        archive_ref: score[:archive_ref],
        title: score[:title]
      ).concerts << concert_db
    end
  end
end

def create_person_if_nonexistent(first_name, last_name)
  first_name = nullify_name(first_name)
  last_name = nullify_name(last_name)
  person_db = find_person(first_name, last_name).first

  if first_name.nil? && last_name.nil?
    nil
  elsif person_db
    person_db
  else
    Person.create!(first_name: first_name, last_name: last_name)
  end
end

def find_person(first_name, last_name)
  if first_name.nil?
    sql = generate_sql('IS NULL')
    Person.find_by_sql([sql, last_name.try(:downcase)])
  else
    sql = generate_sql('= ?')
    Person.find_by_sql([sql, first_name.try(:downcase), last_name.try(:downcase)])
  end
end

def generate_sql(variation)
  "SELECT id FROM people WHERE lower(first_name) #{variation} AND lower(last_name) = ?"
end

def nullify_name(name)
  name = nil if %w[/ ?].include?(name)
  name
end

def import_persons
  import_data_from_ods_file('Acteurs', PERSON_ATTRIBUTES)
end

def import_scores
  import_data_from_ods_file('Données', SCORE_ATTRIBUTES)
end

def import_concerts
  puts "\n#{'-' * 10}Import des concerts#{'-' * 10}\n"
  concerts_columns = ['Y', 'Z', ('AA'..partotheque_file.last_column_as_letter).to_a].flatten

  puts 'Import de la colonne F'
  archive_ref_column = partotheque_file.column('F')
  puts 'Import de la colonne P'
  title_column = partotheque_file.column('P')

  concerts_columns.map.with_index do |column, index|
    print "Import colonne #{column} (#{(index.to_f / concerts_columns.count * 100).floor(1)} %)"
    print 13.chr

    array_column = partotheque_file.column(column)

    array_column_without_nils = {}
    array_column.each_index { |i| array_column_without_nils[i] = array_column[i] if array_column[i] }

    played_scores_indexes = array_column_without_nils.select { |key| key > 5 }.keys

    {
      organizer: array_column_without_nils[0],
      place: array_column_without_nils[1],
      conductors: array_column_without_nils[2].nil? ? [] : array_column_without_nils[2].split("\n"),
      participant: array_column_without_nils[3],
      date: array_column_without_nils[4].to_s,
      name: array_column_without_nils[5],
      scores: scores_from_concert_column(played_scores_indexes, archive_ref_column, title_column)
    }
  end
end

def scores_from_concert_column(played_scores_indexes, archive_ref_column, title_column)
  played_scores_indexes.map do |i|
    {
      archive_ref: archive_ref_column[i],
      title: title_column[i]
    }
  end
end

def import_data_from_ods_file(sheet, data)
  partotheque_file.sheet(sheet).parse(data)
end

def partotheque_file
  Roo::Spreadsheet.open("#{Rails.root.join}/lib/assets/light_partotheque.ods")
end

create_persons
create_scores
create_concerts

# pseudos = ["Rob ARES", "Peter Van ASTEN", "John DARLING", "Laurent DELBECQ", "Luigi di GHISALLO", "Jacob de HAAN",
# "Ted HUGGENS", "Roland KERNEN", "Henk Van LIJNSCHOOTEN", "Dick RAVENAL", "Dizzy STRATFORD", "Cornelis Kees VLAK",
# "Andre WAIGNEIN"]
