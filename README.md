# Instructions d'installation

## Obtenir la bonne version de Ruby (`.ruby-version`)

- rbenv (https://github.com/rbenv/rbenv#basic-github-checkout)
  ``` sh
  git clone https://github.com/rbenv/rbenv.git ~/.rbenv
  cd ~/.rbenv && src/configure && make -C src
  echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.zshrc # si ZSH
  ~/.rbenv/bin/rbenv init
  ```
  
- ruby-build (https://github.com/rbenv/ruby-build#installation)
  ``` sh
  mkdir -p "$(rbenv root)"/plugins
  git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build
  ```

- Vérifier que tout s'est bien passé, et faire les derniers ajustements si besoin :
  ``` sh
  curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-doctor | bash
  ```

## Installer le projet et ses dépendances

### PostgreSQL

La "seule" dépendance au projet c'est PostgreSQL. Son installation sur Manjaro s'effectue comme ceci :
``` sh
sudo pacman -S postgresql
```

Mais j'ai eu quelques soucis au démarrage du service... Ceci a réglé le problème :
``` sh
sudo -iu postgres
initdb  -D '/var/lib/postgres/data' # un dossier qu'il faut peut-être créer et attribuer à l'utilisateur 'postgres'
```

Puis il faut créer le bon utilisateur :
```
psql
CREATE USER xxx WITH PASSWORD 'xxx-xxx' CREATEDB; # USER doit avoir le même nom que celui de la session
```

Enfin :
```
exit
sudo systemctl start postgresql
```

### Le projet en lui-même

Une fois fini :

``` sh
git clone git@gitlab.com:rduthel/partotheque-hdc.git
gem install bundler
bundle
```

## Lancer la partothèque

D'abord peupler la base de données :
```sh
rails db <<< $(cat lib/assets/dump-db-dev-pgadmin--data-plain.sql)
```

Et maintenant... on peut tout faire comme d'habitude avec les commandes `rails` :wink: