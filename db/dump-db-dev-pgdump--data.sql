--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5
-- Dumped by pg_dump version 12.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: romain
--

COPY public.ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2020-10-02 17:42:13.328945	2020-10-02 17:42:13.328945
\.


--
-- Data for Name: concerts; Type: TABLE DATA; Schema: public; Owner: romain
--

COPY public.concerts (id, organizer, place, participant, date, name, created_at, updated_at, conductors) FROM stdin;
1	Comité des fêtes	Centre Culturel Le Sou	CAP Musique, et école de musique de Saint Priest en Jarez	2020-01-12	2020-Nouvel An (dimanche)	2020-10-02 14:14:09.574945	2020-10-02 14:14:09.574945	{456,663}
2	Comité des fêtes	Centre Culturel Le Sou	CAP Musique, et école de musique de Saint Priest en Jarez	2020-01-11	2020-Nouvel An (samedi)	2020-10-02 14:14:09.578783	2020-10-02 14:14:09.578783	{456,663}
3	Harmonie de la Chazotte	Eglise de La Talaudière	Animation de la messe	2019-12-07	2019-Ste Cécile	2020-10-02 14:14:09.581115	2020-10-02 14:14:09.581115	{456}
4	Anciens mieurs Sorbiers, La Talaudière, Saint Jean Bonnefonds	Pole Festif, Saint Jean Bonnefonds	\N	2019-12-04	2019-Ste Barbe	2020-10-02 14:14:09.583458	2020-10-02 14:14:09.583458	{456}
5	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2019-11-11	2019-11/11	2020-10-02 14:14:09.58578	2020-10-02 14:14:09.58578	{456}
6	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2019-07-14	2019-14/07	2020-10-02 14:14:09.587997	2020-10-02 14:14:09.587997	{456}
7	Harmonie de Saint Priest en Jarez	Villa St Michel, Saint Priest en jarez	Harmonies de Saint Priest en Jarez, Cotechaude, Terrenoire, St Bonnet les Oules	2019-06-23	2019-25 ans Harmonie de Saint Priest en Jarez	2020-10-02 14:14:09.590707	2020-10-02 14:14:09.590707	{456,663}
8	Mairie de La Talaudière	Stade	Odyssée, CAP Musique, Collège, choral A tout choeur	2019-06-22	2019-Peplum	2020-10-02 14:14:09.594356	2020-10-02 14:14:09.594356	{664}
9	Harmonie Municipale d' »Aix en Provence	Aix en Provence, Gymnase	HMAP	2019-06-08	2019-Invitation Aix en Provence	2020-10-02 14:14:09.596629	2020-10-02 14:14:09.596629	{456}
10	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2019-05-08	2019-08/05	2020-10-02 14:14:09.598844	2020-10-02 14:14:09.598844	{456}
11	Harmonie de la Chazotte	L'escale, L'Horme	Ass'hum, Kosalisana, Harmonie de Eybens, dir Cédric Rossero	2019-05-05	2019-Concert Solidaire	2020-10-02 14:14:09.601096	2020-10-02 14:14:09.601096	{456}
12	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2019-03-19	2019-FNACA	2020-10-02 14:14:09.603666	2020-10-02 14:14:09.603666	{456}
13	Comité des fêtes	Centre Culturel Le Sou	CAP Musique	2019-01-13	2019-Nouvel An	2020-10-02 14:14:09.607053	2020-10-02 14:14:09.607053	{456,312,663}
14	INSA de Lyon	La Rotonde (Villeurbanne)	Harmonie de l’INSA (François Rousselot)	2019-01-12	2019-Concert d’Hiver	2020-10-02 14:14:09.610284	2020-10-02 14:14:09.610284	{456,312,663}
15	Harmonie de la Chazotte	Eglise de La Talaudière	\N	2018-12-01	2018-Ste Cécile	2020-10-02 14:14:09.613373	2020-10-02 14:14:09.613373	{456,312,663}
16	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2018-11-11	2018-11/11	2020-10-02 14:14:09.615667	2020-10-02 14:14:09.615667	{456}
17	Mairie de La Talaudière	Salle omnisport de la La Talaudière	CAP Musique, Etendart, La Troupe	2018-09-15	2018-Fête du Sport	2020-10-02 14:14:09.636671	2020-10-02 14:14:09.636671	{456,312}
18	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2018-07-14	2018-14/07	2020-10-02 14:14:09.639355	2020-10-02 14:14:09.639355	{456}
19	Mairie de La Talaudière	Parc de la Talaudière	\N	2018-06-21	2018-Fête de la Musique	2020-10-02 14:14:09.644737	2020-10-02 14:14:09.644737	{312}
20	Jumelage Mali La talaudière	Centre Culturel Le Sou	Harmonie d’Aix en Provence	2018-06-09	2018-Concert Solidaire Mali	2020-10-02 14:14:09.647764	2020-10-02 14:14:09.647764	{456,312}
21	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2018-05-08	2018-08/05	2020-10-02 14:14:09.650342	2020-10-02 14:14:09.650342	{456}
22	Harmonie de la Chazotte	La Forge (Chambon Feugerolle)	\N	2018-04-29	2018-Du Metal en Harmonie 2	2020-10-02 14:14:09.65563	2020-10-02 14:14:09.65563	{456}
23	Harmonie de la Chazotte	La Forge (Chambon Feugerolle)	\N	2018-04-28	2018-Du Metal en Harmonie 1	2020-10-02 14:14:09.658176	2020-10-02 14:14:09.658176	{456}
24	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2018-03-19	2018-FNACA	2020-10-02 14:14:09.660543	2020-10-02 14:14:09.660543	{408}
25	Comité des fêtes	Centre Culturel Le Sou	CAP Musique	2018-01-07	2018-Nouvel An	2020-10-02 14:14:09.667975	2020-10-02 14:14:09.667975	{456,312}
26	Harmonie de la Chazotte	Eglise de la Talaudière	\N	2017-12-02	2017-Ste Cécile	2020-10-02 14:14:09.6711	2020-10-02 14:14:09.6711	{456,312}
27	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2017-11-11	2017-11/11	2020-10-02 14:14:09.673668	2020-10-02 14:14:09.673668	{456}
28	Mairie de Sorbiers	Salle de l’Echappée	CAP Musique	2017-11-10	2017-Dix ans de l’Echappée	2020-10-02 14:14:09.680285	2020-10-02 14:14:09.680285	{456,312}
29	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2017-07-14	2017-14/07	2020-10-02 14:14:09.683645	2020-10-02 14:14:09.683645	{456}
30	Harmonie de Heyrieux (38)	\N	\N	2017-06-24	2017-Concert Heyrieux	2020-10-02 14:14:09.690515	2020-10-02 14:14:09.690515	{456,312}
31	Harmonie de la Chazotte	\N	\N	2017-06-21	2017-Fete de la musique	2020-10-02 14:14:09.693712	2020-10-02 14:14:09.693712	{456,312}
32	Harmonie de la Chazotte	Centre Culturel Le Sou	Participation de jeunes enfants	2017-06-10	2017-Gala	2020-10-02 14:14:09.69752	2020-10-02 14:14:09.69752	{456,312}
33	Federation Musicale de la Loire	Palais des spectacles de St Etienne	Harmonies stéphanoises	2017-05-19	2017-Nuit des Harmonies	2020-10-02 14:14:09.700795	2020-10-02 14:14:09.700795	{456,312}
34	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2017-05-08	2017-08/05	2020-10-02 14:14:09.703411	2020-10-02 14:14:09.703411	{456}
35	Journée solidaire Paroisse 	Eglise de la Talaudière	Brass Band Loire Forez	2017-03-26	2017-Concert solidaire	2020-10-02 14:14:09.709325	2020-10-02 14:14:09.709325	{456,312}
36	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2017-03-19	2017-Fnaca	2020-10-02 14:14:09.711972	2020-10-02 14:14:09.711972	{456}
37	Comité des fêtes	Centre Culturel Le Sou	EMAD Berlioz	2017-01-08	2017-Nll an	2020-10-02 14:14:09.717802	2020-10-02 14:14:09.717802	{456,312}
38	Crozet sur Gand	Boulodrome	\N	2016-12-17	2016-Crozet sur Gand	2020-10-02 14:14:09.720934	2020-10-02 14:14:09.720934	{456,312}
39	Harmonie de la Chazotte	Eglise de la Talaudière	\N	2016-12-03	2016-Ste Cécile	2020-10-02 14:14:09.73644	2020-10-02 14:14:09.73644	{456,312}
40	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2016-11-11	2016-11/11	2020-10-02 14:14:09.749467	2020-10-02 14:14:09.749467	{456}
41	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2016-07-14	2016-14/07	2020-10-02 14:14:09.760477	2020-10-02 14:14:09.760477	{456}
42	Harmonie de la Chazotte	Pole Festif	EMAD, Chorale A tout choeur, Les Olives, Harmonie de St Priest en Jarez	2016-06-21	2016-Fete de la Musique	2020-10-02 14:14:09.769439	2020-10-02 14:14:09.769439	{312}
43	Harmonie de la Chazotte	Centre Culturel Le Sou	Animation Jack Reevax (Xavier)	2016-06-12	2016-Gala Musique de Pub	2020-10-02 14:14:09.785518	2020-10-02 14:14:09.785518	{456,312}
44	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2016-05-29	2016-Commémoration Verdun	2020-10-02 14:14:09.814727	2020-10-02 14:14:09.814727	{408}
45	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2016-05-08	2016-08/05	2020-10-02 14:14:09.823416	2020-10-02 14:14:09.823416	{456}
46	Harmonie de la Chazotte	Salle de l'Echappée, Sorbiers	Harmonie de l'INSA (Direction Medhi Lougraida, Association Manef Yam	2016-04-29	2016-Concert d'Harmonie	2020-10-02 14:14:09.830503	2020-10-02 14:14:09.830503	{456,312}
47	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2016-03-19	2016-Fnaca	2020-10-02 14:14:09.843243	2020-10-02 14:14:09.843243	{408}
48	Comité des fêtes	Centre Culturel Le Sou	\N	2016-01-10	2016-Nll an	2020-10-02 14:14:09.85019	2020-10-02 14:14:09.85019	{456,312}
49	Harmonie de la Chazotte	Eglise de la Talaudière	\N	2015-12-05	2015-Ste Cécile	2020-10-02 14:14:09.934562	2020-10-02 14:14:09.934562	{456,312}
50	Saint Priest en Jarez	Salle du NEC	8 autres Harmonies ligériennes (St Priest en Jarez, Terrenoire, St Jean Bonnefonds, St Etienne,...	2015-11-15	2015-Zic en Loire	2020-10-02 14:14:09.953737	2020-10-02 14:14:09.953737	{456,312}
51	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2015-11-11	2015-11/11	2020-10-02 14:14:09.969428	2020-10-02 14:14:09.969428	{456}
52	Mairie de La Talaudière	Monument aux morts	Ancien combattant	2015-07-14	2015-14/07	2020-10-02 14:14:09.979963	2020-10-02 14:14:09.979963	{456}
53	Conservatoire de St Etienne	Palais des spectacles de St Etienne	12 Harmonies stéphanoises plus deux invités	2015-06-19	2015-Nuit des Harmonies	2020-10-02 14:14:09.987814	2020-10-02 14:14:09.987814	{456}
54	Harmonie de la Chazotte	Salle de l'Echappée, Sorbiers	\N	2015-06-07	2015-Gala des 150 ans (dimanche)	2020-10-02 14:14:10.010813	2020-10-02 14:14:10.010813	{456,312,408}
55	Harmonie de la Chazotte	Salle de l'Echappée, Sorbiers	\N	2015-06-06	2015-Gala des 150 ans (samdi)	2020-10-02 14:14:10.057164	2020-10-02 14:14:10.057164	{456,312,408}
56	Mairie de La Talaudière	Monument aux morts	\N	2015-05-08	2015-08/05	2020-10-02 14:14:10.093748	2020-10-02 14:14:10.093748	{456,312}
57	Roche la Molière	\N	\N	2015-04-11	2015-Minenfet	2020-10-02 14:14:10.100976	2020-10-02 14:14:10.100976	{456,312}
58	Harmonie de la Chazotte	Salle omnisport de La Talaudière	Harmonies de la Ricamarie, de Roche la Moliere (HBL)	2015-03-29	2015-Les Houillères	2020-10-02 14:14:10.115382	2020-10-02 14:14:10.115382	{456,312}
59	Mairie de La Talaudière	Monument aux morts	\N	2015-03-19	2015-Fnaca	2020-10-02 14:14:10.129767	2020-10-02 14:14:10.129767	{456}
60	Comité des fêtes	Centre Culturel Le Sou	Les professeurs de l'école de musique de la Talaudière actuels et passés	2015-01-11	2015-Nll AN	2020-10-02 14:14:10.135331	2020-10-02 14:14:10.135331	{456,312}
61	Harmonie de la Chazotte	Eglise de La Talaudière	Chorale Sinfonia, dame de choeur, du Montois et du Provinois	2014-12-07	2014-Ste Cécile	2020-10-02 14:14:10.150617	2020-10-02 14:14:10.150617	{456,312}
62	Saison Culturelle du Sou	Centre Culturel Le Sou	avec Jean Pierre Bodin	2014-11-15	2014-Banquet de Ste Cécile	2020-10-02 14:14:10.165482	2020-10-02 14:14:10.165482	{456,312}
63	Mairie de La Talaudière	Monument aux morts	\N	2014-11-11	2014-11/11	2020-10-02 14:14:10.174381	2020-10-02 14:14:10.174381	{456}
64	Conservatoire de St Etienne	Musée de la mine	l’harmonie junior du Conservatoire, de la Chazotte de la Ricamarie, de Saint\n\nEtienne, de Côte Chaude, de Terrenoire et de l’OHRC	2014-07-05	2014-Concert des Harmonies	2020-10-02 14:14:10.178548	2020-10-02 14:14:10.178548	{312}
65	Mairie de La Talaudière	Monument aux morts	\N	2014-07-14	2014-14/07	2020-10-02 14:14:10.184838	2020-10-02 14:14:10.184838	{312}
66	Harmonie de la Chazotte	Pole Festif	L’orchestre de l’EMAD\nLa batterie-Fanfare de Sorbiers\nA tout choeur\nHarmonie de Veauche		2014-Fête Musique	2020-10-02 14:14:10.191734	2020-10-02 14:14:10.191734	{456,312}
67	Harmonie de La Chazotte	Centre Culturel Le Sou	Jack Revax	2014-05-17	2014-Gala : Top 10	2020-10-02 14:14:10.202845	2020-10-02 14:14:10.202845	{456,312}
68	Mairie de La Talaudière	Monument aux morts	\N		2014-08-05	2020-10-02 14:14:10.225484	2020-10-02 14:14:10.225484	{456}
69	Mairie de La Talaudière	Monument aux morts	Anciens Combattants	2014-03-19	2014-Fnaca	2020-10-02 14:14:10.232553	2020-10-02 14:14:10.232553	{408}
70	Comité des fêtes	Centre Culturel Le Sou	avec les Classes d’orchestres, l’Harmonie de St Symphorien d’Ozon	2014-01-12	2014-Nouvel An	2020-10-02 14:14:10.239916	2020-10-02 14:14:10.239916	{312,456}
71	\N	Eglise de la Talaudière	les mineurs de La Talaudière	2013-11-30	2013-St Cécile	2020-10-02 14:14:10.257545	2020-10-02 14:14:10.257545	{456,312}
72	Mairie de La Talaudière	Monument aux morts	\N		2013-11/11	2020-10-02 14:14:10.268033	2020-10-02 14:14:10.268033	{}
73	\N	Kiosk de Grand Croix	\N	2013-10-06	2013-Danses	2020-10-02 14:14:10.277604	2020-10-02 14:14:10.277604	{456,312}
74	Mairie de La Talaudière	Monument aux morts	\N		2013-14/07’	2020-10-02 14:14:10.300743	2020-10-02 14:14:10.300743	{}
75	Etendart de la Talaudière	Eglise et Pôle festif	\N	2013-06-30	2013-100 ans Etendart	2020-10-02 14:14:10.305836	2020-10-02 14:14:10.305836	{456,312}
76	\N	\N	\N	2013-06-21	2013-Fête Musique	2020-10-02 14:14:10.327653	2020-10-02 14:14:10.327653	{408,456,312}
77	\N	Centre Culturel Le Sou	Animation de Jack Revax	2013-06-01	2013-Gala Cinema	2020-10-02 14:14:10.343312	2020-10-02 14:14:10.343312	{408,456,312}
78	\N	Pole Festif	CAP dance	2013-05-17	2013-Soirée Cabaret	2020-10-02 14:14:10.373432	2020-10-02 14:14:10.373432	{408,312}
79	Mairie de La Talaudière	Monument aux morts	\N		2013-08/05	2020-10-02 14:14:10.378252	2020-10-02 14:14:10.378252	{}
80	\N	à St Just St Rambert	Harmonie de St Just St Rambert	2013-03-30	2013-St Just St Rambert : Soirée Cabaret	2020-10-02 14:14:10.38432	2020-10-02 14:14:10.38432	{456,408,312}
81	Mairie de La Talaudière	Monument aux morts	\N		2013-Fnaca	2020-10-02 14:14:10.413834	2020-10-02 14:14:10.413834	{}
82	\N	Centre Culturel Le Sou	Classes d’orchestre de Marie-Christine Larue : morceau commun Moulinet-Polka	2013-01-13	2013-Nouvel An	2020-10-02 14:14:10.417935	2020-10-02 14:14:10.417935	{408,456}
83	\N	Eglise de La Talaudière	les anciens mineurs	2012-12-08	2012-Saint Cécile	2020-10-02 14:14:10.434326	2020-10-02 14:14:10.434326	{408,456}
84	Mairie de La Talaudière	Monument aux morts	\N		2012-11/11	2020-10-02 14:14:10.444622	2020-10-02 14:14:10.444622	{}
85	Mairie de La Talaudière	Monument aux morts	\N		2012-14/07	2020-10-02 14:14:10.45154	2020-10-02 14:14:10.45154	{}
86	\N	Pole Festif	\N		2012-Fête Musique	2020-10-02 14:14:10.454825	2020-10-02 14:14:10.454825	{}
87	\N	Centre Culturel Le Sou	Duo de Violoncelle (Monique Virat et Daniel Avédissian), Eucalyptus 42	2012-06-12	2012-Concert Espagnol	2020-10-02 14:14:10.459206	2020-10-02 14:14:10.459206	{218,408,456}
88	Mairie de La Talaudière	Monument aux morts	\N		2012-08/05	2020-10-02 14:14:10.474942	2020-10-02 14:14:10.474942	{}
89	\N	Centre Culturel Le Sou	Musique de Jean Philippe Vanbeselaere,\nmise en scène Bruno Fontaine	2012-04-01	2012-Kiosque	2020-10-02 14:14:10.479942	2020-10-02 14:14:10.479942	{}
90	Mairie de La Talaudière	Monument aux morts	\N		2012-Fnaca	2020-10-02 14:14:10.481353	2020-10-02 14:14:10.481353	{}
91	\N	Centre Culturel Le Sou	orchestre de l’EMAD\nLa chorale adulte EMAD de Neringa Lukoseviscius	2012-01-08	2012-Nouvel An	2020-10-02 14:14:10.485401	2020-10-02 14:14:10.485401	{239,408}
92	\N	Eglise de la Talaudière	Concerto pour clarinette joué par André Guillaume	2011-12-10	2011-Ste Cécile	2020-10-02 14:14:10.49818	2020-10-02 14:14:10.49818	{239,408}
93	\N	\N	\N		2011-St Symphorien Ozon	2020-10-02 14:14:10.506498	2020-10-02 14:14:10.506498	{}
94	Mairie de La Talaudière	Monument aux morts	\N		2011-11/11	2020-10-02 14:14:10.507747	2020-10-02 14:14:10.507747	{}
95	Mairie de La Talaudière	Monument aux morts	\N		2011-14/07	2020-10-02 14:14:10.516884	2020-10-02 14:14:10.516884	{}
96	\N	à Reinheim, fête des fleurs	\N	2011-07-03	2011-Küssaberg	2020-10-02 14:14:10.524772	2020-10-02 14:14:10.524772	{239,408}
97	\N	\N	\N		2011-Fête Musique	2020-10-02 14:14:10.555697	2020-10-02 14:14:10.555697	{}
98	\N	Centre Culturel Le Sou	Animation de Jack Revax	2011-05-28	2011-Gala : voyage	2020-10-02 14:14:10.558638	2020-10-02 14:14:10.558638	{239,408,456}
99	\N	\N	\N		2011-Montbrison	2020-10-02 14:14:10.581747	2020-10-02 14:14:10.581747	{}
100	Mairie de La Talaudière	Monument aux morts	\N		2011-08/05	2020-10-02 14:14:10.583134	2020-10-02 14:14:10.583134	{}
101	\N	\N	\N		2011-Tournus	2020-10-02 14:14:10.590422	2020-10-02 14:14:10.590422	{}
102	Mairie de La Talaudière	Monument aux morts	\N		2011-Fnaca	2020-10-02 14:14:10.591908	2020-10-02 14:14:10.591908	{}
103	Club Musique de l’ENTPE	Vaulx en Velin, amphi de l’ENTPE	orchestre de l’ENTPE	2011-01-19	2011-Vaulx en velin	2020-10-02 14:14:10.596534	2020-10-02 14:14:10.596534	{239,408,456}
104	\N	Centre Culturel Le Sou	avec l’orchestre de l’ENTPE, de l’EMAD, morceaux commun Le Seigneur des Anneaux	2011-01-09	2011-Nouvel An	2020-10-02 14:14:10.610695	2020-10-02 14:14:10.610695	{239,408,456}
105	\N	Eglise de la Talaudière	\N	2010-11-27	2010-Ste Cécile	2020-10-02 14:14:10.621707	2020-10-02 14:14:10.621707	{}
106	Mairie de La Talaudière	Monument aux morts	\N		2010-11/11	2020-10-02 14:14:10.632339	2020-10-02 14:14:10.632339	{}
107	\N	Grand Croix	en mineur	2010-10-03	2010-Grand Croix	2020-10-02 14:14:10.635672	2020-10-02 14:14:10.635672	{}
108	\N	\N	\N		2010-Festival Bissieux	2020-10-02 14:14:10.637067	2020-10-02 14:14:10.637067	{}
109	Mairie de La Talaudière	Monument aux morts	\N		2010-14/07	2020-10-02 14:14:10.638439	2020-10-02 14:14:10.638439	{}
110	\N	\N	\N		2010-Appel 18/06	2020-10-02 14:14:10.641807	2020-10-02 14:14:10.641807	{}
111	\N	Monument aux mort	\N	2010-06-19	2010-Fête Musique	2020-10-02 14:14:10.643139	2020-10-02 14:14:10.643139	{}
112	\N	Monument aux mort	\N	2010-06-18	2010-70 ans de l’Appel	2020-10-02 14:14:10.64456	2020-10-02 14:14:10.64456	{}
113	Harmonie de la Chazotte	Centre Culturel Le Sou	Mohamed Baouzzi (conteur)	2010-05-29	2010-Gala : Afrique	2020-10-02 14:14:10.647177	2020-10-02 14:14:10.647177	{239,408,456}
114	\N	St Victor	\N	2010-05-21	2010-St Victor	2020-10-02 14:14:10.666767	2020-10-02 14:14:10.666767	{}
115	Mairie de La Talaudière	Monument aux morts	\N		2010-08/05	2020-10-02 14:14:10.668037	2020-10-02 14:14:10.668037	{}
116	comité de jumelage Mali	Centre Culturel Le Sou	école de musique	2010-04-10	2010 : Concert Somadougou	2020-10-02 14:14:10.672071	2020-10-02 14:14:10.672071	{239,408}
117	Mairie de La Talaudière	Monument aux morts	\N		2010-Fnaca	2020-10-02 14:14:10.686055	2020-10-02 14:14:10.686055	{}
118	\N	\N	\N	annulé ?	2010-Nouvel An	2020-10-02 14:14:10.689375	2020-10-02 14:14:10.689375	{}
119	\N	\N	\N	2009-12-05	2009-Ste Cécile	2020-10-02 14:14:10.692495	2020-10-02 14:14:10.692495	{}
120	\N	Bissieux	\N	2009-11-29	2009-Bissieux	2020-10-02 14:14:10.701055	2020-10-02 14:14:10.701055	{}
121	Mairie de La Talaudière	Monument aux morts	\N		2009-11/11	2020-10-02 14:14:10.702452	2020-10-02 14:14:10.702452	{}
122	Mairie de La Talaudière	Monument aux morts	\N		2009-14/07	2020-10-02 14:14:10.709393	2020-10-02 14:14:10.709393	{}
123	\N	\N	\N		2009-Fête Musique	2020-10-02 14:14:10.712678	2020-10-02 14:14:10.712678	{}
124	\N	\N	\N		2009-Fête Associations	2020-10-02 14:14:10.714084	2020-10-02 14:14:10.714084	{}
125	Harmonie de la Chazotte	Centre Culturel le Sou	Country Girls de Terrenoire\nEternity Street\nDanseuses	2009-05-19	2009-Gala : Amérique	2020-10-02 14:14:10.716236	2020-10-02 14:14:10.716236	{239,408}
126	\N	\N	\N		2009-Aurec	2020-10-02 14:14:10.7376	2020-10-02 14:14:10.7376	{}
127	Mairie de La Talaudière	Monument aux morts	\N		2009-08/05	2020-10-02 14:14:10.739006	2020-10-02 14:14:10.739006	{}
128	\N	\N	\N		2009-Carnaval	2020-10-02 14:14:10.74418	2020-10-02 14:14:10.74418	{}
129	Mairie de La Talaudière	Monument aux morts	\N		2009-Fnaca	2020-10-02 14:14:10.74556	2020-10-02 14:14:10.74556	{}
130	\N	\N	\N		2009-Fête de l'Arc en ciel	2020-10-02 14:14:10.748855	2020-10-02 14:14:10.748855	{}
131	Comité des fêtes de La Talaudière	\N	Orchestre de l’EMAD Berlioz, morceau commun le beau Danube Bleu	2009-01-11	2009-Nouvel An	2020-10-02 14:14:10.751004	2020-10-02 14:14:10.751004	{239,408}
132	\N	Eglise de la Talaudière	\N		2008-Ste Cécile	2020-10-02 14:14:10.767708	2020-10-02 14:14:10.767708	{239,408}
133	Mairie de La Talaudière	Monument aux morts	\N		2008-11/11	2020-10-02 14:14:10.783511	2020-10-02 14:14:10.783511	{}
134	Mairie de La Talaudière	Monument aux morts	\N		2008-14/07	2020-10-02 14:14:10.788655	2020-10-02 14:14:10.788655	{}
135	\N	\N	\N		2008-Fête Musique	2020-10-02 14:14:10.791808	2020-10-02 14:14:10.791808	{}
136	\N	\N	\N		2008-Fête Jumelage	2020-10-02 14:14:10.793165	2020-10-02 14:14:10.793165	{}
137	Harmonie de la Chazotte	Centre culturel Le Sou	le magicien Jack Revax	2008-05-17	2008-Gala : magie	2020-10-02 14:14:10.795317	2020-10-02 14:14:10.795317	{239,408}
138	Mairie de La Talaudière	Monument aux morts	\N		2008-08/05	2020-10-02 14:14:10.809499	2020-10-02 14:14:10.809499	{}
139	\N	\N	\N		2008-Invitation de Tournus	2020-10-02 14:14:10.816499	2020-10-02 14:14:10.816499	{}
140	Mairie de La Talaudière	Monument aux morts	\N		2008-Fnaca	2020-10-02 14:14:10.819809	2020-10-02 14:14:10.819809	{}
141	Comité des fêtes	Centre culturel le Sou	EMAD Berlioz, orchestre symphonique\nQuatuor CUBATAO	2008-01-13	2008-Nouvel An	2020-10-02 14:14:10.823932	2020-10-02 14:14:10.823932	{239,408}
142	\N	\N	\N		2007-Ste Cécile	2020-10-02 14:14:10.838366	2020-10-02 14:14:10.838366	{}
143	\N	\N	\N		2007-Ste Barbe	2020-10-02 14:14:10.854506	2020-10-02 14:14:10.854506	{}
144	Mairie de La Talaudière	Monument aux morts	\N		2007-11/11	2020-10-02 14:14:10.855952	2020-10-02 14:14:10.855952	{}
145	Mairie de La Talaudière	Monument aux morts	\N		2007-14/07	2020-10-02 14:14:10.863021	2020-10-02 14:14:10.863021	{}
146	\N	\N	\N		2007-St Priest	2020-10-02 14:14:10.866385	2020-10-02 14:14:10.866385	{}
147	\N	\N	\N		2007-Fête Musique	2020-10-02 14:14:10.878569	2020-10-02 14:14:10.878569	{}
148	Harmonie de La Chazotte	Centre culturel le Sou	la Kapela du groupe SYRENA	2007-05	2007-Gala : Musique de l’Est	2020-10-02 14:14:10.880635	2020-10-02 14:14:10.880635	{239,408}
149	\N	\N	\N		2007-150 Ans Roche la Molière	2020-10-02 14:14:10.897336	2020-10-02 14:14:10.897336	{}
150	Mairie de La Talaudière	Monument aux morts	\N		2007-08/05	2020-10-02 14:14:10.898939	2020-10-02 14:14:10.898939	{}
151	\N	\N	\N		2007-150 Ans Ricamarie	2020-10-02 14:14:10.904577	2020-10-02 14:14:10.904577	{}
152	\N	\N	\N		2007-Carnaval	2020-10-02 14:14:10.906076	2020-10-02 14:14:10.906076	{}
153	Mairie de La Talaudière	Monument aux morts	\N		2007-Fnaca	2020-10-02 14:14:10.907593	2020-10-02 14:14:10.907593	{}
154	Comité des fêtes	Pole festif	Classe d’orchestre, morceau commun : Grande porte de Kiev	2007-01-07	2007-Nouvel An	2020-10-02 14:14:10.912139	2020-10-02 14:14:10.912139	{239,408}
155	\N	Eglise de La Talaudière	\N	2006-12-02	2006-Ste Cécile	2020-10-02 14:14:10.930543	2020-10-02 14:14:10.930543	{239}
156	Mairie de La Talaudière	Monument aux morts	\N		2006-11/11	2020-10-02 14:14:10.942932	2020-10-02 14:14:10.942932	{}
157	\N	Pole festif	\N		2006-Inauguration pole festif	2020-10-02 14:14:10.95106	2020-10-02 14:14:10.95106	{}
158	Mairie de La Talaudière	Monument aux morts	\N		2006-14/07	2020-10-02 14:14:10.952654	2020-10-02 14:14:10.952654	{}
159	\N	\N	\N		2006-Fête Musique	2020-10-02 14:14:10.956024	2020-10-02 14:14:10.956024	{}
160	\N	\N	\N		2006-Feurs	2020-10-02 14:14:10.95761	2020-10-02 14:14:10.95761	{}
161	Harmonie de la Chazotte	Centre culturel le Sou	Groupes KARIBU et Beija-Flor de Clotilde Vacher	2006-04-08	2006-GALA : Musique Latino	2020-10-02 14:14:10.978268	2020-10-02 14:14:10.978268	{239,408}
162	Mairie de La Talaudière	Monument aux morts	\N		2006-08/05	2020-10-02 14:14:10.996113	2020-10-02 14:14:10.996113	{}
163	Mairie de La Talaudière	Monument aux morts	\N		2006-Fnaca	2020-10-02 14:14:10.999816	2020-10-02 14:14:10.999816	{}
164	Comité des fêtes	\N	EMAD Berlioz : Ensemble de guitares, quatuor de saxophones	2006-01-08	2006-Nouvel An	2020-10-02 14:14:11.003878	2020-10-02 14:14:11.003878	{239}
165	\N	Eglise de La Talaudière	\N	2005-12-10	2005-Ste Cécile	2020-10-02 14:14:11.02193	2020-10-02 14:14:11.02193	{239}
166	Mairie de La Talaudière	Monument aux morts	\N		2005-11/11	2020-10-02 14:14:11.033941	2020-10-02 14:14:11.033941	{}
167	Harmonie de la Chazotte	Eglise de la Talaudière	Avenir de Firminy, Harmonie de Roche la Molière, Batterie Fanfare de Sorbiers, Harmonie de St Jean Bonnefonds, Harmonie de St Priest en Jarez, La Traboule	2005-10-02	2005-140 ans journée	2020-10-02 14:14:11.043249	2020-10-02 14:14:11.043249	{239}
168	Harmonie de la Chazotte	Centre Culturel le Sou	Orchestre professionnel de la  Région Sud Est	2005-10-01	2005-140 ans Soirée	2020-10-02 14:14:11.045356	2020-10-02 14:14:11.045356	{239}
169	Mairie de La Talaudière	Monument aux morts	\N		2005-14/07	2020-10-02 14:14:11.059417	2020-10-02 14:14:11.059417	{}
170	\N	\N	\N		2005-Fête Musique	2020-10-02 14:14:11.063189	2020-10-02 14:14:11.063189	{}
171	Harmonie de la Chazotte	Centre Culturel Le Sou	Chanteurs de l’EMAD Berlioz de Angeline Bouille (opéra comique)	2005-05-28	2005-GALA	2020-10-02 14:14:11.065225	2020-10-02 14:14:11.065225	{239}
172	Mairie de La Talaudière	Monument aux morts	\N		2005-08/05	2020-10-02 14:14:11.083199	2020-10-02 14:14:11.083199	{}
173	Mairie de La Talaudière	Monument aux morts	\N		2005-Fnaca	2020-10-02 14:14:11.086817	2020-10-02 14:14:11.086817	{}
174	Comité des fêtes de la Talaudière	\N	 orchestres de l’EMAD Berlioz, morceaux commun Valse n°2	2005-01-09	2005-Nouvel An	2020-10-02 14:14:11.090981	2020-10-02 14:14:11.090981	{239}
175	\N	Eglise de La Talaudière	\N	2004-12-04	2004-Ste Cécile	2020-10-02 14:14:11.108895	2020-10-02 14:14:11.108895	{239}
176	Mairie de La Talaudière	Monument aux morts	\N		2004-11/11	2020-10-02 14:14:11.11841	2020-10-02 14:14:11.11841	{}
177	Mairie de La Talaudière	Monument aux morts	\N		2004-14/07	2020-10-02 14:14:11.124002	2020-10-02 14:14:11.124002	{}
178	\N	\N	\N		2004-Fête Musique	2020-10-02 14:14:11.127653	2020-10-02 14:14:11.127653	{}
179	\N	Théatre de l’école de musique	\N	2004-06-05	2004-Semaine des Jumelage	2020-10-02 14:14:11.129186	2020-10-02 14:14:11.129186	{}
180	Harmonie de la Chazotte	Eglise de La Talaudière	quintette de jazz « Est Ouest »	2004-05-15	2004-GALA	2020-10-02 14:14:11.131561	2020-10-02 14:14:11.131561	{239,359}
181	Mairie de La Talaudière	Monument aux morts	\N		2004-08/05	2020-10-02 14:14:11.149257	2020-10-02 14:14:11.149257	{}
182	Mairie de La Talaudière	Monument aux morts	\N		2004-Fnaca	2020-10-02 14:14:11.170991	2020-10-02 14:14:11.170991	{}
183	Comité des fêtes de La Talaudière	\N	\N	2004-01-04	2004-Nouvel An	2020-10-02 14:14:11.181602	2020-10-02 14:14:11.181602	{239}
184	\N	Eglise de la Talaudière	\N	2003-12-06	2003-Ste Cécile	2020-10-02 14:14:11.206415	2020-10-02 14:14:11.206415	{239}
185	Mairie de La Talaudière	Monument aux morts	\N		2003-11/11	2020-10-02 14:14:11.216611	2020-10-02 14:14:11.216611	{239}
186	Mairie de La Talaudière	Monument aux morts	\N		2003-14/07	2020-10-02 14:14:11.224404	2020-10-02 14:14:11.224404	{}
187	\N	Saint Priest en Jarez	\N	2003-06-29	2003-Anniversaire St Priest en Jarez	2020-10-02 14:14:11.228132	2020-10-02 14:14:11.228132	{}
188	\N	\N	\N	2003-06-21	2003-Fête Musique	2020-10-02 14:14:11.229736	2020-10-02 14:14:11.229736	{}
189	Harmonie de la Chazotte	salle du Sou	groupe Waesel	2003-05-24	2003-GALA	2020-10-02 14:14:11.232137	2020-10-02 14:14:11.232137	{239,408}
190	Mairie de La Talaudière	Monument aux morts	\N		2003-08/05	2020-10-02 14:14:11.246127	2020-10-02 14:14:11.246127	{}
191	\N	Ecole de Musique	\N	2003-04-12	2003-Mini Festival Berlioz	2020-10-02 14:14:11.251975	2020-10-02 14:14:11.251975	{}
192	Mairie de La Talaudière	Monument aux morts	\N	2003-03-19	2003-Fnaca	2020-10-02 14:14:11.253546	2020-10-02 14:14:11.253546	{}
193	Comité des fêtes	\N	\N	2003-01-05	2003-Nouvel An	2020-10-02 14:14:11.25762	2020-10-02 14:14:11.25762	{239}
194	\N	Eglise de la Talaudière	\N	2002-12-14	2002-Ste Cécile	2020-10-02 14:14:11.273994	2020-10-02 14:14:11.273994	{239}
195	Mairie de La Talaudière	Monument aux morts	\N		2002-11/11	2020-10-02 14:14:11.285029	2020-10-02 14:14:11.285029	{}
196	Mairie de La Talaudière	Monument aux morts	\N		2002-14/07	2020-10-02 14:14:11.292051	2020-10-02 14:14:11.292051	{}
197	\N	\N	\N		2002-Fête Musique	2020-10-02 14:14:11.295263	2020-10-02 14:14:11.295263	{}
198	\N	Eglise de La Talaudière	Chorale A tout choeur	2002-05-24	2002-Soirée Jumelages	2020-10-02 14:14:11.296503	2020-10-02 14:14:11.296503	{}
199	Harmonie de la Chazotte	Centre culturel le Sou	les Bérets	2002-04-21	2002-GALA	2020-10-02 14:14:11.311171	2020-10-02 14:14:11.311171	{239,157}
256	\N	\N	\N		1994 – Nll An	2020-10-02 14:14:11.712871	2020-10-02 14:14:11.712871	{}
200	Mairie de La Talaudière	Monument aux morts	\N		2002-08/05	2020-10-02 14:14:11.330598	2020-10-02 14:14:11.330598	{}
201	Mairie de La Talaudière	Monument aux morts	\N		2002-Fnaca	2020-10-02 14:14:11.336062	2020-10-02 14:14:11.336062	{}
202	les musiciens et amis de l’harmonie municipale de Tournus	Cellier des Moines, Tournus	\N	2002-03-09	2002-Concert Tournus	2020-10-02 14:14:11.339965	2020-10-02 14:14:11.339965	{239}
203	\N	\N	ensemble de cuivres de l’école de musique de Gilles Pezet	2002-01-06	2002-Nouvel An	2020-10-02 14:14:11.35659	2020-10-02 14:14:11.35659	{239}
204	\N	Eglise de La Talaudière	\N	2001-12-08	2001-Ste Cécile	2020-10-02 14:14:11.367229	2020-10-02 14:14:11.367229	{239}
205	Mairie de La Talaudière	Monument aux morts	\N		2001-11/11	2020-10-02 14:14:11.375856	2020-10-02 14:14:11.375856	{}
206	Mairie de La Talaudière	Monument aux morts	\N		2001-14/07	2020-10-02 14:14:11.382906	2020-10-02 14:14:11.382906	{}
207	Municipalité de Sorbiers	Salle des sports du Valjoli\nGrand spectacle Musical	ACEM Berlioz\nMusirêve\nBatterie Fanfare	2001-06-24	2001-Fête Musique	2020-10-02 14:14:11.386256	2020-10-02 14:14:11.386256	{}
208	Harmonie de la Chazotte	Centre Culturel Le Sou	Harmonie de Tournus	2001-05-19	2001-GALA : Grand Concert	2020-10-02 14:14:11.393946	2020-10-02 14:14:11.393946	{239,157}
209	Mairie de La Talaudière	Monument aux morts	\N		2001-08/05	2020-10-02 14:14:11.410514	2020-10-02 14:14:11.410514	{239}
210	Mairie de La Talaudière	Monument aux morts	\N		2001-Fnaca	2020-10-02 14:14:11.415853	2020-10-02 14:14:11.415853	{}
211	Comité des fêtes	Centre culturel Le Sou	ensemble de trompette\nquatuor de saxophone\nQuatuor de clarinette	2001-01-07	2001-Nouvel An	2020-10-02 14:14:11.419652	2020-10-02 14:14:11.419652	{239}
212	\N	Eglise de la Talaudière	\N	2000-12-09	2000-Ste Cécile	2020-10-02 14:14:11.430473	2020-10-02 14:14:11.430473	{239}
213	Mairie de La Talaudière	Monument aux morts	\N		2000-11/11	2020-10-02 14:14:11.439369	2020-10-02 14:14:11.439369	{}
214	Mairie de La Talaudière	Monument aux morts	\N		2000-14/07	2020-10-02 14:14:11.446493	2020-10-02 14:14:11.446493	{}
215	\N	\N	classes d’orchestre de l’ACEM Berlioz	2000-06-23	2000-Fête Musique	2020-10-02 14:14:11.449831	2020-10-02 14:14:11.449831	{}
216	\N	Centre Culturel Communal « Le Sou »	OSMOSE	2000-04-08	2000-GALA : Musique Latino	2020-10-02 14:14:11.452059	2020-10-02 14:14:11.452059	{239,157}
217	\N	\N	\N	2000-...	2000 : Concours de Bourbon Lancy	2020-10-02 14:14:11.464729	2020-10-02 14:14:11.464729	{}
218	Mairie de La Talaudière	Monument aux morts	\N		2000-08/05	2020-10-02 14:14:11.471639	2020-10-02 14:14:11.471639	{}
219	Mairie de La Talaudière	Monument aux morts	\N		2000-Fnaca	2020-10-02 14:14:11.475061	2020-10-02 14:14:11.475061	{}
220	Comité des fêtes	Centre Culturel Le Sou	orchestre de chambre de l’ACEM Berlioz de Solange Becqueriaux\nEnsemble de flûtes Ut à Ut de Mireille Fifre	2000-01-09	2000-Nouvel An	2020-10-02 14:14:11.479194	2020-10-02 14:14:11.479194	{239,157}
221	\N	Eglise de La Talaudière	\N	1999-12-11	1999-Ste Cécile	2020-10-02 14:14:11.493824	2020-10-02 14:14:11.493824	{239}
222	\N	\N	\N		1999-11/11	2020-10-02 14:14:11.502744	2020-10-02 14:14:11.502744	{}
223	\N	\N	\N		1999-14/07	2020-10-02 14:14:11.50989	2020-10-02 14:14:11.50989	{}
224	\N	\N	\N		1999-Fête Musique	2020-10-02 14:14:11.513671	2020-10-02 14:14:11.513671	{}
225	Harmonie de la Chazotte	\N	Chorales de l'ACEM et du Collège Pierre et Marie Curie	1999-05-28	1999-GALA	2020-10-02 14:14:11.515803	2020-10-02 14:14:11.515803	{239}
226	\N	\N	\N		1999-08/05	2020-10-02 14:14:11.540279	2020-10-02 14:14:11.540279	{}
227	\N	\N	\N		1999-Fnaca	2020-10-02 14:14:11.546416	2020-10-02 14:14:11.546416	{}
228	Comité des fêtes	Eglise de la Talaudière	avec l’orchestre de chambre de l’ACEM Berlioz, l’ensemble de jazz	1999-01-10	1999-Nouvel An	2020-10-02 14:14:11.55193	2020-10-02 14:14:11.55193	{239}
229	\N	Eglise de La Talaudière	\N	1998-12-05	1998-Ste Cécile	2020-10-02 14:14:11.569122	2020-10-02 14:14:11.569122	{239}
230	\N	\N	\N		1998-11/11	2020-10-02 14:14:11.579733	2020-10-02 14:14:11.579733	{}
231	\N	\N	\N		1998-14/07	2020-10-02 14:14:11.583879	2020-10-02 14:14:11.583879	{}
232	\N	\N	\N		1998-Fête Musique	2020-10-02 14:14:11.588027	2020-10-02 14:14:11.588027	{}
233	\N	\N	\N	1998-05-15	1998-GALA	2020-10-02 14:14:11.589762	2020-10-02 14:14:11.589762	{}
234	\N	\N	\N		1998-08/05	2020-10-02 14:14:11.599857	2020-10-02 14:14:11.599857	{}
235	\N	\N	\N		1998-Fnaca	2020-10-02 14:14:11.603604	2020-10-02 14:14:11.603604	{}
236	\N	Eglise de la Talaudière, Comité des fêtes	avec la classe de Chant de Myriam Laïdouni, l’orchestre de Jazz	1998-01-11	1998-Nouvel An	2020-10-02 14:14:11.607887	2020-10-02 14:14:11.607887	{66}
237	\N	Musée de la mine	\N	1997-12-05	1997-Ste Barbe	2020-10-02 14:14:11.618457	2020-10-02 14:14:11.618457	{66}
238	\N	Eglise de La Talaudière	\N		1997-Ste Cécile	2020-10-02 14:14:11.620385	2020-10-02 14:14:11.620385	{66}
239	\N	\N	\N		1997-11/11	2020-10-02 14:14:11.630167	2020-10-02 14:14:11.630167	{}
240	\N	\N	\N		1997-14/07	2020-10-02 14:14:11.633851	2020-10-02 14:14:11.633851	{}
241	\N	\N	\N		1997-Fête Musique	2020-10-02 14:14:11.637736	2020-10-02 14:14:11.637736	{}
242	\N	Concours de Chenove	Musique de la Région Aerienne Nord-Est de Dijon	1997-06-15	1997-Concours de Chenove	2020-10-02 14:14:11.639841	2020-10-02 14:14:11.639841	{66}
243	\N	\N	\N		1997-08/05	2020-10-02 14:14:11.647917	2020-10-02 14:14:11.647917	{}
244	\N	au Sou des Ecoles	avec les clarinettistes de la Loire d’André Guillaume, et l’Ecole de danse du Sou des Ecoles	1997-04-12	1997-GALA	2020-10-02 14:14:11.652083	2020-10-02 14:14:11.652083	{66}
245	\N	\N	\N		1997-Fnaca	2020-10-02 14:14:11.659813	2020-10-02 14:14:11.659813	{}
246	\N	au Sou des écoles, comité des fêtes	avec les clarinettistes de la Loire d’André Guillaume	1997-01-12	1997-Nouvel An	2020-10-02 14:14:11.663708	2020-10-02 14:14:11.663708	{66}
247	\N	\N	\N		1996 -Ste Cécile	2020-10-02 14:14:11.673254	2020-10-02 14:14:11.673254	{}
248	\N	Salle du Sou	\N	1995-05-11	1996 – Printemps	2020-10-02 14:14:11.675269	2020-10-02 14:14:11.675269	{66}
249	Comité des fêtes	au Sou des Ecoles	avec l’orchestre junior et l’ensemble de jazz de l’école de musique, et la chorale A tout choeur du centre social	1996-01-14	1996 – Nll An	2020-10-02 14:14:11.677167	2020-10-02 14:14:11.677167	{66}
250	\N	Musée de la Mine, St Etienne	\N	1995-12-02	1995-Ste Barbe	2020-10-02 14:14:11.687356	2020-10-02 14:14:11.687356	{66}
251	\N	Eglise de la Talaudière	\N	1995-11-18	1995 -Ste Cécile	2020-10-02 14:14:11.689385	2020-10-02 14:14:11.689385	{66}
252	\N	\N	\N	1995-05-13	1995 – Printemps : la musique de film	2020-10-02 14:14:11.691002	2020-10-02 14:14:11.691002	{}
253	\N	\N	\N		1995 – Nll An	2020-10-02 14:14:11.708327	2020-10-02 14:14:11.708327	{}
254	\N	\N	\N		1994 -Ste Cécile	2020-10-02 14:14:11.709879	2020-10-02 14:14:11.709879	{}
255	\N	\N	\N		1994 – Printemps	2020-10-02 14:14:11.711398	2020-10-02 14:14:11.711398	{}
257	\N	\N	\N		1993 -Ste Cécile	2020-10-02 14:14:11.714382	2020-10-02 14:14:11.714382	{}
258	\N	Centre Berlioz	Chorale du collège de La Talaudière de Mme DUMAS	1993-06-21	1993-Fete de la Musique 11eme edition	2020-10-02 14:14:11.716325	2020-10-02 14:14:11.716325	{66}
259	Ville d’Oyonnax	Oyonnax, salle Valexpo rez de chaussée	Jury M. Charnay, Fournier, Fromin, Hurier	1993-06-06	1993-2eme Concours d’Oyonnax	2020-10-02 14:14:11.728106	2020-10-02 14:14:11.728106	{66}
260	\N	À Montbrison, Salle André Daval	avec la Lyre Montbrisonnaise d’Aimé Prévost et christine Burlinchon	1993-05-14 ?	1993 – Printemps	2020-10-02 14:14:11.734145	2020-10-02 14:14:11.734145	{66}
261	\N	au sou des écoles, comité des fêtes	avec l’ensemble de flûte ; l’ensemble de saxophone ; ensemble flute, hautbois, basson ; ensemble de cuivres	1993-01-10	1993 – Nll An	2020-10-02 14:14:11.745944	2020-10-02 14:14:11.745944	{66}
262	\N	Eglise de la Talaudière	\N	1992-11-21	1992 -Ste Cécile	2020-10-02 14:14:11.757622	2020-10-02 14:14:11.757622	{66}
263	\N	\N	\N		1992 – Printemps	2020-10-02 14:14:11.763229	2020-10-02 14:14:11.763229	{}
264	\N	au sou des écoles, comité des fêtes	avec l’ensemble de flûte ; l’ensemble de saxophone ; ensemble flute, hautbois, basson	1992-01-05	1992 – Nll An	2020-10-02 14:14:11.765198	2020-10-02 14:14:11.765198	{66}
265	\N	Eglise de la Talaudière	\N		1991 -Ste Cécile	2020-10-02 14:14:11.780732	2020-10-02 14:14:11.780732	{66}
266	\N	Concours d’Oyonnax	\N	1991-06-30	1991-Concours Oyonnax	2020-10-02 14:14:11.790449	2020-10-02 14:14:11.790449	{66}
267	\N	Sou des Ecoles de  La Talaudière	Théatre des Trois Coups « Pince mi et Pince moi »	1991-04-06	1991 – Gala : Tour en Europe	2020-10-02 14:14:11.798584	2020-10-02 14:14:11.798584	{66}
268	\N	\N	\N		1991 – Nll An	2020-10-02 14:14:11.800054	2020-10-02 14:14:11.800054	{}
269	\N	Eglise de La Talaudière	\N	1990-12-01	1990 -Ste Cécile La Talaud	2020-10-02 14:14:11.802034	2020-10-02 14:14:11.802034	{66}
270	\N	Eglise de l’Etrat	\N	1990-11-24	1990-Ste Cécile l’Etrat	2020-10-02 14:14:11.80816	2020-10-02 14:14:11.80816	{66}
271	Sorbiers	Place du 8 mai à Sorbiers	Municipalité et Association	1990-06-18	1990-Cinquantenaire de l'Appel du Général de Gaulle	2020-10-02 14:14:11.817896	2020-10-02 14:14:11.817896	{}
272	\N	au Sou des Ecoles	avec la classe d’orchestre (40 francs)	1990-05-19	1990 – Printemps	2020-10-02 14:14:11.82616	2020-10-02 14:14:11.82616	{66}
273	\N	Eglise de la Talaudière	avec la classe d’orchestre, morceaux commun Round Midnight, la valse des fleurs	1990-03-04	1990-Eglise Talaud	2020-10-02 14:14:11.83638	2020-10-02 14:14:11.83638	{66}
274	\N	Quiétude	\N	1990-01-20	1990 – Nll An	2020-10-02 14:14:11.844301	2020-10-02 14:14:11.844301	{66}
275	\N	Eglise de La Talaudière	\N	1989-12-02	1989 -Ste Cécile	2020-10-02 14:14:11.854336	2020-10-02 14:14:11.854336	{66}
276	\N	Eglise de l’Etrat	\N	1989-11-26	1989-Ste Cécile (Etrat)	2020-10-02 14:14:11.864294	2020-10-02 14:14:11.864294	{66}
277	St Sauveur de Cruzière	\N	\N	1989-09-10	1989-Excursion e ardèche	2020-10-02 14:14:11.875018	2020-10-02 14:14:11.875018	{66}
278	Mairie de La Talaudière	Parc de la Talaudière	\N	1989-06-24	1989-Inauguration de la mairie	2020-10-02 14:14:11.890756	2020-10-02 14:14:11.890756	{66}
279	La Talaudière	Rue de la Chazotte	\N	1989-06-23	1989-Fête de la Musique	2020-10-02 14:14:11.897441	2020-10-02 14:14:11.897441	{66}
280	Mairie de Sorbiers	Monuments aux morts à Sorbiers	retraite aux flambeaux en fin de soirée	1989-06-18	1989-Appel du 18 juin	2020-10-02 14:14:11.899736	2020-10-02 14:14:11.899736	{66}
281	Harmonie de La Chazotte	Sou des écoles	Jeunesse et Chanson de Sorbiers	1989-04-15	1989 – Printemps	2020-10-02 14:14:11.901967	2020-10-02 14:14:11.901967	{66}
282	\N	\N	\N		1988 -Ste Cécile	2020-10-02 14:14:11.904145	2020-10-02 14:14:11.904145	{66}
283	Harmonie de la Chazotte	Sou des Ecoles de La Talaudière	Choeurs et instrumentistes du Lycée Honoré d’Urfé	1988-05-28	1988 – Printemps	2020-10-02 14:14:11.912875	2020-10-02 14:14:11.912875	{66}
284	\N	\N	\N		1987 -Ste Cécile	2020-10-02 14:14:11.921583	2020-10-02 14:14:11.921583	{577}
285	\N	\N	\N		1987 – Printemps	2020-10-02 14:14:11.931869	2020-10-02 14:14:11.931869	{577}
286	\N	Eglise de La Talaudière	\N	1986-11-29	1986 -Ste Cécile	2020-10-02 14:14:11.948878	2020-10-02 14:14:11.948878	{577}
287	\N	\N	\N		1986 – Printemps	2020-10-02 14:14:11.959132	2020-10-02 14:14:11.959132	{577}
288	Harmonie de la Chazotte	\N	L’orchestre d’harmonie Les cadets d’Alsace	1986-04-06	1986-Cadet d’Alsace	2020-10-02 14:14:11.961335	2020-10-02 14:14:11.961335	{577}
289	\N	\N	\N		1985 -Ste Cécile	2020-10-02 14:14:11.963612	2020-10-02 14:14:11.963612	{577}
290	\N	\N	\N		1985 – Printemps	2020-10-02 14:14:11.97446	2020-10-02 14:14:11.97446	{577}
291	\N	\N	\N		1984 -Ste Cécile	2020-10-02 14:14:11.976671	2020-10-02 14:14:11.976671	{577}
292	Harmonie de la Chazotte	\N	La Symphonie de St Chamond dirigée par Serge Debievre	1984-05-04	1984 – Printemps	2020-10-02 14:14:11.987046	2020-10-02 14:14:11.987046	{577}
293	\N	\N	\N		1983 -Ste Cécile	2020-10-02 14:14:12.003006	2020-10-02 14:14:12.003006	{577}
294	Harmonie de la chazotte	Salle de la piscine	Orchestre de jazz de St Etienne	1983-05-30	1983 – Printemps	2020-10-02 14:14:12.005042	2020-10-02 14:14:12.005042	{577}
295	\N	\N	\N		1982 -Ste Cécile	2020-10-02 14:14:12.015444	2020-10-02 14:14:12.015444	{577}
296	\N	\N	\N		1982 – Printemps	2020-10-02 14:14:12.026496	2020-10-02 14:14:12.026496	{577}
297	\N	\N	\N		1981 -Ste Cécile	2020-10-02 14:14:12.035015	2020-10-02 14:14:12.035015	{577}
298	\N	\N	\N		1981 – Printemps	2020-10-02 14:14:12.044625	2020-10-02 14:14:12.044625	{577}
299	Harmonie de la Chazotte	Eglise de la Talaudière	\N	1980-11-23	1980 -Ste Cécile	2020-10-02 14:14:12.058776	2020-10-02 14:14:12.058776	{150}
300	Harmonie de la Chazotte	Salle de la Piscine	\N	1980-06-20	1980-Fete de la Musique	2020-10-02 14:14:12.075593	2020-10-02 14:14:12.075593	{150}
301	Concours internationnal de musique de Nazelles-Negron	Indre et Loire	\N	1980-06-09	1980 – Concours de Nazelle-Negron	2020-10-02 14:14:12.103593	2020-10-02 14:14:12.103593	{150}
302	\N	CNRO	\N	1980-05-18	1980 – Printemps	2020-10-02 14:14:12.124539	2020-10-02 14:14:12.124539	{150}
303	\N	Salle de la Piscine	\N	1980-05-10	1980 – Gala	2020-10-02 14:14:12.142832	2020-10-02 14:14:12.142832	{150}
304	Harmonie de la Chazotte	Eglise de la Talaudière	\N	1979-12-02	1979 – Ste Cécile	2020-10-02 14:14:12.156599	2020-10-02 14:14:12.156599	{150}
305	\N	\N	\N	1979-06-08	1979 – Fete Musique	2020-10-02 14:14:12.169368	2020-10-02 14:14:12.169368	{150}
306	Harmonie de la Chazotte	Salle de la Piscine	Orchestre de la chazotte et Ecole de danse Harmory	1979-04-21	1979 – Soirée Musique Danse	2020-10-02 14:14:12.185955	2020-10-02 14:14:12.185955	{150}
307	St Jean Bonnefonds	Roseraie	\N	1979-04-13	1979 – St Jean Bonnefonds	2020-10-02 14:14:12.210259	2020-10-02 14:14:12.210259	{150}
308	\N	\N	\N		1978 – Ste Cécile	2020-10-02 14:14:12.222658	2020-10-02 14:14:12.222658	{150}
309	\N	\N	\N		1978 – Printemps	2020-10-02 14:14:12.234943	2020-10-02 14:14:12.234943	{585}
310	\N	Eglise de La Talaudière	\N		1977 -Ste Cécile	2020-10-02 14:14:12.255605	2020-10-02 14:14:12.255605	{585}
311	Harmonie de la Chazotte	Salle de la piscine	\N	1977-06-28	1977 – Soirée découverte de la musique	2020-10-02 14:14:12.283064	2020-10-02 14:14:12.283064	{585}
312	Harmonie de la Chazotte	Salle de la piscine	Ecole de danse des œuvres sociales de la  SNCF	1977-06-14	1977 – Printemps	2020-10-02 14:14:12.286687	2020-10-02 14:14:12.286687	{585}
313	\N	Eglise de La Talaudière	\N		1976 -Ste Cécile	2020-10-02 14:14:12.314366	2020-10-02 14:14:12.314366	{585}
314	Comité des fêtes (M. Rascle)	\N	Retraite aux flambeau, Fanfare de l’Epi de Grammond	1976-07-13	1976-Fete nationale	2020-10-02 14:14:12.3249	2020-10-02 14:14:12.3249	{585}
315	Harmonie municipale de Macon	Grand Hall du Parc des Exposition	\N	1976-06-27	1976-Concours à Macon	2020-10-02 14:14:12.328199	2020-10-02 14:14:12.328199	{585}
316	\N	\N	\N		1976 – Printemps	2020-10-02 14:14:12.331476	2020-10-02 14:14:12.331476	{585}
317	\N	\N	\N		1975 -Ste Cécile	2020-10-02 14:14:12.343091	2020-10-02 14:14:12.343091	{585}
318	Harmonie de la Chazotte	Salle de la Piscine	Ecole de danse du Sou des Ecoles	1975-06-13	1975 – Printemps	2020-10-02 14:14:12.351683	2020-10-02 14:14:12.351683	{585}
319	\N	\N	\N		1974 -Ste Cécile	2020-10-02 14:14:12.355884	2020-10-02 14:14:12.355884	{585}
320	\N	\N	\N		1974 – Printemps	2020-10-02 14:14:12.371071	2020-10-02 14:14:12.371071	{2}
321	\N	Eglise de la Talaudière	\N	1973-11-24	1973 -Ste Cécile	2020-10-02 14:14:12.380433	2020-10-02 14:14:12.380433	{2}
322	\N	\N	\N		1973 – Printemps	2020-10-02 14:14:12.392916	2020-10-02 14:14:12.392916	{2}
323	\N	Eglise de la Talaudière	\N		1972 -Ste Cécile	2020-10-02 14:14:12.402203	2020-10-02 14:14:12.402203	{2}
324	Ville de Nevers	\N	\N	1972-06-25	1972-Concours International de Nevers	2020-10-02 14:14:12.411328	2020-10-02 14:14:12.411328	{2}
325	\N	\N	\N		1972 – Printemps	2020-10-02 14:14:12.413118	2020-10-02 14:14:12.413118	{2}
326	\N	Eglise de la Talaudière	\N		1971 -Ste Cécile	2020-10-02 14:14:12.424083	2020-10-02 14:14:12.424083	{2}
327	\N	\N	\N		1971 – Printemps	2020-10-02 14:14:12.431582	2020-10-02 14:14:12.431582	{2}
328	\N	\N	\N		1970 -Ste Cécile	2020-10-02 14:14:12.44229	2020-10-02 14:14:12.44229	{2}
329	Comédie de St Etienne	Petit théatre Copeau de la M.C.L.	\N		1970 – Printemps	2020-10-02 14:14:12.443965	2020-10-02 14:14:12.443965	{2}
330	\N	\N	\N		1969 -Ste Cécile	2020-10-02 14:14:12.457239	2020-10-02 14:14:12.457239	{2}
331	\N	Podium de la piscine de La Talaudière	Les 3 Harmonies des Houillères de la Loire	1969-06-04	1969 – Printemps	2020-10-02 14:14:12.463018	2020-10-02 14:14:12.463018	{2}
332	\N	Eglise de la Talaudière	\N		1968-Ste Cécile	2020-10-02 14:14:12.474419	2020-10-02 14:14:12.474419	{2}
333	\N	\N	\N		196 ?-Foire de Grenoble	2020-10-02 14:14:12.476174	2020-10-02 14:14:12.476174	{2}
334	Harmonie de la Chazotte	La Talaudière	L’espérance de St Martin Lestra\nHarmonie de Beaulieu\nHarmonie des mineurs de la Ricamarie\nHarmonie de Cote Chaude\nHarmonie des ineurs de Roche la Molièer\nMusique de la Surete Nationalle	1965-05-30	1965-Centenaire Chazotte	2020-10-02 14:14:12.47838	2020-10-02 14:14:12.47838	{427,212}
335	Harmonie de la Chazotte	La Talaudière	L’espérance de St Martin Lestra\nHarmonie de Beaulieu\nHarmonie des mineurs de la Ricamarie\nHarmonie de Cote Chaude\nHarmonie des ineurs de Roche la Molièer\nMusique de la Surete Nationalle	1965-05-29	1965-Centenaire Chazotte	2020-10-02 14:14:12.480614	2020-10-02 14:14:12.480614	{427,212}
336	Concours de Louhans	Place Aristide Briand, Louhans	Harmonie de Louhans, harmonie du Coteau, Harmonie de Dole	1961-04-30	1961-Concours de Louhans	2020-10-02 14:14:12.486836	2020-10-02 14:14:12.486836	{427,212}
337	\N	\N	\N	1952-07-06	1952-Concours Orléans	2020-10-02 14:14:12.496583	2020-10-02 14:14:12.496583	{427,212}
338	\N	Place Marengo, St Etienne	\N		1948-Concert Kiosque de Marengo	2020-10-02 14:14:12.498456	2020-10-02 14:14:12.498456	{427}
339	\N	\N	\N	1937-07-17	1937-Concours de St Maur	2020-10-02 14:14:12.50056	2020-10-02 14:14:12.50056	{427,212}
340	\N	\N	\N	1935-07-21	1935-Concours de St Brieuc	2020-10-02 14:14:12.502785	2020-10-02 14:14:12.502785	{427,212}
341	\N	\N	\N		1933-Concours de Menton	2020-10-02 14:14:12.505011	2020-10-02 14:14:12.505011	{427,212}
342	\N	\N	\N		\N	2020-10-02 14:14:12.506814	2020-10-02 14:14:12.506814	{219}
343	Festival d’Issingeaux	\N	Décès de Jean Delorme à la baguette		1926-Festival d’Issingeaux	2020-10-02 14:14:12.508553	2020-10-02 14:14:12.508553	{155}
344	\N	\N	\N		1922-Concours de Montpellier	2020-10-02 14:14:12.510886	2020-10-02 14:14:12.510886	{155}
345	\N	\N	\N		1912-Concours de Paris	2020-10-02 14:14:12.513184	2020-10-02 14:14:12.513184	{155}
346	\N	\N	\N		1910-Concours de Reims	2020-10-02 14:14:12.515479	2020-10-02 14:14:12.515479	{155}
347	\N	\N	\N		1907-Concours de Dijon	2020-10-02 14:14:12.518683	2020-10-02 14:14:12.518683	{155}
348	\N	\N	\N		1902-Concours de Genève	2020-10-02 14:14:12.521263	2020-10-02 14:14:12.521263	{155}
349	Maire de la Talaudière (M. Wery)	La Talaudière	Musique de Marseille	1898-08-13	1898-Festival de musique	2020-10-02 14:14:12.52429	2020-10-02 14:14:12.52429	{155}
350	\N	\N	\N		1897-Concours de Marseille	2020-10-02 14:14:12.527357	2020-10-02 14:14:12.527357	{155}
351	\N	\N	\N		1893-Concours St Chamond	2020-10-02 14:14:12.530544	2020-10-02 14:14:12.530544	{161}
\.


--
-- Data for Name: concerts_scores; Type: TABLE DATA; Schema: public; Owner: romain
--

COPY public.concerts_scores (score_id, concert_id) FROM stdin;
552	16
594	16
594	18
594	21
594	24
594	27
594	29
594	34
594	36
127	38
130	38
131	38
433	38
127	39
130	39
131	39
433	39
582	40
583	40
594	40
567	41
594	41
129	42
345	42
425	42
427	42
428	42
55	43
72	43
126	43
128	43
129	43
260	43
345	43
427	43
428	43
429	43
430	43
431	43
55	44
552	44
594	44
594	45
595	45
128	46
260	46
425	46
426	46
427	46
580	47
594	47
79	48
121	48
123	48
124	48
125	48
389	48
424	48
425	48
426	48
79	49
123	49
124	49
125	49
121	50
122	50
222	50
362	50
423	50
572	51
594	51
615	51
594	52
604	52
122	53
170	53
199	53
222	53
400	53
418	53
419	53
420	53
551	53
14	54
121	54
122	54
222	54
242	54
362	54
421	54
422	54
423	54
542	54
549	54
594	54
619	54
625	54
637	54
641	54
14	55
121	55
122	55
222	55
242	55
362	55
421	55
422	55
423	55
542	55
549	55
594	55
619	55
625	55
637	55
641	55
594	56
619	56
122	57
222	57
362	57
619	57
641	57
122	58
471	58
549	58
619	58
637	58
594	59
239	60
283	60
406	60
471	60
64	61
118	61
119	61
120	61
349	61
229	62
239	62
406	62
594	63
389	64
400	64
587	65
594	65
288	66
352	66
389	66
392	66
52	67
64	67
241	67
288	67
330	67
337	67
340	67
352	67
389	67
392	67
594	68
608	68
580	69
594	69
43	70
73	70
117	70
404	70
405	70
497	70
499	70
43	71
73	71
117	71
497	71
577	72
587	72
594	72
170	73
233	73
269	73
356	73
396	73
398	73
400	73
401	73
402	73
403	73
594	74
114	75
115	75
116	75
170	75
269	75
339	75
396	75
398	75
400	75
233	76
285	76
286	76
356	76
400	76
402	76
109	77
233	77
284	77
285	77
286	77
319	77
353	77
356	77
400	77
401	77
402	77
403	77
404	77
408	77
170	78
345	78
580	79
594	79
72	80
109	80
114	80
170	80
184	80
229	80
269	80
285	80
339	80
345	80
396	80
398	80
400	80
401	80
403	80
495	80
594	81
72	82
112	82
114	82
115	82
116	82
398	82
399	82
495	82
114	83
115	83
116	83
398	83
495	83
565	84
587	84
594	84
594	85
399	86
184	87
231	87
275	87
299	87
395	87
396	87
397	87
399	87
587	88
594	88
594	90
16	91
52	91
71	91
113	91
289	91
290	91
16	92
52	92
71	92
113	92
555	94
573	94
594	94
594	95
282	96
330	96
333	96
336	96
352	96
378	96
388	96
389	96
392	96
658	96
282	98
333	98
347	98
352	98
385	98
388	98
389	98
390	98
392	98
393	98
542	98
583	100
594	100
608	100
594	102
40	103
109	103
111	103
228	103
380	103
658	103
64	104
109	104
110	104
111	104
658	104
40	105
53	105
64	105
94	105
110	105
594	106
594	109
228	113
237	113
272	113
273	113
321	113
380	113
385	113
386	113
387	113
407	113
594	115
108	116
228	116
237	116
304	116
336	116
384	116
387	116
594	117
107	118
92	119
106	119
107	119
108	119
580	121
582	121
594	121
594	122
264	125
337	125
338	125
374	125
375	125
376	125
377	125
378	125
381	125
382	125
383	125
594	127
604	127
594	129
154	131
155	131
370	131
371	131
372	131
373	131
645	131
661	131
253	132
274	132
370	132
371	132
372	132
373	132
645	132
661	132
559	133
594	133
594	134
89	137
235	137
319	137
322	137
363	137
367	137
368	137
565	138
594	138
616	138
369	139
594	140
103	141
104	141
105	141
365	141
467	141
496	141
660	141
103	142
104	142
105	142
351	142
365	142
467	142
496	142
660	142
577	144
587	144
594	144
594	145
353	146
356	146
357	146
361	146
364	146
366	146
213	148
313	148
348	148
350	148
351	148
352	148
355	148
358	148
594	150
616	150
594	153
65	154
81	154
99	154
100	154
101	154
346	154
350	154
351	154
65	155
81	155
99	155
100	155
101	155
580	156
582	156
594	156
594	158
142	160
231	160
340	160
341	160
342	160
343	160
344	160
345	160
347	160
231	161
340	161
341	161
342	161
343	161
344	161
345	161
347	161
594	162
594	163
14	164
71	164
163	164
179	164
283	164
308	164
650	164
14	165
71	165
650	165
570	166
587	166
594	166
330	168
331	168
332	168
333	168
336	168
337	168
594	169
330	171
331	171
332	171
333	171
336	171
337	171
338	171
339	171
594	172
594	173
72	174
95	174
96	174
97	174
98	174
331	174
334	174
335	174
95	175
96	175
97	175
98	175
580	176
594	176
594	177
218	180
237	180
241	180
253	180
288	180
324	180
328	180
329	180
594	181
616	181
594	182
91	183
92	183
93	183
94	183
324	183
326	183
329	183
497	183
91	184
92	184
94	184
497	184
573	185
582	185
594	185
594	186
319	189
320	189
322	189
542	189
543	189
544	189
594	190
616	190
594	192
84	193
85	193
86	193
311	193
314	193
467	193
496	193
84	194
85	194
86	194
496	194
552	195
582	195
594	195
594	196
308	198
309	198
310	198
312	198
313	198
314	198
658	198
262	199
308	199
309	199
310	199
312	199
313	199
314	199
315	199
622	199
658	199
594	200
608	200
594	201
83	202
308	202
309	202
310	202
312	202
313	202
314	202
658	202
79	203
80	203
81	203
82	203
83	203
79	204
80	204
81	204
82	204
556	205
594	205
615	205
594	206
79	207
569	207
622	207
208	208
274	208
297	208
298	208
299	208
300	208
301	208
464	208
565	209
594	209
594	210
76	211
77	211
78	211
79	211
163	211
76	212
77	212
78	212
79	212
559	213
594	213
595	213
594	214
275	216
276	216
277	216
279	216
280	216
281	216
74	217
75	217
278	217
594	218
594	219
27	220
52	220
71	220
72	220
73	220
163	220
181	220
27	221
52	221
71	221
73	221
570	222
587	222
594	222
594	223
242	225
261	225
264	225
266	225
267	225
268	225
659	225
594	226
594	227
12	228
14	228
15	228
39	228
58	228
650	228
12	229
39	229
58	229
650	229
594	230
594	231
256	233
257	233
258	233
259	233
594	234
594	235
69	236
70	236
255	236
657	236
69	238
70	238
255	238
657	238
594	239
594	240
251	242
253	242
254	242
594	243
233	244
253	244
254	244
594	245
43	246
64	246
243	246
252	246
55	249
137	249
220	249
243	249
65	252
139	252
211	252
233	252
235	252
236	252
237	252
241	252
54	258
170	258
193	258
224	258
237	258
224	259
656	259
199	260
221	260
224	260
225	260
237	260
41	261
51	261
52	261
53	261
237	261
52	262
237	262
55	264
56	264
57	264
214	264
217	264
218	264
220	264
55	265
56	265
57	265
220	265
214	266
217	266
218	266
49	269
79	269
12	270
50	270
79	270
655	270
565	271
594	271
611	271
46	272
211	272
212	272
655	272
42	273
47	273
209	273
151	274
153	274
184	274
625	274
30	275
46	275
47	275
471	275
30	276
46	276
47	276
471	276
45	277
153	277
170	277
177	277
208	277
535	277
45	278
574	278
41	282
42	282
152	282
144	283
145	283
147	283
37	284
38	284
39	284
202	284
193	285
197	285
198	285
199	285
200	285
203	285
541	285
24	286
34	286
35	286
36	286
17	289
29	289
190	289
671	289
14	291
483	291
652	291
693	291
167	292
169	292
170	292
171	292
172	292
173	292
465	292
165	294
359	294
539	294
540	294
30	295
152	295
166	295
138	296
140	296
70	297
139	297
479	297
20	298
21	298
535	298
536	298
537	298
142	299
176	299
648	299
670	299
149	300
472	300
484	300
554	300
626	300
143	301
498	301
149	302
484	302
626	302
498	303
626	303
649	303
498	304
662	304
663	304
150	305
151	305
10	306
150	306
151	306
595	306
614	306
149	307
484	307
626	307
12	308
482	308
672	308
4	309
10	309
17	309
179	309
17	310
156	310
157	310
473	310
5	312
6	312
158	312
159	312
179	312
535	312
604	312
643	312
24	313
471	313
671	313
3	316
163	316
537	316
650	316
30	317
52	317
620	317
642	318
24	319
91	319
479	319
634	319
7	320
625	320
642	320
20	321
21	321
174	321
471	321
488	321
146	322
556	322
623	322
633	322
28	323
182	323
523	323
556	323
178	325
577	325
631	325
650	325
652	325
479	326
591	326
669	326
158	327
163	327
176	327
653	327
668	327
9	329
146	329
591	329
647	329
650	329
665	329
634	330
647	330
2	331
548	331
591	331
620	331
633	331
494	335
569	335
152	336
162	336
547	336
653	336
\.


--
-- Data for Name: people; Type: TABLE DATA; Schema: public; Owner: romain
--

COPY public.people (id, first_name, last_name, birth_country, birth_date, death_date, biography, nicknames, created_at, updated_at) FROM stdin;
1	Adolphe	ADAM	\N	\N	\N	\N	\N	2020-10-02 13:40:22.147287	2020-10-02 13:40:22.147287
2	Louis	AGNES	France	?	?	Direction de l'Harmonie de la Chazotte de 1965 à 1974	\N	2020-10-02 13:40:22.151552	2020-10-02 13:40:22.151552
3	Leon	AGUILAR	Pérou	?	-	Leader du groupe de cumbia péruvienne Los Illusionistas, guitariste et compositeur. La colegiala reste le tude qui l'un des morceaux péruviens les plus connus dans le monde, réutilisé par de nombreux artistes.	\N	2020-10-02 13:40:22.153628	2020-10-02 13:40:22.153628
4	J.	ALAZAR	\N	\N	\N	\N	\N	2020-10-02 13:40:22.155517	2020-10-02 13:40:22.155517
5	Tomaso	ALBINONI	Italie	1671	1750	Fils de commerçant italien, il suit en parallèle des études de musique sans prétention. Compose quelques musiques religieuses, puis un opera. Il ouvre une école de chant vers 1710 atteint l'apogée de son œuvre vers 1720. Il n'a pas composé le celèbre adagio, c'est Remo Giazotto (1910 – 1998), un éminent spécialiste de Albinoni qui l'a composé en 1945.	\N	2020-10-02 13:40:22.15722	2020-10-02 13:40:22.15722
6	Karl	ALEXANDER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.159052	2020-10-02 13:40:22.159052
7	Gabriel	ALLIER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.160726	2020-10-02 13:40:22.160726
8	Jean-Claude	AMIOT	France	\N	\N	Directeur du conservatoire de Macon, arrangeur	\N	2020-10-02 13:40:22.16244	2020-10-02 13:40:22.16244
9	Leroy	ANDERSON	Etats Unis	1908	1975	Compositeur reconnu pour ses pièces descriptives empruntant des sons et des rythmes aux musique traditionnelles du monde entier.	\N	2020-10-02 13:40:22.164255	2020-10-02 13:40:22.164255
10	Fernand	ANDRIEU	\N	\N	\N	\N	\N	2020-10-02 13:40:22.165854	2020-10-02 13:40:22.165854
11	Paul	ANKA	\N	\N	\N	\N	\N	2020-10-02 13:40:22.167315	2020-10-02 13:40:22.167315
12	Bert	APPERMONT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.168785	2020-10-02 13:40:22.168785
13	A. den	AREND	\N	\N	\N	\N	\N	2020-10-02 13:40:22.170173	2020-10-02 13:40:22.170173
14	Rob	ARES	Belgique	1942	2015	Pseudonyme de André Waignein.	\N	2020-10-02 13:40:22.171772	2020-10-02 13:40:22.171772
15	Harold	ARLEN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.173412	2020-10-02 13:40:22.173412
16	Fred	ARMBRUESTER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.17494	2020-10-02 13:40:22.17494
17	Franco	ARRIGONI	Italie	1961	-	Diplomé de trompette et instrumenet d'harmonie, professeur d'éducation musicale. Chef d'orchestre en Suisse, compositeur et arrangeur pour la musique d'harmonie, édité chez les grandes maisons d'édition italiennes. Mis en avant en France lors du concours de composition de la CMF en 2001.	\N	2020-10-02 13:40:22.176478	2020-10-02 13:40:22.176478
18	Alessio	ARTONI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.178055	2020-10-02 13:40:22.178055
19	A.	ARUTUNIAN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.179588	2020-10-02 13:40:22.179588
20	Thomas	ASANGER	Autriche	1988	-	Apprend le Basson et le piano en Autriche, puis passe a service de la musique militaire, en train de finir ses études	\N	2020-10-02 13:40:22.18119	2020-10-02 13:40:22.18119
21	Peter Van	ASTEN	Pays Bas	1951	-	Producteur et Compositeur variété, travaillant beaucoup en collaboration avec Richard de Bois. Travaille sous le pseudo Peter Bewley notamment dans les années 70	\N	2020-10-02 13:40:22.182821	2020-10-02 13:40:22.182821
22	Zane Van	AUKEN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.184369	2020-10-02 13:40:22.184369
23	Edmond	AVON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.185917	2020-10-02 13:40:22.185917
24	Charles	AZNAVOUR	France	1924	-	\N	\N	2020-10-02 13:40:22.187523	2020-10-02 13:40:22.187523
25	Jean Sebastien	BACH	Allemagne	1685	1750	Musicien virtuose (violon, alto, clavecin, orgue). Apprend la musique avec son père (Johann Ambrosius), puis son frere ainé (Johann Christoph). Connu pour ses fugues, représentant de la tradition musicale baroque.	\N	2020-10-02 13:40:22.189162	2020-10-02 13:40:22.189162
26	Pierre	BACHELET	\N	\N	\N	\N	\N	2020-10-02 13:40:22.190728	2020-10-02 13:40:22.190728
27	Klaus	BADELT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.192117	2020-10-02 13:40:22.192117
28	Andre de	BAEREMAEKER	?	?	?	Arrangeur de pièce diverses sur la deuxième moitié du Xxe Siècle. Pas plus d'info trouvé sur internet.	\N	2020-10-02 13:40:22.19351	2020-10-02 13:40:22.19351
29	Heinrich Josef	BAERMANN	Allemagne	1784	1847	Fils de militaire, il fait ses étude comme hautboiste à l'école de musique militaire. A 14 ans dans l'amée prussienne, il débute la clarinette. Clarinettiste et compositeur, grand interprète, père du compositeur Carl Baermann. Carl Maria von Weber écrira pour lui plusieurs œuvre pour clarinette, ainsi que Meyerbeer et Mendelssohn.	\N	2020-10-02 13:40:22.195073	2020-10-02 13:40:22.195073
30	Guillaume	BALAY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.196726	2020-10-02 13:40:22.196726
31	J. Ed.	BARAT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.198373	2020-10-02 13:40:22.198373
32	Samuel	BARBER	Etas Unis	1910	1981	Commence tot la musique, puis étudie à l'Institut Curtis, et à l'Académie Américaine de Rome. En 1936 il écrit pour quatuor à corde. Plus tard, il arrangera le second mouvement de ce quatuor pour orchestre à corde, et l'appelera Adagio for Strings. Surtout connu pour cette oauvre, le reste de sa composition peut être décrite de néo-romantique. Il reste connu comme l'un des compositeur américain les plus talentueux.	\N	2020-10-02 13:40:22.199874	2020-10-02 13:40:22.199874
33	Warren	BARKER	Etas Unis	1923	2006	Originaire de Californie, saxophoniste de formation, à 24 ans il travaille comme compositeur arrangeur, en lien avec la radio, la télé, ou le cinema. Il travaillera également pour l'armée, et des maisons d'éditions américaines telles que Barnhouse.	\N	2020-10-02 13:40:22.201392	2020-10-02 13:40:22.201392
34	Th.	BARNIER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.203087	2020-10-02 13:40:22.203087
35	Ary	BARROSO	\N	\N	\N	\N	\N	2020-10-02 13:40:22.2052	2020-10-02 13:40:22.2052
36	John	BARRY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.207374	2020-10-02 13:40:22.207374
37	F.	BAZIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.209791	2020-10-02 13:40:22.209791
38	Gilbert	BECAUD	\N	\N	\N	\N	\N	2020-10-02 13:40:22.217929	2020-10-02 13:40:22.217929
39	Sidney	BECHET	\N	\N	\N	\N	\N	2020-10-02 13:40:22.219559	2020-10-02 13:40:22.219559
40	Wil	VAN DER BEEK	\N	\N	\N	\N	\N	2020-10-02 13:40:22.221222	2020-10-02 13:40:22.221222
41	Ludwig Van	BEETHOVEN	Allemagne	1770	1827	Formé par son père, puis auprès des grands classique à Vienne, il travail à l'évolution vers le romantisme, œuvre marquée par la perte de ses parents, et sa surdité à 27 ans.	\N	2020-10-02 13:40:22.222796	2020-10-02 13:40:22.222796
42	Rene	BELLION	\N	\N	\N	\N	\N	2020-10-02 13:40:22.224616	2020-10-02 13:40:22.224616
43	Michel	BERGER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.226281	2020-10-02 13:40:22.226281
44	Meredith	BERGWILLER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.227916	2020-10-02 13:40:22.227916
45	Robert Van	BERINGER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.229568	2020-10-02 13:40:22.229568
46	Hector	BERLIOZ	France	1803	1869	Son père docteur, le prédestine à la médécine, mais lui offre en parallèle des cours de chant et de flûte, puis la guitare. Dès 12 ans il commence à compposer. En 1823 il quitte ses études de musique, se brouille avec sa famille et étudie la composition au conservatoire de Paris.Renouvelle la forme symphonique après Beethoven, avec la musique à programme, il contribue à l'origine des grands mouvements nationalistes musicaux, publi son traité de l'orchestration, éminent représentatnt du romantisme européen.	\N	2020-10-02 13:40:22.231207	2020-10-02 13:40:22.231207
47	Frank	BERNAERTS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.232946	2020-10-02 13:40:22.232946
48	Yannick	BERNE	France	1973	-	Issu du conservatoire de Lyon (CNR), il officie en tant que Chef du choeur Symphonia à St Galmier dans la Loire (42) depuis 1994. Il arrange alors quelqeus pièces pour harmonie et choeur.	\N	2020-10-02 13:40:22.234692	2020-10-02 13:40:22.234692
49	Leonard	BERNSTEIN	\N	1918	1990	\N	\N	2020-10-02 13:40:22.236778	2020-10-02 13:40:22.236778
50	Pierre	BIGOT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.238837	2020-10-02 13:40:22.238837
115	Marcel	CHAPUIS	France	1941	-	Licencié en Musicologie, prof d'éducation Musical (Fédération Régionale des sociétés Musicales du Nord et du Pas de Calais	\N	2020-10-02 13:40:22.386027	2020-10-02 13:40:22.386027
51	Georges	BIZET	France	1838	1875	Débute la musique avec sa mère. Puis rentre au conservatoire en piano, apprend l'orgue et la composition. Décède d'une crise cardiaque la nuit suivant la 33eme représentation de carmen à 37 ans. Carmen et L'arlesienne restent les deux opéras qui marquent son œuvre et le répertorie français.	\N	2020-10-02 13:40:22.240684	2020-10-02 13:40:22.240684
52	Otis	BLACKWELL	\N	\N	\N	\N	\N	2020-10-02 13:40:22.242561	2020-10-02 13:40:22.242561
53	M.	BLEGER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.244274	2020-10-02 13:40:22.244274
54	L.	BLEMANT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.24587	2020-10-02 13:40:22.24587
55	Lorenzo	BOCCI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.247591	2020-10-02 13:40:22.247591
56	Jay	BOCOOK	Etats Unis	\N	\N	Formation Musicale en Floride	\N	2020-10-02 13:40:22.249592	2020-10-02 13:40:22.249592
57	Bernhard G. M.	BOGISCH	Pays Bas	1932	-	Il commence le piano à 6 ans, étudie le piano, puis s'engage dans la musique militaire canadienne, trompettiste pianiste, et parourt le monde. Puis à son retour au Pays Bas il joue dans des ensemble de musique de l'est. Au sein de la musique militaire canadienne, il officie ensuite en tant que chef d'orchestre, puis est chargé de l'enseignement (instrumentation, direction, composition,...).	\N	2020-10-02 13:40:22.251807	2020-10-02 13:40:22.251807
58	François Adrien	BOIELDIEU	France	1775	1834	\N	\N	2020-10-02 13:40:22.25459	2020-10-02 13:40:22.25459
59	Richard de	BOIS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.257081	2020-10-02 13:40:22.257081
60	Felix	BOISSON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.25895	2020-10-02 13:40:22.25895
61	Paul	BOISTELLE	France	1936	-	Etudes musicale à Strasbourg, solide formation de direction et de pédagogie en Allemagne	\N	2020-10-02 13:40:22.260639	2020-10-02 13:40:22.260639
62	Claude	BOLLING	\N	\N	\N	Musique du film Le mur de l'Atlantique en 1970	\N	2020-10-02 13:40:22.262414	2020-10-02 13:40:22.262414
63	Jean Michel	BONDEAUX	\N	\N	\N	\N	\N	2020-10-02 13:40:22.264103	2020-10-02 13:40:22.264103
64	Paul	BONNEAU	\N	\N	\N	\N	\N	2020-10-02 13:40:22.265757	2020-10-02 13:40:22.265757
65	V.	BONNELLE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.267299	2020-10-02 13:40:22.267299
66	Jean-François	BONURA	France	\N	-	Direction de l'Harmonie de la Chazotte de 1988 à 1998. Flutiste de formation, puis professeur de Tuba à l'Ecole de musique de la Talaudière	\N	2020-10-02 13:40:22.268649	2020-10-02 13:40:22.268649
67	Roger	BOQUET	\N	\N	\N	\N	\N	2020-10-02 13:40:22.270125	2020-10-02 13:40:22.270125
68	E. A.	BORDA	\N	\N	\N	\N	\N	2020-10-02 13:40:22.273066	2020-10-02 13:40:22.273066
69	Elliot A Del	BORGO	\N	\N	\N	\N	\N	2020-10-02 13:40:22.275787	2020-10-02 13:40:22.275787
70	Alexandre	BORODINE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.277748	2020-10-02 13:40:22.277748
71	Auguste	BOSC	\N	\N	\N	\N	\N	2020-10-02 13:40:22.28017	2020-10-02 13:40:22.28017
72	Alfred	BÖSENDORFER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.282419	2020-10-02 13:40:22.282419
73	Menno	BOSGRA	\N	\N	\N	\N	\N	2020-10-02 13:40:22.28417	2020-10-02 13:40:22.28417
74	L.	BOUCARD	\N	\N	\N	\N	\N	2020-10-02 13:40:22.285828	2020-10-02 13:40:22.285828
75	J.	BOUCHEL	\N	\N	\N	Chef de musique militaire, chevalier de la légion d'honneur, à la retraite il arrange Sylvia de Leo Delibes.	\N	2020-10-02 13:40:22.287471	2020-10-02 13:40:22.287471
76	Yves	BOUILLOT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.288939	2020-10-02 13:40:22.288939
77	Rene	BOURBON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.290648	2020-10-02 13:40:22.290648
78	Adolphe	BOURDEAU	\N	\N	\N	\N	\N	2020-10-02 13:40:22.292596	2020-10-02 13:40:22.292596
79	Jean Pierre	BOURQUIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.294634	2020-10-02 13:40:22.294634
80	Jean Pierre	BOURTAYRE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.296723	2020-10-02 13:40:22.296723
81	P.	BOUTOT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.299423	2020-10-02 13:40:22.299423
82	Roger	BOUTRY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.301763	2020-10-02 13:40:22.301763
83	Felix	BOYER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.303503	2020-10-02 13:40:22.303503
84	Johannes	BRAHMS	\N	1833	1897	\N	\N	2020-10-02 13:40:22.305734	2020-10-02 13:40:22.305734
85	Celino	BRATTI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.310656	2020-10-02 13:40:22.310656
86	Tom	BREVIK	\N	\N	\N	\N	\N	2020-10-02 13:40:22.315384	2020-10-02 13:40:22.315384
87	Heinz	BRIEGEL	\N	\N	\N	\N	\N	2020-10-02 13:40:22.319783	2020-10-02 13:40:22.319783
88	Jean	BROUQUIERES	\N	\N	\N	Orchestrateur	\N	2020-10-02 13:40:22.322841	2020-10-02 13:40:22.322841
89	Jerry	BRUBAKER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.325814	2020-10-02 13:40:22.325814
90	Anton	BRUCKNER	Autriche	1824	1896	Combine la carrière d'instituteur et d'organiste d'église. Compositeur post romantique, il compose surtout des œuvres religieuses. Ses compositions le rapproche parfois de Wagner. Termine sa vie à Vienne, où il a comme élève Gustav Malher.	\N	2020-10-02 13:40:22.328329	2020-10-02 13:40:22.328329
91	A.	BRUNEAU	\N	\N	\N	\N	\N	2020-10-02 13:40:22.330791	2020-10-02 13:40:22.330791
92	Ch.	BUGNOT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.332929	2020-10-02 13:40:22.332929
93	Jules	BUISSON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.334927	2020-10-02 13:40:22.334927
94	Gert	BUITENHUIS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.336805	2020-10-02 13:40:22.336805
95	Stephen	BULLA	Etats Unis	\N	\N	Membre de l'ASCAP (American Society of Composers, Authrs and Publishers)	\N	2020-10-02 13:40:22.339011	2020-10-02 13:40:22.339011
96	Jack	BULLOCK	\N	\N	\N	\N	\N	2020-10-02 13:40:22.34127	2020-10-02 13:40:22.34127
97	Jerry	BURNS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.343493	2020-10-02 13:40:22.343493
98	John	CACAVAS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.346158	2020-10-02 13:40:22.346158
99	Giulio	CACCINI	Italie	1551	1618	Compositeur italien du début de la période baroque, au service des Médicis en 1565. Ses compositions (Madrigaux, sonnets) sont novateurs et introduisent les ornemant du bel canto. Cherchant à améliorer la musique pour représenter l'ame humaine (inspiration du monde antique) cela l'amène à l'opéra. Il est connu à tord pour avoir composé un Avé Maria, mais ce serait une méprise...	\N	2020-10-02 13:40:22.348276	2020-10-02 13:40:22.348276
100	Silvio	CALIGARIS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.350625	2020-10-02 13:40:22.350625
101	B.	CAMPORELLI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.355102	2020-10-02 13:40:22.355102
102	Paul	CAPPE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.358301	2020-10-02 13:40:22.358301
103	Roland	CARDON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.361194	2020-10-02 13:40:22.361194
104	Daniele	CARNEVALI	Italie	1957	-	Etude à Parme, trompette	\N	2020-10-02 13:40:22.363879	2020-10-02 13:40:22.363879
105	Alexandre	CARTERON	France	18...	1940	compositeur et arrangeur	\N	2020-10-02 13:40:22.366492	2020-10-02 13:40:22.366492
106	Rene	CASTELAIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.368759	2020-10-02 13:40:22.368759
107	Leonello & Spencer	CASUCCI & WILLIAMS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.370868	2020-10-02 13:40:22.370868
108	Terry	CATHRINE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.372897	2020-10-02 13:40:22.372897
109	François	CATTIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.374871	2020-10-02 13:40:22.374871
110	Franco	CESARINI	Suisse	1961	-	Issu du conservatoire de Milan, flute traversiere et piano, puis à Bale	\N	2020-10-02 13:40:22.376666	2020-10-02 13:40:22.376666
111	E.	CHABRIER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.378311	2020-10-02 13:40:22.378311
112	Cecile	CHAMINADE	France	1857	1944	Issu d'une famille parisienne aisée, elle présente une affinité pour le piano. Sous les conseil de Georges Bizet ami de la famille, elle entre au conservatoire. Elle épousera un éditeur de musique. Elle composera des pièce romantique notamment pour piano.	\N	2020-10-02 13:40:22.38004	2020-10-02 13:40:22.38004
113	Jean	CHAMPEL	France	\N	\N	Chevalier de la Legion d'honneur, officier de l'instruction publique	\N	2020-10-02 13:40:22.38217	2020-10-02 13:40:22.38217
114	Charly	CHAPLIN	Angleterre	1889	1977	Issu d'un milieu très modeste, bercé par le music hall, il se fera un nom dans le domaine du cinéma muet, et son personnage de Charlot. Dès l'enfance il se passione pour a musique, apprends seul le piano, le violon et le violoncelle. Il composera, ou proposera à d'autres compositeurs, les musiques et mélodies des bandes originales de ses films.	\N	2020-10-02 13:40:22.384047	2020-10-02 13:40:22.384047
116	Ch.	CHARLIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.388385	2020-10-02 13:40:22.388385
117	Jay	CHATTAWAY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.39092	2020-10-02 13:40:22.39092
118	Tony	CHESEAUX	\N	\N	\N	\N	\N	2020-10-02 13:40:22.392852	2020-10-02 13:40:22.392852
119	Leon	CHIC	\N	\N	\N	\N	\N	2020-10-02 13:40:22.394788	2020-10-02 13:40:22.394788
120	Leonce	CHOMEL	\N	\N	\N	Chef de musique de 1ere classe au 51eme de Ligne	\N	2020-10-02 13:40:22.396646	2020-10-02 13:40:22.396646
121	Frederic	CHOPIN	Pologne	1809	1849	Etudie le piano et la composition en Pologne, puis emigre en France, compagnon de George Sand, un des plus célèbre pianiste de la période romantique	\N	2020-10-02 13:40:22.398617	2020-10-02 13:40:22.398617
122	James	CHRISTENSEN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.400513	2020-10-02 13:40:22.400513
123	L.	CHRISTOL	\N	\N	\N	\N	\N	2020-10-02 13:40:22.402372	2020-10-02 13:40:22.402372
124	Frank	CHURCHILL	\N	\N	\N	\N	\N	2020-10-02 13:40:22.404175	2020-10-02 13:40:22.404175
125	Johnny	CLEGG	\N	\N	\N	\N	\N	2020-10-02 13:40:22.405882	2020-10-02 13:40:22.405882
126	V.	CLOWEZ	\N	\N	\N	Chef de Musique de la 2eme D. B.	\N	2020-10-02 13:40:22.407643	2020-10-02 13:40:22.407643
127	/	COARD	\N	\N	\N	\N	\N	2020-10-02 13:40:22.409416	2020-10-02 13:40:22.409416
128	\N	COGNET	France	?	?	Second chef à l'Harmonie de la Chazotte de 1977 à 1980.	\N	2020-10-02 13:40:22.411057	2020-10-02 13:40:22.411057
129	Roger	COITTEUX	\N	\N	\N	\N	\N	2020-10-02 13:40:22.412903	2020-10-02 13:40:22.412903
130	Marc	COLANT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.41465	2020-10-02 13:40:22.41465
131	Phil	COLLINS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.41652	2020-10-02 13:40:22.41652
132	Michel	COLOMBIER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.418248	2020-10-02 13:40:22.418248
133	/	COMPAGNONS DE LA CHANSON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.419947	2020-10-02 13:40:22.419947
134	O.	COQUELET	\N	\N	\N	\N	\N	2020-10-02 13:40:22.421635	2020-10-02 13:40:22.421635
135	Silvio	CORIGLIONE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.423351	2020-10-02 13:40:22.423351
136	Georges	CORROYEZ	France	\N	\N	Chef de musique du 110eme Régiment d'infanterie	\N	2020-10-02 13:40:22.425239	2020-10-02 13:40:22.425239
137	Vladimir	COSMA	\N	\N	\N	\N	\N	2020-10-02 13:40:22.42719	2020-10-02 13:40:22.42719
138	James	COWER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.429194	2020-10-02 13:40:22.429194
139	Alain	CREPIN	Belgique	1954	-	Etudie saxophone, violoncelle et piano, rentre au conservatoire royale de musique de Bruxelles, Dirige la musique Royale aérienne	\N	2020-10-02 13:40:22.431224	2020-10-02 13:40:22.431224
140	Douglas	CUOMO	\N	\N	\N	\N	\N	2020-10-02 13:40:22.433567	2020-10-02 13:40:22.433567
141	James	CURNOW	\N	\N	\N	\N	\N	2020-10-02 13:40:22.435511	2020-10-02 13:40:22.435511
142	Jim	CURNOW	\N	\N	\N	\N	\N	2020-10-02 13:40:22.437332	2020-10-02 13:40:22.437332
143	Calvin	CUSTER	Etas Unis	1939	1998	Issue de l'Université de Syracuse ou il étudie la composition et la direction. Il restera associé à son université de départ toute sa carrière an tant que corniste, claviériste, bassiste, second chef, arrangeur. En parallèle, il développe le programme de l'orchestre de musique de chambre, aussi bien que celui du rock ou des percussions.	\N	2020-10-02 13:40:22.439102	2020-10-02 13:40:22.439102
144	J.	DADOR	\N	\N	\N	\N	\N	2020-10-02 13:40:22.441697	2020-10-02 13:40:22.441697
145	John	DARLING	France	1905	1922	Pseudonyme de Laurent Delbecq	\N	2020-10-02 13:40:22.444002	2020-10-02 13:40:22.444002
146	Joe	DASSIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.446384	2020-10-02 13:40:22.446384
147	Louis	DAUNOT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.448626	2020-10-02 13:40:22.448626
148	Johan de	MEY	Pays Bas	1953	-	autrement appelé Johan de Meij	\N	2020-10-02 13:40:22.450912	2020-10-02 13:40:22.450912
149	Pierre	DEANT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.4533	2020-10-02 13:40:22.4533
150	Serge	DEBIEVRE	France	1952	-	Il étudie la clarinette à l'école Massenet, puis au conservatoire de St Etienne. Il sera à la direction de l'Harmonie de la Chazotte de 1978 à 1980. Il dirigera les harmonie de Boen, de St Chamond, d'Ambert et de St Etienne.	\N	2020-10-02 13:40:22.455518	2020-10-02 13:40:22.455518
151	Claude	DEBUSSY	France	1862	1918	Orienté vers la musique par sa tante, et présentant des facilités, il débute le piano avec un ami de son père, puis entre au conservatoire de Paris. Une part importante de son œuvre est pour piano, de style avant tout avant gardiste.	\N	2020-10-02 13:40:22.457455	2020-10-02 13:40:22.457455
152	Laurent	DELBECQ	France	1905	1992	D'un père pianiste, il debute la musique à 4 ans, oncle organiste, dans la région de Macon. Il travaille alors comme monteur de rayon de byciclette et rejoinds l'Harmonie de Macon. Membre clarinettiste, puis directeur de 1954 à 1970 de l'Harmonie de Macon. Compositeur et arrangeur, fonde son groupe, travaille pour robert Martin. pseudonyme : John Darling pour ses œuvres Jazzy.	\N	2020-10-02 13:40:22.459732	2020-10-02 13:40:22.459732
153	Leo	DELIBES	France	1836	1891	Issu du conservatoire de Paris à l'orgue et pour la composition, choriste, il est à l'origine des « ballets symphoniques ».	\N	2020-10-02 13:40:22.461714	2020-10-02 13:40:22.461714
154	Marc	DELMAS	France	1885	1931	Compositeur français qui a notamment coomposée l'operette Sylvette en 1932	\N	2020-10-02 13:40:22.463538	2020-10-02 13:40:22.463538
155	Jean	DELORME	France	?	1926	Direction de l'Harmonie de la Chazotte de 1893 à 1926. Tristement connu pour son décès en plein concert.	\N	2020-10-02 13:40:22.466001	2020-10-02 13:40:22.466001
156	Jules Auguste Edouard	DEMERSSEMAN	France	\N	\N	Ami de Adolphe Sax, il compose beaucoup pour le saxophone	\N	2020-10-02 13:40:22.468664	2020-10-02 13:40:22.468664
157	Jean-Charles	DENIS	France	1969	-	Commence la musique à l'école de musique de l'Harmonie de la Chazotte en tant que trompetitste, puis au conservatoire de St Etienne, de Paris, vant de rentrer au CNSM de Lyon en trompette moderne et baroque. De 2000 à 2002, il officie en tant que chef d'orchestre à la Chazotte. Trompettiste professionnel au sein de divers ensemble (Le concert Spirituel, la Petite Bande, la Chambre Philharmonique, les Siècles, Bach Collegium Japan,...).	\N	2020-10-02 13:40:22.471332	2020-10-02 13:40:22.471332
158	F.	DEVIENNE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.474111	2020-10-02 13:40:22.474111
159	Jacques	DEVOGEL	France	1926	1995	Issu du conservatoire de Roubaix (piano clarinette) il s'engage dans la musique militaire en 1945, orchestrateur pour les orchestres de jeunes	\N	2020-10-02 13:40:22.476902	2020-10-02 13:40:22.476902
160	Pascal	DEVROYE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.479367	2020-10-02 13:40:22.479367
161	\N	DIMIER	France	\N	\N	Direction de l'Harmonie de la Chazotte de 1890 à 1893	\N	2020-10-02 13:40:22.481796	2020-10-02 13:40:22.481796
162	Vivian	DOMENJOZ	\N	\N	\N	\N	\N	2020-10-02 13:40:22.484129	2020-10-02 13:40:22.484129
163	Désiré	DONDEYNE	France	1921	2015	Formation à Lille en 1936 (clarinette), puis Paris (ecriture) proche de la musique amateur, valorise l'orchestre d'harmonie	\N	2020-10-02 13:40:22.485899	2020-10-02 13:40:22.485899
164	\N	DONIZETTI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.488918	2020-10-02 13:40:22.488918
165	Thomas	DOSS	Autriche	1966	-	Après sa formation en Autriche et en Allemagne, il va aux Etats Unis, travaillera avec John Williams, à 18 ans chef d'orchestre actif	\N	2020-10-02 13:40:22.490655	2020-10-02 13:40:22.490655
166	Guy	DUIJCK	\N	\N	\N	\N	\N	2020-10-02 13:40:22.493033	2020-10-02 13:40:22.493033
167	Pierre	DUPOND	\N	\N	\N	\N	\N	2020-10-02 13:40:22.495323	2020-10-02 13:40:22.495323
168	Auguste	DURAND	\N	\N	\N	\N	\N	2020-10-02 13:40:22.497481	2020-10-02 13:40:22.497481
169	Th	DUREAU	\N	\N	\N	\N	\N	2020-10-02 13:40:22.499484	2020-10-02 13:40:22.499484
170	Antonin Leopold	DVORAK	République Tchèque	1841	1904	Fils de boucher aubergiste, il arrête l'école à 11 ans pour suivre les traces de son père. A 12 ans il part vivre avec son oncle pour apprendre la musique. Puis il joue de l'orgue, chante, et découvre l'orchestre de l'intérieur, sous la direction de Smetana, Wagner,... Compose sa première symphonie en 1865 et se consacre uniquement à la composition. Sa musique reconnue lui permet d'avoir des bourses, de rencontrer Brahms. Il sera même nommé directeur du conservatoire New York en 1892, où il composera sa 9eme symphonie.	\N	2020-10-02 13:40:22.501534	2020-10-02 13:40:22.501534
171	John	EDMONDSON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.503846	2020-10-02 13:40:22.503846
172	Danny	ELFMAN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.506206	2020-10-02 13:40:22.506206
173	Edward	ELGAT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.508857	2020-10-02 13:40:22.508857
174	Duke	ELLINGTON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.511559	2020-10-02 13:40:22.511559
175	Fernand	ERBSLAND	\N	\N	\N	\N	\N	2020-10-02 13:40:22.514875	2020-10-02 13:40:22.514875
176	Frank	ERICKSON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.517561	2020-10-02 13:40:22.517561
177	Ch.	EUSTACE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.521158	2020-10-02 13:40:22.521158
178	Harm	EVERS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.52394	2020-10-02 13:40:22.52394
179	Maurice	FAILLENOT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.526266	2020-10-02 13:40:22.526266
180	J.	FARIGOUL	\N	\N	\N	Chef de Musique des équipages de la Flotte à Brest	\N	2020-10-02 13:40:22.528622	2020-10-02 13:40:22.528622
181	Gabriel	FAURE	France	1845	1924	Organiste, Pianiste, compositeur. Eleve de Saint-Saens et de Gustave Lefevre à Paris. Organiste, maitre de chapelle, professeur de composition au conservatoire de Paris, puis directeur de l'établissement en 1905. Dans ces élèves Maurice Ravel.	\N	2020-10-02 13:40:22.530822	2020-10-02 13:40:22.530822
182	Pierre	FAURE	France	19...	/	Arrangeur, notamment pour les éditions Robert Martin.	\N	2020-10-02 13:40:22.533801	2020-10-02 13:40:22.533801
183	Pascal	FAVRE	Suisse	1949	-	Musicien, compositeur, trompettiste chef d'harmonie, débute ses études musicale à 21 ans au conservatoire de Lausanne. Son approche de la pédagogie l'amène à l'enseignement, professeur de direction.  Appelé en tant que jury pour de nombreux exament de direction européens.	\N	2020-10-02 13:40:22.535576	2020-10-02 13:40:22.535576
184	D.	FEKARIS & PERREN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.53839	2020-10-02 13:40:22.53839
185	Henri	FERNAND	\N	\N	\N	\N	\N	2020-10-02 13:40:22.540072	2020-10-02 13:40:22.540072
186	Ferrer	FERRAN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.542835	2020-10-02 13:40:22.542835
187	Nino	FERRER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.545948	2020-10-02 13:40:22.545948
188	H.	FEVRIER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.549615	2020-10-02 13:40:22.549615
189	B. E. W. J.	FEYNE HAWKINS JOHNSON DASH	\N	\N	\N	\N	\N	2020-10-02 13:40:22.552715	2020-10-02 13:40:22.552715
190	O.	FILSFILS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.555134	2020-10-02 13:40:22.555134
191	\N	FLORENTIN	France	?	?	Direction de l'Harmonie de la Chazotte de 1883 à 1886	\N	2020-10-02 13:40:22.556793	2020-10-02 13:40:22.556793
192	Friedrich Von	FLOTOW	Allemagne	1812	1883	Issu d'une famille aristocratique allemande, il viendra par la suite faire une partie de ses études musicales à Paris. Son œuvre comporte surtout des opéras, dont le plus connu Martha, et quelques ballets, joués partout en Europe.	\N	2020-10-02 13:40:22.559396	2020-10-02 13:40:22.559396
193	Trevor J.	FORD	Australie	1931	-	Pianiste à 5 ans, violoniste à 12, puis hautboiste dans l'orchestre de la Marine. Chef d'orchestre en parallèle, il part en Norvège pour enseigner aux chefs d'orchestre	\N	2020-10-02 13:40:22.56203	2020-10-02 13:40:22.56203
194	Ralph	FORD	\N	\N	\N	\N	\N	2020-10-02 13:40:22.564475	2020-10-02 13:40:22.564475
195	Félicien	FORET	\N	\N	\N	\N	\N	2020-10-02 13:40:22.566655	2020-10-02 13:40:22.566655
196	Jose de	FORTEZ	\N	\N	\N	\N	\N	2020-10-02 13:40:22.568947	2020-10-02 13:40:22.568947
197	Fernando	FRANCIA	\N	\N	\N	\N	\N	2020-10-02 13:40:22.571415	2020-10-02 13:40:22.571415
198	Cesar	FRANCK	\N	\N	\N	\N	\N	2020-10-02 13:40:22.573517	2020-10-02 13:40:22.573517
199	G.	FRANOT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.575307	2020-10-02 13:40:22.575307
200	Willy	FRANSEN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.576916	2020-10-02 13:40:22.576916
201	Max C.	FREEDMAN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.578707	2020-10-02 13:40:22.578707
202	Herve	FREYCENON	France	1968	-	Issu du conservatoire de St Etienne, de Lyon, de Lougansk en Ukraine. Chef d'orchestre de l'Harmonie de Roche la Molière (Loire 42), arrangeur des Corons notamment.	\N	2020-10-02 13:40:22.580369	2020-10-02 13:40:22.580369
203	Charles	FRISON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.582206	2020-10-02 13:40:22.582206
204	Julius	FUCIK	\N	\N	\N	\N	\N	2020-10-02 13:40:22.583734	2020-10-02 13:40:22.583734
205	Michel	FUGAIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.585558	2020-10-02 13:40:22.585558
206	J.	FURGEOT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.587254	2020-10-02 13:40:22.587254
207	Thierry	GAILLARD	France	1963	-	Issu du conservatoire de St Etienne, corniste. Soliste au nouel orchestre de st Etienne.	\N	2020-10-02 13:40:22.588989	2020-10-02 13:40:22.588989
208	Louis	GANNE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.590819	2020-10-02 13:40:22.590819
209	Charles	GARCIA	\N	\N	\N	\N	\N	2020-10-02 13:40:22.592832	2020-10-02 13:40:22.592832
210	Georges	GARVARENTZ – AZNAVOUR	\N	\N	\N	\N	\N	2020-10-02 13:40:22.59514	2020-10-02 13:40:22.59514
211	E.	GAUDIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.597012	2020-10-02 13:40:22.597012
212	Jean-Adolphe	GAUTHIER	France	\N	\N	Second chef de l'Harmonie de la Chazotte de 1925 à 1975, clarinettiste et professeur de nombreuses générations.	\N	2020-10-02 13:40:22.599281	2020-10-02 13:40:22.599281
213	Bob	GAY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.601648	2020-10-02 13:40:22.601648
214	J.	GAY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.603676	2020-10-02 13:40:22.603676
215	Giancarlo	GAZZANI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.606339	2020-10-02 13:40:22.606339
216	George	GERSHWIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.608785	2020-10-02 13:40:22.608785
217	Luigi di	GHISALLO	Pays Bas	1938	2014	Pseudonyme de Cornelis Kees Vlak	\N	2020-10-02 13:40:22.611035	2020-10-02 13:40:22.611035
218	Guillaume	GIBERT	France	1985	-	Suit une formation de percussioniste au Conservatoire de St Etienne (CNR), puis cautoie l'ENM de Créteil, le PESM de Bourgogne. Il officiera comme professeur de percussion à l'EMAD Berlioz, et acceptera la direction de l'Harmonie de la Chazotte pour une saison de 2012 à 2013. En parallèle, il joue dans des groupes tels que Alkabaya (chanson française) et Toadstool (Heavy Metal).	\N	2020-10-02 13:40:22.612903	2020-10-02 13:40:22.612903
219	\N	GILLIER	France	?	?	Direction de l'Harmonie de la Chazotte de 1926 à 1931	\N	2020-10-02 13:40:22.614772	2020-10-02 13:40:22.614772
220	Giuseppe	GIORDANI	Italie	1751	1798	Etudie la musique à Naples, nommé maitre de la chapelle de Naples, compositeur et enseignant au sein de sa chapelle, il écrit en parallèle opéra, oratorio, œuvres sacrées. L'oeuvre Caro mioBen elui est généralement attribué, mais à tort, l'oeuvre serait antérieur de 50 ans, écrite par con frère Tomaso Giordani.	\N	2020-10-02 13:40:22.616789	2020-10-02 13:40:22.616789
221	Auguste	GIRONCE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.618817	2020-10-02 13:40:22.618817
222	John	GLENESK MORTIMER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.62078	2020-10-02 13:40:22.62078
223	Christoph Willibald Von	GLUCK	Allemagne	1715	1787	Compositeur Classique d'opéra, rforme ce style musicale. Issu d'une bonne famille allemande, apprend la musique en cachette, son père s'y opposant.. Puis il parours l'Europe pour apprendre.	\N	2020-10-02 13:40:22.622416	2020-10-02 13:40:22.622416
224	G. C. D	GOFFIN KING LIPSIUS THOMAS HALLLIGAN KATZ	\N	\N	\N	\N	\N	2020-10-02 13:40:22.624362	2020-10-02 13:40:22.624362
225	P.	GOGUILLOT	\N	\N	\N	Chef de la musique des équipages de la flotte de Toulon	\N	2020-10-02 13:40:22.626435	2020-10-02 13:40:22.626435
226	Ernest	GOLD	\N	\N	\N	\N	\N	2020-10-02 13:40:22.628462	2020-10-02 13:40:22.628462
227	Jean Jacques	GOLDMAN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.630554	2020-10-02 13:40:22.630554
228	John	GOLLAND	Angleterre	1942	1993	Compositeur, arrangeur et directeur de brass band. Il a beaucoup travaillé pour le répertoire des Brass Band, écrivant pour l'euphonium (Peace) ou le bariton. Il meurt jeune à 50 ans, laissat une œuvre méconnue.	\N	2020-10-02 13:40:22.632898	2020-10-02 13:40:22.632898
229	\N	GOLOVIEW SEDOY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.635659	2020-10-02 13:40:22.635659
230	Michael	GORE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.638144	2020-10-02 13:40:22.638144
231	François Joseph	GOSSEC	France	1734	1829	\N	\N	2020-10-02 13:40:22.641779	2020-10-02 13:40:22.641779
232	Yo	GOTOH	\N	\N	\N	\N	\N	2020-10-02 13:40:22.643745	2020-10-02 13:40:22.643745
233	Charles	GOUNOD	France	1818	1893	Etudie la musique à Paris, puis part en Italie et compose ses premières œuvres religieuses, puis de retour à Paris des opéras. Mouvement orphéonique	\N	2020-10-02 13:40:22.646305	2020-10-02 13:40:22.646305
234	Jos	GRAFF	\N	\N	\N	\N	\N	2020-10-02 13:40:22.649343	2020-10-02 13:40:22.649343
235	\N	GRETRY	\N	1741	1813	\N	\N	2020-10-02 13:40:22.652335	2020-10-02 13:40:22.652335
236	Edvard	GRIEG	Norvege	1843	1907	Issu d'une famille de musicien, compose dès 9 ans, formation classique et romantique, pianiste,, influencé par le folklore nationnal norvégien.	\N	2020-10-02 13:40:22.655161	2020-10-02 13:40:22.655161
237	E.	GROGNET	\N	\N	\N	Chef de musique du 113eme régiment d'infanterie	\N	2020-10-02 13:40:22.657181	2020-10-02 13:40:22.657181
238	Charles R.	GUIDRY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.65934	2020-10-02 13:40:22.65934
239	André	GUILLAUME	France	1961	-	Il étudie la clarinette au conservatoire de St Etienne (ENM), puis au conservatoire de Genève. Il prend la Direction de l'école de musique de la Talaudière (ACEM Berlioz, puis EMAD Berlioz, dirige alors l'l'Harmonie de la Chazotte de 1998 à 2012. Clarinettiste à l'opéra de St Etienne, professeur de clarinette, il crée l'association des Clarinettistes de la Loire, Puis va diriger le Brass Band Loire Forez (arrangement des Virtuoses, spectacle Dogora,...).	\N	2020-10-02 13:40:22.661613	2020-10-02 13:40:22.661613
240	\N	GURTNER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.663717	2020-10-02 13:40:22.663717
241	Stig	GUSTAFSON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.666022	2020-10-02 13:40:22.666022
242	Jacob de	HAAN	Pays Bas	1959	-	Auteur compositeur pour orchestre d'harmonie, inspiré par la musique de film, produit également des arrangements de musique classique (pseudonymes Dizzy Stratford, Ron Sebregts, Tony Jabovsky). Frère de Jan de Haan.	\N	2020-10-02 13:40:22.668466	2020-10-02 13:40:22.668466
243	Jan de	HAAN	Pays Bas	1951	-	Initié à la musique par son père amateur de musique, il s'oriente rapidement sur la direction d'orchestre à vent, étudie le trombone, et la direction d'orchestre. Frère de Jacob de Haan.	\N	2020-10-02 13:40:22.670591	2020-10-02 13:40:22.670591
244	Jean Pierre	HAECK	\N	\N	\N	\N	\N	2020-10-02 13:40:22.672855	2020-10-02 13:40:22.672855
245	Georg Friedrich	HAENDEL	Allemagne	1685	1759	Formation à l'orgue clavecin et composition, de style baroque, il commence en Allemagne, puis va en italie pour enfin s'installer en Angleterre. Il compose des opera, des oratorios. Aveugle à la fin de ses jours	\N	2020-10-02 13:40:22.674632	2020-10-02 13:40:22.674632
246	Earle	HAGEN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.677465	2020-10-02 13:40:22.677465
247	Johnny	HALLIDAY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.679176	2020-10-02 13:40:22.679176
248	Marvin	HAMLISCH	\N	\N	\N	\N	\N	2020-10-02 13:40:22.681447	2020-10-02 13:40:22.681447
249	George Frideric	HANDEL	\N	\N	\N	\N	\N	2020-10-02 13:40:22.683743	2020-10-02 13:40:22.683743
250	Ronan	HARDIMAN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.686256	2020-10-02 13:40:22.686256
251	Nicholas	HARE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.688969	2020-10-02 13:40:22.688969
252	Josef	HASTREITER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.69241	2020-10-02 13:40:22.69241
253	Willy	HAUTVAST	\N	\N	\N	\N	\N	2020-10-02 13:40:22.694484	2020-10-02 13:40:22.694484
254	Dan	HAVELY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.696949	2020-10-02 13:40:22.696949
255	Jim	HENSON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.699302	2020-10-02 13:40:22.699302
256	Jerry	HERMAN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.701542	2020-10-02 13:40:22.701542
257	Ralph	HERMANN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.703814	2020-10-02 13:40:22.703814
258	H.	HESSELING	\N	\N	\N	\N	\N	2020-10-02 13:40:22.706086	2020-10-02 13:40:22.706086
259	John	HIGGINS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.708254	2020-10-02 13:40:22.708254
260	William H.	HILL	Etats Unis	1930	2000	Commence la musique à 4 ans avec sa mère, s'oriente vers le saxophone. Fait ses études à l'Université du nord au Colorado, rentre à la US Air Force. Il travaille pour l'université de Californie. Connu pour ses Dances sacrées et profanes. Compositeur pour Barnhouse.	\N	2020-10-02 13:40:22.715701	2020-10-02 13:40:22.715701
261	William	HIMES	Etas Unis	\N	\N	Université du Michigan, tubiste de formation, chef d'orchestre, compositeur (ASCAP)	\N	2020-10-02 13:40:22.71782	2020-10-02 13:40:22.71782
262	Mark H.	HINDSLEY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.719817	2020-10-02 13:40:22.719817
263	\N	HIPPOLYTE	France	?	?	Direction de l'Harmonie de la Chazotte de 1865 à 1883	\N	2020-10-02 13:40:22.721711	2020-10-02 13:40:22.721711
264	Henk	HOGESTEIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.723509	2020-10-02 13:40:22.723509
265	Gustav	HOLST	Angleterre	1874	1934	D'une famille musicienne, il étudie le violon, le piano, puis le trombone. Il commence à composer à 12 ans, sa santé rendant impossible une carrière de pianiste. A 17 ans il commence la direction d'orchestre. Incapable de vivre de ses compositions, il sera en parallèle tromboniste professionnel, puis professeur. Il doit sa notoriété à son œuvre les Planètes après la première guerre mondiale. Sa musique sera négligé jusque dans les années 1980, toutefois elle marquera des compositeurs du XXEme siècle.	\N	2020-10-02 13:40:22.725372	2020-10-02 13:40:22.725372
266	James	HORNER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.7274	2020-10-02 13:40:22.7274
267	James L.	HOSAY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.729634	2020-10-02 13:40:22.729634
268	Ted	HUGGENS	Pays Bas	1928	2006	Compositeur de musique militaire, enseigne au conservatoire d'Amhem, pseudonyme : Henk Van Lijnschooten, ou encore de Michel Van Delft	\N	2020-10-02 13:40:22.73162	2020-10-02 13:40:22.73162
269	Johann Nepomuk	HUMMEL	Allemagne	1778	1837	Elève de Mozart, Haydn et Salieri. Grand pianiste reconnu en Europe, il a surtout composé des œuvres pour piano. Ami de Beethoven.	\N	2020-10-02 13:40:22.733791	2020-10-02 13:40:22.733791
270	Olivier	HUYARD 	\N	\N	\N	\N	\N	2020-10-02 13:40:22.736321	2020-10-02 13:40:22.736321
271	Soren	HYLDGAARD	\N	\N	\N	\N	\N	2020-10-02 13:40:22.738241	2020-10-02 13:40:22.738241
272	Peter	IPPOLITO	\N	\N	\N	\N	\N	2020-10-02 13:40:22.739836	2020-10-02 13:40:22.739836
273	Ivan	IVANOVICI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.741507	2020-10-02 13:40:22.741507
274	Naohiro	IWAI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.743168	2020-10-02 13:40:22.743168
275	Hirotaka	IZUMI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.744894	2020-10-02 13:40:22.744894
276	Michael	JACKSON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.746622	2020-10-02 13:40:22.746622
277	Robert	JAGER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.748329	2020-10-02 13:40:22.748329
278	D.	JANIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.749973	2020-10-02 13:40:22.749973
279	Maurice	JARRE	France	1924	2009	Né à Lyon, compositeur français, écrit des musique de film (Docteur Givago, Ghost, le cercle des poetes disparus, père de Jean Michel Jarre né en 1948. Après son divorce, il part aux états unis. Il s’intéresse assez tard à la musique, timbalier de formation, il débute en 1946 aux coté de Pierre boulez. Sa première musique commandée est en 1948. Il composera pour le théâtre, le cinéma à partir des années 1950. Sa carrière s’envole outre atlantique avec Laurence d’Arabie en 1962.	\N	2020-10-02 13:40:22.751573	2020-10-02 13:40:22.751573
280	David	JAYMES	\N	\N	\N	\N	\N	2020-10-02 13:40:22.753213	2020-10-02 13:40:22.753213
281	Paul	JENNINGS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.754814	2020-10-02 13:40:22.754814
282	Stuart	JOHNSON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.75634	2020-10-02 13:40:22.75634
283	Timothy	JOHNSON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.758012	2020-10-02 13:40:22.758012
284	Trevor	JONES	\N	\N	\N	\N	\N	2020-10-02 13:40:22.759732	2020-10-02 13:40:22.759732
285	Quincy	JONES	\N	\N	\N	\N	\N	2020-10-02 13:40:22.761408	2020-10-02 13:40:22.761408
286	Scott	JOPLIN	Etas Unis	1868	1917	Enfant d'une famille noire au Texas, ses parents jouent un peu de musique. Il décrouvrirait le piano dès 8 ans en autodidacte (peut être chez une famille blanche où sa mère faisait des ménages ?). Son père lui paie un piano et des cours. Il débute sa carrière à St Louis dans le Missouri, centre important du Ragtime. Cornetiste, pianiste dans les clubs, professeur, puis il part pour New York, écrit des ragtime et 2 opéras, fonde sa soiété d'édition. Il meurt de la syphilis dans un hopital psychatrique, reste comme pianiste une icône du Ragtime.	\N	2020-10-02 13:40:22.762853	2020-10-02 13:40:22.762853
287	F.	JULY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.764464	2020-10-02 13:40:22.764464
288	Walter	KALISCHNIG	\N	\N	\N	\N	\N	2020-10-02 13:40:22.766321	2020-10-02 13:40:22.766321
289	John	KANDER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.768216	2020-10-02 13:40:22.768216
290	Fred	KARLIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.770116	2020-10-02 13:40:22.770116
291	Paul	KELSEN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.772277	2020-10-02 13:40:22.772277
292	Larry	KERCHNER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.774985	2020-10-02 13:40:22.774985
293	Roland	KERNEN	Belgique	1642	2015	Pseudonyme de André Waignein.	\N	2020-10-02 13:40:22.776876	2020-10-02 13:40:22.776876
294	Albert W.	KETELBEY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.778665	2020-10-02 13:40:22.778665
295	Peter	KING	\N	\N	\N	\N	\N	2020-10-02 13:40:22.780189	2020-10-02 13:40:22.780189
296	/	KING HENRY VIII	\N	\N	\N	\N	\N	2020-10-02 13:40:22.781911	2020-10-02 13:40:22.781911
297	John	KINYON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.783464	2020-10-02 13:40:22.783464
298	Bjorn Morten	KJAERNES	\N	\N	\N	\N	\N	2020-10-02 13:40:22.785115	2020-10-02 13:40:22.785115
299	Jules	KLEIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.786771	2020-10-02 13:40:22.786771
300	Conrad	KOCHER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.788398	2020-10-02 13:40:22.788398
301	Jan Van	KRAEYDONCK	\N	\N	\N	\N	\N	2020-10-02 13:40:22.790099	2020-10-02 13:40:22.790099
302	Emile	LACHMANN	France	18...	1919	Né à Montbrison, d'une famille alsacienne, organiste, il compose de nombreux choeur joué lors de concours notamment. Mouvement orpheonique.	\N	2020-10-02 13:40:22.791586	2020-10-02 13:40:22.791586
303	Paul	LACOMBE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.793215	2020-10-02 13:40:22.793215
304	P.	LACOME	\N	\N	\N	\N	\N	2020-10-02 13:40:22.796728	2020-10-02 13:40:22.796728
305	Francis	LAI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.798211	2020-10-02 13:40:22.798211
306	E.	LALO	\N	\N	\N	\N	\N	2020-10-02 13:40:22.799701	2020-10-02 13:40:22.799701
307	Serge	LANCEN	France	1922	-	Issu du conservatoire de Paris, commence à composer dès 4 ans, les 10 dernières années de sa vie, il écrit pour orchestre harmonique.	\N	2020-10-02 13:40:22.801252	2020-10-02 13:40:22.801252
308	P.	LANCON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.803034	2020-10-02 13:40:22.803034
309	Philip J.	LANG	\N	\N	\N	\N	\N	2020-10-02 13:40:22.804584	2020-10-02 13:40:22.804584
310	Antoine	LANGAGNE	France	1964	-	Saxophoniste, expérience des orchestres amateurs, sous chef de musique au conservatoire militaire de l'armée de terre	\N	2020-10-02 13:40:22.805984	2020-10-02 13:40:22.805984
311	Ph.	LANGLET	\N	\N	\N	\N	\N	2020-10-02 13:40:22.807502	2020-10-02 13:40:22.807502
312	Marie-Christine	LARUE	France	1976	-	Issu du conservatoire CRR Massenet de Saint Etienne à la clarinette. Elle fait partie des groupes Duo Mazeppa, Quatuor OUAT. Direction de l'Harmonie de la Chazotte depuis 2013	\N	2020-10-02 13:40:22.809006	2020-10-02 13:40:22.809006
313	E.	LARUE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.810902	2020-10-02 13:40:22.810902
314	Wim	LASEROMS	Pays Bas	1944	-	Débute la musique dans la Fanfare (tambour, accordeon) de sa ville, avec sa famille, puis rentre au conservatoire, obtient ses diplomes de direction et compose.	\N	2020-10-02 13:40:22.812795	2020-10-02 13:40:22.812795
315	Paul	LAVENDER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.814664	2020-10-02 13:40:22.814664
316	Gilbert	Layens	\N	\N	\N	\N	\N	2020-10-02 13:40:22.816476	2020-10-02 13:40:22.816476
317	Ch.	LECOCQ	\N	\N	\N	\N	\N	2020-10-02 13:40:22.818692	2020-10-02 13:40:22.818692
318	A. C. Van	LEEUWEN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.820971	2020-10-02 13:40:22.820971
319	Raymond	LEFEBVRE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.822658	2020-10-02 13:40:22.822658
320	Michel	LEGRAND	\N	\N	\N	\N	\N	2020-10-02 13:40:22.824078	2020-10-02 13:40:22.824078
321	Franz	LEHAR	\N	\N	\N	\N	\N	2020-10-02 13:40:22.825418	2020-10-02 13:40:22.825418
322	\N	LENNON MAC CARTNEY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.826686	2020-10-02 13:40:22.826686
323	W.	LEON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.828042	2020-10-02 13:40:22.828042
324	F.	LEROUX	\N	\N	\N	\N	\N	2020-10-02 13:40:22.829286	2020-10-02 13:40:22.829286
325	F.	LEROY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.830646	2020-10-02 13:40:22.830646
326	CH.	LEVASSEUR	\N	\N	\N	\N	\N	2020-10-02 13:40:22.832017	2020-10-02 13:40:22.832017
327	/	LIANO	\N	\N	\N	\N	\N	2020-10-02 13:40:22.833356	2020-10-02 13:40:22.833356
328	George	LIFERMANN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.834738	2020-10-02 13:40:22.834738
329	F.	LIGNER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.836095	2020-10-02 13:40:22.836095
330	Henk Van	LIJNSCHOOTEN	Pays Bas	1928	2006	Pseudonyme de Ted Huggens	\N	2020-10-02 13:40:22.837493	2020-10-02 13:40:22.837493
331	Franz	LISZT	Hongrie	1811	1886	Issu d'une famille de mélomane, montrant de bonne disposition à la musique, son père l'initie au piano. Il devient un pianiste reconnu dans toute l'Europe. Son œuvre diverse à inspiré plusieurs mouvements de la musique moderne (musique de film, dodécaphonisme, impressionnisme,...).	\N	2020-10-02 13:40:22.838917	2020-10-02 13:40:22.838917
332	Frederick	LOEWE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.840288	2020-10-02 13:40:22.840288
333	Gustave	LOGEART	\N	\N	\N	\N	\N	2020-10-02 13:40:22.841668	2020-10-02 13:40:22.841668
334	Robert	LONGFIELD	\N	\N	\N	\N	\N	2020-10-02 13:40:22.843073	2020-10-02 13:40:22.843073
335	Francis	LOPEZ	\N	\N	\N	\N	\N	2020-10-02 13:40:22.84445	2020-10-02 13:40:22.84445
336	F. P.	LOUP	\N	\N	\N	\N	\N	2020-10-02 13:40:22.845802	2020-10-02 13:40:22.845802
337	Bob	LOWDEN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.847398	2020-10-02 13:40:22.847398
338	J-Cl	LUCAS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.848806	2020-10-02 13:40:22.848806
339	Alexandre	LUIGINI	France	1850	1956	Né a Lyon, d'origine italienne, violoniste compositeur et chef d'orchestre, connu pour ses opéra	\N	2020-10-02 13:40:22.850073	2020-10-02 13:40:22.850073
340	H. C.	LUMBYE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.851514	2020-10-02 13:40:22.851514
341	T.	LUXEMBOURG	\N	\N	\N	\N	\N	2020-10-02 13:40:22.852894	2020-10-02 13:40:22.852894
342	Guy	LUYPAERTS	France	1917	2015	Incité à la musique par son père, il apprend l'accordéon et le piano en autodidact. Apprend sur le tas le jazz et la musique légère dans les cabarets parisiens. Arrangeur pour certains artistes, pianiste de Charles Trenet pour divers enregistrements, il se lance aussi dans la composition, notamment pour les grands noms de la variété française. Il s'illustre dans le style « musiqe symphonique légère ». La musique de l'Air de Paris lui permets d'écrire pour les batteries fanfare. Proche de la CMF, il sera éditer par Robert Martin.	\N	2020-10-02 13:40:22.854542	2020-10-02 13:40:22.854542
343	Paul	Mac Cartenay	\N	\N	\N	\N	\N	2020-10-02 13:40:22.856244	2020-10-02 13:40:22.856244
344	Enrico	MACIAS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.857984	2020-10-02 13:40:22.857984
345	G. Van	MAELE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.859511	2020-10-02 13:40:22.859511
346	Henry	MANCINI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.86127	2020-10-02 13:40:22.86127
347	Chuck	MANGIONE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.863204	2020-10-02 13:40:22.863204
348	Benedetto	MARCELLO	Italie	1686	1739	Issu d'une noble famille vénitienne, destiné à une carrière de juriste, il suit l'enseignement de la musique auprès de grands maîtres. Compositeur Baroque, un des premier à composer pour violoncelle.	\N	2020-10-02 13:40:22.86499	2020-10-02 13:40:22.86499
349	C.	MARCOUX	\N	\N	\N	\N	\N	2020-10-02 13:40:22.866586	2020-10-02 13:40:22.866586
350	E.	MARIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.868162	2020-10-02 13:40:22.868162
351	Louis	MARISCHAL	\N	\N	\N	\N	\N	2020-10-02 13:40:22.869695	2020-10-02 13:40:22.869695
352	Richard	MARKOWITZ	\N	\N	\N	\N	\N	2020-10-02 13:40:22.871198	2020-10-02 13:40:22.871198
353	Anna	MARLY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.872502	2020-10-02 13:40:22.872502
354	Robert	MARTIN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.873951	2020-10-02 13:40:22.873951
355	Marco	MARTOIA	\N	\N	\N	\N	\N	2020-10-02 13:40:22.875357	2020-10-02 13:40:22.875357
356	P.	MASCAGNI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.876756	2020-10-02 13:40:22.876756
357	Toshio	MASHIMA	\N	\N	\N	\N	\N	2020-10-02 13:40:22.878148	2020-10-02 13:40:22.878148
358	Jules	MASSENET	France	1842	1912	Issu d'une famille d'industriel, sa mère lui enseigne le piano. A 11 ans entre au conservatoire de Paris, où il étudiera l'écriture. Ami de Liszt, professeur de piano de composition, compositeur d'opéra, de symphonie de pièce pour piano. Rentre à l'académie des beaux arts à 36 ans.	\N	2020-10-02 13:40:22.879534	2020-10-02 13:40:22.879534
359	Jean-Michel	MATHEVON	France	?	-	Direction de l'Harmonie de la Chazotte en 2004. Clarinettiste  à l'ACEM Berlioz, formé par la fédération de la Loire.	\N	2020-10-02 13:40:22.880977	2020-10-02 13:40:22.880977
360	Em.	MAYER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.882411	2020-10-02 13:40:22.882411
361	L.	MAYEUR	\N	\N	\N	\N	\N	2020-10-02 13:40:22.883744	2020-10-02 13:40:22.883744
362	Clark	Mc ALISTER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.885179	2020-10-02 13:40:22.885179
363	Van	MC COY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.886566	2020-10-02 13:40:22.886566
364	/	Medley	\N	\N	\N	\N	\N	2020-10-02 13:40:22.887986	2020-10-02 13:40:22.887986
365	Etienne Nicolas	MEHUL	\N	\N	\N	\N	\N	2020-10-02 13:40:22.889434	2020-10-02 13:40:22.889434
366	Johan	DE MEIJ	Pays Bas	1953	-	Issu du monde de l'harmonie, puis entre au conservatoire Royal de la Haye au trombone. Chef d'orchestre, euphonium et tromboniste compositeur.	\N	2020-10-02 13:40:22.891076	2020-10-02 13:40:22.891076
367	G.	MEISTER	\N	\N	\N	Chef de musique au 1er Genie	\N	2020-10-02 13:40:22.892612	2020-10-02 13:40:22.892612
368	F.	MENDELSSOHN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.894094	2020-10-02 13:40:22.894094
369	François	MENICHETTI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.895551	2020-10-02 13:40:22.895551
370	Alan	MENKEN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.897179	2020-10-02 13:40:22.897179
371	Alan	MENKEN & SCHWARTZ	\N	\N	\N	\N	\N	2020-10-02 13:40:22.898988	2020-10-02 13:40:22.898988
372	Saverio	MERCADANTE	Italie	1795	1870	Fils illégitime d'un veuf, il arrive à entrer au conservatoire de Naples et étudie la composition. Chargé d'un orchestre étudiant, il compose des œuvres instrumentales grace auxquelles il se fait repérer. Puis il composera des opéras, dans un contexte marqué par la concurrence des autres compositeurs Italiens de l'époque.	\N	2020-10-02 13:40:22.90088	2020-10-02 13:40:22.90088
373	André	MESSAGER	France	1853	1929	Né à Montluçon, il apprend à Paris au contact de Gabriel Fauré, Camille St Saens, Eugene Gigout. Compose surtout pour le théatre	\N	2020-10-02 13:40:22.902904	2020-10-02 13:40:22.902904
374	G.	MEYERBEER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.904946	2020-10-02 13:40:22.904946
375	E.	MICHEL	\N	\N	\N	\N	\N	2020-10-02 13:40:22.907119	2020-10-02 13:40:22.907119
376	Charles	MICHIELS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.908839	2020-10-02 13:40:22.908839
377	Glenn	MILLER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.91032	2020-10-02 13:40:22.91032
378	Louis	MILLET	\N	\N	\N	\N	\N	2020-10-02 13:40:22.911852	2020-10-02 13:40:22.911852
379	Marius	MILLOT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.913302	2020-10-02 13:40:22.913302
380	Kevin	MIXON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.914908	2020-10-02 13:40:22.914908
381	Jos	MOERENHOUT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.916421	2020-10-02 13:40:22.916421
382	Bill	MOFFIT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.917938	2020-10-02 13:40:22.917938
383	Michel	MOISSERON	\N	\N	\N	\N	\N	2020-10-02 13:40:22.919385	2020-10-02 13:40:22.919385
384	Gosling	MOL	\N	\N	\N	\N	\N	2020-10-02 13:40:22.920842	2020-10-02 13:40:22.920842
385	P. J.	MOLENAAR	\N	\N	\N	\N	\N	2020-10-02 13:40:22.92235	2020-10-02 13:40:22.92235
386	Alain	MONCELLE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.923873	2020-10-02 13:40:22.923873
387	Thelonious	MONK & WILLIAMS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.925385	2020-10-02 13:40:22.925385
388	M.	MONNIOTTE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.926808	2020-10-02 13:40:22.926808
389	Bernard de la	MONNOYE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.928353	2020-10-02 13:40:22.928353
390	Yves	MONTAND	\N	\N	\N	\N	\N	2020-10-02 13:40:22.929929	2020-10-02 13:40:22.929929
391	J.	MORALI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.931396	2020-10-02 13:40:22.931396
392	Georges	MOREAU	\N	\N	\N	\N	\N	2020-10-02 13:40:22.933107	2020-10-02 13:40:22.933107
393	C.	MORGAN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.934758	2020-10-02 13:40:22.934758
394	Giorgio	MORODER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.936231	2020-10-02 13:40:22.936231
395	Ennio	MORRICONE	Italie	1928	-	Trompettiste diplomé de l'Académie nationale de Ste Cecile à Rome. Diplomé en composition et instrumentation, il fait des arrangements pour la télévision. Réputé pour ses musique de film qui ont marqué le 7eme art sur tout le deuxième moitié du XX siecle.	\N	2020-10-02 13:40:22.937768	2020-10-02 13:40:22.937768
396	Scotty	MORRIS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.939295	2020-10-02 13:40:22.939295
397	John Glenesk	MORTIMER	\N	\N	\N	\N	\N	2020-10-02 13:40:22.940972	2020-10-02 13:40:22.940972
398	John	MOSS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.94266	2020-10-02 13:40:22.94266
399	C.	MOUGEOT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.944423	2020-10-02 13:40:22.944423
400	A.	MOUSSARD	\N	\N	\N	\N	\N	2020-10-02 13:40:22.946064	2020-10-02 13:40:22.946064
401	Modeste	MOUSSORGSKI	Russie	1839	1881	Issu d'une famille russe, il doit suivre une carrière militaire, il arrete tout et rejoind un groupe de compositeur russe, inspiré par les airs populaires russes, suite à la ruine de sa famille, et l'insuccès de ses œuvres avantgardistes, il sombre dans l'alcoolisme.	\N	2020-10-02 13:40:22.947949	2020-10-02 13:40:22.947949
402	Wolfgang Amadeus	MOZART	Autriche	1756	1791	Enfant prodige d'un père professeur de violon. Apprends avec sa grande sœur, tournée européenne avec son père et sa sœur. Virtuose du piano et du violon.	\N	2020-10-02 13:40:22.950234	2020-10-02 13:40:22.950234
403	Thierry	MULLER	France	1964	-	Issu du conservatoire de Rouen puis de Paris (Flute) Direction d'école de musique	\N	2020-10-02 13:40:22.951951	2020-10-02 13:40:22.951951
404	E.	MULLOT	\N	\N	\N	\N	\N	2020-10-02 13:40:22.953445	2020-10-02 13:40:22.953445
405	Paul	MURTHA	\N	\N	\N	\N	\N	2020-10-02 13:40:22.954971	2020-10-02 13:40:22.954971
406	Monty R.	MUSGRAVE	\N	\N	\N	\N	\N	2020-10-02 13:40:22.956597	2020-10-02 13:40:22.956597
407	J.	MUSY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.958134	2020-10-02 13:40:22.958134
408	Jean-Claude	NARCE	France	1950	-	Clarinettiste issu de l'école de musique Massenet, puis du conservatoire de St Etienne. Direction de l'Harmonie de la Chazotte en 2004, puis de 2006 à 2014. En parallèle, il dirige la batterie fanfare de Sorbiers ainsi que le groupes les Ans chanteurs.	\N	2020-10-02 13:40:22.959668	2020-10-02 13:40:22.959668
409	Jerome	NAULAIS	France	1951	-	Tromboniste, soliste dans l'enseble Pierre Boulez, directeur d'école de musique et d'orchestre	\N	2020-10-02 13:40:22.961423	2020-10-02 13:40:22.961423
410	Sammy	NESTICO	\N	\N	\N	\N	\N	2020-10-02 13:40:22.962988	2020-10-02 13:40:22.962988
411	Randy	NEWMAN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.964609	2020-10-02 13:40:22.964609
412	Alfred	NEWMAN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.966165	2020-10-02 13:40:22.966165
413	Charly	NIESSEN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.967729	2020-10-02 13:40:22.967729
414	Johan	NIJS	\N	\N	\N	\N	\N	2020-10-02 13:40:22.96928	2020-10-02 13:40:22.96928
415	Lucien	NIVERD	\N	\N	\N	\N	\N	2020-10-02 13:40:22.970842	2020-10-02 13:40:22.970842
416	Jean	NOEL	\N	\N	\N	\N	\N	2020-10-02 13:40:22.972459	2020-10-02 13:40:22.972459
417	Siem	NOOM	\N	\N	\N	\N	\N	2020-10-02 13:40:22.974031	2020-10-02 13:40:22.974031
418	Monty	NORMAN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.975918	2020-10-02 13:40:22.975918
419	S.	NORTIZ	\N	\N	\N	\N	\N	2020-10-02 13:40:22.977643	2020-10-02 13:40:22.977643
420	Claude	NOUGARO	\N	\N	\N	\N	\N	2020-10-02 13:40:22.979263	2020-10-02 13:40:22.979263
421	Jerry	NOWAK	\N	1936	2015	\N	\N	2020-10-02 13:40:22.980907	2020-10-02 13:40:22.980907
422	Robert	O'BRIEN	\N	\N	\N	\N	\N	2020-10-02 13:40:22.982675	2020-10-02 13:40:22.982675
423	John	O'REILLY	\N	\N	\N	\N	\N	2020-10-02 13:40:22.984454	2020-10-02 13:40:22.984454
424	Pascal	OBISPO	\N	\N	\N	\N	\N	2020-10-02 13:40:22.986375	2020-10-02 13:40:22.986375
425	\N	ODYSSEE	\N	\N	-	Quintette de Cuivre et percussion, créteur du spectacle Kiosk, mettant en scène un orchestre d'harmonie amateur.	\N	2020-10-02 13:40:22.988525	2020-10-02 13:40:22.988525
426	Jacques	OFFENBACH	\N	\N	\N	\N	\N	2020-10-02 13:40:22.990548	2020-10-02 13:40:22.990548
427	Jean	OLLIER	France	?	?	Direction de l'Harmonie de la Chazotte de 1931 à 1965. Ingénieur à la Manufacture de St Etienne, réussi de nombreux concours et compose pour l'Harmonie.	\N	2020-10-02 13:40:22.992689	2020-10-02 13:40:22.992689
428	Kosuke	ONOZAKI	\N	\N	\N	\N	\N	2020-10-02 13:40:22.994585	2020-10-02 13:40:22.994585
429	Carl	ORFF	Allemagne	1895	1982	Issu d'une famille de militaire, parents bons musiciens, il étudie à l'académie de musique de Munich. En 1916 il est nomé chef d'orchestre, après la guerre, il se conssacre à l'étude et la composition. Passionné de l'époque Renaissance Italienne, en 1924 fonde une école de danse, partie intégrante de son œuvre musicale. Surtout connnu pour son œuvre Carmina Burana, de 1961 à la fin de sa vie, son oeuvre s'oriente vers le théatre musical.	\N	2020-10-02 13:40:22.996318	2020-10-02 13:40:22.996318
430	Nunzio	ORTOLANO	\N	\N	\N	\N	\N	2020-10-02 13:40:22.99808	2020-10-02 13:40:22.99808
431	Eric	OSTERLING	\N	\N	\N	\N	\N	2020-10-02 13:40:22.999662	2020-10-02 13:40:22.999662
432	Johan	PACHELBEL	Allemagne	1653	1706	Montrant e bonne capacité Musicales, il devient organiste puis compositeur, époque Baroque, zmi de la famille Bach, organiste à Vienne, fan de musique italienne	\N	2020-10-02 13:40:23.001343	2020-10-02 13:40:23.001343
433	G.	PANTELIS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.002948	2020-10-02 13:40:23.002948
434	Gabriel	PARES	\N	\N	\N	\N	\N	2020-10-02 13:40:23.004624	2020-10-02 13:40:23.004624
435	Ray	PARKER	\N	\N	\N	\N	\N	2020-10-02 13:40:23.00627	2020-10-02 13:40:23.00627
436	Sir Hubert	PARRY	\N	\N	\N	\N	\N	2020-10-02 13:40:23.007926	2020-10-02 13:40:23.007926
437	Carmine	PASTORE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.009532	2020-10-02 13:40:23.009532
438	\N	PAULUS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.011113	2020-10-02 13:40:23.011113
439	C. Marcel	PEETERS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.012584	2020-10-02 13:40:23.012584
440	Wayne	PEGRAM	\N	\N	\N	\N	\N	2020-10-02 13:40:23.014113	2020-10-02 13:40:23.014113
441	Jef	PENDERS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.015888	2020-10-02 13:40:23.015888
442	Johan	PERIK	\N	\N	\N	\N	\N	2020-10-02 13:40:23.017634	2020-10-02 13:40:23.017634
443	V.	PETER	\N	\N	\N	\N	\N	2020-10-02 13:40:23.019339	2020-10-02 13:40:23.019339
444	J. C.	PETIT	\N	\N	\N	\N	\N	2020-10-02 13:40:23.02105	2020-10-02 13:40:23.02105
445	Fernand	PETIT	\N	\N	\N	\N	\N	2020-10-02 13:40:23.022707	2020-10-02 13:40:23.022707
446	Alexandre S.	PETIT	\N	\N	\N	\N	\N	2020-10-02 13:40:23.024582	2020-10-02 13:40:23.024582
447	Pierre	PHALESE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.026489	2020-10-02 13:40:23.026489
448	Henry	PHARMER	\N	\N	\N	\N	\N	2020-10-02 13:40:23.028633	2020-10-02 13:40:23.028633
449	Edith	PIAF	\N	\N	\N	\N	\N	2020-10-02 13:40:23.030615	2020-10-02 13:40:23.030615
450	Astor	PIAZZOLLA	\N	\N	\N	\N	\N	2020-10-02 13:40:23.032907	2020-10-02 13:40:23.032907
451	S.	PICHOT	\N	\N	\N	\N	\N	2020-10-02 13:40:23.034894	2020-10-02 13:40:23.034894
452	G. 	PIERNE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.036569	2020-10-02 13:40:23.036569
453	Nicolas	PIOVANI	\N	\N	\N	\N	\N	2020-10-02 13:40:23.038189	2020-10-02 13:40:23.038189
454	R.	PLANQUETTE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.039791	2020-10-02 13:40:23.039791
455	J.	PLANTE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.041387	2020-10-02 13:40:23.041387
456	Florent	PLASSARD	France	1987	-	Elève de l'Ecole de Musique de la Talaudière, Saxophoniste de formation, Fin d'étude de saxophone en 2005, Etude de Direction d'orchestre aurpès de la CMF, obtention du Certificat Régional de Direction de Société Musicale (CRDSM) en 2015. Direction de l'Harmonie de la Chazotte depuis 2011.	\N	2020-10-02 13:40:23.042928	2020-10-02 13:40:23.042928
457	James D.	PLOYHAR	\N	\N	\N	\N	\N	2020-10-02 13:40:23.04466	2020-10-02 13:40:23.04466
458	Lew	POLLACK	\N	\N	\N	\N	\N	2020-10-02 13:40:23.046322	2020-10-02 13:40:23.046322
459	Jean Pierre	POMMIER	\N	\N	\N	\N	\N	2020-10-02 13:40:23.047899	2020-10-02 13:40:23.047899
460	Francis	POPY	France	1874	1928	François Joseph Popy est né à Lyon, clarinettiste, s'engage dans la musique militaire, puis compose, musique représentative de la Belle époque	\N	2020-10-02 13:40:23.04951	2020-10-02 13:40:23.04951
461	E.	PORCHET	\N	\N	\N	\N	\N	2020-10-02 13:40:23.051083	2020-10-02 13:40:23.051083
462	A.	POROT	\N	\N	\N	\N	\N	2020-10-02 13:40:23.05259	2020-10-02 13:40:23.05259
463	Mike	POST	\N	\N	\N	\N	\N	2020-10-02 13:40:23.054199	2020-10-02 13:40:23.054199
464	Alexandre	POTTIER / MONCELLE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.055819	2020-10-02 13:40:23.055819
465	Patrick	POUTOIRE	France	1960	-	Tubiste de formation, au sein de la musique militaire, il deviendra chef de musique à Berlin, à Rabat (Maroc), puis à Belfort	\N	2020-10-02 13:40:23.057402	2020-10-02 13:40:23.057402
466	Albert	POYLO	\N	\N	\N	\N	\N	2020-10-02 13:40:23.059359	2020-10-02 13:40:23.059359
467	Perez	PRADO	\N	\N	\N	\N	\N	2020-10-02 13:40:23.06111	2020-10-02 13:40:23.06111
468	Elvis	PRESLEY	\N	\N	\N	\N	\N	2020-10-02 13:40:23.062865	2020-10-02 13:40:23.062865
469	Louis	PRIMAS 	\N	\N	\N	\N	\N	2020-10-02 13:40:23.064563	2020-10-02 13:40:23.064563
470	Sergei	PROKOFIEV	Russie	1891	1953	Initié à la musique par sa mère, doué, sa mère l'oriente vers une carrière musicale. Elle l'accompagne à Moscou pour qu'il suive des cours, puis à St Petersbourg, élève convaincu de sa supériorité et de son talent. Compositeur, pianiste et chef d'orchestre, il parcours le monde après la première guerre mondiale. Il reviendra en URSS dans les années 30 ou il se remet à composer.	\N	2020-10-02 13:40:23.066268	2020-10-02 13:40:23.066268
471	Giacomo	PUCCINI	Italie	1858	1924	Issu d'une famille de longue tradition musicale sur 5 générations, il reçoit une formation de musicien d'église, organiste, puis étudie au conservatoire de Milan.	\N	2020-10-02 13:40:23.068131	2020-10-02 13:40:23.068131
472	Tito	PUENTE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.070102	2020-10-02 13:40:23.070102
473	Tito	PUENTE & VALENS & RIO	\N	\N	\N	\N	\N	2020-10-02 13:40:23.072193	2020-10-02 13:40:23.072193
474	Franco	PULIAFITO	\N	\N	\N	\N	\N	2020-10-02 13:40:23.074214	2020-10-02 13:40:23.074214
475	/	QUERU	\N	\N	\N	\N	\N	2020-10-02 13:40:23.076339	2020-10-02 13:40:23.076339
476	S.	RACHMANINOF	\N	\N	\N	\N	\N	2020-10-02 13:40:23.078281	2020-10-02 13:40:23.078281
477	Jean Philippe	RAMEAU	\N	\N	\N	\N	\N	2020-10-02 13:40:23.080039	2020-10-02 13:40:23.080039
478	Peter	RATNIK	\N	\N	\N	\N	\N	2020-10-02 13:40:23.081759	2020-10-02 13:40:23.081759
479	\N	RAUSKI	\N	\N	\N	Chef de musique du 18eme Régiment d'Infanterie	\N	2020-10-02 13:40:23.083506	2020-10-02 13:40:23.083506
480	Maurice	RAVEL	France	1875	1937	\N	\N	2020-10-02 13:40:23.085252	2020-10-02 13:40:23.085252
481	Dick	RAVENAL	Pays Bas	1938	2014	Pseudonyme de Cornelis Kees Vlak	\N	2020-10-02 13:40:23.086877	2020-10-02 13:40:23.086877
482	Thierry	REDON	\N	\N	\N	\N	\N	2020-10-02 13:40:23.088651	2020-10-02 13:40:23.088651
483	Alfred	REED	\N	\N	\N	\N	\N	2020-10-02 13:40:23.090227	2020-10-02 13:40:23.090227
484	/	REINTER	\N	\N	\N	\N	\N	2020-10-02 13:40:23.09196	2020-10-02 13:40:23.09196
485	Maino	REMMERS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.096431	2020-10-02 13:40:23.096431
486	Fransisco	REPILADO	\N	\N	\N	\N	\N	2020-10-02 13:40:23.098141	2020-10-02 13:40:23.098141
487	Jacques	REVAUX	\N	\N	\N	\N	\N	2020-10-02 13:40:23.099725	2020-10-02 13:40:23.099725
488	Scott	RICHARDS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.101406	2020-10-02 13:40:23.101406
489	Ted	RICKETTS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.103315	2020-10-02 13:40:23.103315
490	Pascal	ROA	France	\N	\N	Musicien du NOAC à St Genest Lerpt (Loire 42). A fait quelques arrangements.	\N	2020-10-02 13:40:23.105048	2020-10-02 13:40:23.105048
491	Camille	ROBERT	\N	\N	\N	\N	\N	2020-10-02 13:40:23.106784	2020-10-02 13:40:23.106784
492	Richard	RODGERS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.108386	2020-10-02 13:40:23.108386
493	G. H. M.	RODRIGUEZ	\N	\N	\N	\N	\N	2020-10-02 13:40:23.110119	2020-10-02 13:40:23.110119
494	F.	ROMAIN	\N	\N	\N	\N	\N	2020-10-02 13:40:23.111951	2020-10-02 13:40:23.111951
495	/	RONDO VENEZIANO	Italie	1979	2009	Groupe italien crée par Gian Piero Reverberi, compositeur et chef d'orchestre, inspiré de la musique baroque et classique pour produire un son pop-rock. Les interprétations classique avaient pour but de rendre cette usique accessible à tous.	\N	2020-10-02 13:40:23.11386	2020-10-02 13:40:23.11386
496	Jan Van Der	ROOST	\N	\N	\N	\N	\N	2020-10-02 13:40:23.115931	2020-10-02 13:40:23.115931
497	Gioacchino	ROSSINI	Italie	1792	1868	Issu d'une faille modeste, père était fan de la révolution française, inspecteur de boucherie, et trompette de ville, sa mère chanteuse. Il s'initie rapidement au chant, et à la composition d'opéra dès ses 14 ans. Son œuvre a marqué l'opéra, et ses opéras la cuisines en fin gastronome qu'il était.	\N	2020-10-02 13:40:23.117876	2020-10-02 13:40:23.117876
498	Nino	ROTA	\N	\N	\N	\N	\N	2020-10-02 13:40:23.120253	2020-10-02 13:40:23.120253
499	Th.	ROTTIER	\N	\N	\N	Chef de musique au 5eme régiment d'infanterie	\N	2020-10-02 13:40:23.122184	2020-10-02 13:40:23.122184
500	Ph.	ROUGERON	\N	\N	\N	\N	\N	2020-10-02 13:40:23.123859	2020-10-02 13:40:23.123859
501	Claude Joseph	ROUGET DE LISLE	France	1760	1836	Né à Lons le Saulnier, ainé de 8 enfants, d'un père avocat, passionné de musique et de poésie dès son enfance, reçoit une formation militaire à Paris. Compose la marseillaise durant la révolution Française. Il décède dans la pauvreté en 1836.	\N	2020-10-02 13:40:23.125614	2020-10-02 13:40:23.125614
502	\N	ROUVEIROLIS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.127286	2020-10-02 13:40:23.127286
503	Eugene	ROUX	\N	\N	\N	\N	\N	2020-10-02 13:40:23.128955	2020-10-02 13:40:23.128955
504	Miklos	ROZSA	\N	\N	\N	\N	\N	2020-10-02 13:40:23.130614	2020-10-02 13:40:23.130614
505	Anton	RUBINSTEIN	\N	\N	\N	\N	\N	2020-10-02 13:40:23.132341	2020-10-02 13:40:23.132341
506	/	RUDD	\N	\N	\N	\N	\N	2020-10-02 13:40:23.133793	2020-10-02 13:40:23.133793
507	John	RYAN	\N	\N	\N	\N	\N	2020-10-02 13:40:23.135438	2020-10-02 13:40:23.135438
508	/	SABAN & LEVY	\N	\N	\N	\N	\N	2020-10-02 13:40:23.136957	2020-10-02 13:40:23.136957
509	Camille	SAINT SAENS	France	1835	1921	Né à Paris, il dévute le piano avec sa tante, puis un professeur de piano. Enfant prodige il entre à 13 ans au concervatoire de Paris. A 18 ans, il est reconnu en tant que grand organiste. Il devient ensuite professeur de piano, et commence à composer. Dès 1875, il se rend en Algérie et tombe amoureux de ce pays. C'est d'ailleurs à Alger qu'il décèdera. Parmis son œuvre on remarquera son Carnaval des animaux. Il peut être considéré comme le premier compositeur pour le septième art en 1908.	\N	2020-10-02 13:40:23.138657	2020-10-02 13:40:23.138657
510	Henri	SALVADOR	\N	\N	\N	\N	\N	2020-10-02 13:40:23.14023	2020-10-02 13:40:23.14023
511	\N	SARRE	France	?	?	Direction de l'Harmonie de la Chazotte de 1886 à 1890	\N	2020-10-02 13:40:23.141922	2020-10-02 13:40:23.141922
512	Richard	SAUCEDO	\N	\N	\N	\N	\N	2020-10-02 13:40:23.143527	2020-10-02 13:40:23.143527
513	Bozz	SCAGGS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.145186	2020-10-02 13:40:23.145186
514	Lalo	SCHIFRIN	\N	\N	\N	\N	\N	2020-10-02 13:40:23.146838	2020-10-02 13:40:23.146838
515	Hardy	SCHNEIDERS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.148635	2020-10-02 13:40:23.148635
516	J.	SCHRAMMEL	\N	\N	\N	\N	\N	2020-10-02 13:40:23.150166	2020-10-02 13:40:23.150166
517	Franz	SCHUBERT	Autriche	1797	1828	Commence la musique auprès de son père, puis apprend à Vienne, choriste, violoniste. Etudie auprès de Salieri, porteur de torche à l'enterrement de Beethoven. Meurt de la syphilis.	\N	2020-10-02 13:40:23.151709	2020-10-02 13:40:23.151709
518	R.	SCHUMANN	\N	\N	\N	\N	\N	2020-10-02 13:40:23.153388	2020-10-02 13:40:23.153388
519	Jose	SCHYNS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.155185	2020-10-02 13:40:23.155185
520	Thomas W.	SCOTT	\N	\N	\N	\N	\N	2020-10-02 13:40:23.156914	2020-10-02 13:40:23.156914
521	\N	SCOTT DIRECTOR 	\N	\N	\N	\N	\N	2020-10-02 13:40:23.158785	2020-10-02 13:40:23.158785
522	Vincent	SCOTTO	\N	\N	\N	\N	\N	2020-10-02 13:40:23.160463	2020-10-02 13:40:23.160463
523	Ron	SEBREGTS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.162415	2020-10-02 13:40:23.162415
524	Jules	SEMLER - COLLERY	\N	\N	\N	\N	\N	2020-10-02 13:40:23.16456	2020-10-02 13:40:23.16456
525	David	SHAFFER	Etas Unis	19...	/	Compositeur, arrangeur pour les éditions Barnhouse, travail à l'Université de Miami	\N	2020-10-02 13:40:23.166455	2020-10-02 13:40:23.166455
526	/	SHARP GOULD ARNOLD	\N	\N	\N	\N	\N	2020-10-02 13:40:23.168252	2020-10-02 13:40:23.168252
527	Robert	SHERMAN	\N	\N	\N	\N	\N	2020-10-02 13:40:23.169925	2020-10-02 13:40:23.169925
528	/	SHERMAN & MENKEN & WRUBEL	\N	\N	\N	\N	\N	2020-10-02 13:40:23.17162	2020-10-02 13:40:23.17162
529	David	SHIRE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.173176	2020-10-02 13:40:23.173176
530	Howard	SHORE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.174808	2020-10-02 13:40:23.174808
531	Dmitri	SHOSTAKOVICH	Russie	1906	1975	Issu d'une famille de révolutionnaire polonais, il commence le piano à 9 ans. Il étudie les compositeurs classiques, puis la composition à 13 ans au conservatoire de Petrograd. Le volume de son œuvre en fait une figure majeur du Xxeme siècle.	\N	2020-10-02 13:40:23.176364	2020-10-02 13:40:23.176364
532	Jean	SIBELIUS	Finlande	1865	1957	Alors qu'il n'a que 3 ans, son père décède, et il est élevé et apprend la musique au contact de son oncle, violoniste amateur, et de sa tante pianiste. A 10 ans il compose sa première œuvre. Il grandit avec le développement de la culture finoise. Après sa rencontre avec Anton Bruckner en 1890, il s'oriente trouve son style musical symphnique. Entre alcool et dépression, il compose au rythme des évènements politique ondiaux.	\N	2020-10-02 13:40:23.178057	2020-10-02 13:40:23.178057
533	/	SIGNARD	\N	\N	\N	\N	\N	2020-10-02 13:40:23.179764	2020-10-02 13:40:23.179764
534	Franck	SINATRA	\N	\N	\N	\N	\N	2020-10-02 13:40:23.181408	2020-10-02 13:40:23.181408
535	Roland	SMEETS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.183026	2020-10-02 13:40:23.183026
536	Bedrich	SMETANA	Republique Tchèque	1824	1884	D'un père musicien, brasseur, il apprend jeune le piano et le violon. Engagé dans les mouvement nationaliste tchèque. Il se lie d'amitié avec Antonin Dvorak à Prague. Sourd à la fin de sa vie, il se consacre à la composition (La moldau,...).	\N	2020-10-02 13:40:23.184606	2020-10-02 13:40:23.184606
537	Robert W.	SMITH	Etats Unis	1958	-	Un des compositeurs américains les plus prolifiques	\N	2020-10-02 13:40:23.186194	2020-10-02 13:40:23.186194
538	Claude T.	SMITH	\N	\N	\N	\N	\N	2020-10-02 13:40:23.187827	2020-10-02 13:40:23.187827
539	Henry	SOHIER	\N	\N	\N	\N	\N	2020-10-02 13:40:23.18949	2020-10-02 13:40:23.18949
540	\N	SOUSA	\N	\N	\N	\N	\N	2020-10-02 13:40:23.191228	2020-10-02 13:40:23.191228
541	Philip	SPARKE	Angleterre	1951	-	Etudie à Londre, piano, trompette, composition	\N	2020-10-02 13:40:23.192882	2020-10-02 13:40:23.192882
542	Jared	SPEARS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.194592	2020-10-02 13:40:23.194592
543	Wim	STALMAN	\N	\N	\N	\N	\N	2020-10-02 13:40:23.196161	2020-10-02 13:40:23.196161
544	Piet	STALMEIER	\N	\N	\N	\N	\N	2020-10-02 13:40:23.197968	2020-10-02 13:40:23.197968
545	Todd	STALTER	\N	\N	\N	\N	\N	2020-10-02 13:40:23.199843	2020-10-02 13:40:23.199843
546	Pavel	STANEK	\N	\N	\N	\N	\N	2020-10-02 13:40:23.201716	2020-10-02 13:40:23.201716
547	Jim	STEINMAN	\N	\N	\N	\N	\N	2020-10-02 13:40:23.203503	2020-10-02 13:40:23.203503
548	/	STING	\N	\N	\N	\N	\N	2020-10-02 13:40:23.20577	2020-10-02 13:40:23.20577
549	Mike	STORY	\N	\N	\N	\N	\N	2020-10-02 13:40:23.208068	2020-10-02 13:40:23.208068
550	Dizzy	STRATFORD	Pays Bas	1959	-	Pseudonyme de Jacob de Haan, auteur compositeur et arrangeur néerlandais.	\N	2020-10-02 13:40:23.209956	2020-10-02 13:40:23.209956
551	Richard	STRAUSS	\N	1864	1949	Connu pour ses marches et musique militaires	\N	2020-10-02 13:40:23.211797	2020-10-02 13:40:23.211797
552	Josef	STRAUSS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.213448	2020-10-02 13:40:23.213448
553	Johan fils	STRAUSS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.215042	2020-10-02 13:40:23.215042
554	Johann	STRAUSS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.216704	2020-10-02 13:40:23.216704
555	Billy	STRAYHORN	\N	\N	\N	\N	\N	2020-10-02 13:40:23.218399	2020-10-02 13:40:23.218399
556	Franz von	SUPPE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.220119	2020-10-02 13:40:23.220119
557	Johan	SVENDSEN	Norvege	1840	1911	Passe l'essentiel de sa vie au Danemark, y débute la musique. A 20 ans il il commence à diriger et à composer, violoniste de formation, ami de Wagner, c'est surtout pour sa direction musicale qu'il voyage en Europe. Ses compositions surtout pour orchestre ne s'exporteront pas beaucoup, mais il fati pruve de grande qualité d'orchestrateur.	\N	2020-10-02 13:40:23.22182	2020-10-02 13:40:23.22182
558	James	SWEARINGEN	\N	\N	\N	\N	\N	2020-10-02 13:40:23.223491	2020-10-02 13:40:23.223491
559	Michael	SWEENEY	Inde	1952	-	Etude Musicale en Inde, il travail ensuite pour Editeur Hal Leonnard, connu également pour ses écritures et arrangement pour jeunes ensembles.	\N	2020-10-02 13:40:23.225106	2020-10-02 13:40:23.225106
560	Norman	TAILOR	Allemagne	\N	\N	de son vrai nom Hardy Schneiders, Allemand émigré aux Etats unis après le seconde guerre mondiale	\N	2020-10-02 13:40:23.226823	2020-10-02 13:40:23.226823
561	Tohru	TAKAHASHI	\N	\N	\N	\N	\N	2020-10-02 13:40:23.228672	2020-10-02 13:40:23.228672
562	Kumiko	TANAKA	\N	\N	\N	\N	\N	2020-10-02 13:40:23.230453	2020-10-02 13:40:23.230453
563	Daniel	TASCA	\N	\N	\N	\N	\N	2020-10-02 13:40:23.232198	2020-10-02 13:40:23.232198
564	John	TATGENHORST	\N	\N	\N	\N	\N	2020-10-02 13:40:23.234069	2020-10-02 13:40:23.234069
565	Ben	TAYOUX / F. CHASSAIGNE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.235833	2020-10-02 13:40:23.235833
619	Klaas Van der	WOUDE	\N	\N	\N	\N	\N	2020-10-02 13:40:24.885205	2020-10-02 13:40:24.885205
620	Donald	FURLANO	\N	\N	\N	\N	\N	2020-10-02 13:40:24.896066	2020-10-02 13:40:24.896066
621	Jean-Michel	SOLIN	\N	\N	\N	\N	\N	2020-10-02 13:40:24.904534	2020-10-02 13:40:24.904534
622	\N	MOSER	\N	\N	\N	\N	\N	2020-10-02 13:40:24.909766	2020-10-02 13:40:24.909766
623	\N	COMPAGNONS DE LA CHANSON	\N	\N	\N	\N	\N	2020-10-02 13:40:25.044255	2020-10-02 13:40:25.044255
566	Piotr Ilitch	TCHAIKOVSKI	Russie	1840	1893	Issu d'une famille aisée, enfant sensible et fragile, il commence le piano dès 5 ans, encourragé par sa mère. Bénéficie d'une très bonne éducation. A la mort de sa mère en 1854, il se tourne vraiment vers la musique, la composition, et assume son homosexualité. Oeuvre marquée par les compositions classiques occidentales et la tradition russe, Figure dominante du romantisme russe du XIXe siècle.	\N	2020-10-02 13:40:23.237523	2020-10-02 13:40:23.237523
567	C.	TEIKE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.239369	2020-10-02 13:40:23.239369
568	Henry	TELLAM	\N	\N	\N	\N	\N	2020-10-02 13:40:23.24096	2020-10-02 13:40:23.24096
569	Miklos	THEODORAKIS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.242963	2020-10-02 13:40:23.242963
570	Xavier & Jacques	THIBAULT & DELAPORTE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.245293	2020-10-02 13:40:23.245293
571	Jerome	THOMAS	Suisse	1963	-	Il débute le saxophone et la flûte traversière à Fribourg jusqu’en 1987, avant d’aller finir son cursus à Boston où il continue de travailler le saxophone, l’improvisation, la composition et l’arrangement. Il jouera dans de nombreuses formations de jazz, salsa, musiques africaines. Dès 1991, il dirige son propre Big Band tout en jouant dans divers orchestres de musiques latines. En 1998, il travaille en arrangeur compositeur chez Marc Reift.	\N	2020-10-02 13:40:23.247321	2020-10-02 13:40:23.247321
572	/	TILLIARD	\N	\N	\N	\N	\N	2020-10-02 13:40:23.249769	2020-10-02 13:40:23.249769
573	Ch.	TILLY	\N	\N	\N	Chef de musique au 141eme regiment de ligne, Officier de l'instruction publique	\N	2020-10-02 13:40:23.252292	2020-10-02 13:40:23.252292
574	Bert	TINGE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.254232	2020-10-02 13:40:23.254232
575	Armand	TOURNEL	\N	\N	\N	Chef de musique à l'ecole militaire de Billom	\N	2020-10-02 13:40:23.256054	2020-10-02 13:40:23.256054
576	Charles	TRENET	\N	\N	\N	\N	\N	2020-10-02 13:40:23.257771	2020-10-02 13:40:23.257771
577	Pierre	TRIOLLIER	France	?	?	Direction de l'Harmonie de la Chazotte de 1981 à 1988. Accordéoniste puis saxophoniste au conservatoire de St Etienne, compose et arrange quelques pièces	\N	2020-10-02 13:40:23.259349	2020-10-02 13:40:23.259349
578	V.	TURINE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.261125	2020-10-02 13:40:23.261125
579	R.	VALENS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.262844	2020-10-02 13:40:23.262844
580	Georges	VAN PARYS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.264582	2020-10-02 13:40:23.264582
581	Chris	VANDEWEYER	\N	\N	\N	\N	\N	2020-10-02 13:40:23.266306	2020-10-02 13:40:23.266306
582	/	VANGELIS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.268046	2020-10-02 13:40:23.268046
583	Giuseppe	VERDI	Italie	1813	1901	Né français 4 mois avant la chute de l'empire de Napoléon, sa mère cachera ce fait. Il est bercé par l'Italie touchée à l'époque par l'art lyrique, au contact des musiciens ambulant qui font halte dans l'auberge parentale, puis initié à la musique par son maitre d'école. Il suit des cours d'orgue. Il commence la composition à 15 ans. Il devient l'un des compositeurs italiens les plus influents du XiXeme siècle, lesthèmes de ses opéras rentrant même dans la culture populaire.	\N	2020-10-02 13:40:23.269802	2020-10-02 13:40:23.269802
584	Steven	VERHAERT	\N	\N	\N	\N	\N	2020-10-02 13:40:23.271542	2020-10-02 13:40:23.271542
585	Jean	VERNAY	France	?	?	Direction de l'Harmonie de la Chazotte de 1974 à 1979. Trompettiste de formation, agent technique de St Jean Bonnefonds, direction de St Chamond	\N	2020-10-02 13:40:23.273378	2020-10-02 13:40:23.273378
586	Johnnie	VINSON	Etas Unis	?	-	Chef et professeur de musique à l'Université d'Auburn (Alabama). Issu de l'univeersité du Mississippi. A eaucoup publié pour les Edition Hal Leonard.	\N	2020-10-02 13:40:23.275211	2020-10-02 13:40:23.275211
587	Antonio	VIVALDI	Italie	1678	1741	Violoniste et compositeur Italien, né à Venise. Compositeur de la muqieu baroque, à l'origine des concerto pour soliste	\N	2020-10-02 13:40:23.276948	2020-10-02 13:40:23.276948
588	H.	VIVET	\N	\N	\N	\N	\N	2020-10-02 13:40:23.278792	2020-10-02 13:40:23.278792
589	Cornelis Kees	VLAK	Pays Bas	1938	2014	Formation à Amsterdam (trompette, piano) et à San Fransisco (direction d'orchestre). Auteur d'environ 500 pièces. Cuivre, passionné de jazz, rock et autres musiques entrainantes, Pseudonyme : Luigi di Ghisallo, Llano, Robert Allmend, Alfred Bosendorfer, Dick Ravenal	\N	2020-10-02 13:40:23.280526	2020-10-02 13:40:23.280526
590	Richard	WAGNER	Allemagne	1813	1883	Jeune, il est plus attiré par le théatre et la poesie. A 15 ans il décrouvre les œuvres de Mozart et Beethoven, et se lance dans la musique. A 20 ans il compose son premier opéra, au coté de son frère, il se perfectionne, et devient un des plus grands chef d'orchestre de son époque. Pour ses opéras, il aime confectionner lui même le livret et la mise en scène. Il voyagera en Europe, connait des hauts et des bas, fini sa vie à Venise.	\N	2020-10-02 13:40:23.282179	2020-10-02 13:40:23.282179
591	Josef Franz	WAGNER	Autriche	1856	1908	Sans lien de parenté avec Richard Wagner. Josef Franz est surtout connu pour avoir écrit la marche la plus connue d'Autriche.	\N	2020-10-02 13:40:23.284395	2020-10-02 13:40:23.284395
592	Andre	WAIGNEIN	Belgique	1942	2015	Débute la musique avec son pére, puis au conservatoire royal de Bruxelles, apprend trompette, piano, financce ses étides en jouant à Lille dans un orchestre de Jazz. Puis il étudie la composition à l'orchester royal de Mons. Il débute ensuite sa carrière, chef d'orchestre, enseignant, compositeur et arrangeur pour harmonie et fanfare. Dès 1990, il travaille pour les éditions de Haske, il devient une icône de la musique a vent belge. Pseudonyme : Roland  Kernen, Rob Ares, Frede Gines, Rita Defoort).	\N	2020-10-02 13:40:23.286791	2020-10-02 13:40:23.286791
593	E.	WALDTEUFEL	\N	\N	\N	\N	\N	2020-10-02 13:40:23.288961	2020-10-02 13:40:23.288961
594	Christoph	WALTER	\N	\N	\N	\N	\N	2020-10-02 13:40:23.291365	2020-10-02 13:40:23.291365
595	Harold L.	WALTERS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.293836	2020-10-02 13:40:23.293836
596	John	WARRINGTON	\N	\N	\N	\N	\N	2020-10-02 13:40:23.295744	2020-10-02 13:40:23.295744
597	John	WASSON	\N	\N	\N	\N	\N	2020-10-02 13:40:23.297372	2020-10-02 13:40:23.297372
598	Jules	WATELLE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.29906	2020-10-02 13:40:23.29906
599	\N	WATTIER	\N	\N	\N	\N	\N	2020-10-02 13:40:23.300634	2020-10-02 13:40:23.300634
600	Lloyd	WEBBER	\N	\N	\N	\N	\N	2020-10-02 13:40:23.302362	2020-10-02 13:40:23.302362
601	Carl Maria Von	WEBER	Allemagne	1786	1826	Il apprend très tôt le chont et le piano. Reçoit des cours auprès de Haydn à Salzbourg. A douze ans il compose ses premières œuvres à Vienne. Il mène ensuite une vie itinérante en Allemagne. Il reste célèbre pour deux de ses opéras, Freischutz et Euryanthe.	\N	2020-10-02 13:40:23.303944	2020-10-02 13:40:23.303944
602	Suzanne	WELTERS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.305753	2020-10-02 13:40:23.305753
603	Emile	WESLY	\N	\N	\N	\N	\N	2020-10-02 13:40:23.307454	2020-10-02 13:40:23.307454
604	Gustave	WETTGE	\N	\N	\N	\N	\N	2020-10-02 13:40:23.309115	2020-10-02 13:40:23.309115
605	John	WILLIAMS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.310791	2020-10-02 13:40:23.310791
606	Pharell	WILLIAMS	\N	\N	\N	\N	\N	2020-10-02 13:40:23.312478	2020-10-02 13:40:23.312478
607	Clayton	WILSON	\N	\N	\N	\N	\N	2020-10-02 13:40:23.314127	2020-10-02 13:40:23.314127
608	G.	WITTMANN	\N	\N	\N	\N	\N	2020-10-02 13:40:23.315913	2020-10-02 13:40:23.315913
609	Koen De	WOLF	\N	\N	\N	\N	\N	2020-10-02 13:40:23.317611	2020-10-02 13:40:23.317611
610	Ray	WOODFIELD	\N	\N	\N	\N	\N	2020-10-02 13:40:23.319361	2020-10-02 13:40:23.319361
611	\N	ZACAR	\N	\N	\N	\N	\N	2020-10-02 13:40:23.32102	2020-10-02 13:40:23.32102
612	V.	ZELLI / MANGALI	\N	\N	\N	\N	\N	2020-10-02 13:40:23.322716	2020-10-02 13:40:23.322716
613	Hans	ZIMMER	\N	\N	\N	\N	\N	2020-10-02 13:40:23.32419	2020-10-02 13:40:23.32419
614	A.	ZIMMERMANN	\N	\N	\N	\N	\N	2020-10-02 13:40:23.325822	2020-10-02 13:40:23.325822
615	Pierre	DUPONT	\N	\N	\N	\N	\N	2020-10-02 13:40:24.479232	2020-10-02 13:40:24.479232
616	\N	RONDO VENEZIANO	\N	\N	\N	\N	\N	2020-10-02 13:40:24.642907	2020-10-02 13:40:24.642907
617	Robert Van	BERINGEN	\N	\N	\N	\N	\N	2020-10-02 13:40:24.667618	2020-10-02 13:40:24.667618
618	\N	KING HENRY VIII	\N	\N	\N	\N	\N	2020-10-02 13:40:24.815005	2020-10-02 13:40:24.815005
624	\N	STING	\N	\N	\N	\N	\N	2020-10-02 13:40:25.094925	2020-10-02 13:40:25.094925
625	Johan	MEY	\N	\N	\N	\N	\N	2020-10-02 13:40:25.107504	2020-10-02 13:40:25.107504
626	\N	VANGELIS	\N	\N	\N	\N	\N	2020-10-02 13:40:25.311644	2020-10-02 13:40:25.311644
627	\N	SHERMAN & MENKEN & WRUBEL	\N	\N	\N	\N	\N	2020-10-02 13:40:25.360714	2020-10-02 13:40:25.360714
628	\N	REINTER	\N	\N	\N	\N	\N	2020-10-02 13:40:25.538318	2020-10-02 13:40:25.538318
629	\N	LIANO	\N	\N	\N	\N	\N	2020-10-02 13:40:25.650093	2020-10-02 13:40:25.650093
630	Robert W.	SMITH 	\N	\N	\N	\N	\N	2020-10-02 13:40:25.758113	2020-10-02 13:40:25.758113
631	\N	SABAN & LEVY	\N	\N	\N	\N	\N	2020-10-02 13:40:25.885785	2020-10-02 13:40:25.885785
632	Willy de	VILLE	\N	\N	\N	\N	\N	2020-10-02 13:40:25.892413	2020-10-02 13:40:25.892413
633	Andrea	RAVIZZA	\N	\N	\N	\N	\N	2020-10-02 13:40:25.894268	2020-10-02 13:40:25.894268
634	Michel	POLNAREFF	\N	\N	\N	\N	\N	2020-10-02 13:40:25.897895	2020-10-02 13:40:25.897895
635	Ramin	DJAWADI	\N	\N	\N	\N	\N	2020-10-02 13:40:25.901958	2020-10-02 13:40:25.901958
636	Bruce	FRASER	\N	\N	\N	\N	\N	2020-10-02 13:40:25.903919	2020-10-02 13:40:25.903919
637	Roger	GLOVER	\N	\N	\N	\N	\N	2020-10-02 13:40:25.907997	2020-10-02 13:40:25.907997
638	Sam	BROWN	\N	\N	\N	\N	\N	2020-10-02 13:40:25.911955	2020-10-02 13:40:25.911955
639	Thomas	BERGERSEN	\N	\N	\N	\N	\N	2020-10-02 13:40:25.916863	2020-10-02 13:40:25.916863
640	Nicolas	SCHIFF	\N	\N	\N	\N	\N	2020-10-02 13:40:25.919214	2020-10-02 13:40:25.919214
641	Robert	FIENGA	\N	\N	\N	\N	\N	2020-10-02 13:40:25.924405	2020-10-02 13:40:25.924405
642	R	GLOEREC	\N	\N	\N	\N	\N	2020-10-02 13:40:25.928256	2020-10-02 13:40:25.928256
643	D.	WYCKHUYS	\N	\N	\N	\N	\N	2020-10-02 13:40:25.938234	2020-10-02 13:40:25.938234
644	Franco	CEZARINI	\N	\N	\N	\N	\N	2020-10-02 13:40:25.942661	2020-10-02 13:40:25.942661
645	Harry	GREGSON – WILLIAMS	\N	\N	\N	\N	\N	2020-10-02 13:40:25.981583	2020-10-02 13:40:25.981583
646	Jiri	KABAT	\N	\N	\N	\N	\N	2020-10-02 13:40:25.983515	2020-10-02 13:40:25.983515
647	Nenad	JANKOVIC	\N	\N	\N	\N	\N	2020-10-02 13:40:25.999392	2020-10-02 13:40:25.999392
648	Stephen	ROBERTS	\N	\N	\N	\N	\N	2020-10-02 13:40:26.001624	2020-10-02 13:40:26.001624
649	Wayne	SHANKLIN	\N	\N	\N	\N	\N	2020-10-02 13:40:26.00665	2020-10-02 13:40:26.00665
650	François	ROUSSELOT	\N	\N	\N	\N	\N	2020-10-02 13:40:26.013163	2020-10-02 13:40:26.013163
651	Peter	GRAHAM	\N	\N	\N	\N	\N	2020-10-02 13:40:26.020632	2020-10-02 13:40:26.020632
652	Lin-Manuel	MIRANDA	\N	\N	\N	\N	\N	2020-10-02 13:40:26.02474	2020-10-02 13:40:26.02474
653	Matt	CONAWAY	\N	\N	\N	\N	\N	2020-10-02 13:40:26.026868	2020-10-02 13:40:26.026868
654	Zequinha	ABREU	\N	\N	\N	\N	\N	2020-10-02 13:40:26.032797	2020-10-02 13:40:26.032797
655	\N	COARD	\N	\N	\N	\N	\N	2020-10-02 13:40:26.237804	2020-10-02 13:40:26.237804
656	\N	QUERU	\N	\N	\N	\N	\N	2020-10-02 13:40:26.326323	2020-10-02 13:40:26.326323
657	\N	SHARP GOULD ARNOLD	\N	\N	\N	\N	\N	2020-10-02 13:40:26.330627	2020-10-02 13:40:26.330627
658	\N	RUDD	\N	\N	\N	\N	\N	2020-10-02 13:40:26.351201	2020-10-02 13:40:26.351201
659	Alexandre / Alain	POTTIER / MONCELLE	\N	\N	\N	\N	\N	2020-10-02 13:40:26.389908	2020-10-02 13:40:26.389908
660	\N	SIGNARD	\N	\N	\N	\N	\N	2020-10-02 13:40:26.497551	2020-10-02 13:40:26.497551
661	Jean-Michel	BONDEAUX	\N	\N	\N	\N	\N	2020-10-02 13:40:26.663274	2020-10-02 13:40:26.663274
662	\N	TILLIARD	\N	\N	\N	\N	\N	2020-10-02 13:40:26.687832	2020-10-02 13:40:26.687832
663	Flora	Lyonnet	\N	\N	\N	\N	\N	2020-10-02 14:14:09.558266	2020-10-02 14:14:09.558266
664	Serge	Dessautel	\N	\N	\N	\N	\N	2020-10-02 14:14:09.592743	2020-10-02 14:14:09.592743
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: romain
--

COPY public.schema_migrations (version) FROM stdin;
20191122144004
20191122144050
20191125120311
20191125120317
20191125132003
20191126202916
20191127134740
20191203152528
20191203201916
20191203203740
20191207134040
20191207134229
20200128091821
20200128140939
20200205122159
20200206102708
20200310165705
20200316083028
20200316083123
20200316083424
20200708123509
20200914081400
20201229065234
\.


--
-- Data for Name: scores; Type: TABLE DATA; Schema: public; Owner: romain
--

COPY public.scores (id, comment, acquisition_date, classification, number, conductor_ref, archive_ref, audio_support, editor, title, duration, difficulty, writing_date, editing_date, score_type, conductor_type, style, substyle, created_at, updated_at, composed_by, arranged_by) FROM stdin;
1	Des copies papiers ont été faites	\N	Ballet	1	ArmCond	Ballet - 1 - ArmCond	t	Evette et Schaeffer (12091)	BALLET EGYPTIEN N° 1 - 2	\N	\N	1875	?	Carton	Réduit en Sib	Classique	\N	2020-10-02 13:40:24.425014	2020-10-02 13:40:24.425014	339	237
2	Des copies papiers ont été faites, le conducteur contient un livret sur Berlioz et son œuvre	\N	Ballet	2	\N	Ballet - 2	t	Andrieu Frères	MENUET DES FOLLETS (de la Damnation de Faust)	\N	\N	1846	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.429815	2020-10-02 13:40:24.429815	46	326
3	\N	\N	Ballet	2	\N	Ballet - 2	t	L’accord parfait (Lyon, Bellecours)	MENUET DU ROI	\N	\N	?	1901	Carton	Absent	\N	\N	2020-10-02 13:40:24.434209	2020-10-02 13:40:24.434209	302	105
4	Des copies papiers ont été faites	\N	Ballet	3	ArmCond	Ballet - 3	t	Evette et Schaeffer (12091)	BALLETS EGYPTIENS N° 3 - 4	\N	\N	1875	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.437621	2020-10-02 13:40:24.437621	339	237
5	Autrement appelé La Nymphe de Diane »	\N	Ballet	4	ArmCond	Ballet - 4	t	Billaudot (LB858)	SYLVIA	\N	\N	1876	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.440882	2020-10-02 13:40:24.440882	153	75
6	\N	\N	Ballet	5	\N	Ballet - 5	t	Andrieu Frères	FAUST (Nuit de VALPURGIS)	\N	Normal	1859	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.444051	2020-10-02 13:40:24.444051	233	120
7	\N	\N	Ballet	6	ArmCond	Ballet - 6	t	Evette et Schaeffer (13062)	ROSAMUNDE N° 1 - 3	\N	\N	1823	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.447177	2020-10-02 13:40:24.447177	517	120
8	\N	\N	Ballet	7	ArmCond	Ballet - 7	t	Evette et Schaeffer (13061)	ROSAMUNDE N° 2	\N	\N	1823	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.452875	2020-10-02 13:40:24.452875	517	120
9	Pièce en 4 mouvements (Gaillarde, Pavane, scene du bouquet, lesquercarde)	\N	Ballet	8	ArmCond	Ballet - 8	t	Andrieu	LE ROI S'AMUSE (Scene du Bal)	\N	\N	1882	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.459004	2020-10-02 13:40:24.459004	153	136
10	\N	\N	Ballet	9	ArmCond	Ballet - 9	t	Andrieu Freres	SUITE - BALLETS	\N	\N	1900 ?	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.463416	2020-10-02 13:40:24.463416	460	\N
11	Autrement appelé La fille aux yeux d'émail », œuvre qui marque le début des ballets symphonique (l musique est pensé pour le concert.	\N	Ballet	10	\N	Ballet - 10	t	Robert Martin	COPPELIA	\N	\N	1870	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.467812	2020-10-02 13:40:24.467812	153	75
12	\N	\N	Ballet	11	\N	Ballet - 11	t	Evette et Schaeffer (11984)	BALLET D’ISOLINE	\N	\N	1888	?	Carton	Réduit en Sib	Classique	Opéra	2020-10-02 13:40:24.472146	2020-10-02 13:40:24.472146	373	177
13	Ballet en six parties	\N	Ballet	12	ArmCond	Ballet - 12	t	Andrieu Freres	LES DEUX PIGEONS	\N	\N	1886	?	Carton	Réduit en Sib	Classique	Opéra	2020-10-02 13:40:24.475874	2020-10-02 13:40:24.475874	373	136
14	\N	\N	Classique	1	\N	Classique - 1	t	Andrieu Freres	PAVANE POUR UNE INFANTE DEFUNTE	\N	\N	1899	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.481517	2020-10-02 13:40:24.481517	480	615
15	\N	\N	Classique	1	ArmCond	Classique - 1	t	Robert Martin (R 1554 M)	GRANDE SARABANDE	\N	\N	1739	1977	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.485427	2020-10-02 13:40:24.485427	245	8
16	\N	\N	Classique	2	\N	Classique - 2	t	Evette et Schaeffer (12065)	PEER GYNT N° 1 ET 2 (Le matin, La mort d’Ase)	455	Difficile	1891	?	Carton	Réduit en Sib	Classique	\N	2020-10-02 13:40:24.489901	2020-10-02 13:40:24.489901	236	34
17	\N	\N	Classique	3	ArmCond	Classique - 3	t	Buffet Crampon 12063	PEER GYNT N° 3 ET 4	\N	\N	1891	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.493281	2020-10-02 13:40:24.493281	236	34
18	Un conducteur en Sib réduit et un manuscrit complet en Ut.	\N	Classique	4	ArmCond	Classique - 4	t	Evette et Schaeffer (20106)	SONATE PATHETIQUE	530	\N	1799	?	Vieux papier	Complet transposé	\N	\N	2020-10-02 13:40:24.496516	2020-10-02 13:40:24.496516	41	119
19	\N	\N	Classique	5	ArmCond	Classique - 5	t	Robert Martin (R 1153 M)	ROMANCE EN FA MAJEUR (Op 50)	480	\N	1798	1969	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.499724	2020-10-02 13:40:24.499724	41	130
20	\N	\N	Classique	6	\N	Classique - 6	t	Harmonie de la Chazotte	SERENADE	\N	\N	?	?	Carton	Absent	\N	\N	2020-10-02 13:40:24.502619	2020-10-02 13:40:24.502619	427	\N
21	Sonate au claire de Lune	\N	Classique	6	\N	Classique - 6	t	Harmonie de la Chazotte	SONATE OP 14, ANDANTE	\N	\N	1801	?	Carton	Absent	\N	\N	2020-10-02 13:40:24.50574	2020-10-02 13:40:24.50574	41	427
22	\N	\N	Classique	7	\N	Classique - 7	t	Andrieu Freres	1 ère SYMPHONIE EN UT MAJEUR (n°4 Adagio)	\N	\N	1800	?	Carton	Absent	\N	\N	2020-10-02 13:40:24.508894	2020-10-02 13:40:24.508894	41	326
23	\N	\N	Classique	8	\N	Classique - 8	t	Andrieu Freres	1 ère SYMPHONIE EN UT MAJEUR (n°2 Andante cantabile con moto)	\N	\N	1800	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.512433	2020-10-02 13:40:24.512433	41	326
24	On a la partie réduite pour Piano	\N	Classique	9	ArmCond	Classique - 9	t	Evette et Schaeffer (13325)	CELEBRE ANDANTE (Op86)	\N	\N	?	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.516137	2020-10-02 13:40:24.516137	402	608
25	\N	Dirigée par Jean Ollier	Classique	9	\N	Classique - 9	t	Andrieu Freres	CONFIDENCE	\N	\N	1919	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.519359	2020-10-02 13:40:24.519359	154	7
26	Des copies papiers ont été faites	\N	Classique	10	ArmCond	Classique - 10	t	Andrieu Freres (S.E.A.438)	SYMPHONIE INACHEVEE	\N	\N	1822	?	Carton	Réduit en Sib	Classique	Romantique	2020-10-02 13:40:24.522757	2020-10-02 13:40:24.522757	517	185
27	Autre arrangement de la symphonie inachevée de Shubert, apporté par Jean-François Bonura, jamais joué.	\N	Classique	10	ArmCond	Classique - 10	t	Evette et Schaeffer (20062)	ANDANTE DE LA SYMPHONIE POSTHUME	\N	\N	1821	?	\N	Réduit en Sib	\N	\N	2020-10-02 13:40:24.526477	2020-10-02 13:40:24.526477	517	379
28	Tampon Union Musicale CASINO	\N	Classique	11	\N	Classique - 11	t	Ecrit à la main	POLONAISE	\N	\N	1842	?	Carton	Absent	\N	\N	2020-10-02 13:40:24.530367	2020-10-02 13:40:24.530367	121	\N
29	Des copies papier ont été faites Andante cf Grenier	\N	Classique	12	\N	Classique - 12	t	Andrieu	5ème SYMPHONIE EN UT MINEUR – Allegro con brio	\N	\N	1808	?	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:24.53429	2020-10-02 13:40:24.53429	41	93
30	\N	1968-03-05	Classique	13	ArmCond	Classique - 13	t	Harmonie de la Chazotte	ROMANCE POUR COR – SONATE N°2 ANDANTE	\N	\N	1710	1968	Carton	Complet transposé	\N	\N	2020-10-02 13:40:24.537576	2020-10-02 13:40:24.537576	245	427
31	La légende dit qu'elle aurait été composé par Henry VIII pour Anne Boleyn	\N	Classique	14	+ ArmCond	Classique - 14	t	Jenson Publication 215-07020	GREENSLEEVES	300	\N	1580	1979	Papier neuf	Complet transposé	Musique traditionnelle	2 Celtique	2020-10-02 13:40:24.540533	2020-10-02 13:40:24.540533	\N	538
32	\N	\N	Classique	14	ArmCond	Classique - 14	t	Robert Martin (R 1156 M)	CANON SUR UNE BASSE OBSTINEE	387	\N	1700	1969	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:24.543842	2020-10-02 13:40:24.543842	432	8
33	\N	\N	Classique	15	ArmCond	Classique - 15	t	Molenaar	GENOVEVA (Poeme symphonique)	\N	\N	1962	1963	Papier neuf	Réduit en Sib	\N	\N	2020-10-02 13:40:24.546842	2020-10-02 13:40:24.546842	166	\N
34	\N	\N	Classique	15	ArmCond	Classique - 15	t	Molenaar 03.1343.08	FOUR CONTRASTS FOR WIND	450	\N	1974	1975	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:24.549863	2020-10-02 13:40:24.549863	193	\N
35	\N	\N	Classique	15	\N	Classique - 15	t	Billaudot (G 3753 B)	PETITE SYMPHONIE LANDAISE	\N	\N	1984	1985	Papier neuf	Absent	\N	\N	2020-10-02 13:40:24.552936	2020-10-02 13:40:24.552936	163	\N
36	Avec 2 trompettes solo	\N	Classique	16	\N	Classique - 16	t	Transatlantiques E.M.T.1314	CONCERTO POUR DEUX TROMPETTES	\N	\N	début XVIII	1975	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:24.55646	2020-10-02 13:40:24.55646	587	500
37	\N	\N	Classique	17	ArmCond	Classique - 17	t	Evette et Schaeffer (12262)	THAIS	\N	\N	1894	?	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:24.559853	2020-10-02 13:40:24.559853	358	221
38	\N	\N	Classique	17	ArmCond	Classique - 17	t	De HASKE	PASTORALE	\N	\N	1985	1985	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:24.563052	2020-10-02 13:40:24.563052	243	\N
426	\N	2015	Fantaisie	107	\N	Fantaisie - 107	t	\N	DEMASIADO CORAZON	200	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.895838	2020-10-02 13:40:25.895838	632	633
39	Pièce récompensé au ABA Ostwald Award	\N	Classique	18	ArmCond	Classique - 18	t	Barnhouse 1579	KYRIE AND GLORIA	\N	\N	1977	1980	Papier neuf	Complet en Ut	\N	\N	2020-10-02 13:40:24.566691	2020-10-02 13:40:24.566691	260	\N
40	\N	\N	Classique	18	ArmCond	Classique - 18	t	Barnhouse 1640	AVANTIA (OVERTURE FOR BAND)	375	Normal	1982	1982	Papier neuf	Complet transposé	Classique	Ouverture	2020-10-02 13:40:24.573499	2020-10-02 13:40:24.573499	525	\N
41	Arrangement en 5 mouvements	\N	Classique	18	ArmCond	Classique - 18	t	Molenaar	MUSIC FOR THE ROYAL FIREWORKS (Arr. Arend))	610	\N	1749	1960	Vieux papier	Réduit en Sib	Classique	Baroque	2020-10-02 13:40:24.582597	2020-10-02 13:40:24.582597	245	13
42	\N	\N	Classique	19	\N	Classique - 19	t	Alfred Publishing 31728S	CANTIQUE DE JEAN RACINE	384	\N	1865	2009	Papier neuf	Complet transposé	Classique	\N	2020-10-02 13:40:24.592129	2020-10-02 13:40:24.592129	181	406
43	\N	\N	Classique	19	ArmCond	Classique - 19	t	Alfred Publishing	CHORALE PRELUDE IN Eb	\N	\N	?	1980	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.6008	2020-10-02 13:40:24.6008	25	297
44	\N	\N	Classique	19	\N	Classique - 19	t	De HASKE 87076	THE YOUNG AMADEUS (le jeune mozart)	\N	\N	1987	1987	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:24.6073	2020-10-02 13:40:24.6073	402	243
45	\N	\N	Classique	19	\N	Classique - 19	t	Molenaar 03.1362.07	PAVANE IN BLUE	202	\N	1974	1976	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:24.611599	2020-10-02 13:40:24.611599	268	\N
46	\N	\N	Classique	20	\N	Classique - 20	t	Molenaar 03.1282.03	ODE AN DIE FREUDE (Ode a la joie)	\N	\N	1824	1972	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:24.61613	2020-10-02 13:40:24.61613	41	253
47	Fresque historique sur des airs de l'époque pour le bicentenaire de la révolution française	\N	Classique	20	ArmCond	Classique - 20	t	Robert Martin (R 2137 M)	ET QUE VIVE LA LIBERTE !	\N	\N	1988	1988	Papier neuf	Réduit en Sib	\N	\N	2020-10-02 13:40:24.620446	2020-10-02 13:40:24.620446	\N	152
48	Il peut y avoir des doutes si ce concerto a été écrit par Benedetto ou bien par son frère Alessandro	\N	Classique	20	\N	Classique - 20	t	Molenaar	CONCERTO EN UT MINEUR (Pour hautbois ou saxo soprano)	\N	Facile	?	1960	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:24.625074	2020-10-02 13:40:24.625074	348	203
49	Concerto pour Flute écrit pour les étudiant du conservatoire de Paris	\N	Classique	20	ArmCond	Classique - 20	t	Carl Fischer N3357	CONCERTINO (concerto pour flute)	480	\N	1902	1960	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:24.62862	2020-10-02 13:40:24.62862	112	607
50	Transcription par Pascal ROA du NOAC, conducteur manuscrit	\N	Classique	21	\N	Classique - 21	t	Partition manuscrites NOAC	LARGO SYMPHONIE N° 9	510	\N	1893	1988	Vieux papier	Complet transposé	Classique	1 Romantique	2020-10-02 13:40:24.631926	2020-10-02 13:40:24.631926	170	490
51	Concerto pour basson	\N	Classique	21	ArmCond	Classique - 21	t	Molenaar 01.1987.05	KONZERT FUR FAGOTT (Concerto pour basson)	\N	\N	1774	1990	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.635912	2020-10-02 13:40:24.635912	402	417
52	Arrangement fait à partir d'une partition symphonique	\N	Classique	21	ArmCond	Classique - 21	t	Partition manuscrites NOAC	VALSE TRISTE (Op44)	390	\N	1903	?	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:24.640052	2020-10-02 13:40:24.640052	532	490
53	Morceau composé par Gian Piero Reverberi e Laura Giordano, interprété par le groupe Rondo Veneziano	\N	Classique	21	ArmCond	Classique - 21	t	Molenaar 01.1949.06	SINFONIA PER UN ADDIO (symphonie pour un adieu)	\N	\N	1981	1983	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:24.645488	2020-10-02 13:40:24.645488	616	252
54	Composé par Remo Giazotto à partir d'éléments emprunté à une œuvre de Tomaso Albinoni	\N	Classique	22	\N	Classique - 22	t	Molenaar 03.1818.03	ADAGIO (Albinoni)	290	\N	1945	1986	Papier neuf	Réduit en Ut	Classique	\N	2020-10-02 13:40:24.649163	2020-10-02 13:40:24.649163	5	253
55	L'ave Maria de Gounod, autrement appelé Méditation sur un prélude de Bach	\N	Classique	22	\N	Classique - 22	t	Molenaar 03.2096.05	THE YOUNG MARIA (avec percussion)	\N	\N	1853	1991	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:24.652646	2020-10-02 13:40:24.652646	233	268
56	\N	\N	Classique	22	\N	Classique - 22	t	Molenaar 03.2096.05	COFFEE SERENADE	225	\N	?	1991	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:24.655905	2020-10-02 13:40:24.655905	268	\N
57	Un conducteur complet de Jean-François Bonura est dans l'armoire à Conducteur	\N	Classique	22	ArmCond	Classique - 22	t	Molenaar 01.1992.05	PAVANE	390	\N	1887	1990	Papier neuf	Réduit en Ut	Classique	\N	2020-10-02 13:40:24.659664	2020-10-02 13:40:24.659664	181	258
58	\N	\N	Classique	22	ArmCond	Classique - 22	t	De HASKE 88106	JULIA (« Samen Zijn »)	300	Facile	1987	1988	Papier neuf	Réduit en Ut	Variété	3 Variété internationale	2020-10-02 13:40:24.663857	2020-10-02 13:40:24.663857	21	59
59	\N	\N	Classique	22	ArmCond	Classique - 22	t	De HASKE 88112	MELODIA D'AMORE (Melodie d’Amour)	300	Facile	?	1988	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.66962	2020-10-02 13:40:24.66962	223	617
60	\N	\N	Classique	22	\N	Classique - 22	t	Robert Martin (R 2262 M)	SEPT CHORALS BWR 639 (choral n°5)	\N	\N	1710	1990	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.672763	2020-10-02 13:40:24.672763	25	82
61	\N	\N	Classique	23	\N	Classique - 23	t	Robert Martin (R. 2376 M.)	CONCERTO FOR TRUMPET (Hummel)	\N	\N	1803	1992	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.675862	2020-10-02 13:40:24.675862	269	163
62	\N	\N	Classique	23	\N	Classique - 23	t	Robert Martin (R 2339 M)	LA MOLDAU	\N	\N	1879	1991	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:24.678942	2020-10-02 13:40:24.678942	536	615
63	\N	\N	Classique	24	\N	Classique - 24	t	Robert Martin (R. 1088 M.)	MUSIQUE FUNEBRE	\N	\N	1844	?	Vieux papier	Absent	\N	\N	2020-10-02 13:40:24.681986	2020-10-02 13:40:24.681986	590	163
64	\N	\N	Classique	24	\N	Classique - 24	t	Molenaar 01.2072.08	ATMOSPHERES	\N	\N	1989	1991	Papier neuf	Complet en Ut	\N	\N	2020-10-02 13:40:24.684611	2020-10-02 13:40:24.684611	228	\N
65	Ballade traditionnelle anglaise d'origine moyen ageuse, rendue connue par Simon & Garfunkel en 1966	\N	Classique	24	\N	Classique - 24	t	Hal Leonard Publishing	SCARBOROUGH FAIR	270	Normal	XVIe	1989	Papier neuf	Complet transposé	Musique traditionnelle	2 Celtique	2020-10-02 13:40:24.687701	2020-10-02 13:40:24.687701	143	\N
66	10 mouvements	\N	Classique	25	\N	Classique - 25	t	Schott'sSohne Mains	CARMINA BURANA	858	Difficile	1935	1958	Vieux papier	Réduit en Sib	Classique	\N	2020-10-02 13:40:24.690994	2020-10-02 13:40:24.690994	429	381
67	En archive Armoire Conducteur un arrangement de Calvin Custer de l'Adagio	\N	Classique	25	\N	Classique - 25	t	Schirmer	ADAGIO (barber)	360	Difficile	1935	1992	Papier neuf	Complet transposé	Classique	\N	2020-10-02 13:40:24.694242	2020-10-02 13:40:24.694242	32	281
68	Oeuvre écrite pour l'enterrement de Carl Maria Von Weber	\N	Classique	25	\N	Classique - 25	t	/	TRAUERSINFONIE	\N	\N	1844	?	Vieux papier	Absent	\N	\N	2020-10-02 13:40:24.697314	2020-10-02 13:40:24.697314	590	\N
69	\N	\N	Classique	25	ArmCond	Classique - 25	t	Belwyn Mills Publishing Corp BD628	ELSA'S PROCESSION TO THE CATHEDRAL (Extr LOHENGRIN)	330	\N	1848	1977	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.700857	2020-10-02 13:40:24.700857	590	176
70	\N	\N	Classique	25	ArmCond	Classique - 25	t	Boosey & Hawkes H.11069.	THE PLANETS	\N	\N	1917	1924	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:24.70439	2020-10-02 13:40:24.70439	265	\N
71	Concerto euphonium trombone	\N	Classique	26	\N	Classique - 26	t	Molenaar 03.2288.04	SALVE MARIA	360	\N	1864	1996	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.708469	2020-10-02 13:40:24.708469	372	253
72	Concerto clarinette	\N	Classique	26	\N	Classique - 26	t	Robert Martin (R 2631 M)	CONCERTO N°1 EN FA MINEUR pour clarinette (cf ref 4)	1340	\N	1811	1995	Papier neuf	Complet transposé	Classique	\N	2020-10-02 13:40:24.71165	2020-10-02 13:40:24.71165	601	82
73	\N	\N	Classique	27	\N	Classique - 27	t	Music Works (04001768)	DIES IRAE (extr Requiem)	230	Difficile	1874	1998	Papier neuf	Complet transposé	Classique	Romantique	2020-10-02 13:40:24.714599	2020-10-02 13:40:24.714599	583	56
74	\N	\N	Classique	27	\N	Classique - 27	t	Amstel Music	WALTZ n°2 (Valse n°2) (from jazz suite n°2)	240	Normal	1938	1994	Papier neuf	Complet transposé	Classique	\N	2020-10-02 13:40:24.717741	2020-10-02 13:40:24.717741	531	366
75	\N	\N	Classique	27	\N	Classique - 27	t	La Chaux-du-milieu 2405	LARGO FROM XERXES	190	Facile	1738	?	Papier neuf	Complet transposé	Classique	Opéra	2020-10-02 13:40:24.720842	2020-10-02 13:40:24.720842	245	79
76	\N	\N	Classique	28	\N	Classique - 28	t	Molenaar 01.2338.09	SUITE ACCADIENNE	820	Normal	1997	1997	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.723718	2020-10-02 13:40:24.723718	57	\N
77	2eme prix concours de composition pour harmonie 1998	\N	Classique	28	\N	Classique - 28	t	Robert Martin (R 3038 M)	EL CAMINO DE SANTIAGO	480	Normal	1998	1999	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.726535	2020-10-02 13:40:24.726535	183	\N
78	\N	\N	Classique	29	\N	Classique - 29	t	Amstel Music	LA QUINTESSENZA	720	Difficile	1997	1998	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.729371	2020-10-02 13:40:24.729371	366	\N
79	\N	\N	Classique	29	\N	Classique - 29	t	De HASKE 960696	CARO MIO BEN	150	Facile	1785	1996	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.73247	2020-10-02 13:40:24.73247	220	264
80	\N	\N	Classique	29	\N	Classique - 29	t	Robert Martin (R 2805 M)	BRAHMS (3eme mouvement 3eme symphonie)	360	Normal	1862	1997	Papier neuf	Complet transposé	Classique	\N	2020-10-02 13:40:24.735868	2020-10-02 13:40:24.735868	84	163
81	1er mouvement 11'30	\N	Classique	30	\N	Classique - 30	t	Robert Hindsley publisher	SYMPHONIE DU NOUVEAU MONDE 1 - 2 - 3 – FINAL	690	Très difficile	1893	1986	Papier neuf	Absent	Classique	1 Romantique	2020-10-02 13:40:24.738931	2020-10-02 13:40:24.738931	170	262
82	solo tuba	\N	Classique	31	\N	Classique - 31	t	CurnoW Music Press	PRAYER	\N	Difficile	1872	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.742164	2020-10-02 13:40:24.742164	233	141
83	Extrait des Tableaux d'une exposition	\N	Classique	31	\N	Classique - 31	t	Marc Reift EMR 1048	THE GREAT GATE OK KIEV (la grande porte de kiev)	156	\N	1874	1997	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.746149	2020-10-02 13:40:24.746149	401	397
84	Extrait de la bande original du film Les Temps Modernes	\N	Classique	31	\N	Classique - 31	t	Marc Reift EMR 1743	SMILE	\N	\N	1936	1954	Papier neuf	Complet transposé	Musique de film	1 Comédies	2020-10-02 13:40:24.750209	2020-10-02 13:40:24.750209	114	397
85	Musique de ballet	\N	Classique	31	\N	Classique - 31	t	Scomegna	LA BELLE AU BOIS DORMANT	321	\N	1889	1998	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.753733	2020-10-02 13:40:24.753733	566	100
86	\N	\N	Classique	32	\N	Classique - 32	t	Marc Reift EMR 1654	AMADEUS (Lacrimosa et Dies Irae extr Requiem)	\N	\N	1791	1999	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.756746	2020-10-02 13:40:24.756746	402	397
87	\N	\N	Classique	32	\N	Classique - 32	t	Anglo Music Press AMP009	PANIS ANGELICUS (extrait de la Messe Solennelle)	\N	Normal	1872	2001	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.759781	2020-10-02 13:40:24.759781	198	541
88	Oeuvre pour piano à l'origine	\N	Classique	32	\N	Classique - 32	t	Mythen Hollanda	LA FILLE AUX CHEVEUX DE LIN	\N	\N	1910	2001	Papier neuf	Complet transposé	Classique	Xxeme siècle	2020-10-02 13:40:24.762812	2020-10-02 13:40:24.762812	151	94
89	\N	\N	Classique	33	\N	Classique - 33	t	Evette et Schaeffer 12780	SYMPHONIE FANTASTIQUE (5 PARTIES)	\N	\N	1830	?	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:24.765868	2020-10-02 13:40:24.765868	46	221
90	\N	\N	Classique	33	\N	Classique - 33	t	Jenson Publication 244-16010	PETER AND THE WOLF (PIERRE ET LE LOUP)	495	\N	1936	1986	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.768928	2020-10-02 13:40:24.768928	470	142
91	Il existait a une époque une référence en fantaisie 59	\N	Classique	34	\N	Classique - 34	t	Jenson Publications 20823272	FANTASIA	406	\N	-	1992	Papier neuf	Complet transposé	Classique	Pot-pourri	2020-10-02 13:40:24.771911	2020-10-02 13:40:24.771911	\N	141
92	avec choeur	\N	Classique	34	\N	Classique - 34	t	Editon MT Musique	LACRIMOSA (du requiem)	\N	\N	1791	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.775049	2020-10-02 13:40:24.775049	402	482
93	\N	\N	Classique	35	\N	Classique - 35	t	Evette et Schaeffer (12227)	CAVATINE DU BARBIER DE SEVILLE (cornet solo)	\N	\N	1816	?	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:24.777975	2020-10-02 13:40:24.777975	497	494
94	\N	\N	Classique	35	\N	Classique - 35	t	De HASKE 1012588	O MIO BABBINO CARO (extrait de Gianni Scchicchi)	\N	Normal	1918	2001	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.780716	2020-10-02 13:40:24.780716	471	561
95	Concerto pour anche, Clarinette à l'origine (sax, clar, ou htbois), œuvre souvent attribuée à tard à Richard Wagner.	\N	Classique	35	\N	Classique - 35	t	Scherzando 0051.91 S	ADAGIO (baermann)	280	\N	1821	?	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:24.783559	2020-10-02 13:40:24.783559	29	14
96	Oeuvre attribuée à tord à Caccini, cet avé Maria est un pastiche enrigistré en 1970 par Vladimir Vavilov d'un auteur inconnu du XVIe siècle.	\N	Classique	35	\N	Classique - 35	t	De HASKE 1002342	AVE MARIA (Caccini)	287	Facile	1970	2001	Papier neuf	Complet transposé	Classique	\N	2020-10-02 13:40:24.78693	2020-10-02 13:40:24.78693	99	40
97	\N	\N	Classique	36	\N	Classique - 36	t	Curnow Music Press	FINALE FROM SYMPHONY N° 3	180	Normal	1886	2003	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.791074	2020-10-02 13:40:24.791074	509	141
98	Exrait de l'opéra Martha	\N	Classique	36	\N	Classique - 36	t	Edition Marc Reift EMR 1846	THE LAST ROSE OF SUMMER (La dernière rose)	\N	\N	1847	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.794792	2020-10-02 13:40:24.794792	192	397
99	\N	\N	Classique	36	\N	Classique - 36	t	De HASKE 910273	NESSUN DORMA (extrait de l’opéra Turandot)	165	Facile	1926	1991	Papier neuf	Complet transposé	Classique	\N	2020-10-02 13:40:24.797951	2020-10-02 13:40:24.797951	471	45
100	\N	\N	Classique	36	\N	Classique - 36	t	Masters Music Publication M9085	HUNGARIAN RHAPSODY N° 2 (rhapsodie hongroise)	\N	\N	1847	2002	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.801098	2020-10-02 13:40:24.801098	331	362
101	Hommage à Jean Sébastien Bach, permet au compositeur d'obtenir le 2eme prix au concours de composition pour orchestre d'harmonie de la CMF en 2001.	\N	Classique	37	\N	Classique - 37	t	Robert Martin (R 3651 M)	TEMA E FUGA (Theme et fugue)	319	Difficile	2001	2002	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.804014	2020-10-02 13:40:24.804014	17	\N
102	\N	\N	Classique	37	\N	Classique - 37	t	Music Works (02308)	REQUIEM K626 INTROIT – KIRIE ELEISON	490	\N	1791	2004	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.807088	2020-10-02 13:40:24.807088	402	334
103	\N	\N	Classique	38	\N	Classique - 38	t	orsk Noteservice	PRELUDE (Svendsen)	364	\N	1898	2004	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.810178	2020-10-02 13:40:24.810178	557	86
104	\N	\N	Classique	38	\N	Classique - 38	t	Marc Reift EMR 1667	2001 A SPACE ODYSSEY (Odysse de l'Espace)	\N	\N	\N	1999	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.812873	2020-10-02 13:40:24.812873	551	397
105	\N	\N	Classique	38	\N	Classique - 38	t	Anglo Music Press AMP064	PASTIME WITH A GOOD COMPANY	393	Normal	\N	2004	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.817109	2020-10-02 13:40:24.817109	618	541
106	\N	\N	Classique	38	\N	Classique - 38	t	De HASKE 1002058	ARIOSO	292	Facile	1738	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.819915	2020-10-02 13:40:24.819915	25	242
107	\N	\N	Classique	38	\N	Classique - 38	t	Robert Martin (R 4225 M)	HYMNE A LA NUIT	172	Facile	\N	2005	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.822616	2020-10-02 13:40:24.822616	477	182
108	\N	\N	Classique	39	\N	Classique - 39	t	Mitropa Music	OS JUSTI	\N	Facile	1879	2004	Papier neuf	Complet transposé	Classique	Musique sacrée	2020-10-02 13:40:24.825477	2020-10-02 13:40:24.825477	90	165
109	\N	\N	Classique	39	\N	Classique - 39	t	Ecrit à la main	CONCERTO POUR TROMPETTE (Arutunian)	\N	\N	\N	?	Vieux papier	Complet transposé	\N	\N	2020-10-02 13:40:24.82922	2020-10-02 13:40:24.82922	19	345
110	\N	\N	Classique	39	\N	Classique - 39	t	ED Belwin	MUSIC FOR THE ROYAL FIREWORKS (Arr. Brubaker)	\N	\N	\N	2005	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.833224	2020-10-02 13:40:24.833224	249	89
111	1er mvt joué en 2013, joué en entier en 2011	\N	Classique	40	\N	Classique - 40	t	Plassard Florent	LE SEIGNEUR DES ANNEAUX (3 mvt)	1156	Normal	\N	2009	Papier neuf	Complet transposé	Musique de film	Fantastique	2020-10-02 13:40:24.837045	2020-10-02 13:40:24.837045	530	456
112	En 3 mouvement	\N	Classique	40	\N	Classique - 40	t	Music Works (04002870)	SUITE FOR WINDS AND PERCUSSION	484	\N	\N	2009	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.83995	2020-10-02 13:40:24.83995	586	\N
113	3 mouvements (Marche des soldats, danses des mirlitons, trepak)	\N	Classique	40	\N	Classique - 40	t	Birsh Island Music Press	THE NUTS AND CRACKERS SWEET	210	Facile	1892	2007	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.843011	2020-10-02 13:40:24.843011	566	525
114	\N	\N	Classique	40	\N	Classique - 40	t	Lema Musikforlag	MOULINET POLKA	\N	\N	\N	1981	Vieux papier	Complet transposé	\N	\N	2020-10-02 13:40:24.846089	2020-10-02 13:40:24.846089	552	241
115	Oeuvre pour piano à l'origine	\N	Classique	41	\N	Classique - 41	t	Robert Martin (R 3649 M)	ROMANCE OP.5	294	\N	1868	2002	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.849142	2020-10-02 13:40:24.849142	566	76
116	\N	\N	Classique	41	\N	Classique - 41	t	Anglo Music Press AMP 106	JERUSALEM (hymn)	132	Normal	1916	2004	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:24.852216	2020-10-02 13:40:24.852216	436	541
117	\N	\N	Classique	41	\N	Classique - 41	t	Mitropa Music	ECCE SACERDOS	307	Normal	1885	2009	Papier neuf	Complet transposé	Classique	Musique sacrée	2020-10-02 13:40:24.855364	2020-10-02 13:40:24.855364	90	165
118	\N	\N	Classique	41	\N	Classique - 41	t	Beriato Music	ADAGIO CANTABILE	200	Normal	\N	2010	Papier neuf	Complet transposé	Classique	Romantique	2020-10-02 13:40:24.858609	2020-10-02 13:40:24.858609	41	392
119	\N	\N	Classique	42	\N	Classique - 42	t	HAFABRA Music – Louis Martinus	SLAVONIC DANCE op46 (Danses Slaves)	249	\N	1878	2001	Papier neuf	Complet transposé	Classique	\N	2020-10-02 13:40:24.8618	2020-10-02 13:40:24.8618	170	519
120	Autrement appelé Va, pensiero, extrait de l'opera Nabucco	2014	Classique	42	\N	Classique - 42	t	\N	LE CHOEUR DES ESCLAVES	240	\N	1842	\N	\N	\N	Classique	Opéra	2020-10-02 13:40:24.864581	2020-10-02 13:40:24.864581	583	\N
121	\N	2014	Classique	42	\N	Classique - 42	t	\N	HALLELUJA «  LE MESSI »	230	\N	1741	\N	\N	\N	Classique	Baroque	2020-10-02 13:40:24.867523	2020-10-02 13:40:24.867523	245	\N
122	\N	2014	Classique	43	\N	Classique - 43	t	Robert Martin R4181M	REQUIEM (Version avec choeur)	1290	\N	1791	?	Papier neuf	Complet transposé	Classique	Romantique	2020-10-02 13:40:24.871123	2020-10-02 13:40:24.871123	402	163
123	\N	2015	Classique	44	\N	Classique - 44	t	Hafabra Music – Louis Martinus	HUNGARIAN DANCE N°1 (Danse Hongroise n°1)	235	\N	\N	2004	Papier neuf	Complet transposé	Classique	Romantique	2020-10-02 13:40:24.875263	2020-10-02 13:40:24.875263	84	519
124	\N	2015	Classique	44	\N	Classique - 44	t	\N	FANFARE FOR A CELEBRATION	125	Facile	\N	\N	Papier neuf	Complet transposé	Classique	Défilé	2020-10-02 13:40:24.879173	2020-10-02 13:40:24.879173	376	\N
125	\N	2015	Classique	44	\N	Classique - 44	t	\N	TRIUMPHANT SPIRIT	400	Normal	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:24.882249	2020-10-02 13:40:24.882249	283	\N
126	\N	2016	Classique	45	\N	Classique - 45	t	\N	ORGAN FUGUE	160	Normal	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:24.887004	2020-10-02 13:40:24.887004	25	619
127	\N	2016	Classique	45	\N	Classique - 45	t	\N	MELODIE IN F	260	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:24.88999	2020-10-02 13:40:24.88999	505	253
128	Pièce regrouant plusieurs mouvement de ce ballet en 3 actes	2016	Classique	45	\N	Classique - 45	t	\N	ROMEO AND JULIET	131	\N	1935	\N	\N	\N	Classique	Romantique	2020-10-02 13:40:24.893046	2020-10-02 13:40:24.893046	470	366
129	\N	2016	Classique	46	\N	Classique - 46	t	\N	PASTORALE SYMPHONY N° 6	200	\N	\N	\N	\N	\N	Classique	Romantique	2020-10-02 13:40:24.897964	2020-10-02 13:40:24.897964	41	620
130	\N	2016	Classique	46	\N	Classique - 46	t	\N	ALLEGRETTO SYMPHONY N° 7	225	\N	\N	\N	\N	\N	Classique	Romantique	2020-10-02 13:40:24.901073	2020-10-02 13:40:24.901073	41	620
131	\N	2016	Classique	46	\N	Classique - 46	t	\N	CARMEN HABANERA	120	\N	\N	\N	\N	\N	Classique	Opéra	2020-10-02 13:40:24.906613	2020-10-02 13:40:24.906613	51	621
132	\N	\N	Classique	46	\N	Classique - 46	\N	\N	CONCERTO POUR CLARINETTE Adagio	390	\N	\N	\N	\N	\N	Classique	Romantique	2020-10-02 13:40:24.91183	2020-10-02 13:40:24.91183	402	622
133	\N	\N	Classique	46	\N	Classique - 46	\N	\N	LORRAINE	640	\N	\N	\N	\N	\N	Classique	Xxeme siècle	2020-10-02 13:40:24.915213	2020-10-02 13:40:24.915213	242	\N
134	\N	\N	Classique	47	\N	Classique - 47	\N	\N	LES TEMPLIERS	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:24.918769	2020-10-02 13:40:24.918769	\N	\N
135	\N	\N	Classique	47	\N	Classique - 47	\N	\N	BARCAROLLES OP 37 N°6	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:24.921862	2020-10-02 13:40:24.921862	\N	\N
136	\N	\N	Classique	47	\N	Classique - 47	\N	\N	THE DA VINCI CODE	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:24.924275	2020-10-02 13:40:24.924275	\N	\N
137	\N	\N	Classique	48	\N	Classique - 48	\N	\N	VALSE DES PATINEURS	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:24.926666	2020-10-02 13:40:24.926666	\N	\N
138	\N	\N	Fantaisie	1	ArmCond	Fantaisie - 1	t	Molenaar	THEME D'ORGUE	\N	\N	\N	1979	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:24.929038	2020-10-02 13:40:24.929038	441	\N
139	\N	\N	Fantaisie	1	\N	Fantaisie - 1	t	Kender Music	PASTORAL AND CONTREPUNCT (pastorale et contrepoint)	375	4	\N	1982	Papier neuf	Complet en Ut	\N	\N	2020-10-02 13:40:24.931659	2020-10-02 13:40:24.931659	69	\N
140	\N	\N	Fantaisie	2	+ ArmCond	Fantaisie - 2	t	Molenaar Music	THE BEATLES IN CONCERT	360	\N	\N	1965	Papier neuf	Réduit en Ut	Variété	\N	2020-10-02 13:40:24.934353	2020-10-02 13:40:24.934353	322	253
141	\N	\N	Fantaisie	2	\N	Fantaisie - 2	t	Molenaar	MANHATTAN SYMPHONY	\N	\N	1962	1964	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:24.936846	2020-10-02 13:40:24.936846	307	163
142	\N	\N	Fantaisie	3	\N	Fantaisie - 3	t	Robert Martin (R1635M)	REFRAINS DE PARIS	\N	\N	\N	?	Vieux papier	Réduit en Sib	Variété	Pot-pourri	2020-10-02 13:40:24.939281	2020-10-02 13:40:24.939281	\N	152
143	\N	\N	Fantaisie	3	\N	Fantaisie - 3	t	\N	GAMMES ET VARIATIONS	185	\N	\N	?	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:24.941938	2020-10-02 13:40:24.941938	64	163
144	\N	\N	Fantaisie	3	\N	Fantaisie - 3	t	Molenaar	HYMNE A LA MUSIQUE	230	\N	\N	1972	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:24.944282	2020-10-02 13:40:24.944282	307	\N
145	\N	\N	Fantaisie	4	\N	Fantaisie - 4	t	Molenaar	RAPSODIE SUR DES THEMES BRETONS	412	\N	\N	1978	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:24.94699	2020-10-02 13:40:24.94699	307	\N
146	\N	\N	Fantaisie	5	+ ArmCond	Fantaisie - 5	t	De HASKE	FOX FROM THE NORTH	196	\N	\N	1986	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:24.94976	2020-10-02 13:40:24.94976	574	242
147	\N	\N	Fantaisie	5	ArmCond	Fantaisie - 5	t	CHAPPELL	FOOTBALL ASSOCIATION	\N	\N	\N	?	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:24.952318	2020-10-02 13:40:24.952318	328	163
148	\N	\N	Fantaisie	6	\N	Fantaisie - 6	t	Harmonia	RAPSODIE RUSSE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.955031	2020-10-02 13:40:24.955031	28	\N
149	\N	\N	Fantaisie	7	\N	Fantaisie - 7	t	Robert Martin	SURPRISE PARTY CHEZ VINCENT SCOTTO	\N	\N	\N	?	Carton	Absent	Danse	Pot-pourri	2020-10-02 13:40:24.957803	2020-10-02 13:40:24.957803	\N	152
150	\N	\N	Fantaisie	8	ArmCond	Fantaisie - 8	t	Evette et Schaeffer (E. S. 1665)	AUBADE PRINTANIERE	\N	\N	\N	?	Carton	Réduit en Ut	\N	\N	2020-10-02 13:40:24.961166	2020-10-02 13:40:24.961166	303	604
151	\N	\N	Fantaisie	9	\N	Fantaisie - 9	t	Evette et Schaeffer	LA CZARINE	\N	\N	\N	?	Carton	Réduit en Sib	Musique traditionnelle	Mazurka	2020-10-02 13:40:24.964996	2020-10-02 13:40:24.964996	208	502
152	\N	\N	Fantaisie	10	ArmCond	Fantaisie - 10	t	Robert Martin	LE CORSO BLANC	\N	\N	\N	?	Carton	Réduit en Sib	Musique traditionnelle	Polka	2020-10-02 13:40:24.967916	2020-10-02 13:40:24.967916	568	539
153	\N	\N	Fantaisie	10	+ ArmCond	Fantaisie - 10	t	Garcia	GUETHARY	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.970519	2020-10-02 13:40:24.970519	209	\N
154	\N	\N	Fantaisie	11	+ ArmCond	Fantaisie - 11	t	Andrieu frères	AU PAYS LORRAIN	480	Facile	\N	?	Carton	Réduit en Sib	\N	ouverture	2020-10-02 13:40:24.977186	2020-10-02 13:40:24.977186	30	\N
155	pièce avec choeur	\N	Fantaisie	12	\N	Fantaisie - 12	t	Andrieu frères	LE BEAU DANUBE BLEU	\N	\N	\N	?	Carton	Réduit en Sib	Classique	Valse	2020-10-02 13:40:24.980031	2020-10-02 13:40:24.980031	553	67
156	\N	\N	Fantaisie	12	\N	Fantaisie - 12	t	EMR	DONAUWELLEN  (valse)	195	\N	\N	?	Papier neuf	Complet transposé	Classique	Valse	2020-10-02 13:40:24.982912	2020-10-02 13:40:24.982912	273	397
157	\N	\N	Fantaisie	12	\N	Fantaisie - 12	t	Wilhelm Hansen	THE BLUE DANUBE	610	\N	\N	?	Papier neuf	Absent	Classique	Valse	2020-10-02 13:40:24.985771	2020-10-02 13:40:24.985771	553	251
158	\N	\N	Fantaisie	13	ArmCond	Fantaisie - 13	t	Evette et Schaeffer	SCENES ALSACIENNES N° 1 2 3	\N	\N	1882	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.988617	2020-10-02 13:40:24.988617	358	308
159	\N	\N	Fantaisie	14	\N	Fantaisie - 14	t	Evette et Schaeffer	SCENES ALSACIENNES N° 4	\N	\N	1882	?	Carton	Absent	\N	\N	2020-10-02 13:40:24.991432	2020-10-02 13:40:24.991432	358	308
160	\N	\N	Fantaisie	15	ArmCond	Fantaisie - 15	t	Andrieu frères	DANSES HONGROISES 5 ET 6	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:24.994039	2020-10-02 13:40:24.994039	84	120
161	pour hautbois solo	\N	Fantaisie	16	ArmCond	Fantaisie - 16	t	Evette et Schaeffer (E.S.648)	SALTARELLE POUR HAUTBOIS	\N	\N	\N	?	Carton	Réduit en Ut	\N	\N	2020-10-02 13:40:24.996513	2020-10-02 13:40:24.996513	78	214
162	\N	\N	Fantaisie	17	\N	Fantaisie - 17	t	\N	L'ESTUDIANTINA	480	\N	\N	?	Carton	Absent	\N	Valse	2020-10-02 13:40:24.999157	2020-10-02 13:40:24.999157	593	608
163	\N	\N	Fantaisie	18	\N	Fantaisie - 18	t	Robert Martin (E383b)	ESQUISSES MEDIEVALES	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:25.00207	2020-10-02 13:40:25.00207	179	\N
164	\N	\N	Fantaisie	18	ArmCond	Fantaisie - 18	t	Billaudot (E 345 B)	PRELUDE ET CORTEGE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:25.004891	2020-10-02 13:40:25.004891	415	\N
165	\N	\N	Fantaisie	19	\N	Fantaisie - 19	t	Billaudot (E 187 B)	VALSE DE L'EMPEREUR	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:25.00826	2020-10-02 13:40:25.00826	554	68
166	Pierre Triollier a du réécrire les partitions, Il manque toutes les parties ??	\N	Fantaisie	20	ArmCond	Fantaisie - 20	t	ecrit à la main	MOSCOW NIGHTS (LE TEMPS DES FLEURS)	130	\N	\N	?	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:25.01128	2020-10-02 13:40:25.01128	229	448
167	Arrangement de Pierre Triollier pour l'Harmonie du bassin	\N	Fantaisie	20	\N	Fantaisie - 20	t	Harmonie de la Chazotte	LE PROFESSIONNEL	\N	\N	1981	?	Vieux papier	Absent	Musique de film	1 Aventure	2020-10-02 13:40:25.013921	2020-10-02 13:40:25.013921	395	577
168	\N	\N	Fantaisie	20	\N	Fantaisie - 20	t	Molenaar 03.1284.09	JESUS CHRIST SUPERSTAR	480	\N	\N	1969	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:25.016537	2020-10-02 13:40:25.016537	600	253
169	Version arrangé pour l'Harmonie du bassin à partir d'une partition de piano	\N	Fantaisie	21	\N	Fantaisie - 21	t	Harmonie de la Chazotte	CHAMPS ELYSEES	140	\N	\N	?	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:25.019077	2020-10-02 13:40:25.019077	444	577
170	Les partitions ont disparues	1984 ?	Fantaisie	21	\N	Fantaisie - 21	t	\N	LA COLEGIALA	\N	\N	1975	?	\N	Absent	\N	\N	2020-10-02 13:40:25.021393	2020-10-02 13:40:25.021393	3	\N
171	\N	\N	Fantaisie	21	\N	Fantaisie - 21	t	Spellgald Music	TV POLICE MEDLEY	\N	\N	\N	1976	Vieux papier	Absent	\N	\N	2020-10-02 13:40:25.023864	2020-10-02 13:40:25.023864	520	564
172	\N	\N	Fantaisie	22	\N	Fantaisie - 22	t	Myers Music	ROCK AROUND THE CLOCK	\N	\N	\N	1953	Vieux papier	Complet en Ut	Variété	Rock	2020-10-02 13:40:25.026318	2020-10-02 13:40:25.026318	201	22
173	\N	\N	Fantaisie	22	+ ArmCond	Fantaisie - 22	t	Chapell	THEY'RE PLAYING OUR SONG	\N	\N	\N	1979	Vieux papier	Complet en Ut	Jazz	\N	2020-10-02 13:40:25.028826	2020-10-02 13:40:25.028826	248	421
174	\N	\N	Fantaisie	22	+ ArmCond	Fantaisie - 22	t	Solow prod	ATLANTIS	\N	\N	\N	1976	Papier neuf	Complet en Ut	\N	\N	2020-10-02 13:40:25.031402	2020-10-02 13:40:25.031402	290	257
175	Medley jazz (Artistry in rhythm, Tuxedo junction, moonlight serenade, take the A train, one o'clock jump)	1984 ?	Fantaisie	21	\N	Fantaisie - 21	t	robbins music	BIG BAND BASH	0	\N	\N	1981	Papier neuf	Absent	Jazz	Pot-pourri	2020-10-02 13:40:25.034004	2020-10-02 13:40:25.034004	\N	337
176	Des copies papiers ont été faites	\N	Fantaisie	23	\N	Fantaisie - 23	t	Billaudot (L. B. 1336)	DANS LES STEPPES DE L'ASIE CENTRALE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:25.036505	2020-10-02 13:40:25.036505	70	123
177	Pièce en 4 mouvements (Aubade, conte bleu, Berceuse de noel, ronde d'antan)	\N	Fantaisie	24	ArmCond	Fantaisie - 24	t	Andrieu Freres	AUTREFOIS	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:25.038849	2020-10-02 13:40:25.038849	185	\N
178	\N	\N	Fantaisie	25	+ ArmCond	Fantaisie - 25	t	Andrieu Freres	SCENES PITTORESQUES N° 1 2 3 4 	\N	\N	1874	1945	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:25.041731	2020-10-02 13:40:25.041731	358	378
179	\N	\N	Fantaisie	26	ArmCond	Fantaisie - 26	t	Robert Martin (R 886 M)	EN TOURNEE AVEC LES COMPAGNONS DE LA CHANSON	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:25.046499	2020-10-02 13:40:25.046499	623	152
180	\N	\N	Fantaisie	27	+ ArmCond	Fantaisie - 27	t	Alphonse Leduc (AL 21.578)	LA SAINT JEAN A BOURBOURG	\N	\N	\N	1955	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:25.050114	2020-10-02 13:40:25.050114	524	\N
181	\N	\N	Fantaisie	28	\N	Fantaisie - 28	t	Robert Martin (R 808 M)	ANNEN POLKA	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:25.053006	2020-10-02 13:40:25.053006	554	354
182	\N	\N	Fantaisie	28	\N	Fantaisie - 28	t	/	LES TUPINIERES EN FLEURS	\N	\N	\N	?	Carton	Absent	Musique traditionnelle	Mazurka	2020-10-02 13:40:25.055694	2020-10-02 13:40:25.055694	416	\N
183	Papier de bonne qualité format défilé restant en nombre	\N	Fantaisie	29	\N	Fantaisie - 29	t	Moncelle	MOUTSY	\N	\N	\N	1974	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:25.057947	2020-10-02 13:40:25.057947	386	\N
184	\N	\N	Fantaisie	30	\N	Fantaisie - 30	t	Robert Martin	OUVERTURE DE LA CARAVANE	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:25.060436	2020-10-02 13:40:25.060436	235	461
185	Cartons qui ont été réécrit sur papier	\N	Fantaisie	31	\N	Fantaisie - 31	t	Edition Salabert (5542EG)	BLANCHE ET LES SEPT NAINS	\N	\N	\N	1938	Vieux papier	Réduit en Sib	Musique de film	1 Comédies	2020-10-02 13:40:25.06272	2020-10-02 13:40:25.06272	124	\N
186	\N	\N	Fantaisie	32	\N	Fantaisie - 32	t	Molenaar 03.0994.04	BLUE TANGO	187	\N	\N	1955	Vieux papier	Réduit en Sib	Danse	Tango	2020-10-02 13:40:25.065089	2020-10-02 13:40:25.065089	9	\N
187	\N	\N	Fantaisie	32	ArmCond	Fantaisie - 32	t	Leonard Publication	EDELWEISS	\N	\N	\N	1981	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.067616	2020-10-02 13:40:25.067616	492	421
188	\N	\N	Fantaisie	32	ArmCond	Fantaisie - 32	t	Leonard Publication	THE MUPPETS SHOW THEME	74	\N	\N	1981	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.070148	2020-10-02 13:40:25.070148	255	421
189	\N	\N	Fantaisie	32	\N	Fantaisie - 32	t	Kendor Music	SONGS OF THE AMERICAN WEST	\N	\N	\N	1981	Vieux papier	Absent	\N	\N	2020-10-02 13:40:25.072439	2020-10-02 13:40:25.072439	\N	421
190	\N	\N	Fantaisie	32	\N	Fantaisie - 32	t	Wayward Music	INTRODUCTION AND DANCE	\N	\N	\N	1978	Papier neuf	Absent	\N	\N	2020-10-02 13:40:25.074844	2020-10-02 13:40:25.074844	117	\N
191	Partition écrites à la main	\N	Fantaisie	32	\N	Fantaisie - 32	t	Harmonie de la Chazotte	EMBOUTEILLAGE	\N	\N	\N	?	Vieux papier	Absent	\N	\N	2020-10-02 13:40:25.077002	2020-10-02 13:40:25.077002	\N	\N
192	Il manque peut être quelques partitions	\N	Fantaisie	33	\N	Fantaisie - 33	t	Corporation of America	THEME FROM "E. T."	\N	\N	\N	1982	Vieux papier	Complet transposé	\N	\N	2020-10-02 13:40:25.079483	2020-10-02 13:40:25.079483	605	457
193	Tampon Union musicale de St Genest Lerpt	\N	Fantaisie	33	ArmCond	Fantaisie - 33	t	Corporation of America	SELECTION FROM "E. T."	\N	\N	\N	1982	Vieux papier	Complet transposé	\N	\N	2020-10-02 13:40:25.082117	2020-10-02 13:40:25.082117	605	98
194	Pièce en 7 mouvements, tampon Ecole Nationale de Musique d'Art dramatique et de Danses.Commande du Ministère des Affaires Culturelle, inspiré de thème irlandais.	\N	Fantaisie	34	ArmCond	Fantaisie - 34	t	Edition Libellule	EVERGREEN	560	\N	1977	1980	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:25.085119	2020-10-02 13:40:25.085119	342	\N
429	\N	2016	Fantaisie	107	\N	Fantaisie - 107	t	\N	LOVE IS ALL	161	\N	\N	\N	\N	\N	Variété	3 Variété internationale	2020-10-02 13:40:25.909879	2020-10-02 13:40:25.909879	637	\N
195	Partitions écrites à la main	\N	Fantaisie	35	+ ArmCond	Fantaisie - 35	t	Metro Goldwyn Mayer	FAME	\N	\N	\N	1980	Vieux papier	Complet transposé	\N	\N	2020-10-02 13:40:25.088338	2020-10-02 13:40:25.088338	230	337
196	\N	\N	Fantaisie	35	\N	Fantaisie - 35	t	Lost Boys Music	TOTAL ECLIPSE OF THE HEART (Bonnie Tyler)	\N	\N	\N	1983	Vieux papier	Complet transposé	\N	\N	2020-10-02 13:40:25.092466	2020-10-02 13:40:25.092466	547	171
197	\N	\N	Fantaisie	35	\N	Fantaisie - 35	t	Magnetic Publishing	EVERY BREATH YOU TAKE	\N	\N	\N	1983	Vieux papier	Absent	\N	\N	2020-10-02 13:40:25.097245	2020-10-02 13:40:25.097245	624	421
198	\N	\N	Fantaisie	35	\N	Fantaisie - 35	t	Famous Music Corp.	FLASHDANCE... WHAT A FEELING	220	\N	\N	1983	Vieux papier	Absent	\N	\N	2020-10-02 13:40:25.100174	2020-10-02 13:40:25.100174	394	421
199	\N	\N	Fantaisie	36	+ ArmCond	Fantaisie - 36	t	Bosworth	SUR UN MARCHE PERSAN	\N	\N	\N	1920	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:25.1026	2020-10-02 13:40:25.1026	294	\N
200	Partitions écrites à la main par Pierre Triollier	\N	Fantaisie	36	\N	Fantaisie - 36	t	Harmonie de la Chazotte	TELE FEUILLETONS	\N	\N	\N	?	Vieux papier	Absent	\N	\N	2020-10-02 13:40:25.105124	2020-10-02 13:40:25.105124	\N	577
201	\N	\N	Fantaisie	36	\N	Fantaisie - 36	t	Molenaar 03.1628.08	MOMENT FOR MORRICONE	\N	\N	1960	1966	Vieux papier	Absent	\N	\N	2020-10-02 13:40:25.109007	2020-10-02 13:40:25.109007	395	625
202	\N	\N	Fantaisie	36	ArmCond	Fantaisie - 36	t	Vogue Music	LES PARAPLUIES DE CHERBOURG	\N	\N	\N	1967	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:25.111552	2020-10-02 13:40:25.111552	320	596
203	\N	\N	Fantaisie	36	\N	Fantaisie - 36	t	Rubank	WAGGERY FOR WOODWINDS	240	\N	\N	1947	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:25.114045	2020-10-02 13:40:25.114045	595	\N
204	\N	\N	Fantaisie	37	+ ArmCond	Fantaisie - 37	t	Kendor Music	CHRISTMAS JOY	240	\N	\N	1971	Vieux papier	Réduit en Ut	Musique traditionnelle	Pot-pourri	2020-10-02 13:40:25.116802	2020-10-02 13:40:25.116802	\N	410
205	\N	\N	Fantaisie	37	\N	Fantaisie - 37	t	Chappell and Co	JUST A GIGOLO / I AIN'T GOT NOBODY	\N	\N	\N	1965	Vieux papier	Absent	\N	\N	2020-10-02 13:40:25.11966	2020-10-02 13:40:25.11966	107	586
206	\N	\N	Fantaisie	37	\N	Fantaisie - 37	t	R. Smith	BANDSTAND BOOGIE	\N	\N	\N	1984	Vieux papier	Absent	\N	\N	2020-10-02 13:40:25.122157	2020-10-02 13:40:25.122157	282	\N
207	Pièce en 4 mouvements (Bareback Riders, Elephant Act, Trapeze Artste, Slapstick)	\N	Fantaisie	38	\N	Fantaisie - 38	t	R. Smith	A CIRCUS SUITE	480	Facile	\N	1985	Vieux papier	Complet transposé	\N	\N	2020-10-02 13:40:25.124761	2020-10-02 13:40:25.124761	282	\N
208	\N	\N	Fantaisie	38	ArmCond	Fantaisie - 38	t	Claude Bolling	BAROQUE AND BLUE	370	\N	\N	1977	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:25.127947	2020-10-02 13:40:25.127947	62	422
209	Partitions écrites à la main par Pierre Triollier	\N	Fantaisie	38	\N	Fantaisie - 38	t	Harmonie de la Chazotte	A STRANGE PARTY	\N	\N	\N	?	Vieux papier	Absent	\N	\N	2020-10-02 13:40:25.130827	2020-10-02 13:40:25.130827	\N	577
210	Pièceen 3 mouvements (Cha cha cha, Calypso, Samba)	\N	Fantaisie	39	\N	Fantaisie - 39	t	De Haske 87081	BRASILIANA	\N	\N	\N	1987	Papier neuf	Réduit en Ut	Musique traditionnelle	2 Amérique du sud	2020-10-02 13:40:25.134479	2020-10-02 13:40:25.134479	496	\N
211	\N	\N	Fantaisie	39	ArmCond	Fantaisie - 39	t	Warner Bros Publication	ROUND MIDNIGHT	220	Normal	\N	1987	Papier neuf	Complet transposé	Jazz	1 Romantique	2020-10-02 13:40:25.137631	2020-10-02 13:40:25.137631	387	410
212	\N	\N	Fantaisie	39	ArmCond	Fantaisie - 39	t	Molenaar 01.1934.05	HILL STREET BLUES	\N	\N	\N	1980	Vieux papier	Complet transposé	\N	\N	2020-10-02 13:40:25.140549	2020-10-02 13:40:25.140549	463	439
213	Le grand blond, Complainte des mystère de paris, Les comperes, Reality, La Chevre, Michel Strogoff, l'as des as)	\N	Fantaisie	40	\N	Fantaisie - 40	t	Robert Martin (R 2145 M)	ZOOM SUR COSMA	\N	\N	\N	1983	Papier neuf	Réduit en Sib	Musique de film	Pot-pourri	2020-10-02 13:40:25.143429	2020-10-02 13:40:25.143429	137	88
214	\N	\N	Fantaisie	40	\N	Fantaisie - 40	t	Hal Leonard Publishing	SINATRA IN CONCERT	\N	\N	\N	1981	Papier neuf	Absent	Variété	3 Variété internationale	2020-10-02 13:40:25.14641	2020-10-02 13:40:25.14641	534	421
215	\N	\N	Fantaisie	40	\N	Fantaisie - 40	t	Robert Martin (R 2239 M)	CARTE POSTALE D'UKRAINE	408	\N	\N	1990	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.149139	2020-10-02 13:40:25.149139	50	\N
216	\N	\N	Fantaisie	40	+ ArmCond	Fantaisie - 40	t	Molenaar 03.1351.07	MUSIQUE A LA CARTE	330	\N	\N	1975	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:25.151671	2020-10-02 13:40:25.151671	253	\N
217	Pièce en 5 mouvement	\N	Fantaisie	41	ArmCond	Fantaisie - 41	t	Jenson Educational Production	ALPEN SKETCHES	527	\N	\N	1979	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.154082	2020-10-02 13:40:25.154082	33	\N
218	Marche écrite pour l'unification de l'Europe, papier format défilé	\N	Fantaisie	41	\N	Fantaisie - 41	t	De Haske 89149	THE LIGHTS OF EUROPE	180	Normal	\N	1989	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.15641	2020-10-02 13:40:25.15641	178	\N
219	\N	\N	Fantaisie	41	ArmCond	Fantaisie - 41	t	Molenaar 03.1881.07	DIVERTIMIENTO	495	\N	\N	1990	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.158832	2020-10-02 13:40:25.158832	307	\N
220	Oeuvre qui permet à Kees VLAK de recevoir le Prix de composition en 1985	\N	Fantaisie	41	\N	Fantaisie - 41	t	De Haske 88111	THE HIGHLANDS	298	\N	1985	1988	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.161167	2020-10-02 13:40:25.161167	589	\N
221	\N	\N	Fantaisie	42	\N	Fantaisie - 42	t	\N	CORNFIELD ROCK	\N	\N	1998	?	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.163492	2020-10-02 13:40:25.163492	242	\N
222	\N	\N	Fantaisie	42	\N	Fantaisie - 42	t	De Haske 90174	SPIRITUAL MOMENTS	360	Normal	\N	1990	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.165703	2020-10-02 13:40:25.165703	550	\N
223	\N	\N	Fantaisie	42	\N	Fantaisie - 42	t	Bruin Music Company	MISSION : IMPOSSIBLE SUITE	\N	\N	\N	1966	Papier neuf	Absent	Musique de film	1 Aventure	2020-10-02 13:40:25.16884	2020-10-02 13:40:25.16884	514	143
224	\N	2015	Fantaisie	42	\N	Fantaisie - 42	t	Bernaerts Music	MISSION IMPOSSIBLE	180	Normal	\N	2000	Papier neuf	Complet transposé	Musique de film	1 Aventure	2020-10-02 13:40:25.172236	2020-10-02 13:40:25.172236	514	507
225	\N	\N	Fantaisie	42	\N	Fantaisie - 42	t	Famous Music Corp.	MANCINI MAGIC	\N	\N	\N	1961	Papier neuf	Complet transposé	Jazz	Pot-pourri	2020-10-02 13:40:25.176183	2020-10-02 13:40:25.176183	346	89
226	\N	\N	Fantaisie	43	ArmCond	Fantaisie - 43	t	Molenaar 03.1938.08	AUNIS ET SAINTONGE EN FETE	790	\N	\N	1987	Vieux papier	Complet transposé	\N	\N	2020-10-02 13:40:25.178937	2020-10-02 13:40:25.178937	307	\N
227	\N	\N	Fantaisie	43	ArmCond	Fantaisie - 43	t	92035	REM RUS LOP TNIAS (Saint Pol sur Mer)	\N	\N	\N	1991	Vieux papier	Complet transposé	\N	\N	2020-10-02 13:40:25.181293	2020-10-02 13:40:25.181293	106	\N
228	\N	\N	Fantaisie	43	\N	Fantaisie - 43	t	Molenaar 03.2045.06	24 MINI ETUDES	\N	\N	1989	1989	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:25.183766	2020-10-02 13:40:25.183766	330	\N
229	Tampon Mylène Richard	\N	Fantaisie	43	\N	Fantaisie - 43	t	Ch. Bugnot Edition	CAPRICCIO – MAZURKA	\N	\N	\N	?	Vieux papier	Absent	\N	\N	2020-10-02 13:40:25.186176	2020-10-02 13:40:25.186176	92	\N
230	\N	\N	Fantaisie	44	\N	Fantaisie - 44	t	De Haske 920385	AFRICAN SYMPHONY	255	\N	\N	1992	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.188785	2020-10-02 13:40:25.188785	363	274
231	\N	\N	Fantaisie	44	\N	Fantaisie - 44	t	De Haske 930475	EL BIMBO	200	\N	\N	1993	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.191479	2020-10-02 13:40:25.191479	393	274
232	\N	\N	Fantaisie	44	\N	Fantaisie - 44	t	De Haske 930476	OMENS OF LOVE	\N	\N	\N	1993	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.205271	2020-10-02 13:40:25.205271	275	357
233	\N	\N	Fantaisie	45	\N	Fantaisie - 45	t	De Haske 930480	CHILDREN OF SANCHEZ	492	\N	\N	1993	Papier neuf	Complet transposé	Musique traditionnelle	2 Espagnol	2020-10-02 13:40:25.208513	2020-10-02 13:40:25.208513	347	274
234	\N	\N	Fantaisie	45	\N	Fantaisie - 45	t	De Haske 930479	WE'RE ALL ALONE	\N	\N	\N	1993	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.211519	2020-10-02 13:40:25.211519	513	428
235	Mvt 1 et 4 joué en 2013	\N	Fantaisie	46	\N	Fantaisie - 46	t	Belwin Inc	JAMES BOND SUITE	251	\N	\N	1962	Papier neuf	Complet transposé	Musique de film	Aventure	2020-10-02 13:40:25.215657	2020-10-02 13:40:25.215657	418	176
236	\N	\N	Fantaisie	46	\N	Fantaisie - 46	t	Belwin Inc	HOLLYWOOD !	\N	\N	\N	1984	Papier neuf	Complet transposé	Musique de film	Pot-pourri	2020-10-02 13:40:25.219536	2020-10-02 13:40:25.219536	\N	33
237	\N	\N	Fantaisie	46	\N	Fantaisie - 46	t	Hal Leonard Publishing	ALADDIN	528	\N	\N	1993	Papier neuf	Complet transposé	Musique de film	Aventure	2020-10-02 13:40:25.223586	2020-10-02 13:40:25.223586	370	398
238	\N	\N	Fantaisie	46	\N	Fantaisie - 46	t	Music Publishing	JURASSIC PARK	670	\N	\N	1993	Papier neuf	Complet transposé	Musique de film	\N	2020-10-02 13:40:25.226988	2020-10-02 13:40:25.226988	605	315
239	\N	\N	Fantaisie	43	\N	Fantaisie - 43	t	Molenaar 03.2079.05	OUT OF AFRICA	270	Facile	\N	1990	Papier neuf	Complet transposé	Musique de film	Romantique	2020-10-02 13:40:25.2303	2020-10-02 13:40:25.2303	36	366
240	\N	\N	Fantaisie	47	\N	Fantaisie - 47	t	De Haske 87080	PATSY	\N	\N	\N	1987	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.23335	2020-10-02 13:40:25.23335	550	\N
241	\N	\N	Fantaisie	47	ArmCond	Fantaisie - 47	t	Edition CAM Spa Rome	HUIT ET DEMI (8 et ½) (extr de LA STRADA)	285	\N	\N	1962	Papier neuf	Réduit en Sib	\N	\N	2020-10-02 13:40:25.2365	2020-10-02 13:40:25.2365	498	88
242	Ya de la joie, La mer, mes jeunes années, je chante,…	\N	Fantaisie	47	\N	Fantaisie - 47	t	Robert Martin R 841 M	FESTIVAL CHARLES TRENET	\N	\N	\N	?	Papier neuf	Réduit en Sib	Variété	Pot-pourri	2020-10-02 13:40:25.239619	2020-10-02 13:40:25.239619	576	152
243	\N	\N	Fantaisie	47	\N	Fantaisie - 47	t	Hal Leonard Publishing	DANCES WITH WOLVES (Danses avec les loups)	572	\N	\N	1991	Papier neuf	Complet transposé	Musique de film	Aventure	2020-10-02 13:40:25.242825	2020-10-02 13:40:25.242825	36	56
244	\N	\N	Fantaisie	48	\N	Fantaisie - 48	t	Molenaar 01.2348.08	TRIBUTE TO MICKAEL JACKSON	\N	\N	\N	?	Papier neuf	Complet transposé	Variété	Pot-pourri	2020-10-02 13:40:25.246069	2020-10-02 13:40:25.246069	276	252
245	Pièce en 3 morceaux (Tequila, Oye Commo va , La bamba) pour orchestre junior	\N	Fantaisie	48	\N	Fantaisie - 48	t	Jenson Publication 259-12030	LATIN GOLD !	260	Facile	\N	1987	Papier neuf	Complet transposé	Musique traditionnelle	Pot-pourri	2020-10-02 13:40:25.249532	2020-10-02 13:40:25.249532	473	315
246	Version pour solo de saxophone (une autre version sur carton référencée jazz est aussi présente	\N	Fantaisie	48	\N	Fantaisie - 48	t	Shapiro, Bernstein & Co	HARLEM NOCTURNE (pour Sax solo)	\N	\N	\N	1940	Papier neuf	Réduit en Ut	Jazz	\N	2020-10-02 13:40:25.252766	2020-10-02 13:40:25.252766	246	483
247	\N	\N	Fantaisie	49	\N	Fantaisie - 49	t	Harmonie de la Chazotte	LES ETOILES DU CINEMA	\N	\N	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.256063	2020-10-02 13:40:25.256063	305	66
248	Version pour défilé petit format	\N	Fantaisie	49	\N	Fantaisie - 49	t	A. Moncelle	DEL REMO (Paso Doble)	\N	\N	\N	1978	Papier neuf	Réduit en Sib	\N	\N	2020-10-02 13:40:25.259417	2020-10-02 13:40:25.259417	386	\N
249	\N	\N	Fantaisie	49	\N	Fantaisie - 49	t	Melorhin M 160 R	FOX – STORY	195	\N	\N	1974	Papier neuf	Réduit en Sib	\N	\N	2020-10-02 13:40:25.263403	2020-10-02 13:40:25.263403	196	\N
250	\N	\N	Fantaisie	49	\N	Fantaisie - 49	t	Melorhin M 158 R	ALSACE – COCKTAIL	\N	\N	\N	1974	Papier neuf	Réduit en Sib	\N	\N	2020-10-02 13:40:25.26704	2020-10-02 13:40:25.26704	175	\N
251	\N	\N	Fantaisie	49	\N	Fantaisie - 49	t	Melorhin M 157 R	BUSSANG DONON	195	\N	\N	1974	Papier neuf	Réduit en Sib	\N	\N	2020-10-02 13:40:25.269986	2020-10-02 13:40:25.269986	175	\N
252	Orchestration de Jean-François Bonura pour solo de marimba pour l'Harmonie en 1995	\N	Fantaisie	49	\N	Fantaisie - 49	t	Harmonie de la Chazotte	LA COMPARSITA	\N	\N	\N	1995	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.273	2020-10-02 13:40:25.273	493	66
253	\N	\N	Fantaisie	50	\N	Fantaisie - 50	t	Robert Martin (R 2527 M)	MAGIE NOIRE	\N	\N	\N	1994	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.275717	2020-10-02 13:40:25.275717	409	\N
254	\N	\N	Fantaisie	50	\N	Fantaisie - 50	t	Molenaar 01.2133.06	FANTAISIE POUR SAX ALTO	375	Difficile	\N	1992	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.27867	2020-10-02 13:40:25.27867	156	252
255	Pièce en 4 mouvements (Naples, Buenos Aires, Paris, New York)	\N	Fantaisie	51	+ ArmCond	Fantaisie - 51	t	Robert Martin (R 2701 M)	DEDICACE	697	Difficile	\N	1995	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.281501	2020-10-02 13:40:25.281501	159	\N
256	Pièce en 2 nouvement (Elegie, la petite chinoise)	\N	Fantaisie	51	ArmCond	Fantaisie - 51	t	Robert Martin (R 2712 M)	DEUX TABLEAUX	385	\N	\N	1996	Papier neuf	Complet en Ut	\N	\N	2020-10-02 13:40:25.284456	2020-10-02 13:40:25.284456	459	\N
257	\N	\N	Fantaisie	52	ArmCond	Fantaisie - 52	t	Robert Martin (R 2364 M)	GRAND ECRAN – OUVERTURE	\N	\N	\N	1991	Papier neuf	Complet en Ut	\N	\N	2020-10-02 13:40:25.287408	2020-10-02 13:40:25.287408	351	\N
258	\N	\N	Fantaisie	52	\N	Fantaisie - 52	t	Robert Martin (R 2820 M)	LA MAISON PRES DE LA FONTAINE	210	Facile	\N	1975	Papier neuf	Complet transposé	Variété	3 Variété française	2020-10-02 13:40:25.290964	2020-10-02 13:40:25.290964	187	409
259	Avec la fiche de Bernard	\N	Fantaisie	53	\N	Fantaisie - 53	t	Robert Martin (R 2802 M)	JOHNNY HALLIDAY	630	Facile	\N	?	Papier neuf	Complet transposé	Variété	3 Variété française	2020-10-02 13:40:25.294354	2020-10-02 13:40:25.294354	247	409
260	\N	\N	Fantaisie	54	+ ArmCond	Fantaisie - 54	t	Robert Martin (R 2480 M)	TOPERS TOP	\N	\N	\N	1993	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.29738	2020-10-02 13:40:25.29738	311	278
261	\N	\N	Fantaisie	54	\N	Fantaisie - 54	t	Robert Martin (R 2782 M)	DITES – MOI POURQUOI	180	\N	\N	1997	Papier neuf	Complet transposé	Danse	Valse	2020-10-02 13:40:25.300497	2020-10-02 13:40:25.300497	61	\N
262	\N	\N	Fantaisie	54	+ ArmCond	Fantaisie - 54	t	Beam me up Music	THE ENTERTAINER	140	\N	1902	1994	Papier neuf	Complet transposé	Jazz	\N	2020-10-02 13:40:25.304187	2020-10-02 13:40:25.304187	286	33
263	An american in Paris, Cuban Overture, Rhapsody in Blue	\N	Fantaisie	55	\N	Fantaisie - 55	t	Music Corp	THE SYMPHONIC GERSHWIN	\N	\N	\N	1984	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.308452	2020-10-02 13:40:25.308452	216	33
264	\N	\N	Fantaisie	56	\N	Fantaisie - 56	t	Marc Reift EMR 1166	1492 « THE CONQUEST OF PARADISE »	\N	\N	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.314188	2020-10-02 13:40:25.314188	626	397
265	\N	\N	Fantaisie	56	\N	Fantaisie - 56	t	Robert Martin (R 2634 M)	STARMANIA	960	Normal	\N	1979	Vieux papier	Complet transposé	Variété	3 Variété française	2020-10-02 13:40:25.317365	2020-10-02 13:40:25.317365	43	409
266	Tampon Avenir Musical de Firminy	\N	Fantaisie	56	\N	Fantaisie - 56	t	Hafabra Music – Louis Martinus	« 1492 - CHRISTOPHER COLUMBUS » Conquest of Paradise	260	\N	\N	1995	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.320653	2020-10-02 13:40:25.320653	626	160
267	\N	\N	Fantaisie	57	\N	Fantaisie - 57	t	De Haske 930502	WEST SIDE STORY (arr. Iwai)	370	Difficile	\N	1958	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.323882	2020-10-02 13:40:25.323882	49	274
268	\N	\N	Fantaisie	57	\N	Fantaisie - 57	t	Robert Martin (R 2883 M)	TACOT RAG	195	Facile	\N	1997	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.326899	2020-10-02 13:40:25.326899	115	\N
269	\N	\N	Fantaisie	57	\N	Fantaisie - 57	t	Robert Martin (R 2882 M)	LES SUD-AMERICAINES	280	Facile	\N	1979	Papier neuf	Complet transposé	Variété	3 Variété française	2020-10-02 13:40:25.330238	2020-10-02 13:40:25.330238	205	409
270	\N	Joué par la classe d'orchestre saison 1998-99	Fantaisie	57	\N	Fantaisie - 57	t	Jalni Publication	WEST SIDE STORY pour classe d'orchestre	\N	Normal	\N	1990	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.333645	2020-10-02 13:40:25.333645	49	56
271	\N	\N	Fantaisie	58	\N	Fantaisie - 58	t	DIFEM	LA SALSA DU DEMON	250	Normal	\N	1979	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.336888	2020-10-02 13:40:25.336888	570	109
272	\N	\N	Fantaisie	58	\N	Fantaisie - 58	t	Robert Martin (R 2702 M)	HENRI SALVADOR	\N	\N	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.340038	2020-10-02 13:40:25.340038	510	465
273	\N	\N	Fantaisie	58	\N	Fantaisie - 58	t	Robert Martin (R 2612 M)	QUERIDO CHACHACHA	\N	\N	\N	1995	Vieux papier	Complet transposé	\N	\N	2020-10-02 13:40:25.343699	2020-10-02 13:40:25.343699	563	338
274	\N	\N	Fantaisie	59	\N	Fantaisie - 59	t	Barnhouse Company	SOLARIS	253	\N	2008	2008	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.34737	2020-10-02 13:40:25.34737	537	\N
275	\N	\N	Fantaisie	59	\N	Fantaisie - 59	t	Hal Leonard Publishing	RIVER OF THE ANCIENTS (La riviere des ancients)	464	Difficile	\N	2004	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.351471	2020-10-02 13:40:25.351471	559	\N
276	\N	\N	Fantaisie	59	\N	Fantaisie - 59	t	Robert Martin (R 2939 M)	LIBERTANGO	165	\N	\N	1998	Papier neuf	Complet transposé	Musique de film	\N	2020-10-02 13:40:25.35474	2020-10-02 13:40:25.35474	450	409
277	\N	\N	Fantaisie	60	\N	Fantaisie - 60	t	Hal Leonard Publishing	THE MASK OF ZORRO (Le Masque de Zorro)	473	Normal	\N	1998	Papier neuf	Complet transposé	Musique de film	1 Aventure	2020-10-02 13:40:25.358035	2020-10-02 13:40:25.358035	266	398
278	\N	\N	Fantaisie	60	\N	Fantaisie - 60	t	Hal Leonard Publishing	DISNEY RAZZAMATAZZ	\N	\N	\N	1998	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.363218	2020-10-02 13:40:25.363218	627	122
279	3 mouvements	\N	Fantaisie	61	\N	Fantaisie - 61	t	Molenaar 01.1192.08	CARIBBEAN CONCERTO (St Eustatius, Saba, St Maarten)	670	\N	1988	1969	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.366176	2020-10-02 13:40:25.366176	589	\N
280	\N	\N	Fantaisie	61	\N	Fantaisie - 61	t	Robert Martin (R 3000 M)	LONG BEACH	260	Facile	\N	1999	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.369156	2020-10-02 13:40:25.369156	409	\N
281	Format défilé	\N	Fantaisie	62	\N	Fantaisie - 62	t	Scomegna	SONDERO (Cha Cha Cha)	\N	\N	\N	1999	Papier neuf	Complet transposé	Musique traditionnelle	2 Amérique du sud	2020-10-02 13:40:25.374653	2020-10-02 13:40:25.374653	197	\N
282	\N	\N	Fantaisie	62	\N	Fantaisie - 62	t	De Haske 970906	VIVO PER LEI	240	Normal	\N	1997	Papier neuf	Complet transposé	Variété	3 Variété internationale	2020-10-02 13:40:25.38394	2020-10-02 13:40:25.38394	612	543
283	\N	\N	Fantaisie	62	\N	Fantaisie - 62	t	Warner Bros Publication	BRAZIL	\N	\N	\N	1997	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.394249	2020-10-02 13:40:25.394249	35	272
284	\N	\N	Fantaisie	62	\N	Fantaisie - 62	t	Beriato Music	ZORBA THE GREEK	245	Normal	\N	2006	Papier neuf	Complet transposé	Musique traditionnelle	2 Musique du monde	2020-10-02 13:40:25.404729	2020-10-02 13:40:25.404729	569	584
285	\N	\N	Fantaisie	63	\N	Fantaisie - 63	t	Robert Martin (R 3023 M)	POLKA aus « Bohmische Suite » OPUS 39	180	Facile	1882	1999	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.41405	2020-10-02 13:40:25.41405	170	310
286	\N	\N	Fantaisie	63	\N	Fantaisie - 63	t	Robert Martin (R 3018 M)	BORSALINO	161	Très facile	\N	1970	Papier neuf	Complet transposé	Musique de film	Comédies	2020-10-02 13:40:25.423212	2020-10-02 13:40:25.423212	62	409
287	\N	\N	Fantaisie	63	\N	Fantaisie - 63	t	Hafabra Music	REALITY	325	Facile	\N	1980	Papier neuf	Complet transposé	Musique de film	1 Romantique	2020-10-02 13:40:25.43089	2020-10-02 13:40:25.43089	137	535
288	\N	\N	Fantaisie	63	\N	Fantaisie - 63	t	Hafabra Music	LA VITA E BELLA (la vie est  belle)	187	\N	\N	1997	Papier neuf	Complet transposé	Musique de film	1 Drame	2020-10-02 13:40:25.437622	2020-10-02 13:40:25.437622	453	135
289	\N	\N	Fantaisie	63	\N	Fantaisie - 63	t	Robert Martin (R 2979 M)	SAVOIR AIMER	180	Facile	\N	1997	Papier neuf	Complet transposé	Variété	3 Variété française	2020-10-02 13:40:25.444924	2020-10-02 13:40:25.444924	424	409
290	\N	\N	Fantaisie	64	\N	Fantaisie - 64	t	Robert Martin (R 3008 M)	CLAUDE NOUGARO	848	Normal	\N	?	Papier neuf	Complet transposé	Variété	Variété française	2020-10-02 13:40:25.45269	2020-10-02 13:40:25.45269	420	409
291	\N	\N	Fantaisie	65	\N	Fantaisie - 65	t	Pro Music PMA 807	OH HAPPY DAY	201	\N	\N	1998	Papier neuf	Complet transposé	Musique traditionnelle	2 Gospel	2020-10-02 13:40:25.459032	2020-10-02 13:40:25.459032	\N	118
292	\N	\N	Fantaisie	65	\N	Fantaisie - 65	t	Alfred	RAMPAGE !	235	Normal	\N	2009	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.465135	2020-10-02 13:40:25.465135	545	\N
293	Musique du bossu de Notre Dame de Disney	\N	Fantaisie	65	\N	Fantaisie - 65	t	Scherzando Music Publication	THE BELLS OF NOTRE DAME	420	Normal	\N	1996	Papier neuf	Complet transposé	Musique de film	1 Comédies	2020-10-02 13:40:25.470477	2020-10-02 13:40:25.470477	371	592
294	\N	\N	Fantaisie	66	\N	Fantaisie - 66	t	Pro Music PMA 804	LES LACS DU CONNEMARA	\N	\N	\N	1998	Papier neuf	Complet transposé	Variété	3 Variété française	2020-10-02 13:40:25.475889	2020-10-02 13:40:25.475889	487	162
295	\N	\N	Fantaisie	66	\N	Fantaisie - 66	t	Pro Music PMA 801	ELLE A FAIT UN BEBE TOUTE SEULE	\N	\N	\N	?	Papier neuf	Complet transposé	Variété	3 Variété française	2020-10-02 13:40:25.48131	2020-10-02 13:40:25.48131	227	162
296	\N	\N	Fantaisie	67	\N	Fantaisie - 67	t	Hal Leonard Publishing	JOHN WILLIAMS IN CONCERT	\N	\N	\N	1990	Papier neuf	Complet transposé	Musique de film	Pot-pourri	2020-10-02 13:40:25.48727	2020-10-02 13:40:25.48727	605	315
297	\N	\N	Fantaisie	67	\N	Fantaisie - 67	t	Robert Martin (R 3495 M)	LARA'S THEME	250	Facile	1965	1965	Papier neuf	Complet transposé	Musique de film	1 Romantique	2020-10-02 13:40:25.493841	2020-10-02 13:40:25.493841	279	139
298	\N	\N	Fantaisie	67	\N	Fantaisie - 67	t	Bernaerts Music	YMCA (VILLAGE PEOPLE)	213	\N	\N	1978	Papier neuf	Réduit en Ut	Variété	3 Variété internationale	2020-10-02 13:40:25.499305	2020-10-02 13:40:25.499305	391	47
299	\N	\N	Fantaisie	68	\N	Fantaisie - 68	t	Hal Leonard Publishing	YOU & ME	\N	Normal	\N	1995	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.504482	2020-10-02 13:40:25.504482	396	512
300	Musique de Tarzan de Walt Disney	\N	Fantaisie	68	\N	Fantaisie - 68	t	Hal Leonard Publishing	TARZAN SOUNDTRACK HIGHLIGHTS	\N	\N	\N	1998	Papier neuf	Complet transposé	Musique de film	1 Aventure	2020-10-02 13:40:25.509911	2020-10-02 13:40:25.509911	131	405
301	\N	\N	Fantaisie	69	\N	Fantaisie - 69	t	Molenaar 01.2528.03	AL CENTENARIO	244	Normal	\N	1998	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.51608	2020-10-02 13:40:25.51608	186	\N
302	Musique de Tay Strory de Disney	\N	Fantaisie	69	\N	Fantaisie - 69	t	Hal Leonard Publishing	MUSIC FROM TOY STORY 2	\N	Normal	\N	1999	Papier neuf	Complet transposé	Musique de film	1 Aventure	2020-10-02 13:40:25.522187	2020-10-02 13:40:25.522187	411	56
303	Musique de 1001 patte de Walt Disney	\N	Fantaisie	69	\N	Fantaisie - 69	t	Hal Leonard Publishing	MUSIC FROM A BUG'S LIFE (1001 pattes)	345	Normal	\N	1999	Papier neuf	Complet transposé	Musique de film	1 Aventure	2020-10-02 13:40:25.528215	2020-10-02 13:40:25.528215	411	398
304	\N	\N	Fantaisie	70	\N	Fantaisie - 70	t	Robert Martin (R 23004 M)	GENTLEMAN CAMBRIOLEUR	240	Facile	\N	1974	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.534105	2020-10-02 13:40:25.534105	80	403
305	Solo de tuba	\N	Fantaisie	70	\N	Fantaisie - 70	t	Scomegna	TUBA SPLENDOUR	\N	\N	\N	1999	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.541291	2020-10-02 13:40:25.541291	628	\N
306	\N	\N	Fantaisie	70	\N	Fantaisie - 70	t	Robert Martin (R 2737 M)	PIAF	630	Facile	\N	?	Papier neuf	Complet transposé	Variété	3 Variété française	2020-10-02 13:40:25.545011	2020-10-02 13:40:25.545011	449	465
307	\N	\N	Fantaisie	71	\N	Fantaisie - 71	t	CO156B7XC	THEMES FROM 007	\N	\N	\N	1973	Papier neuf	Complet transposé	Musique de film	Pot-pourri	2020-10-02 13:40:25.549139	2020-10-02 13:40:25.549139	418	143
308	\N	\N	Fantaisie	71	\N	Fantaisie - 71	t	Robert Martin (R 2898 M)	CANTA ME LA	270	\N	\N	1997	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.5526	2020-10-02 13:40:25.5526	409	\N
309	\N	\N	Fantaisie	71	\N	Fantaisie - 71	\N	\N	LA BAMBA	\N	\N	\N	?	\N	\N	\N	\N	2020-10-02 13:40:25.555461	2020-10-02 13:40:25.555461	579	\N
310	\N	\N	Fantaisie	72	\N	Fantaisie - 72	t	De Haske 910266	MOOD ROMANTIC	206	Facile	\N	1991	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.558302	2020-10-02 13:40:25.558302	414	\N
311	\N	\N	Fantaisie	72	\N	Fantaisie - 72	t	Belwin Inc	PATAPAN	235	\N	\N	1999	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.561363	2020-10-02 13:40:25.561363	389	69
312	\N	\N	Fantaisie	72	\N	Fantaisie - 72	t	Curnow Music Press	SHIPSTON PRELUDE	237	Normal	\N	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.564184	2020-10-02 13:40:25.564184	95	\N
313	\N	\N	Fantaisie	72	\N	Fantaisie - 72	t	Mythen Hollanda	NOCHE DE ABRIL	\N	\N	2000	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.567033	2020-10-02 13:40:25.567033	485	\N
314	\N	\N	Fantaisie	72	\N	Fantaisie - 72	t	Curnow Music Press	LATIN FOLK SONG TRILOGY	509	Normal	\N	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.570645	2020-10-02 13:40:25.570645	261	\N
315	\N	\N	Fantaisie	73	\N	Fantaisie - 73	t	Hal Leonard Publishing	HEBRAIC RHAPSODY	360	Difficile	\N	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.574245	2020-10-02 13:40:25.574245	277	\N
316	Pièce en 6 mouvements	\N	Fantaisie	73	ArmCond	Fantaisie - 73	t	Hal Leonard Publishing	HANS CHRISTIAN ANDERSEN SUITE	1500	\N	1997	1997	Papier neuf	Complet en Ut	\N	\N	2020-10-02 13:40:25.577851	2020-10-02 13:40:25.577851	271	366
317	Avec 4 parties pour un choeur	\N	Fantaisie	73	\N	Fantaisie - 73	t	Harmonie de la Chazotte	AUX MINEURS	\N	\N	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.580797	2020-10-02 13:40:25.580797	116	\N
318	Reccueil de petit morceau d'Ambiance	\N	Fantaisie	73	\N	Fantaisie - 73	t	G. Besson	DARLA DIRLADADA	\N	\N	\N	?	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.583735	2020-10-02 13:40:25.583735	433	407
319	Reccueil de petit morceau d'Ambiance	\N	Fantaisie	73	\N	Fantaisie - 73	t	G. Besson	LE PETIT BONHOMME EN MOUSSE	\N	\N	\N	?	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.586697	2020-10-02 13:40:25.586697	81	101
320	Reccueil de petit morceau d'Ambiance	\N	Fantaisie	73	\N	Fantaisie - 73	t	G. Besson	NUIT DE FOLIE	\N	\N	\N	?	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.589634	2020-10-02 13:40:25.589634	451	419
321	extrait indiana johns en 2013	\N	Fantaisie	74	\N	Fantaisie - 74	t	Hal Leonard Publishing	JOHN WILLIAMS : SYMPHONIC SOUNDTRACKS	391	Difficile	\N	2001	Papier neuf	Complet transposé	Musique de film	Pot-pourri	2020-10-02 13:40:25.592601	2020-10-02 13:40:25.592601	605	398
322	\N	\N	Fantaisie	74	\N	Fantaisie - 74	t	Hal Leonard Publishing	SALUT TO RICHARD RODGERS	333	Normal	\N	2002	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.595396	2020-10-02 13:40:25.595396	492	489
323	\N	\N	Fantaisie	75	\N	Fantaisie - 75	t	Marc Reift EMR 10285	LES MYSTERES DE L'EGYPTE	215	Difficile	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.597939	2020-10-02 13:40:25.597939	560	\N
324	\N	\N	Fantaisie	75	\N	Fantaisie - 75	t	Bernaerts Music	THE SPIRIT OF THE CELTS	391	\N	\N	2002	Papier neuf	Complet transposé	Musique traditionnelle	2 Celtique	2020-10-02 13:40:25.600777	2020-10-02 13:40:25.600777	250	47
325	\N	\N	Fantaisie	75	\N	Fantaisie - 75	t	Robert Martin (R 3573 M)	LES MYSTERES DE L'OUEST	150	Normal	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.603836	2020-10-02 13:40:25.603836	352	207
326	\N	\N	Fantaisie	76	\N	Fantaisie - 76	t	CurnOW Music Press	KIWANISCA	280	Difficile	\N	2003	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.60663	2020-10-02 13:40:25.60663	95	\N
327	A partir d'une partition pour ensemble d’accordéon	\N	Fantaisie	76	\N	Fantaisie - 76	t	Harmonie de la Chazotte	ALPINA SERENADE	\N	\N	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.609348	2020-10-02 13:40:25.609348	85	359
328	Cornet Solo	\N	Fantaisie	77	\N	Fantaisie - 77	t	E. GAUDET (EG3653)	O BELLOS MOUNTAGNOS	\N	\N	\N	?	Vieux papier	Réduit en Sib	Musique traditionnelle	\N	2020-10-02 13:40:25.611995	2020-10-02 13:40:25.611995	\N	446
329	\N	\N	Fantaisie	77	\N	Fantaisie - 77	t	Scomegna	JALARI	\N	\N	\N	2003	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.61493	2020-10-02 13:40:25.61493	474	\N
330	\N	\N	Fantaisie	78	\N	Fantaisie - 78	t	Beriato Music	GULLIVER'S TRAVELS	526	Difficile	\N	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.618041	2020-10-02 13:40:25.618041	12	\N
331	Ecrite lors du 75eme anniversaire de l'Harmonie royale de Vriendenkring	\N	Fantaisie	78	\N	Fantaisie - 78	t	Beriato Music	RATAFIA	486	Difficile	\N	2002	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.621173	2020-10-02 13:40:25.621173	200	\N
332	\N	\N	Fantaisie	79	\N	Fantaisie - 79	t	Hal Leonard Publishing	PIRATES OF THE CARIBBEAN (Arr. Ricketts)	283	Normal	\N	2003	Papier neuf	Complet transposé	Musique de film	Aventure	2020-10-02 13:40:25.624248	2020-10-02 13:40:25.624248	27	489
333	\N	\N	Fantaisie	79	\N	Fantaisie - 79	t	Barnhouse Company	IN THE WINTER OF 1730	313	\N	\N	2003	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.627033	2020-10-02 13:40:25.627033	558	\N
334	\N	\N	Fantaisie	79	\N	Fantaisie - 79	t	Belwin Inc	OVER THE RAINBOW (extrait du magicien d’Oz)	187	\N	\N	1967	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.629867	2020-10-02 13:40:25.629867	15	89
335	Medley de musiques connues	\N	Fantaisie	80	\N	Fantaisie - 80	t	Belwin Inc	GO WEST	180	\N	\N	2004	Papier neuf	Complet transposé	Musique traditionnelle	2 Musique du monde	2020-10-02 13:40:25.63258	2020-10-02 13:40:25.63258	\N	194
336	Trio de percussion, accompagné par Harmonie, sur thème folklorique français	\N	Fantaisie	80	\N	Fantaisie - 80	t	Curnow Music Press	THE THREE DRUMMERS (Les 3 Tambours)	120	Facile	\N	2004	Papier neuf	Complet transposé	Musique traditionnelle	\N	2020-10-02 13:40:25.6352	2020-10-02 13:40:25.6352	\N	141
337	\N	\N	Fantaisie	80	\N	Fantaisie - 80	t	Rundel	MIDNIGHT DANCER	225	\N	\N	1999	Papier neuf	Complet transposé	Danse	Rock	2020-10-02 13:40:25.637614	2020-10-02 13:40:25.637614	217	\N
338	\N	\N	Fantaisie	81	\N	Fantaisie - 81	t	De Haske 1130.04 MS	THE BEST OF CHARLES AZNAVOUR	518	Normal	\N	2004	Papier neuf	Complet transposé	Variété	Variété française	2020-10-02 13:40:25.64014	2020-10-02 13:40:25.64014	24	293
339	\N	\N	Fantaisie	81	\N	Fantaisie - 81	t	Scomegna	HIGHLIGHTS FROM CHICAGO	528	Difficile	1927	1977	Papier neuf	Complet transposé	Musique de film	Comédies	2020-10-02 13:40:25.642704	2020-10-02 13:40:25.642704	289	215
340	Piece en 4 mouvements, pour 4 modèle de voiture, 12'30 au total	\N	Fantaisie	82	\N	Fantaisie - 82	t	Molenaar 01.2749.07	CAR (Ford T)	430	\N	\N	2004	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.645149	2020-10-02 13:40:25.645149	439	\N
341	\N	\N	Fantaisie	83	\N	Fantaisie - 83	t	Obrasso-Verlag	SEE YOU LATER ALLIGATOR	151	\N	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.647963	2020-10-02 13:40:25.647963	238	610
342	\N	\N	Fantaisie	83	\N	Fantaisie - 83	t	Rundel	ALCAZAR	517	\N	\N	2004	Papier neuf	Complet transposé	Musique traditionnelle	\N	2020-10-02 13:40:25.651891	2020-10-02 13:40:25.651891	629	\N
343	\N	\N	Fantaisie	83	\N	Fantaisie - 83	t	Marc Reift EMR 1619	LAWRENCE OF ARABIA (Laurence d’arabie)	285	Normal	1963	?	Papier neuf	Complet transposé	Musique de film	Aventure	2020-10-02 13:40:25.654696	2020-10-02 13:40:25.654696	279	397
344	Pour trombone solo	\N	Fantaisie	84	\N	Fantaisie - 84	t	Rundel	CAUCHO	\N	\N	\N	2004	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.657769	2020-10-02 13:40:25.657769	481	\N
345	\N	\N	Fantaisie	84	\N	Fantaisie - 84	t	Beriato Music	FABLES AND FANTAISIES	479	Normal	\N	2005	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.660662	2020-10-02 13:40:25.660662	200	\N
346	\N	\N	Fantaisie	84	\N	Fantaisie - 84	t	Rundel	OYE COMO VA	221	\N	\N	1970	Papier neuf	Complet transposé	Musique traditionnelle	Amérique du sud	2020-10-02 13:40:25.664722	2020-10-02 13:40:25.664722	472	87
347	\N	\N	Fantaisie	84	\N	Fantaisie - 84	t	Scomegna	LA COLLEGIALA	222	Facile	1975	1979	Papier neuf	Réduit en Ut	Musique traditionnelle	Amérique du sud	2020-10-02 13:40:25.667827	2020-10-02 13:40:25.667827	3	197
348	Avec solo de bugle	\N	Fantaisie	85	\N	Fantaisie - 85	t	Hebu Musikverlag	VIVE DIOS ! (Concerto pour bugle solo)	287	\N	\N	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.670727	2020-10-02 13:40:25.670727	16	\N
349	\N	\N	Fantaisie	85	\N	Fantaisie - 85	t	Marc Reift EMR 1895	GUAJIRA DEL SOL	337	Difficile	2000	2000	Papier neuf	Complet transposé	Musique traditionnelle	Amérique du sud	2020-10-02 13:40:25.67352	2020-10-02 13:40:25.67352	571	\N
350	\N	\N	Fantaisie	85	\N	Fantaisie - 85	t	Marc Reift EMR 1751	EXODUS (pour harmonie)	249	\N	\N	1960	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.676556	2020-10-02 13:40:25.676556	226	397
427	Prèt de Céline Moiroud	2015	Fantaisie	107	\N	Fantaisie - 107	t	\N	LA FOLIE DES GRANDEURS	310	\N	\N	\N	\N	\N	Musique de film	1 Comédies	2020-10-02 13:40:25.899885	2020-10-02 13:40:25.899885	634	397
351	Avec Partie de Choeur, arrangement écrit à la main par Yannick Berne, chef de choeur de Sinfonia à St tienne	Joué avec choeur pour le st Cécile 2014 (150 ans de l'harmonie)	Fantaisie	85	\N	Fantaisie - 85	t	Choeur Sinfonia	EXODUS (avec choeurs)	190	\N	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.679574	2020-10-02 13:40:25.679574	226	48
352	Sous titre : Une journée en Pologne	\N	Fantaisie	86	\N	Fantaisie - 86	t	Rundel	MAZURY RHAPSODY	495	\N	\N	2005	Papier neuf	Complet transposé	Musique traditionnelle	2 Europe centrale	2020-10-02 13:40:25.682444	2020-10-02 13:40:25.682444	72	\N
353	\N	\N	Fantaisie	86	\N	Fantaisie - 86	t	Hafabra Music – Louis Martinus	HAFABRA FOLLIES	519	\N	\N	2002	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.685434	2020-10-02 13:40:25.685434	213	439
354	\N	\N	Fantaisie	87	\N	Fantaisie - 87	t	Edizioni Musicali WICKY	RUSSIAN MELODIES (Mélodies Russes)	509	\N	\N	2003	Papier neuf	Complet transposé	Musique traditionnelle	2 Europe centrale	2020-10-02 13:40:25.68831	2020-10-02 13:40:25.68831	430	\N
355	\N	\N	Fantaisie	87	\N	Fantaisie - 87	t	Marc Reift EMR 1752	BEN HUR	250	Normal	\N	1959	Papier neuf	Complet transposé	Musique de film	Épopée	2020-10-02 13:40:25.691528	2020-10-02 13:40:25.691528	504	397
356	\N	\N	Fantaisie	87	\N	Fantaisie - 87	t	Business Art Productions	THE BEST YEARS OF OUR LIVES (SHREK)	120	\N	\N	2001	Papier neuf	Complet transposé	Musique de film	\N	2020-10-02 13:40:25.694526	2020-10-02 13:40:25.694526	280	489
357	\N	\N	Fantaisie	87	\N	Fantaisie - 87	t	Hal Leonard Publishing	THREE CZECH FOLK SONGS (3 chants folkloriques tcheques)	359	Facile	\N	1992	Papier neuf	Complet transposé	Musique traditionnelle	2 Europe centrale	2020-10-02 13:40:25.697121	2020-10-02 13:40:25.697121	586	\N
358	\N	\N	Fantaisie	88	\N	Fantaisie - 88	t	Marc Reift EMR 10012	BATMAN	216	Difficile	\N	?	Papier neuf	Complet transposé	Musique de film	Fantastique	2020-10-02 13:40:25.699878	2020-10-02 13:40:25.699878	172	295
359	\N	\N	Fantaisie	88	\N	Fantaisie - 88	t	Marc Reift EMR 10029	THE LAST OF THE MOHICANS (Le dernier des mohican)	\N	\N	\N	1992	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.703119	2020-10-02 13:40:25.703119	284	397
360	Pieces imposée en première division du concours pour orchestre a vent (Suisse), avec mesures composées	\N	Fantaisie	89	\N	Fantaisie - 89	t	Mitropa Music	BULGARIAN DANCES (Danses Bulgares)	555	Très difficile	\N	2006	Papier neuf	Complet transposé	Musique traditionnelle	2 Europe centrale	2020-10-02 13:40:25.705975	2020-10-02 13:40:25.705975	110	\N
361	\N	\N	Fantaisie	89	+ ArmCond	Fantaisie - 89	t	Robert Martin (R 1866 M)	LES CORONS (arr. Delbecq)	\N	\N	\N	1982	Papier neuf	Réduit en Sib	Variété	3 Variété française	2020-10-02 13:40:25.709815	2020-10-02 13:40:25.709815	26	152
362	Arrangement de Hervé Freycenon, chef de l'harmonie de Roche la Moliere	\N	Fantaisie	89	\N	Fantaisie - 89	t	Harmonie de Roche la Moliere	LES CORONS (arr. Freycenon)	\N	\N	\N	?	Papier neuf	Complet en Ut	Variété	3 Variété française	2020-10-02 13:40:25.713017	2020-10-02 13:40:25.713017	26	202
363	\N	\N	Fantaisie	90	\N	Fantaisie - 90	t	Molenaar 01.1950.08	STAR WARS	735	\N	\N	1987	Papier neuf	Complet transposé	Musique de film	1 Fantastique	2020-10-02 13:40:25.71608	2020-10-02 13:40:25.71608	605	366
364	\N	\N	Fantaisie	90	\N	Fantaisie - 90	t	Scomegna	THE APE	170	\N	\N	2012	Papier neuf	Complet transposé	Jazz	\N	2020-10-02 13:40:25.718893	2020-10-02 13:40:25.718893	355	\N
365	\N	\N	Fantaisie	91	\N	Fantaisie - 91	t	Difem	LA SOUPE AUX CHOUX	567	Normal	\N	1981	Papier neuf	Complet transposé	Musique de film	1 Comédies	2020-10-02 13:40:25.721787	2020-10-02 13:40:25.721787	319	118
366	Medaillon calls, the black pearl, o the pirate's cave, one last shot, he's a pirate	\N	Fantaisie	91	\N	Fantaisie - 91	t	Walt Disney	PIRATES OF THE CARIBBEAN (Arr. Wasson)	\N	\N	\N	2003	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.724472	2020-10-02 13:40:25.724472	27	597
367	Concerto pour saxo	\N	Fantaisie	92	\N	Fantaisie - 92	t	Studio Music Company	CARNIVAL	503	\N	\N	1998	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.726867	2020-10-02 13:40:25.726867	541	\N
368	\N	\N	Fantaisie	92	\N	Fantaisie - 92	t	Marc Reift EMR 1746	WEST SIDE STORY (arr. Mortimer)	\N	Difficile	\N	1957	Papier neuf	Complet transposé	Musique de film	1 Comédies	2020-10-02 13:40:25.729396	2020-10-02 13:40:25.729396	49	397
369	\N	\N	Fantaisie	93	\N	Fantaisie - 93	t	Difem	BEETLEJUICE	196	\N	\N	?	Papier neuf	Complet transposé	Musique de film	1 Fantastique	2020-10-02 13:40:25.732264	2020-10-02 13:40:25.732264	172	118
370	\N	\N	Fantaisie	93	\N	Fantaisie - 93	t	Marc Reift EMR 1897	MARY POPPINS	272	\N	\N	1963	Papier neuf	Complet transposé	Musique de film	1 Fantastique	2020-10-02 13:40:25.735198	2020-10-02 13:40:25.735198	527	515
371	\N	\N	Fantaisie	93	\N	Fantaisie - 93	t	Robert Martin (R 3855 M)	LES LUNES DE CUZCO	\N	Facile	\N	2003	Papier neuf	Absent	\N	\N	2020-10-02 13:40:25.737771	2020-10-02 13:40:25.737771	562	182
372	\N	\N	Fantaisie	94	\N	Fantaisie - 94	t	Musica Mundana	CHAMPAGNE GALLOP	169	\N	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.740381	2020-10-02 13:40:25.740381	340	288
373	\N	\N	Fantaisie	94	\N	Fantaisie - 94	t	Musica Mundana	EMMANUELLE	240	\N	\N	1974	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.743159	2020-10-02 13:40:25.743159	132	442
374	\N	\N	Fantaisie	94	\N	Fantaisie - 94	t	Marc Reift EMR 1458	THEME FROM SCHINDLER'S LIST (La liste de Schindler)	153	\N	\N	1999	Papier neuf	Complet transposé	Musique de film	1 Drame	2020-10-02 13:40:25.746456	2020-10-02 13:40:25.746456	605	560
375	\N	\N	Fantaisie	95	\N	Fantaisie - 95	t	Mitropa Music	IL BRICCONE	356	\N	\N	2001	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.749802	2020-10-02 13:40:25.749802	165	\N
376	\N	\N	Fantaisie	95	\N	Fantaisie - 95	t	\N	GLACIERS	\N	\N	\N	?	\N	\N	\N	\N	2020-10-02 13:40:25.753122	2020-10-02 13:40:25.753122	521	\N
377	\N	\N	Fantaisie	95	\N	Fantaisie - 95	t	Hal Leonard Publishing	SHACKELFORD BANKS	205	\N	\N	2003	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.755984	2020-10-02 13:40:25.755984	56	\N
378	Piece en 4 mouvements	\N	Fantaisie	96	\N	Fantaisie - 96	t	Barnhouse Company	THE SYMPHONY OF SOULS	\N	\N	\N	2007	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.760148	2020-10-02 13:40:25.760148	630	\N
379	\N	\N	Fantaisie	96	\N	Fantaisie - 96	t	De Haske 1033406	SING SING SING 	\N	\N	\N	1936	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.762689	2020-10-02 13:40:25.762689	469	274
380	Medley de 6 morceaux du King !	\N	Fantaisie	96	\N	Fantaisie - 96	t	De Haske 1140.04 MS	GRACELAND MEMORIES	135	Normal	1956	?	Papier neuf	Complet transposé	Variété	Rock	2020-10-02 13:40:25.765456	2020-10-02 13:40:25.765456	468	55
381	\N	\N	Fantaisie	96	\N	Fantaisie - 96	t	De Haske 950643	I WILL SURVIVE	\N	\N	\N	1995	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:25.767899	2020-10-02 13:40:25.767899	184	523
382	\N	\N	Fantaisie	97	\N	Fantaisie - 97	t	Belwin Inc	SWAHILI FOLK HYMN	150	\N	\N	2008	Papier neuf	Complet transposé	Musique traditionnelle	2 Musique du monde	2020-10-02 13:40:25.770263	2020-10-02 13:40:25.770263	\N	380
383	Papier format défilé	\N	Fantaisie	97	\N	Fantaisie - 97	t	Scomegna	SWING TIME	161	Facile	\N	1999	Papier neuf	Complet transposé	Danse	Rock	2020-10-02 13:40:25.772633	2020-10-02 13:40:25.772633	270	\N
384	Papier format défilé	\N	Fantaisie	97	\N	Fantaisie - 97	t	Tierolff Muziekcentral	DRUMBO	170	Facile	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.775273	2020-10-02 13:40:25.775273	314	\N
385	Variation sur Deux thèmes nord américains	\N	Fantaisie	97	\N	Fantaisie - 97	t	Harmonie de la Chazotte	SECESSION	\N	Facile	\N	2009	Papier neuf	Absent	Musique traditionnelle	2 Musique du monde	2020-10-02 13:40:25.777769	2020-10-02 13:40:25.777769	\N	408
386	mise à l'honneur des percussions	\N	Fantaisie	97	\N	Fantaisie - 97	t	Belwin Inc	WALTZING CAT	180	\N	1950	1950	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.780523	2020-10-02 13:40:25.780523	9	309
387	3 mouvements	\N	Fantaisie	98	\N	Fantaisie - 98	t	Marc Reift EMR 10044	SAHARA	518	\N	\N	?	Papier neuf	Complet transposé	Musique traditionnelle	2 Musique du monde	2020-10-02 13:40:25.783001	2020-10-02 13:40:25.783001	560	\N
428	\N	2015	Fantaisie	107	\N	Fantaisie - 107	t	\N	GAME OF THRONES	225	Normal	\N	\N	\N	\N	Musique de film	1 Épopée	2020-10-02 13:40:25.905478	2020-10-02 13:40:25.905478	635	636
388	Coté Humoristique, moreau coupé par les discussions	\N	Fantaisie	98	\N	Fantaisie - 98	t	Beriato Music	TALKING SHOES	317	Normal	\N	2008	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.785638	2020-10-02 13:40:25.785638	581	\N
389	Macweo signifie levé de soleil en Swahili, possibilté de choeur	\N	Fantaisie	98	\N	Fantaisie - 98	t	Beriato Music	MACHWEO	321	Difficile	\N	2008	Papier neuf	Complet transposé	Musique traditionnelle	2 Musique du monde	2020-10-02 13:40:25.788655	2020-10-02 13:40:25.788655	73	\N
390	cf fantaisie 21	\N	Fantaisie	99	\N	Fantaisie - 99	t	Bernaerts Music	LES CHAMPS ELYSEES	197	\N	\N	2008	Papier neuf	Complet transposé	Variété	3 Variété française	2020-10-02 13:40:25.792072	2020-10-02 13:40:25.792072	146	301
391	\N	\N	Fantaisie	99	\N	Fantaisie - 99	t	Hal Leonard Publishing	THE LORD OF THE DANCE	169	\N	\N	1997	Papier neuf	Complet transposé	Musique traditionnelle	2 Celtique	2020-10-02 13:40:25.795704	2020-10-02 13:40:25.795704	250	512
392	\N	\N	Fantaisie	99	\N	Fantaisie - 99	t	Robert Martin (R 4943 M)	YVES MONTAND	340	\N	\N	2001	Papier neuf	Complet transposé	Variété	3 Variété française	2020-10-02 13:40:25.798876	2020-10-02 13:40:25.798876	390	403
393	\N	\N	Fantaisie	99	\N	Fantaisie - 99	t	?	THE LORD OF THE RINGS	\N	\N	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.801538	2020-10-02 13:40:25.801538	530	\N
394	\N	\N	Fantaisie	100	\N	Fantaisie - 100	t	Anglo music Press	A KLEZMER KARNIVAL	342	Normal	\N	2004	Papier neuf	Complet transposé	Musique traditionnelle	2 Europe centrale	2020-10-02 13:40:25.803993	2020-10-02 13:40:25.803993	541	\N
395	\N	\N	Fantaisie	100	\N	Fantaisie - 100	t	Alfred	TOKYO 2000	360	Normal	2000	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.806405	2020-10-02 13:40:25.806405	423	\N
396	Avec un tampon « Offert par H. M. M. O. »	\N	Fantaisie	100	\N	Fantaisie - 100	t	Hal Leonard Publishing	AMERICAN VISIONS	\N	\N	\N	1997	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.808769	2020-10-02 13:40:25.808769	\N	398
397	\N	\N	Fantaisie	101	\N	Fantaisie - 101	t	De Haske 930500	SALAMANCA	201	\N	\N	1993	Papier neuf	Complet transposé	Musique traditionnelle	2 Espagnol	2020-10-02 13:40:25.811092	2020-10-02 13:40:25.811092	609	\N
398	\N	\N	Fantaisie	103	\N	Fantaisie - 103	t	Obrasso-Verlag	PATRICIA	163	\N	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.813681	2020-10-02 13:40:25.813681	467	594
399	Piece en 3 mouvements	\N	Fantaisie	101	\N	Fantaisie - 101	t	Scomegna	PICTURES FROM SPAIN	366	\N	\N	2009	Papier neuf	Complet transposé	Musique traditionnelle	2 Espagnol	2020-10-02 13:40:25.816223	2020-10-02 13:40:25.816223	104	\N
400	\N	\N	Fantaisie	102	\N	Fantaisie - 102	t	CURNOW Music Press	LET IT SHINE	300	Normal	\N	1997	papier neuf	complet transposé	Musique traditionnelle	Gospel	2020-10-02 13:40:25.819237	2020-10-02 13:40:25.819237	\N	267
401	\N	\N	Fantaisie	102	\N	Fantaisie - 102	t	Robert Martin (R 4084 M)	SEVILLANA	340	\N	2004	2004	papier neuf	complet transposé	\N	\N	2020-10-02 13:40:25.821861	2020-10-02 13:40:25.821861	163	\N
402	\N	\N	Fantaisie	103	\N	Fantaisie - 103	t	Hafabra Music – Louis Martinus	I WILL FOLLOW HIM (Sister Act)	212	\N	\N	?	Papier neuf	Complet transposé	Musique de film	1 Comédies	2020-10-02 13:40:25.824288	2020-10-02 13:40:25.824288	455	535
403	\N	\N	Fantaisie	103	\N	Fantaisie - 103	t	De Haske 1114972	THE ECTASY OF GOLD (le bon la brute et truand)	250	\N	1966	1968	Papier neuf	Complet transposé	Musique de film	1 Aventure	2020-10-02 13:40:25.826886	2020-10-02 13:40:25.826886	395	55
404	\N	\N	Fantaisie	103	\N	Fantaisie - 103	t	Difem	SEX AND THE CITY	149	\N	\N	2008	Papier neuf	Complet transposé	Musique de film	1 Comédies	2020-10-02 13:40:25.830311	2020-10-02 13:40:25.830311	140	6
405	\N	\N	Fantaisie	103	\N	Fantaisie - 103	t	Bernaerts Music	GHOSTBUSTERS	203	\N	\N	1980	Papier neuf	Complet transposé	Musique de film	1 Fantastique	2020-10-02 13:40:25.833564	2020-10-02 13:40:25.833564	435	47
406	\N	\N	Fantaisie	104	\N	Fantaisie - 104	t	HAFABRA Music – Louis Martinus	THE ROCK (Highlights from « the rock »)	708	Difficile	\N	1996	Papier neuf	Complet transposé	Musique de film	1 Aventure	2020-10-02 13:40:25.837401	2020-10-02 13:40:25.837401	613	160
407	\N	\N	Fantaisie	104	\N	Fantaisie - 104	t	Scomegna	FUNKY TIME	222	\N	\N	2012	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.840175	2020-10-02 13:40:25.840175	18	\N
408	Piece pour plusieurs solistes	2014	Fantaisie	104	\N	Fantaisie - 104	t	Tierolff Muziekcentral	CRAZY TONGUES	248	Difficile	2013	2013	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.842589	2020-10-02 13:40:25.842589	20	\N
409	\N	\N	Fantaisie	105	\N	Fantaisie - 105	t	Harmonie de la Chazotte	ASIMBONANGA	198	Facile	\N	2010	Papier neuf	Complet transposé	Variété	3 Variété internationale	2020-10-02 13:40:25.845342	2020-10-02 13:40:25.845342	125	456
410	\N	\N	Fantaisie	105	\N	Fantaisie - 105	t	Harmonie de la Chazotte	20TH CENTURY FOX	26	Facile	\N	2013	Papier neuf	Complet transposé	Musique de film	\N	2020-10-02 13:40:25.847877	2020-10-02 13:40:25.847877	412	456
411	Petit format pour défilé	\N	Fantaisie	105	\N	Fantaisie - 105	t	Chappel ad Co	GREAT BALLS OF FIRE	\N	\N	\N	2001	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.850477	2020-10-02 13:40:25.850477	52	559
412	Petit format pour défilé	\N	Fantaisie	105	\N	Fantaisie - 105	t	Music Corp	I GOT RHYTHM	\N	\N	\N	2001	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.853047	2020-10-02 13:40:25.853047	216	97
413	Petit format pour défilé	\N	Fantaisie	105	\N	Fantaisie - 105	t	Emi Fuel Keel Music	OYE COMO VA	\N	\N	\N	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.855634	2020-10-02 13:40:25.855634	472	549
414	Petit format pour défilé	\N	Fantaisie	105	\N	Fantaisie - 105	t	Silhouette Music	SOUL BOSSA NOVA	\N	\N	\N	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.858224	2020-10-02 13:40:25.858224	285	254
415	Petit format pour défilé	\N	Fantaisie	105	\N	Fantaisie - 105	t	Hal Leonard Publishing	TAILGATE RAMBLE	\N	\N	\N	1962	Papier neuf	Absent	\N	\N	2020-10-02 13:40:25.860803	2020-10-02 13:40:25.860803	22	\N
416	Petit format pour défilé	\N	Fantaisie	105	\N	Fantaisie - 105	t	Myers Music	ROCK AROUND THE CLOCK (format défilé)	\N	\N	\N	1974	Papier neuf	Absent	\N	\N	2020-10-02 13:40:25.863587	2020-10-02 13:40:25.863587	201	22
417	Petit format pour défilé	\N	Fantaisie	105	\N	Fantaisie - 105	t	Hal Leonard Publishing	BILL BAILEY	\N	\N	\N	1959	Papier neuf	Absent	\N	\N	2020-10-02 13:40:25.866093	2020-10-02 13:40:25.866093	\N	22
418	Petit format pour défilé	\N	Fantaisie	105	\N	Fantaisie - 105	t	Hal Leonard Publishing	TUXEDO  JUNCTION	127	\N	\N	1979	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.868714	2020-10-02 13:40:25.868714	189	586
419	Petit format pour défilé	\N	Fantaisie	105	\N	Fantaisie - 105	t	Metro Goldwyn Mayer	FAME (Arr. Kerchner)	\N	\N	\N	1980	Papier neuf	Absent	\N	\N	2020-10-02 13:40:25.871574	2020-10-02 13:40:25.871574	230	292
420	\N	\N	Fantaisie	106	\N	Fantaisie - 106	t	Musikverlag Rundel	TRANEN LUGEN NICHT	\N	\N	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.874645	2020-10-02 13:40:25.874645	611	546
421	\N	\N	Fantaisie	106	\N	Fantaisie - 106	t	Marc Reift EMR 10063	DIX	\N	\N	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.87821	2020-10-02 13:40:25.87821	300	515
422	Morceau acheté par la fédération musicale de la Loire pour sélection d'oeuvre commune	\N	Fantaisie	106	\N	Fantaisie - 106	t	Marc Reift EMR 10092	REDEMPTION	\N	\N	\N	?	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:25.881073	2020-10-02 13:40:25.881073	488	\N
423	\N	2015	Fantaisie	106	\N	Fantaisie - 106	t	Molenaar 01.3259.05	CHAN CHAN (Buena vista social club)	282	\N	1997	1997	Papier neuf	Complet transposé	Musique traditionnelle	2 Amérique du sud	2020-10-02 13:40:25.883828	2020-10-02 13:40:25.883828	486	55
424	\N	2015	Fantaisie	106	\N	Fantaisie - 106	t	DIFEM	INSPECTOR GADGET (Inspecteur Gadget)	173	\N	\N	?	Papier neuf	Complet transposé	Musique de film	1 Comédies	2020-10-02 13:40:25.887809	2020-10-02 13:40:25.887809	631	478
425	\N	2015	Fantaisie	106	\N	Fantaisie - 106	t	Bernaerts Music	HAPPY	235	\N	2013	2013	Papier neuf	Complet transposé	Variété	3 Variété internationale	2020-10-02 13:40:25.890347	2020-10-02 13:40:25.890347	606	47
430	Avec partie chant	2016	Fantaisie	107	\N	Fantaisie - 107	t	Harmonie de la Chazotte	YOU BETTER STOP	270	\N	\N	2016	Papier neuf	\N	Variété	3 Variété internationale	2020-10-02 13:40:25.91419	2020-10-02 13:40:25.91419	638	312
431	Produit par Two steps from hell	2016	Fantaisie	108	\N	Fantaisie - 108	t	\N	HEART OF COURAGE	230	Facile	\N	\N	\N	\N	Musique de film	1 Aventure	2020-10-02 13:40:25.921318	2020-10-02 13:40:25.921318	639	640
432	\N	2016	Fantaisie	108	\N	Fantaisie - 108	t	\N	THAT NIGHT DIM	330	\N	\N	\N	\N	\N	Musique de film	1 Romantique	2020-10-02 13:40:25.926158	2020-10-02 13:40:25.926158	514	641
433	\N	2016	Fantaisie	108	\N	Fantaisie - 108	t	Harmonie de la Chazotte	JEAN MINEUR (Mediavision)	35	Normal	\N	2016	Papier neuf	Complet transposé	Musique de film	1 Comédies	2020-10-02 13:40:25.930341	2020-10-02 13:40:25.930341	642	456
434	\N	2016	Fantaisie	108	\N	Fantaisie - 108	\N	\N	SAMHAIN	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.933184	2020-10-02 13:40:25.933184	620	\N
435	\N	2016	Fantaisie	109	\N	Fantaisie - 109	\N	\N	THE HUNGER GAMES	245	\N	\N	\N	\N	\N	Musique de film	1 Épopée	2020-10-02 13:40:25.935959	2020-10-02 13:40:25.935959	56	\N
436	\N	2016	Fantaisie	109	\N	Fantaisie - 109	\N	\N	NOEL	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.940346	2020-10-02 13:40:25.940346	643	\N
437	\N	2016	Fantaisie	109	\N	Fantaisie - 109	\N	\N	OLD RUSSIAN ROMANCES	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.94475	2020-10-02 13:40:25.94475	644	\N
438	\N	\N	Fantaisie	109	\N	Fantaisie - 109	\N	\N	SANUKI RHAPSODY	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.947164	2020-10-02 13:40:25.947164	\N	\N
439	\N	\N	Fantaisie	110	\N	Fantaisie - 110	\N	\N	TINTIN	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.949554	2020-10-02 13:40:25.949554	\N	\N
440	\N	\N	Fantaisie	110	\N	Fantaisie - 110	\N	\N	UP	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.952062	2020-10-02 13:40:25.952062	\N	\N
441	\N	\N	Fantaisie	110	\N	Fantaisie - 110	\N	\N	PASADENA	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.954396	2020-10-02 13:40:25.954396	\N	\N
442	\N	\N	Fantaisie	110	\N	Fantaisie - 110	\N	\N	TORNA ASORRENTO	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.956791	2020-10-02 13:40:25.956791	\N	\N
443	\N	\N	Fantaisie	110	\N	Fantaisie - 110	\N	\N	GONNA FLY NOW	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.959095	2020-10-02 13:40:25.959095	\N	\N
444	\N	\N	Fantaisie	111	\N	Fantaisie - 111	\N	\N	LOONEY TUNES	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.961983	2020-10-02 13:40:25.961983	\N	\N
445	\N	\N	Fantaisie	111	\N	Fantaisie - 111	\N	\N	RATATOUILLE	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.964861	2020-10-02 13:40:25.964861	\N	\N
446	\N	\N	Fantaisie	111	\N	Fantaisie - 111	\N	\N	LAPUTA CASTLE IN THE SKY	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.968214	2020-10-02 13:40:25.968214	\N	\N
447	\N	\N	Fantaisie	112	\N	Fantaisie - 112	\N	\N	BUGS BUNNY AND FRIENDS	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.970908	2020-10-02 13:40:25.970908	\N	\N
448	\N	\N	Fantaisie	112	\N	Fantaisie - 112	\N	\N	DRAGON	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.973176	2020-10-02 13:40:25.973176	\N	\N
449	\N	\N	Fantaisie	112	\N	Fantaisie - 112	\N	\N	KUNG FU PANDA	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.975291	2020-10-02 13:40:25.975291	\N	\N
450	\N	\N	Fantaisie	113	\N	Fantaisie - 113	\N	\N	LATIN BAND	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.977378	2020-10-02 13:40:25.977378	\N	\N
451	\N	\N	Fantaisie	113	\N	Fantaisie - 113	\N	\N	HALLELUJHA	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.979543	2020-10-02 13:40:25.979543	\N	\N
452	\N	\N	Fantaisie	113	\N	Fantaisie - 113	\N	\N	THE CHRONICLES OF NARNIA	300	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.985117	2020-10-02 13:40:25.985117	645	646
453	\N	\N	Fantaisie	114	\N	Fantaisie - 114	\N	\N	HUNAB KU	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.987231	2020-10-02 13:40:25.987231	\N	\N
454	\N	\N	Fantaisie	114	\N	Fantaisie - 114	\N	\N	LA POURSUITE	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.98927	2020-10-02 13:40:25.98927	\N	\N
455	\N	\N	Fantaisie	114	\N	Fantaisie - 114	\N	\N	SCOTLAND AIR	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.991708	2020-10-02 13:40:25.991708	\N	\N
456	\N	\N	Fantaisie	114	\N	Fantaisie - 114	\N	\N	ERIAN'S MYSTICAL RHYMES	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.99436	2020-10-02 13:40:25.99436	\N	\N
457	\N	\N	Fantaisie	115	\N	Fantaisie - 115	\N	\N	THE LION KING	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:25.997073	2020-10-02 13:40:25.997073	\N	\N
458	\N	\N	Fantaisie	115	\N	Fantaisie - 115	\N	\N	DJINJI RINDGII BUBAMARA	210	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:26.003648	2020-10-02 13:40:26.003648	647	648
459	\N	\N	Fantaisie	116	\N	Fantaisie - 116	\N	\N	CHANSON D'AMOUR	205	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:26.009709	2020-10-02 13:40:26.009709	649	366
460	\N	\N	Fantaisie	116	\N	Fantaisie - 116	\N	\N	VOIE LACTEE	418	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:26.015573	2020-10-02 13:40:26.015573	650	\N
461	\N	\N	Fantaisie	116	\N	Fantaisie - 116	\N	\N	WINDS ON FIRE	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:26.018326	2020-10-02 13:40:26.018326	12	\N
462	\N	\N	Fantaisie	117	\N	Fantaisie - 117	\N	\N	WINDOWS OF THE WORLD	\N	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:26.022748	2020-10-02 13:40:26.022748	651	\N
463	Moana dysney	\N	Fantaisie	117	\N	Fantaisie - 117	\N	\N	YOU'RE WELCOME	164	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:26.028495	2020-10-02 13:40:26.028495	652	653
464	\N	\N	Fantaisie	117	\N	Fantaisie - 117	\N	\N	MAMBO CUBANO	120	\N	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:26.0308	2020-10-02 13:40:26.0308	558	\N
465	\N	\N	Fantaisie	118	\N	Fantaisie - 118	\N	\N	TICO TICO	164	Difficile	\N	\N	\N	\N	\N	\N	2020-10-02 13:40:26.034866	2020-10-02 13:40:26.034866	654	274
466	Basé sur That's a-plenty	\N	Fantaisie	68	\N	Fantaisie - 68	t	Hal Leonard Publishing	DRUMS A-PLENTY	190	Normal	\N	1999	Papier neuf	Complet transposé	Jazz	\N	2020-10-02 13:40:26.037891	2020-10-02 13:40:26.037891	458	122
467	\N	\N	Fantaisie	21	+ ArmCond	Fantaisie - 21	t	Jenson Publication 210-12010	LE SABRE	\N	\N	\N	1978	Vieux papier	Complet en Ut	\N	\N	2020-10-02 13:40:26.040626	2020-10-02 13:40:26.040626	431	\N
468	morceaux de style fantaisie	\N	Classique	19	\N	Classique - 19	t	Molenaar	FASCINATING DRUMS	\N	\N	\N	1976	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:26.043078	2020-10-02 13:40:26.043078	268	\N
469	Tamponné EMAD Berlioz	\N	Grandes Marches	1	\N	Grandes Marches - 1	t	Phoenix Music (PM 9428)	RADETZKY MARCH	166	\N	\N	2006	Papier neuf	Absent	Classique	Grandes marche	2020-10-02 13:40:26.045622	2020-10-02 13:40:26.045622	554	108
470	\N	Des copies papiers ont été faites	Grandes Marches	1	\N	Grandes Marches - 1	t	Robert Martin	LA MARCHE DE RADETZKY	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.048965	2020-10-02 13:40:26.048965	554	354
471	Tamponné HdC après 1957	\N	Grandes Marches	1	\N	Grandes Marches - 1	t	Robert Martin (R 837 M)	MARCHE DE RADETSKY	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.05234	2020-10-02 13:40:26.05234	554	354
472	\N	\N	Grandes Marches	2	ArmCond	Grandes Marches - 2	t	Robert Martin (R 811 M)	MARCHE DES RUINES D'ATHENES	\N	\N	1811	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.056638	2020-10-02 13:40:26.056638	41	354
473	\N	\N	Grandes Marches	3	ArmCond	Grandes Marches - 3	t	Andrieu Freres	MARCHE DE TANNHAUSER	370	\N	1845	1951	Carton	Réduit en Sib	Classique	Opéra	2020-10-02 13:40:26.059839	2020-10-02 13:40:26.059839	590	185
474	\N	\N	Grandes Marches	4	\N	Grandes Marches - 4	t	Evette et Schaeffer (11675)	MARCHE LORRAINE	336	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.062716	2020-10-02 13:40:26.062716	208	\N
475	Tamponné Union musicale de St Chamond	\N	Grandes Marches	5	\N	Grandes Marches - 5	t	Evette et Schaeffer (1741)	SUITE ALGERIENNE (n°4 Marche militaire)	\N	\N	1879	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.065456	2020-10-02 13:40:26.065456	509	65
476	\N	\N	Grandes Marches	6	\N	Grandes Marches - 6	t	Alliance Musicale	MARCHE SOLENNELLE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.067878	2020-10-02 13:40:26.067878	603	\N
477	\N	\N	Grandes Marches	6	ArmCond	Grandes Marches - 6	t	Andrieu Freres	CORTEGE ET CARILLON	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.070277	2020-10-02 13:40:26.070277	31	\N
478	Conducteur manuscrit de Jean Ollier daté.	1947-02-17	Grandes Marches	7	ArmCond	Grandes Marches - 7	t	Harmonie de la Chazotte	MARCHE SOLENNELLE N° 1	\N	\N	\N	1947	Carton	Absent	\N	\N	2020-10-02 13:40:26.072691	2020-10-02 13:40:26.072691	427	\N
479	\N	\N	Grandes Marches	8	\N	Grandes Marches - 8	t	Harmonie de la Chazotte	MARCHE SOLENNELLE N° 2	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.075126	2020-10-02 13:40:26.075126	427	\N
480	cf Grenier	\N	Grandes Marches	9	\N	Grandes Marches - 9	t	Harmonie de la Chazotte	MARCHE SOLENNELLE N° 3 (ref Gde Marche)	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.077582	2020-10-02 13:40:26.077582	427	\N
481	\N	\N	Grandes Marches	10	\N	Grandes Marches - 10	t	Andrieu Freres	MARCHE DE RIENZI	215	\N	1840	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.080724	2020-10-02 13:40:26.080724	590	54
482	\N	\N	Grandes Marches	10	\N	Grandes Marches - 10	t	Andrieu	MARCHE DU SONGE D'UNE NUIT D'ETE	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.083715	2020-10-02 13:40:26.083715	368	10
483	\N	\N	Grandes Marches	11	\N	Grandes Marches - 11	t	Champel	TYROL – MARCHE (Marche Allemande)	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.086095	2020-10-02 13:40:26.086095	77	\N
484	Tampon Harmonie des Houilleres de la Loire	\N	Grandes Marches	12	ArmCond	Grandes Marches - 12	t	Andrieu Freres	MARCHE DU SACRE DU PROPHETE	293	\N	1849	?	Carton	Réduit en Sib	Classique	Opéra	2020-10-02 13:40:26.088692	2020-10-02 13:40:26.088692	374	185
485	\N	\N	Grandes Marches	13	\N	Grandes Marches - 13	t	Boosey	POMP AND CIRCONSTANCE (Military Marches n°1)	\N	\N	\N	1902	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:26.091849	2020-10-02 13:40:26.091849	173	\N
486	Super connu !	\N	Grandes Marches	14	\N	Grandes Marches - 14	t	Salabert	L’ENTREE DES GLADIATEURS (Marche triomphale)	180	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.095451	2020-10-02 13:40:26.095451	204	608
487	\N	\N	Grandes Marches	14	\N	Grandes Marches - 14	t	L’accord parfait (Lyon, Bellecours)	NUIT FANTASTIQUE (Ouverture)	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.099558	2020-10-02 13:40:26.099558	74	\N
488	\N	\N	Grandes Marches	15	\N	Grandes Marches - 15	t	Andrieu (F. A. 1201)	MARCHE MILITAIRE N° 2	\N	\N	1818 ?	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.102824	2020-10-02 13:40:26.102824	517	10
489	Pas Redoublés avec Batterie Fanfare	\N	Grandes Marches	15	\N	Grandes Marches - 15	t	Buffet-Crampon, Evette et Schaeffer (11054)	LE LILLOIS	\N	\N	\N	?	Carton	Réduit en Sib	\N	Défilé	2020-10-02 13:40:26.105642	2020-10-02 13:40:26.105642	324	\N
490	\N	\N	Grandes Marches	16	\N	Grandes Marches - 16	t	Andrieu Freres	CELEBRE MARCHE MILITAIRE	\N	\N	1824 ?	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.108625	2020-10-02 13:40:26.108625	517	10
491	\N	\N	Grandes Marches	16	\N	Grandes Marches - 16	t	Andrieu Freres	MUSIQUE EN TETE	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.111319	2020-10-02 13:40:26.111319	460	\N
492	Tamponné Ecole Nationale de musique de St Etienne	\N	Grandes Marches	17	\N	Grandes Marches - 17	t	Robert Martin	1 ère MARCHE AUX FLAMBEAUX	\N	\N	\N	?	Vieux papier	Absent	\N	\N	2020-10-02 13:40:26.114311	2020-10-02 13:40:26.114311	374	608
493	\N	\N	Grandes Marches	17	\N	Grandes Marches - 17	t	Molenaar	SUITE ANVERSOISE DE 1583 (Suite uit het Antwerpse Dansboek 1583)	360	\N	\N	1975	Vieux papier	Réduit en Ut	\N	\N	2020-10-02 13:40:26.117327	2020-10-02 13:40:26.117327	447	330
494	\N	\N	Grandes Marches	18	ArmCond	Grandes Marches - 18	t	Robert Martin R 2157 M	DEUX MARCHES DE LA REVOLUTION FRANCAISE	\N	\N	1789	1988	Papier neuf	Réduit en Sib	\N	\N	2020-10-02 13:40:26.120478	2020-10-02 13:40:26.120478	231	88
495	Tamponné « Offert par H. M. M. O. »	\N	Grandes Marches	18	ArmCond	Grandes Marches - 18	t	Belwyn Mills Publishing Corp	HIGHLAND PARK	\N	Difficile	\N	1989	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:26.123699	2020-10-02 13:40:26.123699	96	\N
496	cf marche ac clique 8	\N	Grandes Marches	19	\N	Grandes Marches - 19	t	Harmonie de la Chazotte	MARCHE DU CENTENAIRE	\N	\N	1965 ?	1965 ?	Carton	Absent	\N	\N	2020-10-02 13:40:26.126922	2020-10-02 13:40:26.126922	427	\N
497	Il s'agit du 4eme mouvement de la Symphonie Fantastique	\N	Grandes Marches	20	\N	Grandes Marches - 20	t	De HASKE	MARCHE AU SUPPLICE	323	Difficile	1830	1999	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:26.130332	2020-10-02 13:40:26.130332	46	40
498	aussi appelée Marche de Rakoczy dans la Damnation de Faust	\N	Grandes Marches	20	\N	Grandes Marches - 20	t	Kjos west	MARCHE HONGROISE de la damnation de Faust	323	Difficile	1846	1986	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:26.134156	2020-10-02 13:40:26.134156	46	232
499	\N	\N	Grandes Marches	21	\N	Grandes Marches - 21	t	De HASKE	HOMAGE MARCH (from SIGURD JORSALFAR)	626	Difficile	1893	1994	Papier neuf	Complet transposé	Classique	Grandes marche	2020-10-02 13:40:26.13798	2020-10-02 13:40:26.13798	236	40
500	Pièce en 3 mouvement Prélude, Intermede et Marche solennelle dite homage march	\N	Grandes Marches	21	ArmCond	Grandes Marches - 21	t	Buffet-Crampon, Evette et Schaeffer (13887)	SIGURD JORSALFAR	\N	\N	1893	?	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:26.142093	2020-10-02 13:40:26.142093	236	167
501	\N	\N	Grandes Marches	21	\N	Grandes Marches - 21	t	Marc Reift	EGYPTIAN MARCH (marche egyptienne)	240	\N	1880 ?	?	Papier neuf	Complet transposé	Classique	Grandes marche	2020-10-02 13:40:26.145302	2020-10-02 13:40:26.145302	554	397
502	\N	\N	Grenier	13	\N	Grenier - 13	t	Paris Alphonse Leduc	MARCHE TURQUE	\N	\N	1872	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.148367	2020-10-02 13:40:26.148367	401	225
503	aussi appelée Marche de Rakoczy dans la Damnation de Faust	\N	Grenier	23	\N	Grenier - 23	t	Andrieu Frères	MARCHE HONGROISE DE LA DAMNATION DE FAUST	\N	\N	1846	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.151286	2020-10-02 13:40:26.151286	46	221
504	\N	\N	Grenier	16	\N	Grenier - 16	t	Evette et Schaeffer	MARCHE MILITAIRE	\N	\N	1893	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.15427	2020-10-02 13:40:26.15427	566	400
505	cf Grande Marche 9	\N	Grenier	18	\N	Grenier - 18	t	Harmonie de la Chazotte	MARCHE SOLENNELLE N° 3 (ref Grenier)	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.157006	2020-10-02 13:40:26.157006	427	\N
506	\N	\N	Grenier	14	ArmCond	Grenier - 14	t	evette et Schaeffer 20085	HULDA MARCHE DU 4 ème ACTE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.159921	2020-10-02 13:40:26.159921	198	367
507	cf religieux 9	\N	Grenier	5	ArmCond	Grenier - 5	t	Andrieu	MARCHE FUNEBRE (ref Grenier)	\N	\N	1837	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.162738	2020-10-02 13:40:26.162738	121	10
508	Numéro du répertoire 56 N° ordre 8	Remis à l’harmonie de la Chazotte Le 13 – 10 – 1961	Grenier	15	ArmCond	Grenier - 15	t	Evette et Schaeffer 11843	MARCHE RELIGIEUSE	\N	\N	1882 ?	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.165945	2020-10-02 13:40:26.165945	233	214
509	\N	\N	Grenier	15	ArmCond	Grenier - 15	t	Evette et Schaeffer 11749	MARCHE HEROIQUE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.168904	2020-10-02 13:40:26.168904	517	169
510	\N	\N	Grenier	22	ArmCond	Grenier - 22	t	Andrieu Frères	OUVERTURE DE FETE	\N	\N	1815	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.171762	2020-10-02 13:40:26.171762	41	333
511	Conducteur manuscrit de Jean Ollier daté du 16 juillet 1959	1959-07-16	Grenier	19	ArmCond	Grenier - 19	t	Harmonie de la Chazotte	LARGHETTO DE LA II SYMPHONIE	\N	\N	1802	?	Carton	Complet transposé	\N	\N	2020-10-02 13:40:26.175884	2020-10-02 13:40:26.175884	41	427
512	\N	\N	Grenier	21	ArmCond	Grenier - 21	t	Andrieu	SYMPHONIE n°V en ut mineur – ANDANTE	540	\N	1808	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.179635	2020-10-02 13:40:26.179635	41	93
513	Morceau de choix présenté et executé au concours de Lisieux le 2 mai 1965 par L'Harmonie des Houilleres	\N	Grenier	3	ArmCond	Grenier - 3	t	Andrieu	OUVERTURE DE LA FLUTE ENCHANTEE	\N	Normal	1791	?	Carton	Réduit en Ut	\N	\N	2020-10-02 13:40:26.183333	2020-10-02 13:40:26.183333	402	211
514	\N	\N	Grenier	7	ArmCond	Grenier - 7	t	Andrieu	OUVERTURE DU ROI DE LAHORE	\N	\N	1877	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.186547	2020-10-02 13:40:26.186547	358	169
515	Extrait de la Foire de Sorotchintsi	\N	Grenier	17	ArmCond	Grenier - 17	t	Alphonse LEDUC	GOPAK	\N	\N	1866	1939	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.189482	2020-10-02 13:40:26.189482	401	225
516	\N	\N	Grenier	8	ArmCond	Grenier - 8	t	andrieu frères	DIVERTISSEMENT DES ERINNYES (Danse Grecque, La troyenne, Saturnales)	\N	\N	1876	1946	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.192367	2020-10-02 13:40:26.192367	358	499
517	\N	\N	Grenier	1	ArmCond	Grenier - 1	t	Andrieu Frères	LE FREYSCHUTZ (Ouverture)	\N	Normal	1821	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.195325	2020-10-02 13:40:26.195325	601	326
518	\N	\N	Grenier	9	ArmCond	Grenier - 9	t	Andrieu frères	FETE AU TRIANON	\N	\N	1900 ?	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.198067	2020-10-02 13:40:26.198067	460	\N
519	\N	\N	Grenier	24	ArmCond	Grenier - 24	t	Evette et Schaeffer 13362	L'ETOILE ouverture	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.201015	2020-10-02 13:40:26.201015	111	588
520	\N	\N	Grenier	25	\N	Grenier - 25	t	Evette et Schaeffer	BALLETS RUSSES (CZARDAS, VALSE LENTE, MAZURKA, MARCHE RUSSE)	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.203964	2020-10-02 13:40:26.203964	339	237
521	\N	\N	Grenier	2	ArmCond	Grenier - 2	t	Andrieu frères	L’ENLEVEMENT AU SERAIL (Ouverture)	\N	\N	1782	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.207198	2020-10-02 13:40:26.207198	402	10
522	\N	\N	Grenier	12	\N	Grenier - 12	t	?	SYMPHONIE N° IV – ANDANTE	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.21019	2020-10-02 13:40:26.21019	368	\N
523	\N	\N	Grenier	4	ArmCond	Grenier - 4	t	Evette et Schaeffer 13416	AGNES DAME GALANTE N° 1 - 2 - 3  - 4	\N	\N	\N	1912	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.21333	2020-10-02 13:40:26.21333	188	221
524	\N	\N	Grenier	6	ArmCond	Grenier - 6	t	Andrieu Frères	PRIMEROSE (Esquisse symphonique)	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.216801	2020-10-02 13:40:26.216801	10	598
525	\N	\N	Grenier	6	ArmCond	Grenier - 6	t	Andrieu Frères	NUIT D'AMOUR	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.220235	2020-10-02 13:40:26.220235	154	185
526	BWV 1042	\N	Grenier	10	\N	Grenier - 10	t	?	ADAGIO DU CONCERTO EN MI	\N	\N	1720	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.223927	2020-10-02 13:40:26.223927	25	\N
527	\N	\N	Grenier	27	ArmCond	Grenier - 27	t	Evette et Schaeffer 11657	LA FERIA, Suite espagnole	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.226985	2020-10-02 13:40:26.226985	304	503
528	La référence 26 du grenier contient également des partition du chant du départ référence marche avec Clique 19	\N	Grenier	26	ArmCond	Grenier - 26	t	Editions Maurice Decruck	DU TCHAD A STRASBOURG	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.229528	2020-10-02 13:40:26.229528	126	\N
529	\N	\N	Grenier	20	\N	Grenier - 20	t	Evette et Schaeffer	FANTAISIE SUR MIREILLE (ref grenier)	\N	\N	1864	?	Carton	Complet transposé	\N	\N	2020-10-02 13:40:26.232247	2020-10-02 13:40:26.232247	233	434
530	Pas de pochette	\N	Grenier	28	\N	Grenier - 28	t	E. GAUDET	FANTAISIE SUR ARIANE	\N	\N	1906	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.234929	2020-10-02 13:40:26.234929	358	287
531	Pas de pochette	\N	Grenier	28	\N	Grenier - 28	t	Margueritat	OUVERTURE DU ROI ETIENNE	\N	\N	1811	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.239594	2020-10-02 13:40:26.239594	41	655
532	Pas de pochette	\N	Grenier	28	\N	Grenier - 28	t	Andrieu Frères	LA DAME BLANCHE	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.242567	2020-10-02 13:40:26.242567	58	499
533	Pas de pochette	\N	Grenier	28	\N	Grenier - 28	t	E. GAUDET	SELECTION SUR CARMEN	\N	\N	1875	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.245458	2020-10-02 13:40:26.245458	51	206
534	Tiroirs du grenier. Il s'agit d'un ensemble d'extrait de l'opera La jolie fille de Perth	\N	Grenier	28	\N	Grenier - 28	t	Andrieu	SCENES BOHEMIENNES	\N	\N	1866	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.248594	2020-10-02 13:40:26.248594	51	308
535	Tiroirs	\N	Grenier	28	\N	Grenier - 28	t	Margueritat	LE VOYAGE EN CHINE (ext. Opera)	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.25163	2020-10-02 13:40:26.25163	37	53
536	Tiroirs	\N	Grenier	28	\N	Grenier - 28	t	Margueritat	NUAGE DE DENTELLE	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.254479	2020-10-02 13:40:26.254479	299	379
537	tamponné harmonie des mineurs Roche la Molière	\N	Jazz	1	ArmCond	Jazz - 1	t	Robert Martin (R 1007 M)	DANSORAMA	450	\N	\N	1965	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.257334	2020-10-02 13:40:26.257334	145	\N
538	Des copies papiers ont été faites	\N	Jazz	2	ArmCond	Jazz - 2	t	Robert Martin (R 1464 M)	IN THE MOOD (Dans l’ambiance)	\N	\N	1939	1939	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.260971	2020-10-02 13:40:26.260971	377	145
539	Medley connu de Gershwin (Summertime, It ain’t necessarly so, Bess you is my woman. Des copies pâpiers ont été faites	\N	Jazz	3	ArmCond	Jazz - 3	t	CHAPELL Robert Martin	PORGY AND BESS	\N	\N	\N	1935	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.264559	2020-10-02 13:40:26.264559	216	\N
540	Version sur carton, arrangée par Wayne PEGRAM	\N	Jazz	4	ArmCond	Jazz - 4	t	Shapiro, Bernstein	HARLEM NOCTURNE (ref Jazz)	129	\N	1940	1979	Carton	Complet transposé	\N	\N	2020-10-02 13:40:26.2686	2020-10-02 13:40:26.2686	246	440
541	Des copies papiers ont été faites	\N	Jazz	4	+ ArmCond	Jazz - 4	t	Lewis Music Publishing	TUXEDO JUNCTION	127	\N	1940	1979	Carton	Complet transposé	\N	\N	2020-10-02 13:40:26.271921	2020-10-02 13:40:26.271921	189	586
542	Un peu disco	\N	Jazz	5	ArmCond	Jazz - 5	t	Ensign Music Corporation	MANHATTAN SKYLINE (from Saturday night fever)	\N	\N	1979	1977	Carton	Complet transposé	Musique de film	\N	2020-10-02 13:40:26.274944	2020-10-02 13:40:26.274944	529	382
543	Medley connu de Duke Ellington (Don’t get around much anymore, Mood Indigo, Caravan, Sophisticated lady)	\N	Jazz	6	ArmCond	Jazz - 6	t	Mills Music	SOPHISTICATED LADIES	\N	\N	1931 à 1937	1982	Vieux papier	Complet transposé	\N	\N	2020-10-02 13:40:26.277799	2020-10-02 13:40:26.277799	174	171
544	\N	\N	Jazz	7	\N	Jazz - 7	t	Obrasso-Verlag AG (CH 4537)	MILLER MAGIC	558	\N	\N	2001	Papier neuf	Complet transposé	Jazz	\N	2020-10-02 13:40:26.280761	2020-10-02 13:40:26.280761	377	610
545	\N	\N	Jazz	7	\N	Jazz - 7	t	De HASKE	TAKE THE A-TRAIN	280	Normal	\N	2002	Papier neuf	Réduit en Ut	Jazz	\N	2020-10-02 13:40:26.283872	2020-10-02 13:40:26.283872	555	274
546	Medley de morceaux jazzy	\N	Jazz	7	\N	Jazz - 7	t	CHAPELL / WARNER	BLOOD SWEAT …, AT THEIR BEST	647	Difficile	\N	1998	Papier neuf	Complet transposé	Jazz	\N	2020-10-02 13:40:26.286914	2020-10-02 13:40:26.286914	224	298
547	\N	\N	Marche avec clique	1	ArmCond	Marche avec clique - 1	t	Evette et Schaeffer (12161)	SALUT AU 85EME	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.289749	2020-10-02 13:40:26.289749	445	\N
548	\N	\N	Marche avec clique	1	\N	Marche avec clique - 1	t	/	LE TEMERAIRE	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.292426	2020-10-02 13:40:26.292426	399	\N
549	\N	\N	Marche avec clique	2	\N	Marche avec clique - 2	t	Robert Martin (R 864 M)	LA MARCHE DE BABETTE	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.295457	2020-10-02 13:40:26.295457	38	354
550	\N	\N	Marche avec clique	3	ArmCond	Marche avec clique - 3	t	Alphonse LEDUC (A.L.18200)	LE GRENADIER DU CAUCASE	\N	\N	\N	1933	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.298397	2020-10-02 13:40:26.298397	367	\N
551	Morceau composé par Jean Ollier pour les harmonie de mineur	\N	Marche avec clique	4	\N	Marche avec clique - 4	t	Harmonie de la Chazotte	ROCHARIC	170	Normal	1958	1965	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.301392	2020-10-02 13:40:26.301392	427	\N
552	\N	\N	Marche avec clique	5	\N	Marche avec clique - 5	t	Bethune (A 132 B) (Robert Martin)	SANS PEUR	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.304631	2020-10-02 13:40:26.304631	598	\N
553	\N	\N	Marche avec clique	6	ArmCond	Marche avec clique - 6	t	Millereau	LES ALLOBROGES	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.308086	2020-10-02 13:40:26.308086	\N	462
554	Une réédition Harmonie de la Chazotte faite par Florent Plassard	\N	Marche avec clique	7	+ ArmCond	Marche avec clique - 7	t	E. GAUDET	QUAND MADELON...	\N	\N	\N	1916	Carton	Complet transposé	\N	\N	2020-10-02 13:40:26.312281	2020-10-02 13:40:26.312281	491	7
555	Morceau composé par Jean Ollier pour le centenaire de l’Harmonie, conducteur manuscrit	Conducteur daté du 15 février 1965	Marche avec clique	8	+ ArmCond	Marche avec clique - 8	t	Harmonie de la Chazotte	CHAZOTTE CENTENAIRE (ac clique)	\N	\N	\N	1965	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.315475	2020-10-02 13:40:26.315475	427	\N
556	Tampon Harmonie de l’Ondaine	\N	Fantaisie 30 et Marche avec clique	9	+ ArmCond	Fantaisie 30 et Marche avec clique - 9	t	Robert Martin	RONCEVAUX	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.318222	2020-10-02 13:40:26.318222	325	\N
557	Des copies papiers ont été faites	\N	Marche avec clique	10	+ ArmCond	Marche avec clique - 10	t	Robert Martin	MARCHE DES TIRAILLEURS	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.321029	2020-10-02 13:40:26.321029	369	\N
558	Des copies papiers ont été faites	\N	Marche avec clique	11	ArmCond	Marche avec clique - 11	t	Evette et Schaeffer (20 054)	MARCHE DE LA GARDE CONSULAIRE A MARENGO	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.32384	2020-10-02 13:40:26.32384	\N	206
559	\N	\N	Marche avec clique	11	ArmCond	Marche avec clique - 11	t	Andrieu Freres	MARCHE DE LA LEGION ETRANGERE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.32833	2020-10-02 13:40:26.32833	656	\N
561	\N	\N	Marche avec clique	13	ArmCond	Marche avec clique - 13	t	Alphonse LEDUC (AL 21 461)	LES DRAGONS DE NOAILLES	\N	\N	\N	1954	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.335911	2020-10-02 13:40:26.335911	\N	524
562	\N	\N	Marche avec clique	14	\N	Marche avec clique - 14	t	Buffet Crampon (12043°	LA FILLE DU REGIMENT (Ar. Roux)	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.339019	2020-10-02 13:40:26.339019	164	503
563	\N	\N	Marche avec clique	15	ArmCond	Marche avec clique - 15	t	/	LA VICTOIRE OU LA MORT	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.342013	2020-10-02 13:40:26.342013	\N	120
564	\N	\N	Marche avec clique	16	\N	Marche avec clique - 16	t	Bornemann (OB5002)	MARCHE DU 1ER ZOUAVE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.344929	2020-10-02 13:40:26.344929	350	\N
565	\N	\N	Marche avec clique	17	+ ArmCond	Marche avec clique - 17	t	Margueritat (F8206) (JOUBERT)	LE REGIMENT DE SAMBRE ET MEUSE	\N	\N	\N	1897	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.348024	2020-10-02 13:40:26.348024	479	\N
566	\N	\N	Marche avec clique	18	ArmCond	Marche avec clique - 18	t	Alphonse LEDUC (A.L.20400)	PAS REDOUBLE SUR LA CELEBRE MARCHE LORRAINE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.353198	2020-10-02 13:40:26.353198	208	658
567	Des copies papiers ont été faites	\N	Marche avec clique	18	+ ArmCond	Marche avec clique - 18	t	Robert Martin (M 1312 D) ( Maurice Dedruck)	MARCHE DE LA 2EME DB	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.357255	2020-10-02 13:40:26.357255	126	\N
568	\N	\N	Marche avec clique	19	\N	Marche avec clique - 19	t	Louis Jacquot & Fils	ALSACE ET LORRAINE	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.360527	2020-10-02 13:40:26.360527	565	349
569	Des copies papiers ont été faites	\N	 Grenier 26 et Marche avec clique	19	+ ArmCond	 Grenier 26 et Marche avec clique - 19	t	E. GAUDET	LE CHANT DU DEPART (Allier)	\N	\N	1794	1915	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.363472	2020-10-02 13:40:26.363472	365	7
570	Des copies papiers ont été faites	\N	Marche avec clique	20	\N	Marche avec clique - 20	t	Millereau	HYMNE A L’INFANTERIE DE MARINE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.366339	2020-10-02 13:40:26.366339	102	\N
571	Des copies papiers ont été faites	\N	Marche avec clique	21	\N	Marche avec clique - 21	t	Andrieu Freres	LOUIS XIV	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.369164	2020-10-02 13:40:26.369164	379	\N
572	\N	\N	Marche avec clique	22	+ ArmCond	Marche avec clique - 22	t	Robert Martin (L. B. 1073)	LA FILLE DU REGIMENT (Ar. Allier)	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.372238	2020-10-02 13:40:26.372238	164	7
573	\N	\N	Marche avec clique	22	ArmCond	Marche avec clique - 22	t	Andrieux Freres	MARCHE DES MOUSSES	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.375123	2020-10-02 13:40:26.375123	180	\N
574	Régiment d'infanterie basé à St Etienne	\N	Marche avec clique	24	\N	Marche avec clique - 24	t	/	MARCHE DU 38EME REGIMENT D’INFANTERIE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.37792	2020-10-02 13:40:26.37792	466	\N
575	Des copies papiers ont été faites	\N	Marche avec clique	25	ArmCond	Marche avec clique - 25	t	Robert Martin (R 1475 M)	LE CHANT DU DEPART (Delbecq)	\N	\N	1794	1975	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:26.381065	2020-10-02 13:40:26.381065	365	152
576	Des copies papiers ont été faites	\N	Marche avec clique	25	+ ArmCond	Marche avec clique - 25	t	Robert Martin (R 1101 M)	LE DRAPEAU DE L’EUROPE (Ode à la joie)	\N	\N	1824	1968	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:26.384219	2020-10-02 13:40:26.384219	41	152
577	\N	\N	Marche avec clique	26	\N	Marche avec clique - 26	t	Moncelle	ROUGE ET BLEU	\N	\N	\N	1976	Carton	Absent	\N	\N	2020-10-02 13:40:26.387165	2020-10-02 13:40:26.387165	386	\N
578	Des copies papiers ont été faites	\N	Marche avec clique	26	\N	Marche avec clique - 26	t	Moncelle	LA FRATERNELLE	\N	\N	\N	1977	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.392528	2020-10-02 13:40:26.392528	659	\N
579	Des copies papiers ont été faites	\N	Marche avec clique	27	+ ArmCond	Marche avec clique - 27	t	Andrieu Freres	MARCHE DES ENFANTS DE TROUPE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.396371	2020-10-02 13:40:26.396371	575	\N
580	\N	\N	Marche avec clique	27	\N	Marche avec clique - 27	t	Edition Muscal Gessienne (Alphonse Leduc AL 18205)	PARIS - BELFORT	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.408485	2020-10-02 13:40:26.408485	180	\N
581	Conducteur manuscrit	\N	Marche avec clique	28	ArmCond	Marche avec clique - 28	t	Harmonie de la Chazotte	VAILLANT MINEUR DE LA LOIRE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.411754	2020-10-02 13:40:26.411754	427	\N
582	\N	\N	Marche avec clique	29	\N	Marche avec clique - 29	t	Robert Martin (R 2259 M)	MARCHE DES SOLDATS DE ROBERT BRUCE	\N	\N	\N	1990	Papier neuf	Réduit en Sib	\N	\N	2020-10-02 13:40:26.415361	2020-10-02 13:40:26.415361	88	\N
583	\N	\N	Marche avec clique	29	ArmCond	Marche avec clique - 29	t	Moncelle	CONCORDIA	\N	\N	\N	1980	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:26.418548	2020-10-02 13:40:26.418548	386	\N
584	\N	\N	Marche avec clique	29	\N	Marche avec clique - 29	t	Robert Martin	LE TRAM	\N	\N	\N	?	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:26.421396	2020-10-02 13:40:26.421396	399	\N
585	\N	\N	Marche avec clique	29	ArmCond	Marche avec clique - 29	t	Harmonie de la Chazotte	LES AFRICAINS	\N	\N	\N	2011	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:26.424854	2020-10-02 13:40:26.424854	83	456
586	\N	\N	Marche avec clique	29	\N	Marche avec clique - 29	t	Margueritat	CLEMENCEAU	\N	\N	\N	?	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:26.427991	2020-10-02 13:40:26.427991	208	129
587	\N	\N	Marche avec clique	29	ArmCond	Marche avec clique - 29	t	Andel Uitgave	THE WAY IN	\N	\N	\N	?	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:26.430838	2020-10-02 13:40:26.430838	103	\N
588	\N	\N	Marche avec clique	29	\N	Marche avec clique - 29	t	Moncelle	ROSE DES VENTS	\N	\N	\N	1973	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:26.434211	2020-10-02 13:40:26.434211	386	\N
589	Des copies papiers ont été faites	\N	Marche avec clique	30	\N	Marche avec clique - 30	t	E. GAUDET	SAINT CYR	\N	\N	\N	1913	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.437946	2020-10-02 13:40:26.437946	4	408
590	\N	\N	Marche avec clique	30	ArmCond	Marche avec clique - 30	t	Andrieu Freres	MARCHE DES APPRENTIS MARINS	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.441547	2020-10-02 13:40:26.441547	180	\N
591	\N	\N	Marche Sans clique	1	ArmCond	Marche Sans clique - 1	t	Robert Martin (R 923 M)	LA MARCHE DES ANGES	\N	\N	\N	1961	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.444713	2020-10-02 13:40:26.444713	210	354
592	\N	\N	Marche Sans clique	1	ArmCond	Marche Sans clique - 1	t	Robert Martin (R 978 M)	UN P’TIT CHAPEAU TYROLIEN	\N	\N	\N	1962	Carton	Réduit en Sib	Musique de film	\N	2020-10-02 13:40:26.447841	2020-10-02 13:40:26.447841	413	152
593	\N	\N	Marche Sans clique	2	+ ArmCond	Marche Sans clique - 2	t	Foetish Frères F5848F (Robert Martin)	VIEUX CAMARADES	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.450931	2020-10-02 13:40:26.450931	567	313
594	\N	\N	Marche Sans clique	3	ArmCond	Marche Sans clique - 3	t	Le Pavillon (S18024)	CORSO	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.453676	2020-10-02 13:40:26.453676	190	\N
595	\N	\N	Marche Sans clique	3	\N	Marche Sans clique - 3	t	Le Pavillon	FLEURON	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.456266	2020-10-02 13:40:26.456266	190	\N
596	Même partition que l’arrangement du Cmdt Pierre Dupont de 1938, et que l’édition Margueritat 2436	\N	Marche Sans clique	4	ArmCond	Marche Sans clique - 4	t	Andrieu Freres	LA MARSEILLAISE	56	\N	\N	1936	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.459087	2020-10-02 13:40:26.459087	501	\N
597	Partition reçue par Georges Jaboulay en novembre 1982	1982	Marche Sans clique	5	ArmCond	Marche Sans clique - 5	t	Robert Martin (Harmonia)	ANCHORS AWEIGH	\N	\N	\N	1970	Carton	Réduit en Sib	Variété	3 Variété internationale	2020-10-02 13:40:26.46216	2020-10-02 13:40:26.46216	614	28
598	Avec des copies papiers	\N	Marche Sans clique	6	ArmCond	Marche Sans clique - 6	t	Robert Martin (R 1161 M)	DANS LES RUES D’ANTIBES	\N	\N	\N	1958	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.465289	2020-10-02 13:40:26.465289	39	354
599	Avec des copies papiers	\N	Marche Sans clique	6	ArmCond	Marche Sans clique - 6	t	Alphonse LEDUC (A.L.18204)	SPEARMINT	\N	\N	\N	1933	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.468099	2020-10-02 13:40:26.468099	578	\N
600	Tampon Harmonie des Mineurs Roche la Molière	\N	Marche Sans clique	7	ArmCond	Marche Sans clique - 7	t	Molenaar 05,0034,02	SEMPER FIDELIS	\N	\N	\N	1947	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.471037	2020-10-02 13:40:26.471037	540	385
601	\N	\N	Marche Sans clique	7	\N	Marche Sans clique - 7	t	Jos. Graff	JOYEUX MINEUR	\N	\N	\N	1964	Carton	Absent	\N	\N	2020-10-02 13:40:26.474205	2020-10-02 13:40:26.474205	234	\N
602	\N	\N	Marche Sans clique	8	\N	Marche Sans clique - 8	t	Robert Martin (R 1001 M)	SYLVIA MARCHE	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.477794	2020-10-02 13:40:26.477794	42	\N
603	\N	\N	Marche Sans clique	8	ArmCond	Marche Sans clique - 8	t	Robert Martin (R 958 M)	BACCHUS	\N	\N	\N	1963	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.481597	2020-10-02 13:40:26.481597	336	\N
604	\N	\N	Marche Sans clique	9	ArmCond	Marche Sans clique - 9	t	Robert Martin (R 1224 M) (Bleu Blanc Rouge)	GOD BLESS RUGBY (Le mur de l’Atlantique)	\N	\N	\N	1970	Carton	Réduit en Sib	Musique de film	1 Aventure	2020-10-02 13:40:26.485437	2020-10-02 13:40:26.485437	62	152
605	\N	\N	Marche Sans clique	9	ArmCond	Marche Sans clique - 9	t	Robert Martin (R 1225 M) (Bleu Blanc Rouge)	KAMERAD, WENN DIE ROSEN BLUHN (Le mur de l’Atlantique)	\N	\N	\N	1970	Carton	Absent	Musique de film	1 Aventure	2020-10-02 13:40:26.488562	2020-10-02 13:40:26.488562	62	354
606	Avec des copies papiers	\N	Marche sans clique	10	\N	Marche sans clique - 10	t	Boosey & Hawkes	MARCHING THRO’ GEORGIA	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.491454	2020-10-02 13:40:26.491454	377	\N
607	\N	\N	Marche sans clique	11	ArmCond	Marche sans clique - 11	t	Molenaar	WIEN BLEIBT WIEN (Vienne reste Vienne)	\N	\N	\N	1959	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.494572	2020-10-02 13:40:26.494572	516	384
608	Marche composée en référence aux deux aigles des armoiries d'autriche Hongrie, reprise pour le cinema dans le dessins animé Les chansons de la Mère l'oie en 1931.	\N	Marche sans clique	12	\N	Marche sans clique - 12	t	Salabert	SOUS L’AIGLE DOUBLE (Op 159)	180	\N	1902	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.499469	2020-10-02 13:40:26.499469	591	660
609	\N	\N	Marche sans clique	12	\N	Marche sans clique - 12	t	Meridian (N. M. 1631)	SI TOUS LES GARS DU MONDE	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.502557	2020-10-02 13:40:26.502557	580	149
610	Avec des copies papiers	\N	Marche sans clique	13	\N	Marche sans clique - 13	t	Robert Martin (R 939 M) (Spanka Music Corp)	LE JOUR LE PLUS LONG	\N	\N	\N	1962	Carton	Réduit en Sib	Musique de film	1 Aventure	2020-10-02 13:40:26.505503	2020-10-02 13:40:26.505503	11	354
611	\N	\N	Marche sans clique	13	ArmCond	Marche sans clique - 13	t	Robert Martin (R 940 M)	AH ! SI J’ETAIS RESTE CELIBATAIRE	\N	\N	\N	1960	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.508915	2020-10-02 13:40:26.508915	341	152
612	Avec des copies papiers	\N	Marche sans clique	14	ArmCond	Marche sans clique - 14	t	Robert Martin (R 1294 M)	CINCINNATI	\N	\N	\N	1972	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.511822	2020-10-02 13:40:26.511822	138	\N
613	Avec des copies papiers	\N	Marche sans clique	15	ArmCond	Marche sans clique - 15	t	Alphonse LEDUC (A.L.18207)	LES CADETS DE BRABANT	\N	\N	\N	?	Carton	Réduit en Ut	\N	\N	2020-10-02 13:40:26.514615	2020-10-02 13:40:26.514615	578	\N
614	\N	\N	Marche sans clique	16	ArmCond	Marche sans clique - 16	t	Robert Martin (R 1008 M)	ENFANTS DE TOUS PAYS	\N	\N	\N	?	Carton	Réduit en Sib	Variété	3 Variété française	2020-10-02 13:40:26.518261	2020-10-02 13:40:26.518261	344	354
615	\N	\N	Marche sans clique	17	\N	Marche sans clique - 17	t	Margueritat (Robert Martin)	EN LIESSE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.521378	2020-10-02 13:40:26.521378	578	\N
616	\N	\N	Marche Sans clique	18	ArmCond	Marche Sans clique - 18	t	Billaudot (LB 574)	LE JOYEUX FORGERON	134	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.525084	2020-10-02 13:40:26.525084	443	75
617	Avec des copies papiers	\N	Marche Sans clique	19	ArmCond	Marche Sans clique - 19	t	Molenaar	BROADWAY	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.528121	2020-10-02 13:40:26.528121	318	\N
618	Avec des copies papiers	\N	Marche Sans clique	20	\N	Marche Sans clique - 20	t	Robert Martin (R 1105 M)	LE CHANT DES PARTISANS	\N	\N	\N	1966	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.532854	2020-10-02 13:40:26.532854	353	388
619	\N	\N	Marche Sans clique	21	\N	Marche Sans clique - 21	t	Margueritat	LE PÈRE LA VICTOIRE	\N	\N	\N	?	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:26.53664	2020-10-02 13:40:26.53664	208	\N
620	\N	\N	Marche Sans clique	21	ArmCond	Marche Sans clique - 21	t	Margueritat	RONDE DES PETITS PIERROTS	\N	\N	\N	?	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:26.54001	2020-10-02 13:40:26.54001	71	\N
621	\N	\N	Marche Sans clique	21	\N	Marche Sans clique - 21	t	\N	LA MARCHE DU CENTENAIRE	146	\N	\N	?	Papier neuf	\N	\N	\N	2020-10-02 13:40:26.543476	2020-10-02 13:40:26.543476	244	\N
622	\N	\N	Opera	1	\N	Opera - 1	t	Andrieu Freres	LE PAYS DU SOURIRE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.546518	2020-10-02 13:40:26.546518	321	10
623	\N	\N	Opera	1	ArmCond	Opera - 1	t	Andrieu Freres	ROYAL CHASSEUR	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.549253	2020-10-02 13:40:26.549253	185	\N
624	Des copies papiers ont été faites, le conducteur original est réduit en Sib dans l'armoire Conducteur	\N	Opera	2	ArmCond	Opera - 2	t	Evette et Schaeffer (1162)	AIDA	\N	\N	1871	?	Carton	Complet transposé	\N	\N	2020-10-02 13:40:26.552361	2020-10-02 13:40:26.552361	583	438
625	\N	\N	Opera	3	\N	Opera - 3	t	Bethune	LES CLOCHES DE CORNEVILLE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.555428	2020-10-02 13:40:26.555428	454	503
626	\N	\N	Opera	4	\N	Opera - 4	t	Andrieu Freres	MIREILLE (opéra Gounod)	\N	\N	1864	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.558343	2020-10-02 13:40:26.558343	233	65
627	\N	\N	Opera	5	\N	Opera - 5	t	Billaudot (E 351 B)	LE CHANTEUR DE MEXICO	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.562002	2020-10-02 13:40:26.562002	335	68
628	\N	\N	Opera	6	ArmCond	Opera - 6	t	Andrieu Freres	HANS, LE JOUEUR DE FLUTE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.565906	2020-10-02 13:40:26.565906	208	10
629	\N	\N	Opera	7	\N	Opera - 7	t	Andrieu Freres	LES CONTES D'HOFFMANN	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.569799	2020-10-02 13:40:26.569799	426	10
630	Opera bouffe parisien	\N	Opera	8	\N	Opera - 8	t	Evette et Schaeffer (12569)	VERONIQUE	\N	\N	1898	?	Carton	Réduit en Sib	Classique	Opéra	2020-10-02 13:40:26.572941	2020-10-02 13:40:26.572941	373	134
631	\N	\N	Opera	9	\N	Opera - 9	t	Buffet-Crampon, Evette et Schaeffer (1341)	LEICHTE CAVALERIE	\N	\N	\N	1937 ?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.576089	2020-10-02 13:40:26.576089	556	119
632	avec Hans le jouerur de Flute, les deux morceaux les plus connu de Louis Ganne	\N	Opera	10	\N	Opera - 10	t	Andrieu Freres	LES SALTIMBANQUES	\N	\N	\N	1932	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.579114	2020-10-02 13:40:26.579114	208	185
633	tamponné harmonie d’Izieux (Loire)	\N	Opera	11	\N	Opera - 11	t	Robert Martin (R 675 M) Joubert	VIOLETTES IMPERIALES	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.582105	2020-10-02 13:40:26.582105	522	152
634	\N	\N	Opera	12	ArmCond	Opera - 12	t	J. M. Champel	« CARMEN »	\N	\N	1875	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.584955	2020-10-02 13:40:26.584955	51	113
635	Tampon Harmonie des Houilleres de la Loire	\N	Opera	13	ArmCond	Opera - 13	t	Andrieu Freres	LA POUPEE DE NUREMBERG	\N	\N	1840 ?	1950	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.587857	2020-10-02 13:40:26.587857	1	499
636	Il 'y a que le conducteur du prélude, premier mouvement de la première suite de l'Arlesienne.	\N	Opera	14	ArmCond	Opera - 14	t	Millereau	L'ARLESIENNE N° 1 -2-3	\N	\N	1872	?	Carton	Complet transposé	\N	\N	2020-10-02 13:40:26.590845	2020-10-02 13:40:26.590845	51	434
637	\N	\N	Opera	15	\N	Opera - 15	t	Millereau	L'ARLESIENNE N° 4	259	\N	1872	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.594203	2020-10-02 13:40:26.594203	51	434
638	L'arlesienne se décompose en 2 suite. La première (Prélude, Minuetto, Adagietto, carillon) et la deuxième (Pastorale, Intermezzo, Farandole)	\N	Opera	16	ArmCond	Opera - 16	t	Andrieu Freres	L'ARLESIENNE 1ere SUITE	\N	\N	1872	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.597558	2020-10-02 13:40:26.597558	51	120
639	Acheté le 29 septembre 1967Tampon Harmonie des Houilleres de la Loire	\N	Opera	16	ArmCond	Opera - 16	t	Andrieu Freres	L'ARLESIENNE 2nd SUITE (Bassin)	\N	\N	1872	1967	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.600729	2020-10-02 13:40:26.600729	51	120
640	opera comique	\N	Opera	17	\N	Opera - 17	t	Andrieu Freres (G. A. 638)	LA FILLE DE MADAME ANGOT	\N	\N	1872	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.603966	2020-10-02 13:40:26.603966	317	93
641	\N	\N	Opera	18	\N	Opera - 18	t	Edwin H. Morris	HELLO DOLLY (Highlights from a broadway musical)	\N	\N	\N	1963	Vieux papier	Absent	\N	\N	2020-10-02 13:40:26.607819	2020-10-02 13:40:26.607819	256	98
642	Tampon Union musicale de St Chamond	\N	Opera	18	ArmCond	Opera - 18	t	CHAPELL CSA5351	MY FAIR LADY	\N	\N	\N	1956	Vieux papier	Réduit en Sib	\N	\N	2020-10-02 13:40:26.611601	2020-10-02 13:40:26.611601	332	\N
643	\N	\N	Opera	19	\N	Opera - 19	t	Robert Martin (R 3914 M)	GALOP INFERNAL	160	Normal	\N	2003	Papier neuf	Complet transposé	Classique	Opéra	2020-10-02 13:40:26.615415	2020-10-02 13:40:26.615415	426	383
644	\N	\N	Ouverture	1	\N	Ouverture - 1	t	Monvoisin et Cie (Orph 4619)	LE NOUVEAU SEIGNEUR DU VILLAGE	345	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.618405	2020-10-02 13:40:26.618405	58	291
645	\N	\N	Ouverture	2	ArmCond	Ouverture - 2	t	Andrieu Freres FA1707	LE BARBIER DE SEVILLE	\N	\N	1816	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.621201	2020-10-02 13:40:26.621201	497	10
646	Ballet Fantaisie	\N	Ouverture	2	\N	Ouverture - 2	t	Andrieu Freres	L’ILE DES FEES	\N	\N	1900 ?	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.623929	2020-10-02 13:40:26.623929	460	\N
647	Des copies papiers ont été faites	\N	Ouverture	3	\N	Ouverture - 3	t	Andrieu, collection Pares	LES NOCES DE FIGARO (ouverture)	295	\N	1786	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.62685	2020-10-02 13:40:26.62685	402	329
648	Nabuchodonosor étant le titre initial de l'opéra Nabusso. Fantaisie	\N	Ouverture	3	\N	Ouverture - 3	t	Margueritat	NABUCHODONOSOR	\N	\N	1842	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.629586	2020-10-02 13:40:26.629586	583	60
649	\N	\N	Ouverture	4	\N	Ouverture - 4	t	Billaudot	TITUS (1791)	\N	\N	1791	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.632458	2020-10-02 13:40:26.632458	402	333
650	\N	\N	Ouverture	5	\N	Ouverture - 5	t	evette et Schaeffer (11734)	OUVERTURE DE LEONORE	\N	\N	1806	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.635405	2020-10-02 13:40:26.635405	41	119
651	cf grenier et opéra	\N	Ouverture	6	\N	Ouverture - 6	t	Evette et Schaeffer (1052)	MIREILLE (ouverture de Gounod)	\N	\N	1864	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.638484	2020-10-02 13:40:26.638484	233	199
652	Des copies papiers ont été faites, édition Robert Martin, Billaudot et Andrieu réunis	\N	Ouverture	7	ArmCond	Ouverture - 7	t	Andrieu Freres	EGMONT	530	\N	1810	1952	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.641481	2020-10-02 13:40:26.641481	41	185
653	Entr’Acte symphonique	\N	Ouverture	7	\N	Ouverture - 7	t	Evette et Schaeffer (12716)	MESSIDOR	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.644396	2020-10-02 13:40:26.644396	91	221
654	Des copies papiers ont été faites	\N	Ouverture	8	ArmCond	Ouverture - 8	t	Andrieu Freres (J M Champel)	LE CALIFE DE BAGDAD	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.647482	2020-10-02 13:40:26.647482	58	499
655	\N	\N	Ouverture	9	\N	Ouverture - 9	t	Evette et Schaeffer (13248)	OUVERTURE DE TANCREDE	\N	\N	1813	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.651019	2020-10-02 13:40:26.651019	497	361
656	\N	\N	Ouverture	10	\N	Ouverture - 10	t	Robert Martin R 1729 M	OUVERTURE POUR INSTRUMENTS A VENT	\N	\N	1794	1981	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:26.654848	2020-10-02 13:40:26.654848	158	82
657	Avec 5 percussionnstes	\N	Ouverture	10	\N	Ouverture - 10	t	Carl Fisher	ROARING MOUNTAIN OVERTURE (Ouverture des montagnes)	216	Normal	\N	1987	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:26.6583	2020-10-02 13:40:26.6583	542	\N
658	tamponné union musicale de St Genest Lerpt	\N	Ouverture	10	ArmCond	Ouverture - 10	t	CHAPELL CSA7087	OUVERTURE TEXANE	360	\N	\N	1971	Papier neuf	Réduit en Ut	\N	\N	2020-10-02 13:40:26.661036	2020-10-02 13:40:26.661036	307	\N
659	\N	\N	Ouverture	10	\N	Ouverture - 10	t	HAFABRA Music – Louis Martinus	OUVERTURE DE « GERMANA »	176	\N	1993	1993	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:26.665279	2020-10-02 13:40:26.665279	661	\N
660	\N	\N	Ouverture	11	\N	Ouverture - 11	t	CURNOW Music Press	GRANADA OUVERTURE	310	Difficile	\N	2001	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:26.667761	2020-10-02 13:40:26.667761	437	\N
661	\N	\N	Ouverture	11	\N	Ouverture - 11	t	Music Works	CELTIC RITUAL (ouverture for band)	260	Normal	\N	1997	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:26.670497	2020-10-02 13:40:26.670497	259	\N
662	\N	\N	Ouverture	12	\N	Ouverture - 12	t	De HASKE	MARINARELLA (Ouverture)	745	Difficile	1914 ?	2005	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:26.673401	2020-10-02 13:40:26.673401	204	40
663	\N	\N	Ouverture	12	\N	Ouverture - 12	t	Molenaar	LA FORZA DEL DESTINO (La force du destin)	505	Normal	1862	2000	Papier neuf	Complet transposé	\N	\N	2020-10-02 13:40:26.676324	2020-10-02 13:40:26.676324	583	544
664	Jolis Cartons ! Tamponné Harmonie des houilleres de la Loire	\N	Religieux et Funèbre	1	\N	Religieux et Funèbre - 1	t	Evette et Schaeffer (13422)	PRELUDE (Rachmaninof)	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.679198	2020-10-02 13:40:26.679198	476	221
665	\N	\N	Religieux et Funèbre	2	ArmCond	Religieux et Funèbre - 2	t	Evette et Schaeffer (12163)	INTERMEZZO DE CAVALLERIA RUSTICANA	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.682201	2020-10-02 13:40:26.682201	356	144
666	Extrait de l'Or du Rhin, lui même Prologue de la Tétralogie de Wagner (Or du Rhin, La Walkyrie, Siegfried, Le Crépuscule des Dieux).	\N	Religieux et Funèbre	2	\N	Religieux et Funèbre - 2	t	Em. Mayer (Alsace Strasbourg)	L’ENTREE DES DIEUX AU WALHALL	\N	\N	1869	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.685201	2020-10-02 13:40:26.685201	590	360
667	\N	\N	Religieux et Funèbre	3	ArmCond	Religieux et Funèbre - 3	t	Billaudot	AVE VERUM	\N	\N	1791	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.689582	2020-10-02 13:40:26.689582	402	662
668	Partition du conducteur écrit à la main par Jean Ollier	\N	Religieux et Funèbre	3	\N	Religieux et Funèbre - 3	t	Harmonie de la Chazotte	ELEVATION	\N	\N	\N	?	Carton	Complet transposé	\N	\N	2020-10-02 13:40:26.692562	2020-10-02 13:40:26.692562	427	\N
669	Partition du conducteur écrit à la main par Jean Ollier	\N	Religieux et Funèbre	3	\N	Religieux et Funèbre - 3	t	Harmonie de la Chazotte	ARIA (lento)	\N	\N	1730	?	Carton	Complet transposé	\N	\N	2020-10-02 13:40:26.695877	2020-10-02 13:40:26.695877	25	427
670	Tampon Harmonie des Houilleres de la Loire 00020	\N	Religieux et Funèbre	4	\N	Religieux et Funèbre - 4	t	Alphonse LEDUC (A.L.11,311)	VEILLEE DE L'ANGE GARDIEN	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.700402	2020-10-02 13:40:26.700402	452	573
671	\N	\N	Religieux et Funèbre	5	ArmCond	Religieux et Funèbre - 5	t	Evette et Schaeffer (12146)	PRELUDE DU DELUGE	\N	\N	1875	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.703649	2020-10-02 13:40:26.703649	509	434
674	\N	\N	Religieux et Funèbre	8	\N	Religieux et Funèbre - 8	t	Harmonie de la Chazotte	SARABANDE	\N	\N	1739	?	Carton	Absent	Classique	Baroque	2020-10-02 13:40:26.712433	2020-10-02 13:40:26.712433	245	427
675	\N	\N	Religieux et Funèbre	8	\N	Religieux et Funèbre - 8	t	Harmonie de la Chazotte	RECUEILLEMENT 	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.715147	2020-10-02 13:40:26.715147	427	\N
676	cf grenier	\N	Religieux et Funèbre	9	\N	Religieux et Funèbre - 9	t	Margueritat	MARCHE FUNEBRE (arr. Mullot)	\N	\N	1837	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.717746	2020-10-02 13:40:26.717746	121	404
677	\N	\N	Religieux et Funèbre	9	\N	Religieux et Funèbre - 9	t	Margueritat	LA NEIGE (Marche Funebre)	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.720245	2020-10-02 13:40:26.720245	53	\N
678	Marche Funebre, avec tampon de l’Harmonie des mineurs de Roche la Molière	\N	Religieux et Funèbre	10	\N	Religieux et Funèbre - 10	t	Evette et Schaeffer (13301)	HOMMAGE AUX BRAVES DE L'ARMEE D'ITALIE	\N	\N	\N	?	Carton	Complet transposé	\N	\N	2020-10-02 13:40:26.722794	2020-10-02 13:40:26.722794	240	\N
679	Marche Funebre à la mémoire de sa fille Martha	\N	Religieux et Funèbre	11	ArmCond	Religieux et Funèbre - 11	t	L’accord parfait (Lyon, Bellecours)	PAUVRE FLEUR (Marche Funebre)	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.725225	2020-10-02 13:40:26.725225	23	\N
673	\N	\N	Religieux et Funèbre	6 et 7	\N	Religieux et Funèbre - 6 et 7	t	Alphonse LEDUC (A.L.19,250)	CHANTS RUSSES	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.709528	2020-12-29 06:58:58.076131	306	225
680	Marche Funebre a la mémoire des naufragés du Titanic ? Paroles de J. M. Dreuilhe	\N	Religieux et Funèbre	11	\N	Religieux et Funèbre - 11	t	Andrieu	DEVANT L'ETERNITE	\N	\N	\N	1912	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.727394	2020-10-02 13:40:26.727394	10	\N
681	\N	\N	Religieux et Funèbre	12	ArmCond	Religieux et Funèbre - 12	t	Andrieu Frères (FA1220)	CELEBRE LARGO (Cavatine de l’opera Xerxes)	\N	\N	1738	?	Carton	Réduit en Sib	Classique	Opéra	2020-10-02 13:40:26.730131	2020-10-02 13:40:26.730131	245	10
682	\N	\N	Religieux et Funèbre	12	\N	Religieux et Funèbre - 12	t	Evette et Schaeffer (12058)	LE DERNIER SOMMEIL DE LA VIERGE	\N	\N	1880	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.733332	2020-10-02 13:40:26.733332	358	434
683	\N	\N	Religieux et Funèbre	13	\N	Religieux et Funèbre - 13	t	Evette et Schaeffer (13472)	MANFRED	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.736509	2020-10-02 13:40:26.736509	518	375
684	Conducteur manuscrit	\N	Religieux et Funèbre	14	ArmCond	Religieux et Funèbre - 14	t	P. Goumas	CHACONE	\N	\N	\N	?	Carton	Complet transposé	\N	\N	2020-10-02 13:40:26.73959	2020-10-02 13:40:26.73959	168	119
685	\N	\N	Religieux et Funèbre	15	\N	Religieux et Funèbre - 15	t	Andrieu Frères	LE CALME	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.743049	2020-10-02 13:40:26.743049	517	120
686	Même conducteur que la 2eme symphonie de Beethoven arrangé par Ollier	1959-07-16	Religieux et Funèbre	16	ArmCond	Religieux et Funèbre - 16	t	Harmonie de la Chazotte	OFFRANDE	\N	\N	\N	?	Carton	Complet transposé	\N	\N	2020-10-02 13:40:26.745462	2020-10-02 13:40:26.745462	427	\N
687	Conducteur manuscrit de Jean Ollier	Partition de Elegie datée du 28 septembre 1935	Religieux et Funèbre	17	ArmCond	Religieux et Funèbre - 17	t	Harmonie de la Chazotte	PRIERE DE L'ENFANT & ELEGIE	\N	\N	\N	?	Carton	Complet transposé	\N	\N	2020-10-02 13:40:26.747905	2020-10-02 13:40:26.747905	427	\N
688	3eme mouvement d'une œuvre pour piano	\N	Religieux et Funèbre	18	\N	Religieux et Funèbre - 18	t	Andrieu Frères	CHANT SANS PAROLE	\N	\N	1867	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.750206	2020-10-02 13:40:26.750206	566	\N
689	\N	\N	Religieux et Funèbre	18	\N	Religieux et Funèbre - 18	t	Andrieu Frères	MINUETTO (op 49)	\N	\N	1796	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.752713	2020-10-02 13:40:26.752713	41	93
690	\N	\N	Religieux et Funèbre	19	\N	Religieux et Funèbre - 19	t	Evette et Schaeffer	LA PLAINTE DU CLOCHER	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.755059	2020-10-02 13:40:26.755059	30	\N
691	\N	\N	Religieux et Funèbre	20	ArmCond	Religieux et Funèbre - 20	t	Andrieu	QUATRE CHANTS RELIGIEUX	\N	\N	1735	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.757585	2020-10-02 13:40:26.757585	25	10
692	\N	\N	Religieux et Funèbre	20	\N	Religieux et Funèbre - 20	t	?	ELEVATION	\N	\N	\N	?	Carton	Absent	\N	\N	2020-10-02 13:40:26.759961	2020-10-02 13:40:26.759961	599	\N
693	\N	\N	Religieux et Funèbre	20	\N	Religieux et Funèbre - 20	t	Andrieu Frères	ENTR’ACTE DE LA COLOMBE opéra comique	\N	\N	1866	?	Carton	Absent	Classique	Opéra	2020-10-02 13:40:26.762757	2020-10-02 13:40:26.762757	233	120
694	\N	\N	Religieux et Funèbre	21	ArmCond	Religieux et Funèbre - 21	t	Andrieu Frères	SERENADE DE L’AMANT JALOUX	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.765493	2020-10-02 13:40:26.765493	235	10
695	solo trompette	\N	Religieux et Funèbre	22	\N	Religieux et Funèbre - 22	t	Robert Martin	O JESUS QUE MA JOIE DEMEURE	\N	\N	1723	?	Vieux papier	Absent	\N	\N	2020-10-02 13:40:26.768044	2020-10-02 13:40:26.768044	25	195
560	\N	\N	Marche avec clique	12 et 23	\N	Marche avec clique - 12 et 23	t	Robert Martin (E 449 B Feldman)	LA MARCHE DES GOSSES (L’auberge du 6e bonheur)	\N	\N	\N	1958	Carton	Réduit en Sib	Musique de film	\N	2020-10-02 13:40:26.332985	2020-12-29 06:58:58.011532	657	369
672	\N	\N	Religieux et Funèbre	21 et 6	ArmCond	Religieux et Funèbre - 21 et 6	t	Andrieu Frères	PAYSAGE	\N	\N	\N	?	Carton	Réduit en Sib	\N	\N	2020-10-02 13:40:26.706632	2020-12-29 06:58:58.074002	154	499
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: romain
--

COPY public.users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, confirmation_token, confirmed_at, confirmation_sent_at, unconfirmed_email, failed_attempts, unlock_token, locked_at, created_at, updated_at) FROM stdin;
\.


--
-- Name: concerts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: romain
--

SELECT pg_catalog.setval('public.concerts_id_seq', 351, true);


--
-- Name: people_id_seq; Type: SEQUENCE SET; Schema: public; Owner: romain
--

SELECT pg_catalog.setval('public.people_id_seq', 664, true);


--
-- Name: scores_id_seq; Type: SEQUENCE SET; Schema: public; Owner: romain
--

SELECT pg_catalog.setval('public.scores_id_seq', 695, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: romain
--

SELECT pg_catalog.setval('public.users_id_seq', 1, false);


--
-- PostgreSQL database dump complete
--

