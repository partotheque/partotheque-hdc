# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2024_11_05_065720) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_trgm"
  enable_extension "plpgsql"

  create_table "concerts", force: :cascade do |t|
    t.string "organizer"
    t.string "place"
    t.string "participant"
    t.string "date"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "concerts_people", id: false, force: :cascade do |t|
    t.bigint "person_id", null: false
    t.bigint "concert_id", null: false
    t.index ["concert_id"], name: "index_concerts_people_on_concert_id"
    t.index ["person_id"], name: "index_concerts_people_on_person_id"
  end

  create_table "concerts_scores", id: false, force: :cascade do |t|
    t.bigint "score_id", null: false
    t.bigint "concert_id", null: false
    t.index ["concert_id", "score_id"], name: "index_concerts_scores_on_concert_id_and_score_id", unique: true
    t.index ["score_id", "concert_id"], name: "index_concerts_scores_on_score_id_and_concert_id", unique: true
  end

  create_table "people", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "birth_country"
    t.string "birth_date"
    t.string "death_date"
    t.text "biography"
    t.string "nicknames"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "pg_search_documents", force: :cascade do |t|
    t.text "content"
    t.string "searchable_type"
    t.bigint "searchable_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable_type_and_searchable_id"
  end

  create_table "scores", force: :cascade do |t|
    t.text "comment"
    t.string "acquisition_date"
    t.string "classification"
    t.string "number"
    t.string "conductor_ref"
    t.string "archive_ref"
    t.boolean "audio_support"
    t.string "editor"
    t.string "title"
    t.integer "duration"
    t.string "difficulty"
    t.string "writing_date"
    t.string "editing_date"
    t.string "score_type"
    t.string "conductor_type"
    t.string "style"
    t.string "substyle"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "composed_by"
    t.integer "arranged_by"
    t.index ["arranged_by"], name: "index_scores_on_arranged_by"
    t.index ["composed_by"], name: "index_scores_on_composed_by"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "scores", "people", column: "arranged_by"
  add_foreign_key "scores", "people", column: "composed_by"
end
