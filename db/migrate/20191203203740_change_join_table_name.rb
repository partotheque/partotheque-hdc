# frozen_string_literal: true

class ChangeJoinTableName < ActiveRecord::Migration[6.0]
  def change
    rename_table :concerts_persons, :concerts_people
  end
end
