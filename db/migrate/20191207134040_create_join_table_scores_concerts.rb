# frozen_string_literal: true

class CreateJoinTableScoresConcerts < ActiveRecord::Migration[6.0]
  def change
    create_join_table :scores, :concerts do |t|
      t.index %i[score_id concert_id], unique: true
      t.index %i[concert_id score_id], unique: true
    end
  end
end
