# frozen_string_literal: true

class AddIdsToScore < ActiveRecord::Migration[6.0]
  def change
    add_column :scores, :composed_by, :integer
    add_column :scores, :arranged_by, :integer
  end
end
