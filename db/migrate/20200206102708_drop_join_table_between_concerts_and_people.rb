# frozen_string_literal: true

class DropJoinTableBetweenConcertsAndPeople < ActiveRecord::Migration[6.0]
  def change
    drop_join_table :concerts, :people
  end
end
