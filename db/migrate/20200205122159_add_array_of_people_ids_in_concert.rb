# frozen_string_literal: true

class AddArrayOfPeopleIdsInConcert < ActiveRecord::Migration[6.0]
  def change
    add_column :concerts, :conductors, :integer, array: true, default: []
  end
end
