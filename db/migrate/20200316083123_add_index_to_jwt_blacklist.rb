# frozen_string_literal: true

class AddIndexToJwtBlacklist < ActiveRecord::Migration[6.0]
  def change
    add_index :jwt_blacklist, :jti
  end
end
