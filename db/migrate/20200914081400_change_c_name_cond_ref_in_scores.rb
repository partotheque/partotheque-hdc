# frozen_string_literal: true

class ChangeCNameCondRefInScores < ActiveRecord::Migration[6.0]
  def change
    rename_column :scores, :conducter_ref, :conductor_ref
  end
end
