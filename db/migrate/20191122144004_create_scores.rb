# frozen_string_literal: true

class CreateScores < ActiveRecord::Migration[6.0]
  def change
    create_table :scores do |t|
      t.text :comment
      t.string :acquisition_date
      t.string :classification
      t.integer :number
      t.string :conducter_ref
      t.string :archive_ref
      t.boolean :audio_support
      t.string :editor
      t.string :title
      t.integer :duration
      t.string :difficulty
      t.string :writing_date
      t.string :editing_date
      t.string :partition_type
      t.string :conductor_type
      t.string :style
      t.string :substyle

      t.timestamps
    end
  end
end
