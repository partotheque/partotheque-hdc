# frozen_string_literal: true

class CreateJoinTablePersonsConcerts < ActiveRecord::Migration[6.0]
  def change
    create_join_table :persons, :concerts do |t|
      t.index %i[person_id concert_id], unique: true
      t.index %i[concert_id person_id], unique: true
    end
  end
end
