# frozen_string_literal: true

class CreatePeople < ActiveRecord::Migration[6.0]
  def change
    create_table :people do |t|
      t.string :first_name
      t.string :last_name
      t.string :birth_country
      t.string :birth_date
      t.string :death_date
      t.text :biography
      t.string :nicknames

      t.timestamps
    end
  end
end
