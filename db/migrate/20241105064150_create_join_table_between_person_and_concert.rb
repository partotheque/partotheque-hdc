class CreateJoinTableBetweenPersonAndConcert < ActiveRecord::Migration[6.0]
  def change
    create_join_table :people, :concerts do |t|
      t.index :person_id
      t.index :concert_id
    end
  end
end
