# frozen_string_literal: true

class CreateConcerts < ActiveRecord::Migration[6.0]
  def change
    create_table :concerts do |t|
      t.string :organizer
      t.string :place
      t.string :participant
      t.string :date
      t.string :name

      t.timestamps
    end
  end
end
