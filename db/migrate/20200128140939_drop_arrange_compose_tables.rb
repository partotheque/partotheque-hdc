# frozen_string_literal: true

class DropArrangeComposeTables < ActiveRecord::Migration[6.0]
  def change
    drop_table :composes
    drop_table :arranges
  end
end
