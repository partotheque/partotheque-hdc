# frozen_string_literal: true

class AddReferencesToConcert < ActiveRecord::Migration[6.0]
  def change
    add_reference :concerts, :people, null: true, foreign_key: true
    add_reference :concerts, :score, null: true, foreign_key: true
  end
end
