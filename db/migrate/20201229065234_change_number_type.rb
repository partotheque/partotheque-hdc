# frozen_string_literal: true

class ChangeNumberType < ActiveRecord::Migration[6.0]
  def up
    change_column :scores, :number, :string
  end

  def down
    change_column :scores, :number, :integer
  end
end
