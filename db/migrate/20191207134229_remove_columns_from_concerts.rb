# frozen_string_literal: true

class RemoveColumnsFromConcerts < ActiveRecord::Migration[6.0]
  def change
    remove_columns :concerts, :people_id, :score_id
  end
end
