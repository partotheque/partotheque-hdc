# frozen_string_literal: true

class ChangeNicknamesPeople < ActiveRecord::Migration[6.0]
  def change
    change_column :people, :nicknames, :string, using: 'nicknames::character varying[]'
  end
end
