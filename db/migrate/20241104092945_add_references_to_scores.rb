class AddReferencesToScores < ActiveRecord::Migration[6.0]
  def change
    add_foreign_key :scores, :people, column: :composed_by
    add_foreign_key :scores, :people, column: :arranged_by

    add_index :scores, :composed_by
    add_index :scores, :arranged_by
  end
end
