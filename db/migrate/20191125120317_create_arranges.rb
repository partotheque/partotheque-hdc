# frozen_string_literal: true

class CreateArranges < ActiveRecord::Migration[6.0]
  def change
    create_table :arranges do |t|
      t.references :person, null: false, foreign_key: true
      t.references :score, null: false, foreign_key: true

      t.timestamps
    end
  end
end
