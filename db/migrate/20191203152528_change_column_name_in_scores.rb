# frozen_string_literal: true

class ChangeColumnNameInScores < ActiveRecord::Migration[6.0]
  def change
    rename_column :scores, :partition_type, :score_type
  end
end
