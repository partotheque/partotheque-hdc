class RemoveConductorsFromConcert < ActiveRecord::Migration[6.0]
  def change
    remove_column :concerts, :conductors
  end
end
