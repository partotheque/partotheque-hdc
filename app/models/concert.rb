# frozen_string_literal: true

# Un concert <-> personnes (chefs d'orchestre)
class Concert < ApplicationRecord
  include PgSearch::Model
  multisearchable against: %i[organizer place participant date name]

  pg_search_scope :search, (lambda { |query, *args|
    return {
      against: args.flatten,
      query: query,
      using: {
        tsearch: {
          prefix: true
        },
        trigram: {
          threshold: 0.6
        }
      }
    }
  })

  has_and_belongs_to_many :scores
  has_and_belongs_to_many :conductors, class_name: 'Person'

  validates_presence_of :name
end
