# frozen_string_literal: true

# Un 'acteur'
class Person < ApplicationRecord
  include PgSearch::Model
  multisearchable against: %i[first_name last_name birth_country biography]

  pg_search_scope :search, (lambda { |query, *args|
    return {
      against: args.flatten,
      query: query,
      using: {
        tsearch: {
          prefix: true
        },
        trigram: {
          threshold: 0.6
        }
      }
    }
  })

  has_many :composed_scores, class_name: 'Score', foreign_key: 'composed_by'
  has_many :arranged_scores, class_name: 'Score', foreign_key: 'arranged_by'

  has_and_belongs_to_many :concerts

  validates_presence_of :last_name

  validates_length_of :biography,
                      in: 10..1000,
                      allow_blank: true,
                      too_short: "Merci d'ajouter un peu de matière ;)",
                      too_long: 'Faites plus court, merci'
end
