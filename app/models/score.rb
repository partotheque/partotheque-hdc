# frozen_string_literal: true

# Une partition <-> personnes (arrangeur et compositeur)
class Score < ApplicationRecord
  include PgSearch::Model
  multisearchable against: %i[
    comment acquisition_date classification conductor_ref
    archive_ref editor title difficulty score_type conductor_type
    style substyle
  ]

  pg_search_scope :search, (lambda { |query, *args|
    return {
      against: args.flatten,
      query: query,
      using: {
        tsearch: {
          prefix: true
        },
        trigram: {
          threshold: 0.6
        }
      }
    }
  })

  belongs_to :composer, class_name: 'Person', foreign_key: 'composed_by', optional: true
  belongs_to :arranger, class_name: 'Person', foreign_key: 'arranged_by', optional: true

  has_and_belongs_to_many :concerts

  validates_presence_of :title
  validates_presence_of :archive_ref
  validates_presence_of :number

  validates_numericality_of :duration,
                            only_integer: true,
                            allow_nil: true
end
