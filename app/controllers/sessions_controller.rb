# frozen_string_literal: true

# Authentication
class SessionsController < Devise::SessionsController
  def destroy
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    cookies.delete 'XSRF-TOKEN'

    set_flash_message! :notice, :signed_out if signed_out

    yield if block_given?
    respond_to_on_destroy
  end
end
