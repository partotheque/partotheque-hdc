# frozen_string_literal: true

# Les 'acteurs'
class PeopleController < ApplicationController
  before_action :verify_xsrf_token
  before_action :authenticate_user!
  before_action :set_person, only: %i[show update destroy]

  # GET /people
  def index
    @people = Person.all

    render json: @people
  end

  # GET /people/1
  def show
    render json: @person, show_details: true
  end

  # POST /people
  def create
    @person = Person.new(person_params)

    if @person.save
      render json: @person, status: :created, location: @person
    else
      render json: @person.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /people/1
  def update
    if @person.update(person_params)
      render json: @person
    else
      render json: @person.errors, status: :unprocessable_entity
    end
  end

  # DELETE /people/1
  def destroy
    @person.destroy
  end

  # POST /people/search
  def search
    render json: Person.search(params['search'].values.join(' '), params['search'].keys)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_person
    @person = Person.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def person_params
    params.require(:person).permit(:first_name, :last_name, :birth_country, :birth_date, :death_date, :biography,
                                   :nicknames)
  end
end
