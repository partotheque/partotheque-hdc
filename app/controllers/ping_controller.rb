# frozen_string_literal: true

# For pings
class PingController < ApplicationController
  def ping
    head :no_content
  end
end
