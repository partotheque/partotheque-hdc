# frozen_string_literal: true

class ApplicationController < ActionController::API
  include ActionController::MimeResponds
  include ActionController::RequestForgeryProtection
  include ActionController::Cookies

  protect_from_forgery with: :exception, if: -> { request.format.json? }

  before_action :set_xsrf_cookie, if: -> { cookies['XSRF-TOKEN'].blank? }

  private

  def verify_xsrf_token
    xsrf_token_header = request.headers.env['HTTP_X_XSRF_TOKEN']
    head :unauthorized unless cookies.signed['XSRF-TOKEN'] == xsrf_token_header
  end

  def set_xsrf_cookie
    xsrf_token = form_authenticity_token
    cookies.signed['XSRF-TOKEN'] = {
      value: xsrf_token,
      http_only: true,
      secure: Rails.env.production?
    }
    headers['X-XSRF-TOKEN'] = xsrf_token
  end
end
