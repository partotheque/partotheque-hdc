# frozen_string_literal: true

# Password creation
class PasswordsController < Devise::PasswordsController
  def create
    return head :bad_request unless params.dig('user', 'email')&.match?(Devise.email_regexp)

    self.resource = resource_class.send_reset_password_instructions(resource_params)
    yield resource if block_given?
  end
end
