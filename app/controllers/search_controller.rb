# frozen_string_literal: true

# PgSearch
class SearchController < ApplicationController
  before_action :verify_xsrf_token
  before_action :authenticate_user!

  def multi_search
    render json: PgSearch
      .multisearch(params[:search])
      .map(&:searchable)
      .group_by { |e| e.class.name.tableize }
  end
end
