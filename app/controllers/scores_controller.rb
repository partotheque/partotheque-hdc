# frozen_string_literal: true

# Les partitions
class ScoresController < ApplicationController
  before_action :verify_xsrf_token
  before_action :authenticate_user!
  before_action :set_score, only: %i[show update destroy]

  # GET /scores
  def index
    @scores = Score.includes(:composer, :arranger)

    render json: @scores
  end

  # GET /scores/1
  def show
    render json: @score, show_concerts: true
  end

  # POST /scores
  def create
    @score = Score.new(score_params)

    @score['composed_by'] = find_or_create_composer&.id
    @score['arranged_by'] = find_or_create_arranger&.id

    if @score.save
      render json: @score, status: :created, location: @score
    else
      render json: @score.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /scores/1
  def update
    updated_params = score_params
    updated_params['composed_by'] = find_or_create_composer&.id
    updated_params['arranged_by'] = find_or_create_arranger&.id

    if @score.update(updated_params)
      render json: @score
    else
      render json: @score.errors, status: :unprocessable_entity
    end
  end

  # DELETE /scores/1
  def destroy
    @score.destroy
  end

  # POST /scores/search
  def search
    render json: Score.search(params['search'].values.join(' '), params['search'].keys)
  end

  private

  def find_or_create_composer
    find_or_create_person(score_params['composed_by'])
  end

  def find_or_create_arranger
    find_or_create_person(score_params['arranged_by'])
  end

  def find_or_create_person(person_params)
    return unless person_params

    Person.find_by(id: person_params['id']) || create_person(person_params)
  end

  def create_person(person)
    Person.create!(last_name: person['last_name'], first_name: person['first_name'])
  end

  def set_score
    @score = Score.find(params[:id])
  end

  def score_params
    params.require(:score).permit(:comment, :acquisition_date, :classification,
                                  :number, :conductor_ref, :archive_ref,
                                  :audio_support, :editor, :title, :duration,
                                  :difficulty, :writing_date, :editing_date,
                                  :score_type, :conductor_type,
                                  :style, :substyle, { composed_by: %i[id first_name last_name] },
                                  arranged_by: %i[id first_name last_name])
  end
end
