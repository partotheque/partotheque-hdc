# frozen_string_literal: true

# Les concerts
class ConcertsController < ApplicationController
  before_action :verify_xsrf_token
  before_action :authenticate_user!
  before_action :set_concert, only: %i[show update destroy]

  # GET /concerts
  def index
    @concerts = Concert.includes(:conductors).all

    render json: @concerts
  end

  # GET /concerts/1
  def show
    render json: @concert, show_scores: true
  end

  # POST /concerts
  def create
    @concert = Concert.new(concert_params.except(:conductors))

    @concert.conductors << find_or_create_conductors

    if @concert.save
      render json: @concert, status: :created, location: @concert
    else
      render json: @concert.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /concerts/1
  def update
    updated_params = concert_params
    updated_params[:conductors] = find_or_create_conductors

    if @concert.update(updated_params)
      render json: @concert
    else
      render json: @concert.errors, status: :unprocessable_entity
    end
  end

  # DELETE /concerts/1
  def destroy
    @concert.destroy
  end

  # POST /concerts/search
  def search
    render json: Concert.search(params['search'].values.join(' '), params['search'].keys)
  end

  private

  def find_or_create_conductors
    concert_params['conductors']&.map do |c|
      Person.find_by(id: c['id']) || Person.create!(last_name: c['last_name'], first_name: c['first_name'])
    end || []
  end

  def set_concert
    @concert = Concert.find(params[:id])
  end

  def concert_params
    params.require(:concert).permit(:organizer, :place, :participant,
                                    :date, :name, conductors: %i[id first_name last_name])
  end
end
