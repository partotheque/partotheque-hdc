class PersonConcertSerializer < ActiveModel::Serializer
  attributes :id, :name, :date, :place
end
