class ConcertSerializer < ActiveModel::Serializer
  attributes :id,
             :date,
             :name,
             :place,
             :organizer,
             :participant

  has_many :conductors, serializer: ConcertPersonSerializer
  has_many :scores, serializer: ConcertScoreSerializer, if: -> { instance_options[:show_scores] }
end
