class ScoreConcertSerializer < ActiveModel::Serializer
  attributes :id, :organizer, :place, :participant, :date, :name
end
