class PersonSerializer < ActiveModel::Serializer
  attributes :id,
             :first_name,
             :last_name,
             :birth_country,
             :birth_date,
             :death_date,
             :nicknames

  has_many :concerts, serializer: PersonConcertSerializer, if: -> { instance_options[:show_details] }
  has_many :composed_scores, serializer: PersonScoreSerializer, if: -> { instance_options[:show_details] }
  has_many :arranged_scores, serializer: PersonScoreSerializer, if: -> { instance_options[:show_details] }
end
