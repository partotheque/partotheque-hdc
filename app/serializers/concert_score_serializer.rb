class ConcertScoreSerializer < ActiveModel::Serializer
  attributes :id, :title, :archive_ref, :conductor_ref, :editor
end
