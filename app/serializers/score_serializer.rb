# Score Serializer, useful to include data about composer and arranger
class ScoreSerializer < ActiveModel::Serializer
  attributes :id,
             :comment,
             :acquisition_date,
             :classification,
             :number,
             :conductor_ref,
             :archive_ref,
             :audio_support,
             :editor,
             :title,
             :duration,
             :difficulty,
             :writing_date,
             :editing_date,
             :score_type,
             :conductor_type,
             :style,
             :substyle

  belongs_to :composer, serializer: ScorePersonSerializer
  belongs_to :arranger, serializer: ScorePersonSerializer
  has_many :concerts, serializer: ScoreConcertSerializer, if: -> { instance_options[:show_concerts] }
end
