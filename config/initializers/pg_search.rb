# frozen_string_literal: true

PgSearch.multisearch_options = {
  using: {
    tsearch: {
      prefix: true
    },
    trigram: {
      threshold: 0.6,
      word_similarity: true
    }
  }
}
