# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users,
             controllers: { passwords: 'passwords', sessions: 'sessions' }
  resources :concerts
  resources :people
  resources :scores

  post 'multi_search', to: 'search#multi_search'

  post 'concerts/search', to: 'concerts#search'
  post 'people/search', to: 'people#search'
  post 'scores/search', to: 'scores#search'

  root to: 'ping#ping'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
